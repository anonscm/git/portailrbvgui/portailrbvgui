package fr.sedoo.commons.shared.misc;

import java.util.ArrayList;

import fr.sedoo.commons.client.util.StringUtil;

public class FileUtils {

	protected static ArrayList<String> imageExtensions = new ArrayList<String>();

	static {
		imageExtensions.add("png");
		imageExtensions.add("jpg");
		imageExtensions.add("jpeg");
		imageExtensions.add("bmp");
		imageExtensions.add("gif");
	}

	protected FileUtils() {
	}

	public static boolean isImageFile(String fileName) {
		String fileExtension = getFileExtension(fileName).toLowerCase();
		return imageExtensions.contains(fileExtension);
	}

	private static String getFileExtension(String fileName) {
		String aux = StringUtil.trimToEmpty(fileName);
		int extensionIndex = aux.lastIndexOf(".");
		return aux.substring(extensionIndex + 1);
	}

	public static boolean isPdfFile(String fileName) {
		String fileExtension = getFileExtension(fileName).toLowerCase();
		return (fileExtension.compareTo("pdf") == 0);
	}

	public static boolean isWordFile(String fileName) {
		String fileExtension = getFileExtension(fileName).toLowerCase();
		return ((fileExtension.compareTo("doc") == 0) || (fileExtension
				.compareTo("docx") == 0));
	}

	public static boolean isExcelFile(String fileName) {
		String fileExtension = getFileExtension(fileName).toLowerCase();
		return ((fileExtension.compareTo("xls") == 0) || (fileExtension
				.compareTo("xlsx") == 0));
	}

	public static boolean isZipFile(String fileName) {
		String fileExtension = getFileExtension(fileName).toLowerCase();
		return (fileExtension.compareTo("zip") == 0);
	}

}
