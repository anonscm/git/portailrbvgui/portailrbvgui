package fr.sedoo.commons.client.news.widget;

import java.util.Date;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.ui.MessageEditViewImpl;
import fr.sedoo.commons.client.style.SedooStyleBundle;
import fr.sedoo.commons.client.util.DateUtil;
import fr.sedoo.commons.client.util.HtmlUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class DefaultTitledMessageRenderer implements TitledMessageRenderer {

	private static int DEFAULT_LENGTH = 200;
	
	private static DateTimeFormat formater = DateTimeFormat.getFormat("yyyy-MMM-dd");
	
	private boolean displayDateIcon = false;

	public void render(Context context, final TitledMessage message, final SafeHtmlBuilder sb, final String language) {

		render(message, DEFAULT_LENGTH, sb, language);
	}

	public void render(TitledMessage message, int length, SafeHtmlBuilder sb, String language) {
		if (message == null) {
			return;
		}
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsBlock() + "\">");
		sb.appendHtmlConstant("<a>");

		if (message.getImageType() == null)
		{
			message.setImageType(Message.NO_IMAGE);
		}
		
		if (isDisplayDateIcon())
		{
			Date date = message.getDate();
			String aux = formater.format(date);
			String[] split = aux.split("-");
			String year = split[0];
			String month = split[1];
			String day = split[2];
			
			sb.appendHtmlConstant("<div class=\"newsDateContainer\">");
			sb.appendHtmlConstant("<div style=\"text-align:center;border:1px solid #DADADA;border-radius: 5px;width:40px\">");
			sb.appendHtmlConstant("<span class=\"day\">"+day+"</span>");
			sb.appendHtmlConstant("<span class=\"month\">"+month+"</span>");
			sb.appendHtmlConstant("<span class=\"year\">"+year+"</span></div>");
			sb.appendHtmlConstant("</div>");
		}
		
		if (message.getImageType().compareToIgnoreCase(Message.NO_IMAGE) != 0) {
			String url = StringUtil.trimToEmpty(message.getUrl());
			if (message.getImageType().compareToIgnoreCase(Message.INTERNAL_IMAGE) == 0) {
				url = GWT.getModuleBaseURL() + MessageEditViewImpl.getStoredFileServicePath() + "ILLUSTRATION_OF_" + message.getUuid();
			}
			sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsImageContainer() + "\">" + "<img src=\"" + url + "\" class=\""
					+ SedooStyleBundle.INSTANCE.sedooStyle().newsImage() + "\"/>" + "</div>");
		}
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsTitle() + "\">" + StringUtil.trimToEmpty(message.getTitle()) + "</div>");
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsAddedOrModified() + "\">" + NewsMessages.INSTANCE.addedOrModified() + " ");
		if (!isDisplayDateIcon())
		{
			sb.appendHtmlConstant(DateUtil.getDateByLocale(message.getDate(), language) + " ");
		}
		sb.appendHtmlConstant(CommonMessages.INSTANCE.by() + " " + StringUtil.trimToEmpty(message.getAuthor()) + "</div>");
		String aux = StringUtil.trimToEmpty(HtmlUtil.removeHtmlTags(message.getContent()));

		if (aux.length() > length) {
			aux = aux.substring(0, length) + " [...]";
		}
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsContent() + "\">" + aux + "</div>");
		sb.appendHtmlConstant("<table width=\"100%\" style=\"border-bottom: 1px solid #D4D4CF;text-align:justify;cursor:default\" class=\"localCellList\">");
		sb.appendHtmlConstant("<tr><td><div style=\"border-bottom: 3px; border-bottom-style:solid; border-bottom-color:transparent;\">");
		sb.appendHtmlConstant("</td></tr>");
		sb.appendHtmlConstant("</table>");
		sb.appendHtmlConstant("</a>");
		sb.appendHtmlConstant("</div>");
	}

	public boolean isDisplayDateIcon() {
		return displayDateIcon;
	}

	public void setDisplayDateIcon(boolean displayDateIcon) {
		this.displayDateIcon = displayDateIcon;
	}

}