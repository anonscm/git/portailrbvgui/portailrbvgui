package fr.sedoo.commons.client.longlist;


public interface LongListPresenter {

	void getEntries(int position, int pageSize);

}
