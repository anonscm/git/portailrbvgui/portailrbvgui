package fr.sedoo.commons.client.longlist.component;

import java.util.List;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.longlist.LongListPresenter;
import fr.sedoo.commons.client.longlist.widget.LongListTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public abstract class LongListViewImpl extends AbstractView implements LongListView {
	
	public void setLongListPresenter(LongListPresenter presenter)
	{
		getLongListTable().setPresenter(presenter);
	}
	
	public abstract LongListTable getLongListTable();
	private int hits;
	
	public int getHits() {
		return hits;
	}
	
	@Override
	public void setHits(int hits) {
		this.hits = hits;
	}
	
	@Override
	public void setEntries(int position, List<? extends HasIdentifier> result)
	{
		getLongListTable().setDataPage(position, result, hits);

	}
}
