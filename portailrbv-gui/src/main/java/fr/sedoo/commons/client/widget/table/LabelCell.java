package fr.sedoo.commons.client.widget.table;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.IsUrl;

public class LabelCell extends AbstractCell<IsUrl> {
	private static ImageRenderer renderer;

	/**
	 * Construct a new ImageResourceCell.
	 */
	public LabelCell() {
		super();
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			IsUrl value, SafeHtmlBuilder sb) {
		sb.appendEscaped(StringUtil.trimToEmpty(value.getLabel()));
	}

}
