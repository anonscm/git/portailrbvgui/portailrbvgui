package fr.sedoo.commons.client.widget;

public interface SelectionCallBack {
	public void processSelection(Object selection);
}
