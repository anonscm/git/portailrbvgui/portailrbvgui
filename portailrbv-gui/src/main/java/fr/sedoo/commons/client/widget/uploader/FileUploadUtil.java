package fr.sedoo.commons.client.widget.uploader;

import com.google.gwt.core.client.GWT;

import fr.sedoo.commons.shared.misc.FileUtils;

public class FileUploadUtil {

	public static String getImageUrlFromId(Long id) {
		return GWT.getModuleBaseURL() + "rest/image/storedById/" + id;
	}

	public static String getImageUrlFromId(Long id, String fileName) {
		if (FileUtils.isImageFile(fileName)) {
			return GWT.getModuleBaseURL() + "rest/image/storedById/" + id;
		} else {
			return GWT.getModuleBaseURL() + "rest/image/storedDocumentById/"
					+ id;
		}
	}
}
