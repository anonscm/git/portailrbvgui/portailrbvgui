package fr.sedoo.commons.client.widget.table;

import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.commons.shared.misc.FileUtils;

public class StoredFilePreviewCell extends PreviewCell {

	private static StoredFileImageRenderer renderer;

	public StoredFilePreviewCell() {
		super();
		if (renderer == null) {
			renderer = new StoredFileImageRenderer("100px",
					"snapshot clickable");
		}
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			IsUrl value, SafeHtmlBuilder sb) {
		boolean rendered = false;
		if (StringUtil.isNotEmpty(value.getLabel())) {
			// A label exist - It is the filename. If it isn't an image file we
			// indicate a fake URL
			String fileName = StringUtil.trimToEmpty(value.getLabel());
			if (FileUtils.isImageFile(fileName) == false) {
				if (FileUtils.isPdfFile(fileName)) {
					Image aux = AbstractImagePrototype.create(
							GlobalBundle.INSTANCE.pdf()).createImage();
					sb.append(SafeHtmlUtils.fromTrustedString(aux.toString()));
					rendered = true;
				} else if (FileUtils.isWordFile(fileName)) {

					Image aux = AbstractImagePrototype.create(
							GlobalBundle.INSTANCE.word()).createImage();
					sb.append(SafeHtmlUtils.fromTrustedString(aux.toString()));
					rendered = true;
				} else if (FileUtils.isExcelFile(fileName)) {
					Image aux = AbstractImagePrototype.create(
							GlobalBundle.INSTANCE.excel()).createImage();
					sb.append(SafeHtmlUtils.fromTrustedString(aux.toString()));
					rendered = true;
				} else if (FileUtils.isZipFile(fileName)) {
					Image aux = AbstractImagePrototype.create(
							GlobalBundle.INSTANCE.zip()).createImage();
					sb.append(SafeHtmlUtils.fromTrustedString(aux.toString()));
					rendered = true;
				}

				else {
					Image aux = AbstractImagePrototype.create(
							GlobalBundle.INSTANCE.document()).createImage();
					sb.append(SafeHtmlUtils.fromTrustedString(aux.toString()));
					rendered = true;
				}
			}
		}

		if (rendered == false) {
			super.render(context, value, sb);
		}
	}

	@Override
	public void onBrowserEvent(Context context, Element parent, IsUrl value,
			NativeEvent event, ValueUpdater<IsUrl> valueUpdater) {
		if (FileUtils.isImageFile(value.getLabel())) {
			super.onBrowserEvent(context, parent, value, event, valueUpdater);
		}
	}
}
