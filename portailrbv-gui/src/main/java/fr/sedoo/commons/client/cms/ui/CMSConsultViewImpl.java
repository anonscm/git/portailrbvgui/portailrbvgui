package fr.sedoo.commons.client.cms.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.print.PrintStyleProvider;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;

public class CMSConsultViewImpl extends AbstractView implements CMSConsultView {

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends
			UiBinder<Widget, CMSConsultViewImpl> {
	}

	@UiField
	DivElement contentContainer;

	@UiField
	Label title;

	@UiField
	HorizontalPanel titlePanel;

	// @UiField
	// Image printImage;

	@UiField
	VerticalPanel topContainer;

	@UiField
	Button editButton;

	private boolean displayTitle = true;

	private PrintStyleProvider printStyleProvider;

	private Presenter presenter;

	public CMSConsultViewImpl(PrintStyleProvider printStyleProvider) {
		super();
		this.printStyleProvider = printStyleProvider;
		// GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		// TODO injection par IOC
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		editButton.getElement().getStyle().setMarginLeft(5, Unit.PX);

	}

	public CMSConsultViewImpl(PrintStyleProvider printStyleProvider,
			boolean displayTitle) {
		this(printStyleProvider);
		setDisplayTitle(displayTitle);
		if (isDisplayTitle() == false) {
			topContainer.remove(titlePanel);
		}
	}

	@Override
	public void setContent(String title, String content) {
		if (isDisplayTitle()) {
			this.title.setText(StringUtil.trimToEmpty(title));
		}
		contentContainer.setInnerHTML(content);
	}

	@Override
	public void reset() {
		contentContainer.setInnerHTML("");
		title.setText("");
		ElementUtil.setVisible(editButton, false);
		topContainer.remove(editButton);
	}

	// @UiHandler("printImage")
	// void onPrintImageClicked(ClickEvent event) {
	// Print.it(getPrintStyleProvider().getPrintStyle(), topContainer);
	// }

	@UiHandler("editButton")
	void onEditButtoneClicked(ClickEvent event) {
		presenter.edit();
	}

	public PrintStyleProvider getPrintStyleProvider() {
		return printStyleProvider;
	}

	public boolean isDisplayTitle() {
		return displayTitle;
	}

	public void setDisplayTitle(boolean displayTitle) {
		this.displayTitle = displayTitle;
	}

	@Override
	public void showEditButton() {
		topContainer.insert(editButton, 0);
		ElementUtil.setVisible(editButton, true);

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void scrollToAnchor(String anchorName) {
		// TODO Auto-generated method stub

	}

}
