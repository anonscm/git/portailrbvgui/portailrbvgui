package fr.sedoo.commons.client.widget.dialog;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;

public class CancelDialog extends DialogBox implements ClickHandler {
	private CancelContent content;
	private Button cancelButton;
	private HorizontalPanel toolBar;

	public CancelDialog(String title, CancelContent content) {
		super();
		this.content = content;
		setText(title);
		setGlassEnabled(true);
		getGlassElement().getStyle().setProperty("zIndex",
				"" + DialogBoxTools.getHigherZIndex());
		getElement().getStyle().setProperty("zIndex",
				"" + DialogBoxTools.getHigherZIndex());
		VerticalPanel panel = new VerticalPanel();
		panel.add(content);
		if (content.getPreferredHeight() != null) {
			content.asWidget().setHeight(content.getPreferredHeight());
		}

		toolBar = new HorizontalPanel();
		toolBar.setSpacing(5);

		cancelButton = new Button(CommonMessages.INSTANCE.cancel());

		cancelButton.addClickHandler(this);

		toolBar.add(cancelButton);

		HorizontalPanel centeringPanel = new HorizontalPanel();
		centeringPanel.setWidth("100%");
		centeringPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
		centeringPanel.add(toolBar);

		panel.add(centeringPanel);
		setWidget(panel);
		content.setDialogBox(this);
		setAutoHideEnabled(true);
		center();
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == cancelButton) {
			content.cancelClicked();
		}
	}

	public HorizontalPanel getToolBar() {
		return toolBar;
	}
}
