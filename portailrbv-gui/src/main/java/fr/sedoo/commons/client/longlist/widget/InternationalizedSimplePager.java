package fr.sedoo.commons.client.longlist.widget;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.Range;

import fr.sedoo.commons.client.message.CommonMessages;

public class InternationalizedSimplePager extends SimplePager
{
	
	public InternationalizedSimplePager()
	{
		super(TextLocation.CENTER, false, true);
	}
	
	protected String createText() {
	    // Default text is 1 based.
	    NumberFormat formatter = NumberFormat.getFormat("#,###");
	    HasRows display = getDisplay();
	    Range range = display.getVisibleRange();
	    int pageStart = range.getStart() + 1;
	    int pageSize = range.getLength();
	    int dataSize = display.getRowCount();
	    int endIndex = Math.min(dataSize, pageStart + pageSize - 1);
	    endIndex = Math.max(pageStart, endIndex);
	    boolean exact = display.isRowCountExact();
	    return formatter.format(pageStart) + "-" + formatter.format(endIndex)
	        + (exact ? " "+CommonMessages.INSTANCE.pagerOf()+" " : " "+CommonMessages.INSTANCE.pagerOfOver()+" ") + formatter.format(dataSize);
	  }
}
