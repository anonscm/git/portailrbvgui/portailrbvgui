package fr.sedoo.commons.client.cms.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.cms.ClientScreenList;
import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.cms.event.MenuChangeEvent;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.cms.service.ScreenService;
import fr.sedoo.commons.client.cms.service.ScreenServiceAsync;
import fr.sedoo.commons.client.cms.ui.CMSEditView;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.CMSClientFactory;
import fr.sedoo.commons.client.mvp.CommonClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.mvp.activity.PlaceActivity;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.storedfile.service.StoredImagesService;
import fr.sedoo.commons.client.storedfile.service.StoredImagesServiceAsync;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.uploader.FileUploadUtil;
import fr.sedoo.commons.shared.domain.cms.TitledScreen;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public abstract class CMSEditActivity extends AdministrationActivity implements
		LoadCallBack, PlaceActivity, CMSEditView.Presenter {

	private CMSEditView editView;
	private AcceptsOneWidget containerWidget;
	private String screenName;

	private static final ScreenServiceAsync SCREEN_SERVICE = GWT
			.create(ScreenService.class);
	private static final StoredImagesServiceAsync STORED_IMAGE_SERVICE = GWT
			.create(StoredImagesService.class);
	private String lastSavedContent;

	public CMSEditActivity(CMSEditPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
		screenName = place.getScreenName();
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		lastSavedContent = "";
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		this.containerWidget = containerWidget;
		editView = ((CMSClientFactory) clientFactory).getCmsEditView();
		editView.activityStart();
		editView.setPresenter(this);
		eventBus.fireEvent(new ActivityStartEvent(this));
		ClientScreenList.getScreenByName(screenName, this);
	}

	@Override
	public void postLoadProcess(Object result) {
		HashMap<String, TitledScreen> aux = (HashMap<String, TitledScreen>) result;
		lastSavedContent = computeContentSignature(aux);
		Iterator<String> iterator = clientFactory.getLanguages().iterator();

		// On force la présence des différentes langues
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			if (aux.get(language) == null) {
				aux.put(language, new TitledScreen());
			}
		}
		editView.reset();
		editView.setUuid(screenName);
		HashMap<String, String> screenNames = ((CMSClientFactory) clientFactory)
				.getScreenNames();
		editView.setContent(aux, clientFactory.getLanguages());
		editView.setScreenName(screenName);
		containerWidget.setWidget(editView.asWidget());

		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		STORED_IMAGE_SERVICE.getStoredImagesByContainerId(screenName,
				new DefaultAbstractCallBack<ArrayList<ImageUrl>>(e, eventBus) {

					@Override
					public void onSuccess(ArrayList<ImageUrl> result) {
						super.onSuccess(result);
						Iterator<ImageUrl> imageIterator = result.iterator();
						while (imageIterator.hasNext()) {
							ImageUrl imageUrl = (ImageUrl) imageIterator.next();
							imageUrl.setLink(FileUploadUtil.getImageUrlFromId(
									imageUrl.getId(), imageUrl.getLabel()));

						}
						editView.setImages(result);
					}

				});

	}

	private String computeContentSignature(
			HashMap<String, TitledScreen> screenContents) {
		ArrayList<String> keys = new ArrayList<String>(screenContents.keySet());
		StringBuffer result = new StringBuffer();
		Collections.sort(keys);
		for (String key : keys) {

			TitledScreen titledScreen = screenContents.get(key);

			result.append(StringUtil.trimToEmpty((key)));
			result.append("|");
			result.append(StringUtil.trimToEmpty((titledScreen.getTitle())));
			result.append("|");
			result.append(StringUtil.trimToEmpty((titledScreen.getContent())));
			result.append("|");
		}
		return result.toString();
	}

	@Override
	public void save(final HashMap<String, TitledScreen> content) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.saving(),
				ActionEventConstants.BASIC_SAVING_EVENT, true);
		eventBus.fireEvent(e);

		SCREEN_SERVICE.saveScreen(screenName, content,
				new DefaultAbstractCallBack<Void>(e, eventBus) {

					@Override
					public void onSuccess(Void result) {
						super.onSuccess(result);
						lastSavedContent = computeContentSignature(content);
						clientFactory.getEventBus().fireEvent(
								new NotificationEvent(CommonMessages.INSTANCE
										.savedElement()));
						ClientScreenList
								.updateScreenByName(screenName, content);

						clientFactory.getEventBus().fireEvent(
								new MenuChangeEvent());

					}

				});
	}

	@Override
	public void deleteImageById(final String id) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.deleting(),
				ActionEventConstants.BASIC_DELETING_EVENT, true);
		eventBus.fireEvent(e);
		STORED_IMAGE_SERVICE.deleteStoredImageById(new Long(id),
				new DefaultAbstractCallBack<Void>(e, eventBus) {

					@Override
					public void onSuccess(Void result) {
						super.onSuccess(result);
						clientFactory.getEventBus().fireEvent(
								new NotificationEvent(CommonMessages.INSTANCE
										.deletedElement()));
						editView.broadcastDeletion(id);
					}

				});

	}

	@Override
	public void onStop() {
		editView.activityStop();
		super.onStop();
	}

	@Override
	public String mayStop() {
		if (editView == null) {
			return null;
		}
		String aux1 = StringUtil.trimToEmpty(computeContentSignature(editView
				.getCurrentContent()));
		String aux2 = StringUtil.trimToEmpty(lastSavedContent);
		if (aux1.compareTo(aux2) == 0) {
			return null;
		} else {
			return CMSMessages.INSTANCE.confirmNavigation();
		}
	}

	@Override
	public void view() {
		CMSConsultPlace consultPlace = new CMSConsultPlace();
		consultPlace.setScreenName(screenName);
		((CommonClientFactory) clientFactory).getPlaceController().goTo(
				consultPlace);

	}
}