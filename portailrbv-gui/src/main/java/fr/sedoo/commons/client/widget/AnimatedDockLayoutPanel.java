package fr.sedoo.commons.client.widget;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class AnimatedDockLayoutPanel extends DockLayoutPanel{

	public AnimatedDockLayoutPanel(Unit unit) {
		super(unit);
	}
	
	public void setWidgetSize(Widget widget, double size, int ms) {
//	    assertIsChild(widget);
	    LayoutData data = (LayoutData) widget.getLayoutData();

	    assert data.direction != Direction.CENTER :
	        "The size of the center widget can not be updated.";

	    data.size = size;

	    // Update the layout.
	    animate(ms);
	  }

}
