package fr.sedoo.commons.client.widget;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.TextBox;

public class UpperCaseTextBox extends TextBox implements KeyUpHandler {
	
	public UpperCaseTextBox()
	{
		super();
		addKeyUpHandler(this);
	}

	@Override
	public void onKeyUp(KeyUpEvent event) {
		setText(getText().toUpperCase());
	}

}
