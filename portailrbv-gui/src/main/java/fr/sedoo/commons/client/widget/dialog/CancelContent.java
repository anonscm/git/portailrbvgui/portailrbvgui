package fr.sedoo.commons.client.widget.dialog;

public interface CancelContent extends DialogBoxContent {

	public void cancelClicked();

}