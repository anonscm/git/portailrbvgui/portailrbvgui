package fr.sedoo.commons.client.cms.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.ckeditor.CKEditor;
import fr.sedoo.commons.client.ckeditor.DefaultCKEditorConfig;
import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.api.HtmlTextEditor;
import fr.sedoo.commons.client.widget.decorator.BoldDecorator;
import fr.sedoo.commons.client.widget.uploader.ImagePanelContent;
import fr.sedoo.commons.shared.domain.cms.TitledScreen;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public class CMSEditViewImpl extends AbstractView implements CMSEditView {

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, CMSEditViewImpl> {
	}

	boolean tabActivated = false;
	boolean scheduledEnabled = false;
	HashMap<String, HtmlTextEditor> editors = new HashMap<String, HtmlTextEditor>();
	HashMap<String, TextBox> titles = new HashMap<String, TextBox>();

	@UiField
	VerticalPanel mainPanel;

	@UiField
	DockLayoutPanel dockLayoutPanel;

	@UiField
	HorizontalPanel titlePanel;

	@UiField
	Label title;

	@UiField
	Button saveButton;

	@UiField
	Button viewButton;

	private Presenter presenter;
	private String uuid;
	private String displayLanguage;
	private Integer selectedTab;

	private ImagePanelContent imagePanelContent = new ImagePanelContent();
	private TabPanel tabPanel;
	private boolean resizeNeeded;
	private int maxLangagesTab;
	private List<String> languages;

	private boolean displayTitle = true;

	public CMSEditViewImpl(String displayLanguage) {
		super();
		this.displayLanguage = displayLanguage;
		// GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		// TODO injection par IOC
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		mainPanel.setWidth("100%");
	}

	public CMSEditViewImpl(String displayLanguage, boolean displayTitle) {
		this(displayLanguage);
		setDisplayTitle(displayTitle);
		if (isDisplayTitle() == false) {
			dockLayoutPanel.setWidgetHidden(titlePanel, true);
		}
	}

	@Override
	public void setUuid(String uuid) {
		this.uuid = uuid;
		imagePanelContent.setUuid(uuid);
	}

	@Override
	public void setContent(HashMap<String, TitledScreen> screens,
			List<String> languages) {
		this.languages = languages;
		reset();

		maxLangagesTab = languages.size() - 1;
		tabPanel = new TabPanel();
		tabPanel.setWidth("100%");
		tabPanel.setHeight("100%");
		tabPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		editors.clear();
		titles.clear();

		Iterator<String> iterator = languages.iterator();
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			HtmlTextEditor editor = new CKEditor(new DefaultCKEditorConfig(
					uuid, displayLanguage));

			VerticalPanel panel = new VerticalPanel();
			HorizontalPanel titlePanel = new HorizontalPanel();
			titlePanel.setWidth("100%");
			titlePanel.setHorizontalAlignment(HasAlignment.ALIGN_LEFT);
			TextBox title = new TextBox();
			title.setWidth("100%");
			BoldDecorator titleLabel = new BoldDecorator(new Label(
					CommonMessages.INSTANCE.label()));

			titlePanel.add(titleLabel);
			titlePanel.setCellVerticalAlignment(titleLabel,
					HasVerticalAlignment.ALIGN_MIDDLE);
			titlePanel.add(title);
			titlePanel.setSpacing(7);

			titlePanel.setCellWidth(titleLabel, "1%");

			TitledScreen cmsScreen = screens.get(language);

			editor.setHtml(cmsScreen.getContent());
			title.setText(StringUtil.trimToEmpty(cmsScreen.getTitle()));
			editors.put(language, editor);
			titles.put(language, title);

			panel.add(titlePanel);
			panel.add(editor);
			tabPanel.add(panel, LocaleUtil.getLanguageFlag(language));
		}

		Image picture = new Image(CommonBundle.INSTANCE.picture());
		picture.setTitle(CommonMessages.INSTANCE.images());
		tabPanel.add(imagePanelContent, picture);

		selectedTab = 0;
		tabPanel.selectTab(selectedTab);
		mainPanel.add(tabPanel);

		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				selectedTab = event.getSelectedItem();
				if (selectedTab <= maxLangagesTab) {
					resizeNeeded = true;
				} else {
					resizeNeeded = false;
				}
				Iterator<HtmlTextEditor> editorIterator = editors.values()
						.iterator();
				while (editorIterator.hasNext()) {
					HtmlTextEditor aux = (HtmlTextEditor) editorIterator.next();
					if (aux instanceof CKEditor) {
						((CKEditor) aux).onVisible();
					}

				}
			}
		});
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				onResize();
			}
		});
	}

	@Override
	public void reset() {
		mainPanel.clear();
		title.setText("");
		imagePanelContent.reset();
	}

	@UiHandler("saveButton")
	void onResetParametersButtonClicked(ClickEvent event) {
		presenter.save(getCurrentContent());
	}

	public HashMap<String, TitledScreen> getCurrentContent() {
		HashMap<String, TitledScreen> result = new HashMap<String, TitledScreen>();
		Iterator<Entry<String, HtmlTextEditor>> iterator = editors.entrySet()
				.iterator();
		while (iterator.hasNext()) {
			Entry<String, HtmlTextEditor> entry = (Entry<String, HtmlTextEditor>) iterator
					.next();
			TitledScreen aux = new TitledScreen();
			aux.setContent(entry.getValue().getHtml());

			TextBox textBox = titles.get(entry.getKey());
			if (textBox != null) {
				aux.setTitle(textBox.getText());
			}
			result.put(entry.getKey(), aux);
		}
		return result;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		imagePanelContent.setPresenter(presenter);
	}

	@Override
	public void setScreenName(String screenLabel) {
		title.setText(CMSMessages.INSTANCE.screenModification() + " : "
				+ screenLabel);
	}

	@Override
	public void setImages(ArrayList<ImageUrl> images) {
		imagePanelContent.setImages(images);
	}

	@Override
	public void broadcastDeletion(String id) {
		imagePanelContent.broadcastDeletion(id);
	}

	@Override
	public void onResize() {
		super.onResize();
		resizeNeeded = true;
	}

	private void resizeCurrentEditor() {

		if (resizeNeeded) {
			if (selectedTab != null) {
				String index = "";
				CKEditor current = (CKEditor) editors.get(languages
						.get(selectedTab));
				NodeList<com.google.gwt.dom.client.Element> divs = current
						.getElement().getParentElement()
						.getElementsByTagName("div");
				if (divs == null) {
					return;
				} else {
					int length = divs.getLength();
					for (int i = 0; i < length; i++) {
						String id = divs.getItem(i).getId();
						if ((id.startsWith("cke_"))
								&& (id.endsWith("_contents"))) {
							String[] split = id.split("_");
							index = split[1];
							break;
						}

					}
				}
				int containerHeight = mainPanel.getElement().getParentElement()
						.getClientHeight();
				Element top = DOM.getElementById("cke_" + index + "_top");
				Element contents = DOM.getElementById("cke_" + index
						+ "_contents");
				Element bottom = DOM.getElementById("cke_" + index + "_bottom");

				if ((top != null) && (contents != null) && (bottom != null)) {
					int topHeight = top.getClientHeight();
					int bottomHeight = bottom.getClientHeight();
					contents.getStyle().setHeight(
							containerHeight - topHeight - bottomHeight - 95,
							Unit.PX);
					resizeNeeded = false;
				}
			}
		}
	}

	@Override
	public void activityStart() {
		scheduledEnabled = true;
		Scheduler.get().scheduleFixedDelay(new Scheduler.RepeatingCommand() {
			@Override
			public boolean execute() {
				if (scheduledEnabled) {
					resizeCurrentEditor();
					return scheduledEnabled;
				} else {
					return false;
				}
			}
		}, 1000);
	}

	@Override
	public void activityStop() {
		scheduledEnabled = false;

	}

	public boolean isDisplayTitle() {
		return displayTitle;
	}

	public void setDisplayTitle(boolean displayTitle) {
		this.displayTitle = displayTitle;
	}

	public Button getSaveButton() {
		return saveButton;
	}

	@UiHandler("viewButton")
	void onViewButtonClick(ClickEvent e) {
		presenter.view();
	}

	public Button getViewButton() {
		return viewButton;
	}

}