package fr.sedoo.commons.client.longlist.component;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.longlist.LongListPresenter;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public interface LongListView extends IsWidget {

	void setLongListPresenter(LongListPresenter presenter);
	void setEntries(int position, List<? extends HasIdentifier> items);
	void setHits(int hits);
}