package fr.sedoo.commons.server.image;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.cms.dao.CMSScreenDAO;
import fr.sedoo.commons.server.storedfile.TmpFileManager;
import fr.sedoo.commons.server.storedfile.dao.StoredFileDAO;
import fr.sedoo.commons.shared.domain.StoredFile;
import fr.sedoo.commons.shared.domain.cms.TitledScreen;

@Path("/image")
public class ImageService {
	Logger logger = LoggerFactory.getLogger(ImageService.class);

	StoredFileDAO storedFileDAO;
	CMSScreenDAO cmsScreenDAO;
	private long FILE_SIZE_LIMIT = 20 * 1024 * 1024; // 20 MiB

	@GET
	@Path("/isAlive")
	public Response getVersion() {
		logger.debug("Traitement web service isAlive");
		String answer = "Yes";
		return Response.status(200).entity(answer).build();
	}

	@GET
	@Path("/tmp/{fileName}")
	@Produces("image/*")
	public Response getTmpImage(@PathParam("fileName") String fileName) {
		logger.debug("Traitement web service tmp");
		try {
			String pathForFile = TmpFileManager.getPathForFile(StringUtil
					.trimToEmpty(fileName));
			if (StringUtil.isEmpty(pathForFile)) {
				return Response.status(404).build();
			} else {
				File tmpFile = new File(pathForFile);
				if (tmpFile.exists() == false) {
					return Response.status(404).build();
				} else {
					return Response.ok(
							new ByteArrayInputStream(FileUtils
									.readFileToByteArray(tmpFile))).build();
				}
			}
		} catch (Exception e) {
			return Response.status(404).build();
		}
	}

	@GET
	@Path("/byContainerId/{containerId}")
	public Response getImagesExplorerByUuid(
			@PathParam("containerId") String containerId,
			@QueryParam("CKEditorFuncNum") String functionNum,
			@Context UriInfo uri) {
		logger.debug("Traitement web service byContainerId");
		StringBuffer answer = new StringBuffer();
		String[] split = uri.getBaseUri().toString().split("/");
		String appName = split[split.length - 2];
		// if (uri.getBaseUri().toString().indexOf("8888") < 0) {
		// appName = appName + "/" + appName;
		// }

		MultivaluedMap<String, String> queryParameters = uri
				.getQueryParameters(true);
		String language = queryParameters.getFirst("langCode");
		ResourceBundle messages = ResourceBundle.getBundle(this.getClass()
				.getName(), new Locale(language));

		String chooseImage = messages.getString("chooseImage");
		String chooseDocument = messages.getString("chooseDocument");
		String chooseScreen = messages.getString("chooseScreen");
		initDao();
		ArrayList<StoredFile> findFilesByAttacherUuid = storedFileDAO
				.findFilesByAttacherUuid(containerId, false);

		answer.append("<html><head>");
		answer.append("<style type=\"text/css\"> .border {margin:5px;border: 10px solid white;-webkit-box-shadow: 0 0 12px rgba(0, 0, 0, 0.1);-moz-box-shadow: 0 0 12px rgba(0, 0, 0, 0.1);box-shadow: 0 0 12px rgba(0, 0, 0, 0.1);}</style>");
		answer.append("</head><body>");

		ArrayList<StoredFile> images = getImages(findFilesByAttacherUuid);
		if (images.isEmpty() == false) {
			answer.append("<div align='left' style='margin-left:10px;font-family:arial;font-size:32px;color: #4a99c6;'>"
					+ chooseImage + "</div>");
			Iterator<StoredFile> iterator = images.iterator();
			while (iterator.hasNext()) {
				String current = "/" + appName + "/rest/image/storedById/"
						+ iterator.next().getId();
				appendImageContent(answer, current, functionNum);
			}
		}

		ArrayList<StoredFile> documents = getDocuments(findFilesByAttacherUuid);
		if (documents.isEmpty() == false) {
			answer.append("<div align='left' style='margin-left:10px;font-family:arial;font-size:32px;color: #4a99c6;'>"
					+ chooseDocument + "</div><ul>");
			Iterator<StoredFile> iterator = documents.iterator();
			while (iterator.hasNext()) {
				StoredFile currentStoredFile = iterator.next();
				String current = "/" + appName
						+ "/rest/image/storedDocumentById/"
						+ currentStoredFile.getId();
				appendDocumentContent(answer, current, functionNum,
						currentStoredFile);
			}
			answer.append("</ul>");
		}

		initDao();
		ArrayList<TitledScreen> screens = cmsScreenDAO
				.findAllByLanguage(language.toUpperCase());
		if (screens.isEmpty() == false) {

			ArrayList<String> languages = (ArrayList<String>) DefaultServerApplication
					.getSpringBeanFactory().getBeanByName("languages");

			answer.append("<div align='left' style='margin-left:10px;font-family:arial;font-size:32px;color: #4a99c6;'>"
					+ chooseScreen + "</div><ul>");

			for (TitledScreen screen : screens) {
				answer.append("<li>" + screen.getName() + ":");
				for (int i = 0; i < languages.size(); i++) {
					String aux = "";
					if (i > 0) {
						aux = "?locale=" + languages.get(i);
					}
					String current = appName + aux + "#CMSConsultPlace:"
							+ screen.getName();

					appendScreenContent(answer, current, functionNum, languages
							.get(i).toUpperCase());

				}
				answer.append("</li>");
			}

			answer.append("</ul>");
		}
		answer.append("</html>");
		return Response.status(200).entity(answer.toString()).build();
	}

	private void appendScreenContent(StringBuffer answer, String current,
			String functionNum, String language) {
		answer.append(" <a onClick='window.opener.CKEDITOR.tools.callFunction("
				+ functionNum
				+ ",\"http://"
				+ current
				+ "\"	);window.close();' style=\"cursor:pointer;max-width:150px\" />"
				+ language + "</a>");
	}

	private ArrayList<StoredFile> getImages(ArrayList<StoredFile> files) {
		ArrayList<StoredFile> result = new ArrayList<StoredFile>();
		for (StoredFile storedFile : files) {
			if (fr.sedoo.commons.shared.misc.FileUtils.isImageFile(storedFile
					.getName())) {
				result.add(storedFile);
			}
		}
		return result;
	}

	private ArrayList<StoredFile> getDocuments(ArrayList<StoredFile> files) {
		ArrayList<StoredFile> result = new ArrayList<StoredFile>();
		for (StoredFile storedFile : files) {
			if (fr.sedoo.commons.shared.misc.FileUtils.isImageFile(storedFile
					.getName()) == false) {
				result.add(storedFile);
			}
		}
		return result;
	}

	private void appendImageContent(StringBuffer answer, String current,
			String functionNum) {
		answer.append("<img class=\"border\" src=\""
				+ current
				+ "\" onClick='window.opener.CKEDITOR.tools.callFunction("
				+ functionNum
				+ ",\""
				+ current
				+ "\"	);window.close();' style=\"cursor:pointer;max-width:150px\" title=\"Choose\" />");
	}

	private void appendDocumentContent(StringBuffer answer, String current,
			String functionNum, StoredFile file) {
		answer.append("<li><a onClick='window.opener.CKEDITOR.tools.callFunction("
				+ functionNum
				+ ",\"http://"
				+ current
				+ "\"	);window.close();' style=\"cursor:pointer;max-width:150px\" />"
				+ file.getName() + "</a></li>");
	}

	@POST
	@Path("upload/{containerId}")
	public Response uploadImageForContainer(
			@PathParam("containerId") String containerId,
			@Context HttpServletRequest request) {
		logger.debug("Traitement web service upload");
		initDao();
		try {
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			ServletFileUpload fileUpload = new ServletFileUpload(
					fileItemFactory);
			fileUpload.setSizeMax(FILE_SIZE_LIMIT);

			List<FileItem> items = fileUpload.parseRequest(request);

			for (FileItem item : items) {

				if (!item.isFormField()) {
					if (item.getSize() > FILE_SIZE_LIMIT) {
						return Response.status(404)
								.entity("File size exceeds limit").build();
					}

					StoredFile aux = new StoredFile();
					aux.setName(item.getName());
					aux.setAttacherUuid(containerId);
					aux.setContent(item.get());
					StoredFile saved = storedFileDAO.save(aux);

					if (!item.isInMemory()) {
						item.delete();
					}
					return Response.status(200).entity("" + saved.getId())
							.build();
				}
			}
		} catch (Exception e) {
			return Response.status(404).build();
		}

		// We dont exit by there...
		return Response.status(404).build();

	}

	@GET
	@Path("/stored/{uuid}")
	@Produces("image/*")
	public Response getStoredImage(@PathParam("uuid") String uuid) {
		try {
			logger.debug("Traitement web service stored");
			initDao();
			StoredFile aux = storedFileDAO.findByAttacherUuid(uuid);
			if ((aux.getContent() == null) || (aux.getContent().length == 0)) {
				return Response.status(404).build();
			}
			return Response.ok(new ByteArrayInputStream(aux.getContent()))
					.build();
		} catch (Exception e) {
			return Response.status(404).build();
		}
	}

	@GET
	@Path("/storedById/{id}")
	@Produces("image/*")
	public Response getStoredImage(@PathParam("id") Long id) {
		try {
			logger.debug("Traitement web service storedById. id=" + id);
			initDao();
			StoredFile aux = storedFileDAO.findById(id);
			if ((aux.getContent() == null) || (aux.getContent().length == 0)) {
				return Response.status(404).build();
			}
			return Response.ok(new ByteArrayInputStream(aux.getContent())).type("image/*")
					.build();
		} catch (Exception e) {
			return Response.status(404).build();
		}
	}

	@GET
	@Path("/storedDocumentById/{id}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getStoredDocument(@PathParam("id") Long id) {
		try {
			logger.debug("Traitement web service storedById");
			initDao();
			StoredFile aux = storedFileDAO.findById(id);
			if ((aux.getContent() == null) || (aux.getContent().length == 0)) {
				return Response.status(404).build();
			}
			return Response
					.ok(new ByteArrayInputStream(aux.getContent()))
					.header("Content-Disposition",
							"attachment; filename=\"" + aux.getName() + "\"")
					.build();
		} catch (Exception e) {
			return Response.status(404).build();
		}
	}

	private void initDao() {
		if (storedFileDAO == null) {
			storedFileDAO = (StoredFileDAO) DefaultServerApplication
					.getSpringBeanFactory().getBeanByName(
							StoredFileDAO.BEAN_NAME);
		}

		if (cmsScreenDAO == null) {
			cmsScreenDAO = (CMSScreenDAO) DefaultServerApplication
					.getSpringBeanFactory().getBeanByName(
							CMSScreenDAO.BEAN_NAME);
		}
	}

}
