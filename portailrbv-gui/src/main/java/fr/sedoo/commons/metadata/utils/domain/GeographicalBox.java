package fr.sedoo.commons.metadata.utils.domain;

public class GeographicalBox implements GeographicalExtent {

	private String northLatitude;
	private String southLatitude;
	private String eastLongitude;
	private String westLongitude;

	public String getNorthLatitude() {
		return northLatitude;
	}

	public void setNorthLatitude(String northLatitude) {
		this.northLatitude = northLatitude;
	}

	public String getSouthLatitude() {
		return southLatitude;
	}

	public void setSouthLatitude(String southLatitude) {
		this.southLatitude = southLatitude;
	}

	public String getEastLongitude() {
		return eastLongitude;
	}

	public void setEastLongitude(String eastLongitude) {
		this.eastLongitude = eastLongitude;
	}

	public String getWestLongitude() {
		return westLongitude;
	}

	public void setWestLongitude(String westLongitude) {
		this.westLongitude = westLongitude;
	}

}
