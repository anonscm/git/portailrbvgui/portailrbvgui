package fr.sedoo.commons.metadata.utils.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.metadata.shared.ResourceIdentifier;

public class Summary {
	private String resourceAbstract;
	private HashMap<String, String> titleTranslations = new HashMap<String, String>();
	private HashMap<String, String> abstractTrasnlations = new HashMap<String, String>();
	private String resourceTitle;
	private String uuid;
	private String parentUuid;
	private String modificationDate;
	private String hierarchyLevelName;
	private List<DescribedURL> links = new ArrayList<DescribedURL>();
	private List<ResourceIdentifier> identifiers = new ArrayList<ResourceIdentifier>();
	private String metadataLanguage;

	private GeographicalExtentList geographicalExtentList = new GeographicalExtentList();

	/* backward compatibility use */
	private String experimentalSiteName;

	public String getResourceTitle() {
		return resourceTitle;
	}

	public void setResourceTitle(String resourceTitle) {
		this.resourceTitle = resourceTitle;
	}

	public String getResourceAbstract() {
		return resourceAbstract;
	}

	public void setResourceAbstract(String resourceAbstract) {
		this.resourceAbstract = resourceAbstract;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public String toString() {
		return getUuid() + "-" + getModificationDate() + "-"
				+ getResourceAbstract() + "-" + getResourceTitle();
	}

	/* used by digester */
	public void addLink(DescribedURL link) {
		getLinks().add(link);
	}

	public List<DescribedURL> getLinks() {
		return links;
	}

	public void setLinks(List<DescribedURL> links) {
		this.links = links;
	}

	public List<ResourceIdentifier> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<ResourceIdentifier> identifiers) {
		this.identifiers = identifiers;
	}

	/* used by digester */
	public void addIdentifier(ResourceIdentifier identifier) {
		getIdentifiers().add(identifier);
	}

	public String getHierarchyLevelName() {
		return hierarchyLevelName;
	}

	public void setHierarchyLevelName(String hierarchyLevelName) {
		this.hierarchyLevelName = hierarchyLevelName;
	}

	public String getParentUuid() {
		return parentUuid;
	}

	public void setParentUuid(String parentUuid) {
		this.parentUuid = parentUuid;
	}

	public String getExperimentalSiteName() {
		return experimentalSiteName;
	}

	public void setExperimentalSiteName(String experimentalSiteName) {
		this.experimentalSiteName = experimentalSiteName;
	}

	public void addTitleTranslation(String key, String value) {
		if (StringUtils.isEmpty(value) == false) {
			titleTranslations.put(key.toLowerCase(), value);
		}
	}

	public void addAbstractTranslation(String key, String value) {
		if (StringUtils.isEmpty(value) == false) {
			abstractTrasnlations.put(key.toLowerCase(), value);
		}
	}

	public String getMetadataLanguage() {
		return metadataLanguage;
	}

	public void setMetadataLanguage(String metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}

	public HashMap<String, String> getTitleTranslations() {
		return titleTranslations;
	}

	public HashMap<String, String> getAbstractTrasnlations() {
		return abstractTrasnlations;
	}

	public GeographicalExtentList getGeographicalExtentList() {
		return geographicalExtentList;
	}

	public void setGeographicalExtentList(
			GeographicalExtentList geographicalExtentList) {
		this.geographicalExtentList = geographicalExtentList;
	}

	public void addGeographicalExtent(GeographicalExtent extent) {
		geographicalExtentList.add(extent);
	}
}
