package fr.sedoo.commons.metadata.utils.domain;

public class GeographicalPolygon implements GeographicalExtent {

	private String posList;

	public String getPosList() {
		return posList;
	}

	public void setPosList(String posList) {
		this.posList = posList;
	}

}
