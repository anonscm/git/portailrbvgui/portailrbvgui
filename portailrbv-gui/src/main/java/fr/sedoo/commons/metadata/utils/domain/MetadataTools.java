package fr.sedoo.commons.metadata.utils.domain;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.geotoolkit.csw.xml.CSWMarshallerPool;
import org.geotoolkit.gml.xml.TimeIndeterminateValueType;
import org.geotoolkit.metadata.iso.citation.DefaultAddress;
import org.geotoolkit.metadata.iso.citation.DefaultCitation;
import org.geotoolkit.metadata.iso.citation.DefaultContact;
import org.geotoolkit.metadata.iso.citation.DefaultResponsibleParty;
import org.geotoolkit.metadata.iso.constraint.DefaultConstraints;
import org.geotoolkit.metadata.iso.constraint.DefaultLegalConstraints;
import org.geotoolkit.metadata.iso.extent.DefaultExtent;
import org.geotoolkit.metadata.iso.extent.DefaultTemporalExtent;
import org.geotoolkit.metadata.iso.identification.AbstractIdentification;
import org.geotoolkit.metadata.iso.identification.DefaultDataIdentification;
import org.geotoolkit.service.ServiceIdentificationImpl;
import org.geotoolkit.util.DefaultInternationalString;
import org.geotoolkit.util.SimpleInternationalString;
import org.opengis.metadata.Metadata;
import org.opengis.metadata.citation.Citation;
import org.opengis.metadata.citation.ResponsibleParty;
import org.opengis.metadata.citation.Role;
import org.opengis.metadata.constraint.Constraints;
import org.opengis.metadata.constraint.LegalConstraints;
import org.opengis.metadata.constraint.Restriction;
import org.opengis.metadata.extent.Extent;
import org.opengis.metadata.extent.TemporalExtent;
import org.opengis.metadata.identification.DataIdentification;
import org.opengis.metadata.identification.Identification;
import org.opengis.util.InternationalString;

import fr.sedoo.commons.metadata.shared.utils.Iso6392LanguageConverter;
import fr.sedoo.commons.metadata.utils.domain.patch.temporalextend.TemporalExtendPatch;
import fr.sedoo.commons.metadata.utils.domain.patch.temporalextend.TimePeriod;

public class MetadataTools {

	private static Map<String, Locale> localeMap = new HashMap<String, Locale>();
	private static DateFormat englishDateFormatter = new SimpleDateFormat(
			"yyyy-MM-dd");

	static {
		String[] languages = Locale.getISOLanguages();
		for (String language : languages) {
			Locale locale = new Locale(language);
			localeMap.put(locale.getISO3Language(), locale);
		}
		// ISOLanguage renvoie FRA pour FR, alors que FRE est la vraie valeur...
		// TODO: Correct this patch
		Locale locale = new Locale("fr");
		localeMap.put("fre", locale);
	}

	protected MetadataTools() {

	}

	public static DefaultCitation getCitation(SedooMetadata metadata) {
		DefaultCitation citation = null;
		AbstractIdentification identification = getFisrtIdentificationInfo(metadata);

		if (metadata.getIdentificationInfo().isEmpty()) {
			citation = new DefaultCitation();
			identification.setCitation(citation);
			return citation;

		} else {
			Citation aux = identification.getCitation();
			if (aux instanceof DefaultCitation) {
				return (DefaultCitation) aux;
			} else {
				citation = new DefaultCitation();
				identification.setCitation(citation);
				return citation;
			}
		}
	}

	public static AbstractIdentification getFisrtIdentificationInfo(
			SedooMetadata metadata) {
		AbstractIdentification identification = null;

		if (metadata.getIdentificationInfo().isEmpty()) {
			identification = new DefaultDataIdentification();
			metadata.setIdentificationInfo(new ArrayList<Identification>(
					Collections.singletonList(identification)));
			return identification;
		} else {
			Identification aux = (Identification) metadata
					.getIdentificationInfo().iterator().next();
			if (aux instanceof DefaultDataIdentification) {
				return (DefaultDataIdentification) aux;
			} else if (aux instanceof ServiceIdentificationImpl) {
				return (ServiceIdentificationImpl) aux;
			} else {
				if (aux instanceof DataIdentification) {
					identification = new DefaultDataIdentification(
							(DataIdentification) aux);
					metadata.setIdentificationInfo(new ArrayList<Identification>(
							Collections.singletonList(identification)));
					return identification;
				} else {
					// cas impossible
					return null;
				}
			}
		}
	}

	public static Locale getLocaleFromISO3(String code) {
		return localeMap.get(code);
	}

	public static Date parseStringToDate(String string) throws Exception {

		try {
			Date date = (Date) englishDateFormatter.parse(string);
			return date;
		} catch (Exception e) {
			throw e;
		}
	}

	public static String formatDate(Date date) {
		if (date == null) {
			return "";
		} else {
			return englishDateFormatter.format(date);
		}
	}

	public static ResponsibleParty metadataContactToResponsibleParty(
			Contact metadataContact) {
		DefaultResponsibleParty responsibleParty = new DefaultResponsibleParty(
				Role.POINT_OF_CONTACT);
		responsibleParty.setIndividualName(metadataContact.getIndividualName());
		responsibleParty.setOrganisationName(new DefaultInternationalString(
				metadataContact.getOrganisationName()));
		DefaultContact contactInfo = new DefaultContact();
		DefaultAddress address = new DefaultAddress();
		address.setElectronicMailAddresses(Collections
				.singletonList(metadataContact.getEmailAddress()));
		contactInfo.setAddress(address);
		responsibleParty.setContactInfo(contactInfo);
		return responsibleParty;
	}

	public static SedooMetadata fromISO19139(String xml) throws Exception {
		Unmarshaller unmarshaller = CSWMarshallerPool.getInstance()
				.acquireUnmarshaller();
		String input = correctPreUnmarshallingProblems(xml);
		final StringReader in = new StringReader(input);

		// JAXBContext context = JAXBContext.newInstance(RBVMetadata.class);
		// Unmarshaller unmarshaller = context.createUnmarshaller();

		Object unmarshalledObject;
		try {
			unmarshalledObject = unmarshaller.unmarshal(in);
		} catch (Exception e) {
			throw new Exception("Unmarshalling problem");
		} finally {
			CSWMarshallerPool.getInstance().release(unmarshaller);
		}

		if (unmarshalledObject instanceof SedooMetadata) {
			SedooMetadata aux = (SedooMetadata) unmarshalledObject;
			correctPostUnmarshallingProblems(aux, input);
			return aux;
		} else if (unmarshalledObject instanceof Metadata) {
			SedooMetadata aux = new SedooMetadata((Metadata) unmarshalledObject);
			correctPostUnmarshallingProblems(aux, input);
			return aux;
		} else {
			throw new Exception(
					"Unmarshalling problem - Object not an instance of Metadata");
		}
	}

	private static void correctPostUnmarshallingProblems(SedooMetadata aux,
			String xml) throws Exception {
		// La période temporelle est ignorée par le marshalling ...
		TimePeriod firstTimePeriod = TemporalExtendPatch
				.getFirstTimePeriodFromXml(xml);
		if (!StringUtils.isEmpty(firstTimePeriod.getBeginPosition())) {
			aux.setResourceBeginDate(StringUtils.trimToEmpty(firstTimePeriod
					.getBeginPosition()));
		}
		if (!StringUtils.isEmpty(firstTimePeriod.getEndPosition())) {
			aux.setResourceEndDate(StringUtils.trimToEmpty(firstTimePeriod
					.getEndPosition()));
		} else if (!StringUtils.isEmpty(firstTimePeriod
				.getIndeterminatePosition())) {
			aux.setResourceEndDate(TimeIndeterminateValueType.NOW.name());
		}
	}

	/**
	 * Cette fonction corrige assez mécaniquement les erreurs empéchant la
	 * désérialisation
	 * 
	 * @param result
	 * @return
	 */
	public static String correctPreUnmarshallingProblems(String input) {

		String result = input;
		result = result.replaceAll("(?m)^[ \t]*\r?\n", "");
		result = result.replace("locale=\"#FR\"", "locale=\"#locale-fra\"");
		result = result.replace("locale=\"#ENG\"", "locale=\"#locale-eng\"");
		result = result.replace("&", "&#038;");
		// Si rbv:rbvExtent est présent on rajoute le nameSpace et on remplace
		// le noeud gmd:MD_Metadata par rbv:RBVMetadata

		// On ajoute l'espace de nommage rbv et on transfor

		// Si l'espace de nombage rbv est présent dans la fiche alors on
		// remplace le noeud gmd:MD_Metadata par rbv:RBVMetadata
		// if (result.indexOf("rbv:rbvExtent")>0)
		// {
		// if (result.indexOf("xmlns:rbv=\"http://www.sedoo.fr/rbv\"")<0)
		// {
		// result = result.replace("<gmd:MD_Metadata",
		// "<gmd:MD_Metadata xmlns:rbv=\"http://www.sedoo.fr/rbv\" ");
		// }
		// result = result.replace("gmd:MD_Metadata","rbv:RBVMetadata");
		// }

		return result;

	}

	public static String toISO19139(SedooMetadata metadata) throws Exception {

		Marshaller marshaller = CSWMarshallerPool.getInstance()
				.acquireMarshaller();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			marshaller.marshal(metadata, outputStream);
		} catch (Exception e) {
			throw new Exception("Marshalling exception");
		} finally {
			CSWMarshallerPool.getInstance().release(marshaller);
		}
		String result = outputStream.toString();
		String correctedString = correctMarshallingProblems(result);
		return prettyFormat(correctedString, 2);
	}

	/**
	 * Cette fonction corrige assez mécaniquement les erreurs constatées sur la
	 * sérialisation fournie par Geotoolkit
	 * 
	 * @param result
	 * @return
	 */
	private static String correctMarshallingProblems(String input) {

		String result = input;

		// Forcage des codes de langage:
		result = result.replace("codeListValue=\"fra\"",
				"codeListValue=\"fre\"");
		result = result.replace("codeSpace=\"fra\"", "codeSpace=\"fre\"");
		result = result.replace("#locale-fra", "#locale-fre");
		result = result.replace("French</gmd:LanguageCode>",
				"fre</gmd:LanguageCode>");
		result = result.replace("English</gmd:LanguageCode>",
				"eng</gmd:LanguageCode>");

		// Remplacement des after par des now ...
		result = result.replace("indeterminatePosition=\"after\"",
				"indeterminatePosition=\"now\"");

		return result;
	}

	public static String prettyFormat(String input, Integer indent) {
		try {
			Source xmlInput = new StreamSource(new StringReader(input));
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			// transformerFactory.setAttribute("indent-number", indent);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(xmlInput, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (Exception e) {
			throw new RuntimeException(e); // simple exception handling, please
											// review it
		}
	}

	public static SedooMetadata getEmptyMetadata() {
		return new SedooMetadata();
	}

	public static Summary toSummary(SedooMetadata metadata, Locale locale) {
		Summary summary = new Summary();
		summary.setUuid(metadata.getUuid());
		// summary.setResourceAbstract(metadata.getResourceAbstractDisplay(locale));
		// summary.setResourceTitle(metadata.getResourceTitleDisplay(locale));
		return summary;
	}

	/**
	 * Retourne le temporalExtent d'une métadonnées. Les éléments manquants sont
	 * créés si nécessaire Le temporalExtent est wrappé dans un
	 * DefaultTemporalExtent pour pouvoir être modifié
	 * 
	 * @param metadata
	 * @return
	 */
	public static DefaultTemporalExtent getDefaultTemporalExtent(
			SedooMetadata metadata) {
		AbstractIdentification aux = MetadataTools
				.getFisrtIdentificationInfo(metadata);
		if (aux instanceof DefaultDataIdentification) {
			DefaultDataIdentification dataIdentification = (DefaultDataIdentification) MetadataTools
					.getFisrtIdentificationInfo(metadata);
			Collection<Extent> extents = dataIdentification.getExtents();

			if (extents == null) {
				extents = new ArrayList<Extent>();
				dataIdentification.setExtents(extents);
			}
			DefaultExtent uniqueExtent = null;
			if (extents.isEmpty()) {
				uniqueExtent = new DefaultExtent();
				extents.add(uniqueExtent);
			} else {
				Extent next = extents.iterator().next();
				if (next instanceof DefaultExtent) {
					uniqueExtent = (DefaultExtent) next;
				} else {
					uniqueExtent = new DefaultExtent(extents.iterator().next());
					extents.clear();
					extents.add(uniqueExtent);
				}
			}

			Collection<TemporalExtent> temporalElements = uniqueExtent
					.getTemporalElements();

			if ((temporalElements == null) || (temporalElements.isEmpty())) {
				DefaultTemporalExtent defExtent = new DefaultTemporalExtent();
				uniqueExtent.setTemporalElements(Collections
						.singletonList(defExtent));
				return defExtent;
			} else {
				// On étudie le premier
				TemporalExtent existingTemporalExtent = temporalElements
						.iterator().next();

				// C'est déjà un DefaultTemporalExtent
				if (existingTemporalExtent instanceof DefaultTemporalExtent) {
					return (DefaultTemporalExtent) existingTemporalExtent;
				} else
				// On le wrappe et on le relie
				{
					DefaultTemporalExtent wrapped = new DefaultTemporalExtent(
							existingTemporalExtent);
					uniqueExtent.setTemporalElements(Collections
							.singletonList(wrapped));
					return wrapped;
				}
			}
		} else {
			// Cas d'un ServiceIdentification
			return new DefaultTemporalExtent();
		}
	}

	public static DefaultConstraints getUseConditionConstraint(
			SedooMetadata metadata) {
		AbstractIdentification dataIdentification = MetadataTools
				.getFisrtIdentificationInfo(metadata);
		Collection<Constraints> resourceConstraints = dataIdentification
				.getResourceConstraints();
		if (resourceConstraints == null) {
			resourceConstraints = new ArrayList<Constraints>();
			dataIdentification.setResourceConstraints(resourceConstraints);
		}
		Iterator<Constraints> iterator = resourceConstraints.iterator();
		boolean replacementNecessary = false;
		Constraints toBeReplacedConstraint = null;
		while (iterator.hasNext()) {
			Constraints current = (Constraints) iterator.next();
			// On retourne la première contraintes ayant des useLimitations
			if (current.getUseLimitations().isEmpty() == false) {
				if (current instanceof DefaultConstraints) {
					return (DefaultConstraints) current;
				} else {
					replacementNecessary = true;
					toBeReplacedConstraint = current;
					break;
				}
			}
		}

		if (replacementNecessary) {
			// Cas normalement impossible
			resourceConstraints.remove(toBeReplacedConstraint);
			DefaultConstraints wrapped = new DefaultConstraints(
					toBeReplacedConstraint);
			resourceConstraints.add(wrapped);
			return wrapped;
		} else {
			// Aucune contrainte ayant des useLimitations
			DefaultConstraints newConstraints = new DefaultConstraints();
			resourceConstraints.add(newConstraints);
			return newConstraints;
		}

	}

	public static DefaultLegalConstraints getPublicAccessLimitationConstraint(
			SedooMetadata metadata) {
		AbstractIdentification dataIdentification = MetadataTools
				.getFisrtIdentificationInfo(metadata);
		Collection<Constraints> resourceConstraints = dataIdentification
				.getResourceConstraints();
		if (resourceConstraints == null) {
			resourceConstraints = new ArrayList<Constraints>();
			dataIdentification.setResourceConstraints(resourceConstraints);
		}
		Iterator<Constraints> iterator = resourceConstraints.iterator();
		boolean replacementNecessary = false;
		Constraints toBeReplacedConstraint = null;
		while (iterator.hasNext()) {
			Constraints current = iterator.next();
			if (current instanceof LegalConstraints) {
				// TODO: if faudrait tester que accessConstraint n'a qu'un seul
				// noeud de type RestrictionCode
				if (current instanceof DefaultLegalConstraints) {
					return (DefaultLegalConstraints) current;
				} else {
					replacementNecessary = true;
					toBeReplacedConstraint = current;
					break;
				}
			}
		}

		if (replacementNecessary) {
			// Cas normalement impossible
			resourceConstraints.remove(toBeReplacedConstraint);
			DefaultLegalConstraints wrapped = new DefaultLegalConstraints(
					(LegalConstraints) toBeReplacedConstraint);
			resourceConstraints.add(wrapped);
			return wrapped;
		} else {
			// Aucune contrainte ayant des useLimitations
			DefaultLegalConstraints newConstraints = new DefaultLegalConstraints();
			Restriction otherRestrictions = Restriction.OTHER_RESTRICTIONS;
			newConstraints.setAccessConstraints(Collections
					.singletonList(otherRestrictions));
			newConstraints.setOtherConstraints(Collections.EMPTY_LIST);
			resourceConstraints.add(newConstraints);
			return newConstraints;
		}

	}

	public static HashMap<Locale, String> toLocaleMap(
			HashMap<String, String> i18nValues) {
		HashMap<Locale, String> result = new HashMap<Locale, String>();

		Iterator<String> iterator = i18nValues.keySet().iterator();
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			String value = StringUtils.trimToEmpty(i18nValues.get(language));
			if (StringUtils.isNotEmpty(value)) {
				result.put(
						new Locale(Iso6392LanguageConverter
								.convertIso6392Tolocale(language)), value);
			}
		}

		return result;
	}

	public static String getDefaultValue(HashMap<String, String> i18nValues,
			String metadataLanguage) {
		if (StringUtils.isEmpty(metadataLanguage)) {
			return "";
		} else {
			return StringUtils.trimToEmpty(i18nValues.get(metadataLanguage));
		}
	}

	public static Map<String, String> getExistingValues(
			InternationalString value, String metadataLanguage) {
		HashMap<String, String> result = new HashMap<String, String>();
		if (value == null) {
			return result;
		} else {
			if (value instanceof SimpleInternationalString) {
				result.put(null, value.toString());
				return result;
			} else {
				Map<Locale, String> aux = SedooMetadata
						.getPrivateLocaleMap((DefaultInternationalString) value);
				Iterator<Locale> iterator = aux.keySet().iterator();
				while (iterator.hasNext()) {
					Locale locale = (Locale) iterator.next();
					if ((locale == null)
							|| (StringUtils.isEmpty((locale.getLanguage())))) {
						result.put(metadataLanguage, aux.get(locale));
					} else {
						result.put(Iso6392LanguageConverter
								.convertLocaleToIso6392(locale.getLanguage()),
								aux.get(locale));
					}
				}
				return result;
			}
		}
	}

	public static boolean isI18n(InternationalString value) {
		if (value == null) {
			return false;
		} else if (value instanceof SimpleInternationalString) {
			return false;
		} else {
			return true;
		}
	}

	public static String getDefaultValue(InternationalString value) {
		if (value == null) {
			return "";
		} else {
			return StringUtils.trimToEmpty(value.toString());
		}
	}
}
