package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;

public interface KeywordListener {

	public void keywordDeleted(EditableKeywordLabel label);

	public void addKeywords(ArrayList<String> checkboxesForValue);

	public void removeKeywords(ArrayList<String> checkboxesForValue);

	public ArrayList<String> getCurrentKeywords();

}
