package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.widget.panel.CenteringPanel;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.ClientThesaurusList;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.EditableKeywordLabel;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.KeywordLabel;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.KeywordListener;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.ThesaurusButton;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.thesaurus.DefaultSingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;

public class Keywords extends HorizontalPanel implements IsDisplay, IsEditor,
		LoadCallBack<Map<String, DefaultSingleLevelThesaurus>>, KeywordListener {
	private static final String THESAURUS_PANEL_WIDTH = "150px";
	private VerticalPanel thesaurusPanel;
	private FlowPanel keywordContainer;
	private List<String> keywordList;
	private ArrayList<String> thesaurusNames;

	public Keywords(ArrayList<String> thesaurusNames) {
		super();
		this.thesaurusNames = thesaurusNames;
		setWidth(FieldConstant.HUNDRED_PERCENT);
	}

	@Override
	public void display(MetadataDTO metadata) {
		clear();
		List<String> keywords = metadata.getKeywordPart().getKeywords();

		if (ListUtil.isEmpty(keywords)) {
			add(new Label(CommonMessages.INSTANCE.emptyList()));
		} else {
			FlowPanel aux = new FlowPanel();
			aux.setWidth("100%");
			for (String keyword : keywords) {
				KeywordLabel label = new KeywordLabel(keyword);
				aux.add(label);
			}
			add(aux);
		}
	}

	@Override
	public void reset() {
		clear();
		add(new Label(CommonMessages.INSTANCE.emptyList()));
	}

	@Override
	public void flush(MetadataDTO metadata) {
		metadata.getKeywordPart().setKeywords(keywordList);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		clear();
		keywordList = metadata.getKeywordPart().getKeywords();
		HorizontalPanel mainPanel = new HorizontalPanel();
		mainPanel.setWidth("100%");
		mainPanel.setSpacing(15);

		keywordContainer = new FlowPanel();
		keywordContainer.setWidth("100%");
		for (String keyword : keywordList) {
			EditableKeywordLabel label = new EditableKeywordLabel(keyword, this);
			keywordContainer.add(label);
		}
		mainPanel.add(keywordContainer);
		if (thesaurusPanel == null) {
			thesaurusPanel = new VerticalPanel();
			thesaurusPanel.setSpacing(3);
			thesaurusPanel.setWidth(THESAURUS_PANEL_WIDTH);
			InlineLabel titleLabel = new InlineLabel(
					MetadataMessage.INSTANCE.availableThesauri());

			thesaurusPanel.add(new CenteringPanel(titleLabel));
			mainPanel.add(thesaurusPanel);
			ClientThesaurusList.getThesauri(thesaurusNames, this);
		} else {
			mainPanel.add(thesaurusPanel);
		}
		mainPanel.setCellWidth(thesaurusPanel, THESAURUS_PANEL_WIDTH);
		add(mainPanel);
	}

	@Override
	public void postLoadProcess(Map<String, DefaultSingleLevelThesaurus> result) {

		for (String thesaurusName : thesaurusNames) {
			Thesaurus thesaurus = result.get(thesaurusName);
			if (thesaurus != null) {
				ThesaurusButton thesaurusButton = new ThesaurusButton(
						thesaurus, this);

				thesaurusPanel.add(thesaurusButton);
			}
		}
	}

	@Override
	public void keywordDeleted(EditableKeywordLabel label) {
		keywordList.remove(label.getLabel());
		keywordContainer.remove(label);
	}

	@Override
	public void addKeywords(ArrayList<String> selectedValues) {
		for (String value : selectedValues) {
			if (keywordList.contains(value) == false) {
				keywordList.add(value);
				keywordContainer.add(new EditableKeywordLabel(value, this));
			}
		}
	}

	@Override
	public ArrayList<String> getCurrentKeywords() {
		return new ArrayList<String>(keywordList);
	}

	@Override
	public void removeKeywords(ArrayList<String> unSelectedValues) {
		for (String value : unSelectedValues) {
			if (keywordList.contains(value) == true) {
				keywordList.remove(value);
				for (Widget widget : keywordContainer) {
					if (widget instanceof EditableKeywordLabel) {
						if (((EditableKeywordLabel) widget).getLabel()
								.compareToIgnoreCase(value) == 0) {
							keywordContainer.remove(widget);
							break;
						}
					}
				}
			}
		}
	}
}
