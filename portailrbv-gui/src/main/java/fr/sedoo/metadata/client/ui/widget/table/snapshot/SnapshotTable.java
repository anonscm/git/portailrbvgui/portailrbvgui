package fr.sedoo.metadata.client.ui.widget.table.snapshot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

import fr.obsmip.sedoo.client.message.Message;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.dialog.CancelDialog;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.ImagedActionCell.Delegate;
import fr.sedoo.commons.client.widget.table.PreviewCell;
import fr.sedoo.commons.client.widget.table.TableBundle;
import fr.sedoo.commons.client.widget.uploader.FileUploadUtil;
import fr.sedoo.commons.client.widget.uploader.UploadListener;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.table.common.DescribedStringTable;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class SnapshotTable extends DescribedStringTable implements
		UploadListener {

	protected Column<HasIdentifier, String> seeColumn;
	private final Image seeImage = new Image(TableBundle.INSTANCE.view());
	private Label importImageLabel;
	private Image importImage;

	@UiConstructor
	public SnapshotTable(String waterMark, String descriptionWatermark,
			String addItemText, String valueColumnHeader,
			String descriptionColumnHeader) {
		super(waterMark, descriptionWatermark, addItemText, valueColumnHeader,
				descriptionColumnHeader);

		HorizontalPanel aux = super.getToolBarPanel();
		importImage = new Image(TableBundle.INSTANCE.add());
		importImage.setStyleName("clickable");
		aux.add(importImage);
		importImageLabel = new Label(Message.INSTANCE.importFile());
		importImageLabel.setStyleName("clickable");
		aux.add(importImageLabel);
		importImage.addClickHandler(this);
		importImageLabel.addClickHandler(this);

	}

	@Override
	protected void initEditColumns() {

		super.initEditColumns();
		Cell<HasIdentifier> seeActionCell = new ImagedActionCell<HasIdentifier>(
				seeAction, seeImage, CommonMessages.INSTANCE.view());
		seeColumn = new Column(seeActionCell) {

			@Override
			public HasIdentifier getValue(Object object) {
				return (HasIdentifier) object;
			}
		};

		itemTable.addColumn(seeColumn);
		itemTable.setColumnWidth(seeColumn, 30.0, Unit.PX);
	}

	@Override
	protected void initDisplayColumns() {

		super.initDisplayColumns();
		Cell<IsUrl> previewCell = new PreviewCell();
		Column previewColumn = new Column(previewCell) {

			@Override
			public IsUrl getValue(Object object) {
				return (IsUrl) object;
			}
		};

		itemTable.addColumn(previewColumn);
		itemTable.setColumnWidth(previewColumn, 110.0, Unit.PX);
	}

	Delegate<HasIdentifier> seeAction = new Delegate<HasIdentifier>() {

		@Override
		public void execute(final HasIdentifier hasId) {

			IdentifiedDescribedString aux = (IdentifiedDescribedString) hasId;
			if ((aux.getValue() == null)
					|| (aux.getValue().trim().length() == 0)) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(),
						MetadataMessage.INSTANCE.urlAreMandatory());
				return;
			} else {
				DialogBoxTools.popUpImage(CommonMessages.INSTANCE.view(),
						aux.getValue());
			}

		}

	};
	private CancelDialog importDialog;

	public void onClick(ClickEvent event) {
		if ((event.getSource() == importImage)
				|| (event.getSource() == importImageLabel)) {

			ImportFileContent content = new ImportFileContent();
			content.addUploadListener(this);
			importDialog = new CancelDialog(Message.INSTANCE.importFile(),
					content);
			importDialog.show();
		} else {
			super.onClick(event);
		}
	}

	@Override
	public void fileUploaded(Long id) {
		String url = FileUploadUtil.getImageUrlFromId(id);
		importDialog.hide();

		Long maxId = 0L;
		List<IdentifiedDescribedString> newValues = new ArrayList<IdentifiedDescribedString>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) {
			IdentifiedDescribedString aux = (IdentifiedDescribedString) iterator
					.next();
			newValues.add(aux);
			if (aux.getId() > maxId) {
				maxId = aux.getId();
			}
		}
		IdentifiedDescribedString newLine = new IdentifiedDescribedString();
		newLine.setValue(url);
		newLine.setId(maxId + 1);
		newValues.add(newLine);
		init(newValues);
	}

	@Override
	public void fileUploaded(Long id, String fileName) {
		fileUploaded(id);
	}

}
