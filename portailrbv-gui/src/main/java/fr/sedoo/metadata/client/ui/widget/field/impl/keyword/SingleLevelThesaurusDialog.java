package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;

import fr.sedoo.commons.client.widget.dialog.OkCancelDialog;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.shared.domain.thesaurus.SingleLevelThesaurus;

public class SingleLevelThesaurusDialog extends OkCancelDialog {

	public SingleLevelThesaurusDialog(SingleLevelThesaurus thesaurus,
			ArrayList<String> currentKeywords, KeywordListener listener) {
		super(MetadataMessage.INSTANCE.keywordSelection(),
				new SingleLevelThesaurusDialogContent(thesaurus,
						currentKeywords, listener));
	}

}
