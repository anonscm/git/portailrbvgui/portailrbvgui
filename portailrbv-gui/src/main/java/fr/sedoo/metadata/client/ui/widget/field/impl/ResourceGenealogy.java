package fr.sedoo.metadata.client.ui.widget.field.impl;

import java.util.ArrayList;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.field.primitive.I18nTextArea;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class ResourceGenealogy extends I18nTextArea {

	public ResourceGenealogy(ArrayList<String> languages) {
		super(languages);
	}

	@Override
	public void display(MetadataDTO metadata) {
		String value = metadata.getMeasurementPart().getResourceGenealogy()
				.getDisplayValue(languages);
		if (StringUtil.isEmpty(value)) {
			value = MetadataMessage.INSTANCE.noInformations();
		}
		super.display(value);
	}

	@Override
	public void flush(MetadataDTO metadata) {

		I18nString aux = new I18nString();
		aux.setI18nValues(getI18nValues());
		metadata.getMeasurementPart().setResourceGenealogy(aux);
	}

	@Override
	public void edit(MetadataDTO metadata) {
		super.edit(metadata.getMeasurementPart().getResourceGenealogy()
				.getI18nValues());
	}
}
