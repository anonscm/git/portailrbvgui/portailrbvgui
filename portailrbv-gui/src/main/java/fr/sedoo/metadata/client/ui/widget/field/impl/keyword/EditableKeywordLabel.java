package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.constants.IconType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.InlineLabel;

import fr.sedoo.commons.client.message.CommonMessages;

public class EditableKeywordLabel extends HTMLPanel {

	private String label;
	private KeywordListener listener;

	protected EditableKeywordLabel() {
		super("span", "");
		addStyleName("label");
		addStyleName("label-default");
		addStyleName("keywordLabel");

	}

	public EditableKeywordLabel(String label, KeywordListener listener) {
		this();
		this.listener = listener;
		this.label = label;
		add(new InlineLabel(label));
		Anchor cross = new Anchor();
		cross.setTitle(CommonMessages.INSTANCE.delete());
		cross.setIcon(IconType.TIMES);
		cross.getElement().getStyle().setColor("#545454");
		cross.getElement().getStyle().setMarginLeft(3, Unit.PX);
		cross.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				getListener().keywordDeleted(EditableKeywordLabel.this);
			}
		});
		add(cross);
	}

	public KeywordListener getListener() {
		return listener;
	}

	public String getLabel() {
		return label;
	}

}
