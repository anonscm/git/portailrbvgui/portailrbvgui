package fr.sedoo.metadata.client.ui.widget.table.link;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.cellview.client.Column;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.table.WaterMarkCell;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.table.common.DescribedStringTable;
import fr.sedoo.metadata.shared.domain.dto.InternetLink;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class InternetLinkTable extends DescribedStringTable {

	private boolean protocolVisible;
	private static HashMap<String, String> protocolLabels = new HashMap<String, String>();

	static {
		protocolLabels.put(InternetLink.DOWNLOAD_PROTOCOL,
				MetadataMessage.INSTANCE.downloadProtocol());
		protocolLabels.put(InternetLink.LINK_PROTOCOL,
				MetadataMessage.INSTANCE.linkProtocol());
		protocolLabels.put(InternetLink.WMS_SERVICE,
				MetadataMessage.INSTANCE.wmsProtocol());
		protocolLabels.put(InternetLink.WFS_SERVICE,
				MetadataMessage.INSTANCE.wfsProtocol());
		protocolLabels.put(InternetLink.WMS_SERVICE_SHORT,
				MetadataMessage.INSTANCE.wmsProtocol());
		protocolLabels.put(InternetLink.WFS_SERVICE_SHORT,
				MetadataMessage.INSTANCE.wfsProtocol());
		protocolLabels.put(InternetLink.DEFAULT_PROTOCOL,
				MetadataMessage.INSTANCE.linkProtocol());
	}

	@UiConstructor
	public InternetLinkTable(String waterMark, String descriptionWatermark,
			String addItemText, String valueColumnHeader,
			String descriptionColumnHeader) {
		super(waterMark, descriptionWatermark, addItemText, valueColumnHeader,
				descriptionColumnHeader);

	}

	@Override
	protected void initEditColumns() {

		valueColumn = new Column<HasIdentifier, String>(new WaterMarkCell(
				getWatermark(), "25", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedDescribedString) identifiedString)
						.getValue();
			}
		};

		valueColumn.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
			@Override
			public void update(int index, HasIdentifier identifiedString,
					String value) {
				((IdentifiedDescribedString) identifiedString).setValue(value);
			}
		});

		descriptionColumn = new Column<HasIdentifier, String>(
				new WaterMarkCell(getDescriptionWatermark(), "25", "120")) {
			@Override
			public String getValue(HasIdentifier identifiedString) {
				return ((IdentifiedDescribedString) identifiedString)
						.getDescription();
			}
		};

		descriptionColumn
				.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
					@Override
					public void update(int index,
							HasIdentifier identifiedString, String description) {
						((IdentifiedDescribedString) identifiedString)
								.setDescription(description);
					}
				});

		// valueColumn.setCellStyleNames("url");
		itemTable.addColumn(valueColumn, getValueColumnHeader());
		itemTable.setColumnWidth(valueColumn, 50.0, Unit.PCT);
		itemTable.addColumn(descriptionColumn, getDescriptionColumnHeader());
		itemTable.setColumnWidth(descriptionColumn, 50.0, Unit.PCT);
		itemTable.addColumn(deleteColumn);
		itemTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);
		setAddButtonEnabled(true);
		;

		if (isProtocolVisible()) {

			List<String> protocols = new ArrayList<String>();
			protocols.add(protocolLabels.get(InternetLink.LINK_PROTOCOL));
			protocols.add(protocolLabels.get(InternetLink.DOWNLOAD_PROTOCOL));
			protocols.add(protocolLabels.get(InternetLink.WMS_SERVICE));
			protocols.add(protocolLabels.get(InternetLink.WFS_SERVICE));

			SelectionCell protocolCell = new SelectionCell(protocols);

			Column<HasIdentifier, String> protocolColumn = new Column<HasIdentifier, String>(
					protocolCell) {
				@Override
				public String getValue(HasIdentifier object) {
					String protocol = ((InternetLink) object).getProtocol();
					String aux = protocolLabels.get(protocol);
					if (aux == null) {
						aux = protocolLabels.get(InternetLink.DEFAULT_PROTOCOL);
					}
					return aux;
				}
			};

			protocolColumn
					.setFieldUpdater(new FieldUpdater<HasIdentifier, String>() {
						@Override
						public void update(int index, HasIdentifier object,
								String value) {
							Iterator<String> iterator = protocolLabels.keySet()
									.iterator();
							while (iterator.hasNext()) {
								String key = iterator.next();
								String label = protocolLabels.get(key);
								if (label.compareTo(value) == 0) {
									((InternetLink) object).setProtocol(key);
									break;
								}
							}
						}
					});

			itemTable.insertColumn(2, protocolColumn,
					CommonMessages.INSTANCE.type());
			itemTable.setColumnWidth(valueColumn, 37.0, Unit.PCT);
			itemTable.setColumnWidth(descriptionColumn, 37.0, Unit.PCT);
			itemTable.setColumnWidth(protocolColumn, 26.0, Unit.PCT);
		}
	}

	@Override
	protected void initDisplayColumns() {

		Column<HasIdentifier, IsUrl> linkColumn = new Column<HasIdentifier, IsUrl>(
				new InternetLinkCell(false, isProtocolVisible())) {
			@Override
			public IsUrl getValue(HasIdentifier identifiedDescribedString) {
				return (IsUrl) identifiedDescribedString;
			}
		};

		itemTable.addColumn(linkColumn);
		itemTable.setColumnWidth(linkColumn, 80.0, Unit.PX);
	}

	public boolean isProtocolVisible() {
		return protocolVisible;
	}

	public void setProtocolVisible(boolean protocolVisible) {
		this.protocolVisible = protocolVisible;
	}

	/**
	 * On créé un lien vide de type {@link InternetLink}.LINK_PROTOCOL
	 */
	protected InternetLink getDefaultIdentifiedString(Long id) {

		InternetLink link = new InternetLink();
		link.setValue("");
		link.setDescription("");
		link.setProtocol(InternetLink.LINK_PROTOCOL);
		link.setId(id);
		return link;
	}

	public static String getProtocolLabelByCode(String code) {
		String aux = StringUtil.trimToEmpty(protocolLabels.get(code));
		if (aux == null) {
			// On est dans le cas d'un code non identifé...on le laisse tél
			// quel.
			return code;
		} else {
			return aux;
		}
	}

}
