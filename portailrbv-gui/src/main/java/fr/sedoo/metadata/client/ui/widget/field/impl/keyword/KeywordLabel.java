package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import org.gwtbootstrap3.client.ui.Label;

public class KeywordLabel extends Label {

	public KeywordLabel() {
		super();
		addStyleName("keywordLabel");

	}

	public KeywordLabel(String label) {
		this();
		setText(label);
	}
}
