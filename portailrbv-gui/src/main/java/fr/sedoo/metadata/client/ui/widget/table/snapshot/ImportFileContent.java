package fr.sedoo.metadata.client.ui.widget.table.snapshot;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.widget.dialog.CancelContent;
import fr.sedoo.commons.client.widget.uploader.FileUploader;
import fr.sedoo.commons.client.widget.uploader.UploadListener;

public class ImportFileContent implements CancelContent {

	private static final String IMPORTED_IMAGE = "IMPORTED_IMAGE";
	private DialogBox dialog;
	private VerticalPanel panel;
	private FileUploader uploader;

	public ImportFileContent() {
		panel = new VerticalPanel();
		uploader = new FileUploader();
		panel.add(uploader);
		uploader.setUuid(IMPORTED_IMAGE);

	}

	public void addUploadListener(UploadListener listener) {
		uploader.addUploadListener(listener);
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	@Override
	public void setDialogBox(DialogBox dialog) {
		this.dialog = dialog;

	}

	@Override
	public String getPreferredHeight() {
		return asWidget().getOffsetHeight() + "px";
	}

	@Override
	public void cancelClicked() {
		dialog.hide();
	}

}
