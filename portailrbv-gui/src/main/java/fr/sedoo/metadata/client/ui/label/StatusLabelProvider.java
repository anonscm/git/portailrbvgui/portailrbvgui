package fr.sedoo.metadata.client.ui.label;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.sedoo.metadata.client.message.MetadataMessage;

public class StatusLabelProvider extends PropertyLabelProvider {

	private static Map<String, String> editLabels = null;
	private static Map<String, String> displayLabels = null;

	public static String getLabel(String key) {
		if ((key == null) || (key.trim().length() == 0)) {
			return "";
		}
		if (editLabels == null) {
			initLabels();
		}
		String label = displayLabels.get(toKey(key));
		if (label != null) {
			return label;
		} else {
			return "";
		}
	}

	private static void initLabels() {
		editLabels = new LinkedHashMap<String, String>();
		displayLabels = new LinkedHashMap<String, String>();
		String aux = MetadataMessage.INSTANCE.metadataEditingStatusItem();
		initLabels(aux);
	}

	public static Map<String, String> getLabels() {
		if (editLabels == null) {
			initLabels();
		}
		return editLabels;
	}

	protected static void initLabels(String inputString) {
		String[] split = inputString.split("@");
		for (int i = 0; i < split.length; i++) {
			String[] split2 = split[i].split("\\|");
			editLabels.put(split2[0], split2[1]);
			displayLabels.put(toKey(split2[0]), split2[1]);
			System.out.println(toKey(split2[0]));
		}
	}

	private static String toKey(String value) {
		String result = value.toLowerCase();
		result = result.replace("_", "");
		return result;
	}

}
