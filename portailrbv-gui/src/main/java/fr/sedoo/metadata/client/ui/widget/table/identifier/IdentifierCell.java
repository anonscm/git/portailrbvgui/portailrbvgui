package fr.sedoo.metadata.client.ui.widget.table.identifier;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public class IdentifierCell extends AbstractCell<IdentifiedResourceIdentifier> {

	private static SafeHtml dot;

	static {
		Image aux = AbstractImagePrototype.create(
				GlobalBundle.INSTANCE.menuDot()).createImage();
		dot = SafeHtmlUtils.fromTrustedString(aux.toString());
	}

	public IdentifierCell() {
		super();
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			IdentifiedResourceIdentifier s, SafeHtmlBuilder sb) {

		String aux = StringUtil.trimToEmpty(((IdentifiedResourceIdentifier) s)
				.getNameSpace());
		if (StringUtil.isNotEmpty(aux)) {
			aux = " (" + aux + ")";
		}

		sb.append(dot);
		sb.append(SafeHtmlUtils.fromSafeConstant(" "));
		sb.append(SafeHtmlUtils.fromString(s.getCode() + aux));

	}
}