package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.widget.dialog.OkCancelContent;
import fr.sedoo.metadata.shared.domain.thesaurus.SingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class SingleLevelThesaurusDialogContent extends Composite implements
		OkCancelContent {

	private static SingleLevelThesaurusDialogContentUiBinder uiBinder = GWT
			.create(SingleLevelThesaurusDialogContentUiBinder.class);

	interface SingleLevelThesaurusDialogContentUiBinder extends
			UiBinder<Widget, SingleLevelThesaurusDialogContent> {
	}

	@UiField
	VerticalPanel contentPanel;
	private KeywordListener listener;

	public SingleLevelThesaurusDialogContent(SingleLevelThesaurus thesaurus,
			List<String> currentKeywords, KeywordListener listener) {
		super();
		this.listener = listener;
		checkBoxes = new ArrayList<CheckBox>();

		initWidget(uiBinder.createAndBindUi(this));
		for (ThesaurusItem current : thesaurus.getKeywords()) {
			String currentValue = current.getValue();
			CheckBox checkBox = new CheckBox(currentValue);
			if (currentKeywords.contains(currentValue)) {
				checkBox.setValue(true);
			}
			checkBoxes.add(checkBox);
			contentPanel.add(checkBox);
		}
	}

	private DialogBox dialog;
	private ArrayList<CheckBox> checkBoxes;

	@Override
	public void setDialogBox(DialogBox dialog) {
		this.dialog = dialog;
	}

	public DialogBox getDialog() {
		return dialog;
	}

	@Override
	public void okClicked() {
		if (getDialog() != null) {
			getDialog().hide();
			listener.addKeywords(getCheckboxesForValue(true));
			listener.removeKeywords(getCheckboxesForValue(false));
		}
	}

	private ArrayList<String> getCheckboxesForValue(boolean value) {
		ArrayList<String> result = new ArrayList<String>();
		for (CheckBox checkBox : checkBoxes) {
			if (checkBox.getValue() == value) {
				result.add(checkBox.getText());
			}
		}
		return result;
	}

	@Override
	public void cancelClicked() {
		if (getDialog() != null) {
			getDialog().hide();
		}

	}

	@Override
	public String getPreferredHeight() {
		return "300px";
	}

}
