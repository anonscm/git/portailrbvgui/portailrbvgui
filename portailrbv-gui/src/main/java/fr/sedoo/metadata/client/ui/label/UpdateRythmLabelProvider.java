package fr.sedoo.metadata.client.ui.label;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;

public class UpdateRythmLabelProvider extends PropertyLabelProvider {

	private static Map<String, String> editLabels = null;
	private static Map<String, String> displayLabels = null;

	private static final String UNKNOWN_KEY = "unknown";

	/**
	 * @param key
	 * @return Le libellé internationalisé si la clé est trouvée dans la liste -
	 *         La recherche est insensible à la casse unknown sinon.
	 */
	public static String getLabel(String key) {
		String aux = key.toLowerCase();
		if (editLabels == null) {
			initLabels();
		}
		if (StringUtil.isEmpty(key)) {
			aux = UNKNOWN_KEY;
		}
		String label = displayLabels.get(aux);
		if (label != null) {
			return label;
		} else {
			return getLabel(UNKNOWN_KEY);
		}
	}

	private static void initLabels() {
		editLabels = new LinkedHashMap<String, String>();
		displayLabels = new LinkedHashMap<String, String>();
		String aux = MetadataMessage.INSTANCE.metadataEditingUpdateRythmItem();
		initLabels(aux);
	}

	public static Map<String, String> getLabels() {
		if (editLabels == null) {
			initLabels();
		}
		return editLabels;
	}

	protected static void initLabels(String inputString) {
		String[] split = inputString.split("@");
		for (int i = 0; i < split.length; i++) {
			String[] split2 = split[i].split("\\|");
			editLabels.put(split2[0], split2[1]);
			displayLabels.put(split2[0].toLowerCase(), split2[1]);
		}
	}

}
