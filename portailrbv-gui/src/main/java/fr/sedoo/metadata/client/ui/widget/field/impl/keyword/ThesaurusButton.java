package fr.sedoo.metadata.client.ui.widget.field.impl.keyword;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;

import fr.sedoo.metadata.shared.domain.thesaurus.SingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;

public class ThesaurusButton extends Button implements ClickHandler {

	private Thesaurus thesaurus;
	private KeywordListener listener;
	private ArrayList<String> currentKeywords;

	public ThesaurusButton(Thesaurus thesaurus, KeywordListener listener) {
		super(thesaurus.getShortLabel());
		this.thesaurus = thesaurus;
		this.listener = listener;

		setWidth("100%");
		addClickHandler(this);

	}

	@Override
	public void onClick(ClickEvent event) {
		if (thesaurus instanceof SingleLevelThesaurus) {

			SingleLevelThesaurusDialog dialog = new SingleLevelThesaurusDialog(
					(SingleLevelThesaurus) thesaurus,
					listener.getCurrentKeywords(), listener);

			dialog.show();
		}
	}

}
