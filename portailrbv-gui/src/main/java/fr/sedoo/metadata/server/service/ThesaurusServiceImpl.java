package fr.sedoo.metadata.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.metadata.client.service.ThesaurusService;
import fr.sedoo.metadata.server.service.thesaurus.ThesauriFactory;
import fr.sedoo.metadata.shared.domain.thesaurus.DefaultSingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class ThesaurusServiceImpl extends RemoteServiceServlet implements
		ThesaurusService {

	ThesauriFactory factory = null;

	public ThesaurusServiceImpl() {
		factory = (ThesauriFactory) DefaultServerApplication
				.getSpringBeanFactory()
				.getBeanByName(ThesauriFactory.BEAN_NAME);

	}

	@Override
	public ArrayList<ThesaurusItem> getItems(String thesaurusId, String language)
			throws ServiceException {
		return factory.getThesaurusItems(thesaurusId, language);
	}

	@Override
	public ArrayList<Thesaurus> getThesaurus(String language)
			throws ServiceException {
		return factory.getThesauri(language);
	}

	private String getThesaurusLabel(String id, String language)
			throws ServiceException {
		ArrayList<Thesaurus> thesaurus = getThesaurus(language);
		for (Thesaurus current : thesaurus) {
			if (current.getId().compareToIgnoreCase(id) == 0) {
				return current.getShortLabel();
			}
		}
		return id;
	}

	@Override
	public ArrayList<DefaultSingleLevelThesaurus> getDefaultSingleLevelThesauri(
			ArrayList<String> ids, String language) throws ServiceException {

		ArrayList<DefaultSingleLevelThesaurus> result = new ArrayList<DefaultSingleLevelThesaurus>();
		for (String id : ids) {
			DefaultSingleLevelThesaurus aux = new DefaultSingleLevelThesaurus();
			aux.setKeywords(getItems(id, language));
			aux.setShortLabel(getThesaurusLabel(id, language));
			aux.setId(id);
			result.add(aux);
		}
		return result;
	}

}
