package fr.sedoo.rbv.geonetwork.request.csw;

import java.util.ArrayList;

import org.apache.commons.digester3.binder.AbstractRulesModule;

import fr.sedoo.commons.metadata.shared.DefaultResourceIdentifier;
import fr.sedoo.commons.metadata.utils.domain.DescribedURL;
import fr.sedoo.commons.metadata.utils.domain.GeographicalBox;
import fr.sedoo.commons.metadata.utils.domain.GeographicalPolygon;
import fr.sedoo.commons.metadata.utils.domain.Summary;

public class SummaryModule extends AbstractRulesModule {
	public static final String VALUE_TAG = "value";
	public static final String LANG_TAG = "lang";

	@Override
	public void configure() {
		forPattern("*/csw:SearchResults").createObject()
				.ofType(ArrayList.class).then().setProperties();
		forPattern("*/csw:SearchResults/gmd:MD_Metadata").createObject()
				.ofType(Summary.class).then().setProperties().then()
				.setNext("add");
		forPattern("*/gmd:dateStamp/gco:Date").setBeanProperty().withName(
				"modificationDate");
		forPattern("*/gmd:fileIdentifier/gco:CharacterString")
				.setBeanProperty().withName("uuid");
		forPattern("*/gmd:language/gmd:LanguageCode").setBeanProperty()
				.withName("metadataLanguage");
		forPattern("*/gmd:parentIdentifier/gco:CharacterString")
				.setBeanProperty().withName("parentUuid");
		forPattern("*/gmd:hierarchyLevelName/gco:CharacterString")
				.setBeanProperty().withName("hierarchyLevelName");
		forPattern("*/gmd:abstract/gco:CharacterString").setBeanProperty()
				.withName("resourceAbstract");
		forPattern(
				"*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString")
				.callMethod("addAbstractTranslation").withParamTypes(
						"java.lang.String", "java.lang.String");
		forPattern(
				"*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/lang")
				.callParam().ofIndex(0);
		forPattern(
				"*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/value")
				.callParam().ofIndex(1);
		forPattern(
				"*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString")
				.setBeanProperty().withName("resourceTitle");
		forPattern(
				"*/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString")
				.setBeanProperty().withName("resourceTitle");
		forPattern(
				"*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString")
				.callMethod("addTitleTranslation").withParamTypes(
						"java.lang.String", "java.lang.String");
		forPattern(
				"*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/"
						+ LANG_TAG).callParam().ofIndex(0);
		forPattern(
				"*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/"
						+ VALUE_TAG).callParam().ofIndex(1);
		forPattern("*/gmd:MD_BrowseGraphic").createObject()
				.ofType(DescribedURL.class).then().setProperties().then()
				.setNext("addLink");
		forPattern("*/gmd:MD_BrowseGraphic/gmd:fileName/gco:CharacterString")
				.setBeanProperty().withName("link");
		forPattern(
				"*/gmd:MD_BrowseGraphic/gmd:fileDescription/gco:CharacterString")
				.setBeanProperty().withName("label");
		forPattern("*/gmd:identifier/").createObject()
				.ofType(DefaultResourceIdentifier.class).then().setProperties()
				.then().setNext("addIdentifier");
		forPattern(
				"*/gmd:identifier/gmd:RS_Identifier/gmd:code/gco:CharacterString")
				.setBeanProperty().withName("code");
		forPattern(
				"*/gmd:identifier/gmd:RS_Identifier/gmd:codeSpace/gco:CharacterString")
				.setBeanProperty().withName("nameSpace");
		forPattern(
				"*/rbv:rbvExtent/rbv:localisationExtent/rbv:drainageBasinName")
				.setBeanProperty().withName("experimentalSiteName");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/")
				.createObject().ofType(GeographicalBox.class).then()
				.setProperties().then().setNext("addGeographicalExtent");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:westBoundLongitude/gco:Decimal")
				.setBeanProperty().withName("westLongitude");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:eastBoundLongitude/gco:Decimal")
				.setBeanProperty().withName("eastLongitude");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:southBoundLatitude/gco:Decimal")
				.setBeanProperty().withName("southLatitude");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:northBoundLatitude/gco:Decimal")
				.setBeanProperty().withName("northLatitude");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_BoundingPolygon/")
				.createObject().ofType(GeographicalPolygon.class).then()
				.setProperties().then().setNext("addGeographicalExtent");

		forPattern(
				"*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_BoundingPolygon/gmd:polygon/gml:LineString/gml:posList")
				.setBeanProperty().withName("posList");
	}

}
