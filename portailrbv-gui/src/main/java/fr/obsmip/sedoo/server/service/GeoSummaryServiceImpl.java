package fr.obsmip.sedoo.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.GeoSummaryService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.core.dao.GeoSummaryDAO;
import fr.obsmip.sedoo.core.dao.GeonetworkUserDAO;
import fr.obsmip.sedoo.server.service.dtotool.GeoSummaryDTOTools;
import fr.obsmip.sedoo.shared.domain.AbstractGeoSummaryDTO;
import fr.obsmip.sedoo.shared.domain.DatasetGeoSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteGeoSummaryDTO;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.GeoSummary;
import fr.sedoo.rbv.geonetwork.request.GeoSummaryRequest;

public class GeoSummaryServiceImpl extends RemoteServiceServlet implements GeoSummaryService
{

	GeoSummaryDAO dao = null;
	
	public GeoSummaryServiceImpl() 
	{
		dao = RBVApplication.getInstance().getGeoSummaryDAO();
	}

	@Override
	public ArrayList<? extends AbstractGeoSummaryDTO> getGeoSummaries(String observatoryName, ArrayList<String> languages) throws Exception {
		ArrayList<AbstractGeoSummaryDTO> summaries = new ArrayList<AbstractGeoSummaryDTO>();
		
		GeonetworkUser user = GeonetworkUserDAO.getUserFromObservatoryShortLabel(observatoryName);
		
		List<GeoSummary> datasetGeoSummaries = dao.getGeoSummaries(user, GeoSummaryRequest.DATASET_TYPE, languages);
		Iterator<GeoSummary> iterator = datasetGeoSummaries.iterator();
		while (iterator.hasNext()) {
			GeoSummary geoSummary = (GeoSummary) iterator.next();
			DatasetGeoSummaryDTO dto = GeoSummaryDTOTools.toDatasetDTO(geoSummary, languages);
			summaries.add(dto);
		}
		
		List<GeoSummary> experimentalSiteGeoSummaries = dao.getGeoSummaries(user, GeoSummaryRequest.EXPERIMENTAL_SITE_TYPE, languages);
		iterator = experimentalSiteGeoSummaries.iterator();
		while (iterator.hasNext()) {
			GeoSummary geoSummary = (GeoSummary) iterator.next();
			ExperimentalSiteGeoSummaryDTO dto = GeoSummaryDTOTools.toExperimentalSiteDTO(geoSummary, languages);
			summaries.add(dto);
		}
		
		return summaries;
	}

		
}

