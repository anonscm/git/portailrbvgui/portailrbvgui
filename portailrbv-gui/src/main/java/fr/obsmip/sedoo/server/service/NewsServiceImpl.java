package fr.obsmip.sedoo.server.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.NewsService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.core.dao.NewsDAO;
import fr.obsmip.sedoo.core.domain.MetadataUpdateNew;
import fr.obsmip.sedoo.server.service.dtotool.MetadataDTOTools;
import fr.obsmip.sedoo.shared.domain.MetadataUpdateNewDTO;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class NewsServiceImpl extends RemoteServiceServlet implements NewsService
{
	private static Logger logger = LoggerFactory.getLogger(NewsServiceImpl.class);
	
	public NewsServiceImpl()
	{
		super();
		logger.debug("Démarrage NewsServiceImpl");
	}
	
	@Override
	public ArrayList<New> getLatest(String preferredLanguage, List<String> alternateLanguages, ArrayList<String> displayLanguages) throws Exception 
	{
		NewsDAO newsDAO = RBVApplication.getInstance().getNewsDAO();
		ArrayList<? extends New> news = newsDAO.getLatest(preferredLanguage, alternateLanguages);
		ArrayList<New> result = new ArrayList<New>();
		if (news != null)
		{
			Iterator<? extends New> iterator = news.iterator();
			while (iterator.hasNext()) 
			{
				New current = iterator.next();
				if (current instanceof MetadataUpdateNew)
				{
					result.add(new MetadataUpdateNewDTO(MetadataDTOTools.toSummaryDTO(((MetadataUpdateNew) current).getSummary(),displayLanguages)));
				}
				if (current instanceof TitledMessage)
				{
					result.add(current);
				}
			}
		}
		
		return result;
	}

		
}

