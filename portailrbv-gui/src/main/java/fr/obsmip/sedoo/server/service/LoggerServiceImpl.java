package fr.obsmip.sedoo.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.LoggerService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.core.dao.LogDAO;
import fr.obsmip.sedoo.server.service.dtotool.LogDTOTools;
import fr.obsmip.sedoo.shared.domain.LogDTO;
import fr.sedoo.commons.client.util.ServiceException;

public class LoggerServiceImpl extends RemoteServiceServlet implements
LoggerService {

	LogDAO dao = null;

	@Override
	public ArrayList<LogDTO> findPage(int pageNumber, int pageSize)
			throws ServiceException {
		initDao();
		try
		{
			return LogDTOTools.toDTOList(dao.findPage(pageNumber, pageSize));
		}
		catch (Exception e)
		{
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void add(LogDTO log) throws ServiceException {
		initDao();
		try
		{
			dao.add(LogDTOTools.fromDTO(log));
		}
		catch (Exception e)
		{
			throw new ServiceException(e);
		}
	}

	private void initDao() {
		if (dao == null)
		{
			dao = RBVApplication.getInstance().getLogDAO();
		}
	}

	@Override
	public int getHits() throws ServiceException {
		initDao();
		try
		{
			return dao.getHits();
		}
		catch (Exception e)
		{
			throw new ServiceException(e);
		}
	}

}

