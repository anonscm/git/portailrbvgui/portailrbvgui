package fr.obsmip.sedoo.server.dao.rss;

import java.util.ArrayList;

import org.apache.commons.digester3.binder.AbstractRulesModule;

public class RSSModule extends AbstractRulesModule {

	@Override
	public void configure() {

		forPattern("rss").createObject().ofType(ArrayList.class);
		forPattern("*/item/guid").createObject().ofType(UuidWrapper.class).then().setBeanProperty().withName("uuidFromUrl").then().setNext("add");
	}
}
