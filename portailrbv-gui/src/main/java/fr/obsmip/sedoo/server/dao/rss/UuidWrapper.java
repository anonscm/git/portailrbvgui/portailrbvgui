package fr.obsmip.sedoo.server.dao.rss;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UuidWrapper {
	
	private String uuid;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	public void setUuidFromUrl(String url)
	{
		int aux = url.lastIndexOf("=");
		if (aux>0)
		{
			uuid = url.substring(aux+1);
		}
		else
		{
			uuid = url;
		}
	}
	
	public static List<String> toStringList(List<UuidWrapper> wrapper)
	{
		List<String> result = new ArrayList<String>();
		if (wrapper != null)
		{
			Iterator<UuidWrapper> iterator = wrapper.iterator();
			while (iterator.hasNext()) 
			{
				result.add(iterator.next().getUuid());
			}
		}
		return result;
	}

}
