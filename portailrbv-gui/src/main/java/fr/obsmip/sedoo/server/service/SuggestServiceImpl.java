package fr.obsmip.sedoo.server.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.SuggestService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.core.dao.ObservatoryNameDAO;

public class SuggestServiceImpl extends RemoteServiceServlet implements SuggestService
{

	@Override
	public ArrayList<String> getObservatoryNames() 
	{
		ObservatoryNameDAO dao = (ObservatoryNameDAO) RBVApplication.getInstance().getBeanFactory().getBeanByName(ObservatoryNameDAO.OBSERVATORY_NAME_DAO_BEAN_NAME);
		ArrayList<String> result = new ArrayList<String>();
		List<String> suggestions = dao.getObservatoryNameFromEntries();
		if (suggestions != null)
		{
			result.addAll(suggestions);
		}
		
		return result;
	}


		
}

