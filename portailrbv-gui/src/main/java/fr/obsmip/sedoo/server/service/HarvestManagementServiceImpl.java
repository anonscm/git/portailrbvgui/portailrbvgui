package fr.obsmip.sedoo.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.HarvestManagementService;
import fr.obsmip.sedoo.core.domain.Harvester;
import fr.sedoo.commons.client.util.ServiceException;

public class HarvestManagementServiceImpl extends RemoteServiceServlet implements
HarvestManagementService {

	@Override
	public boolean execute() throws ServiceException 
	{
		Harvester updater = new Harvester();
		try {
			updater.execute();
		} catch (Exception e) 
		{
			throw new ServiceException(e.getMessage());
		}
		return true;		
	}

	
	
}

