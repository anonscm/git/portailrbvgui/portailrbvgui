package fr.obsmip.sedoo.server.service.dtotool;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.obsmip.sedoo.core.domain.RBVUser;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;

public class UserDTOTools {
	
	public static RBVUserDTO toDTO(RBVUser user) 
	{
		RBVUserDTO dto = new RBVUserDTO();
		dto.setId(user.getId());
		dto.setLogin(user.getLogin());
		dto.setPassword(user.getPassword());
		dto.setObservatories(user.getObservatories());
		dto.setAdmin(user.isAdmin());
		dto.setName(user.getName());
		dto.setExternalAuthentication(user.isExternalAuthentication());
		return dto;
	}
	
	public static RBVUser fromDTO(RBVUserDTO dto)
	{
		RBVUser user = new RBVUser();
		user.setId(dto.getId());
		user.setLogin(dto.getLogin());
		user.setPassword(dto.getPassword());
		user.setObservatories(dto.getObservatories());
		user.setAdmin(dto.isAdmin());
		user.setName(dto.getName());
		user.setExternalAuthentication(dto.isExternalAuthentication());
		return user;
	}
	
	public static ArrayList<RBVUserDTO> toDTOList(List<RBVUser> users)
	{
		ArrayList<RBVUserDTO> result = new ArrayList<RBVUserDTO>();
		if (users != null)
		{
			Iterator<RBVUser> iterator = users.iterator();
			while (iterator.hasNext()) {
				result.add(toDTO(iterator.next()));
			}
		}
		return result;
	}
}
