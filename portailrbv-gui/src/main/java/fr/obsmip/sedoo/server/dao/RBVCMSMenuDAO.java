package fr.obsmip.sedoo.server.dao;

import java.util.Arrays;

import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.server.cms.dao.CMSMenuDAOJPAImpl;

/**
 * Etends la classe CMSMenuDAOJPAImpl en ajoutant une valeur par défaut
 * 
 * @author andre
 * 
 */
public class RBVCMSMenuDAO extends CMSMenuDAOJPAImpl {

	private String defaultValue;

	@Override
	public String getContent() {
		String aux = super.getContent();
		if (StringUtil.isEmpty(aux)) {
			String tmp = StringUtil.trimToEmpty(getDefaultValue());
			// On remplace les | par des retours chariot
			return ListUtil.toSeparatedString(Arrays.asList(tmp.split("\\|")), "\n");
		} else {
			return aux;
		}
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}
