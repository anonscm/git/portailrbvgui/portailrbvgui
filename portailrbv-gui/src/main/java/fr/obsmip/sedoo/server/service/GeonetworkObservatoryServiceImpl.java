package fr.obsmip.sedoo.server.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.GeonetworkObservatoryService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.core.dao.GeonetworkObservatoryDAO;
import fr.obsmip.sedoo.core.dao.GeonetworkUserDAO;
import fr.obsmip.sedoo.server.service.dtotool.GeonetworkObservatoryDTOTools;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkGroup;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.GroupRequest;
import fr.sedoo.rbv.geonetwork.request.UserRequest;

public class GeonetworkObservatoryServiceImpl extends RemoteServiceServlet implements GeonetworkObservatoryService{

	private GeonetworkObservatoryDAO dao;

	@Override
	public ArrayList<GeonetworkObservatoryDTO> findAll() throws ServiceException {
		initDao();
		return GeonetworkObservatoryDTOTools.toDTOList(dao.findAll());
	}

	@Override
	public void delete(GeonetworkObservatoryDTO observatory) throws ServiceException {
		initDao();
		dao.delete(GeonetworkObservatoryDTOTools.fromDTO(observatory));
	}

	@Override
	public GeonetworkObservatoryDTO create(GeonetworkObservatoryDTO observatory) throws ServiceException {
		initDao();		
		GroupRequest groupRequest = new GroupRequest();
		UserRequest userRequest = new UserRequest();
		String groupName="RBV-"+observatory.getName().toUpperCase();
		
		try {
			groupRequest.add(groupName);
			GeonetworkGroup group = groupRequest.get(groupName);
			//GeonetworkUser user = new GeonetworkUser();
			//String aux = observatory.getName().toLowerCase();
			GeonetworkUser user = GeonetworkUserDAO.getUserFromObservatoryShortLabel(observatory.getName());
			user.setProfile(GeonetworkUser.USER_ADMIN_PROFILE);
			ArrayList<GeonetworkGroup> groups = new ArrayList<GeonetworkGroup>();
			groups.add(group);
			userRequest.add(user, groups);
			
			
		} 
		catch (Exception e) 
		{
			throw new ServiceException(e.getMessage());
		}
		return GeonetworkObservatoryDTOTools.toDTO(dao.save(GeonetworkObservatoryDTOTools.fromDTO(observatory)));
	}
	
	private void initDao()
	{
		if (dao == null)
		{
			dao = RBVApplication.getInstance().getGeonetworkObservatoryDAO();
		}
	}
	
	@Override
	public GeonetworkObservatoryDTO edit(GeonetworkObservatoryDTO observatory) throws ServiceException {
		initDao();
		return GeonetworkObservatoryDTOTools.toDTO(dao.save(GeonetworkObservatoryDTOTools.fromDTO(observatory)));
	}

}
