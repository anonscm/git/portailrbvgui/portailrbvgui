package fr.obsmip.sedoo.server.service.dtotool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.client.util.SeparatorUtil;

public class StringTools  {

	private static final String DEFAULT_SEPARATOR =  SeparatorUtil.AROBAS_SEPARATOR;

	/**
	 * To prevent instanciation
	 */
	private StringTools()
	{

	}

	public static ArrayList<String> fromString(String input)
	{
		return fromString(input, DEFAULT_SEPARATOR);
	}

	public static ArrayList<String> fromString(String input, String separator) 
	{
		ArrayList<String> result = new ArrayList<String>();
		String[] split = input.split(separator);
		for (int i = 0; i < split.length; i++) 
		{
			if (StringUtils.isEmpty(split[i])== false)
			{
				result.add(split[i]);
			}
		}
		return result;
	}

	public static String fromList(List<String> input)
	{
		return fromList(input, DEFAULT_SEPARATOR);
	}

	public static String fromList(List<String> input, String separator)
	{
		StringBuilder sb = new StringBuilder();
		if (input != null)
		{
			Iterator<String> iterator = input.iterator();
			boolean needSeparator = false;
			while (iterator.hasNext()) 
			{
				String current = iterator.next();
				if (StringUtils.isEmpty(current) == false)
				{
					if (needSeparator)
					{
						sb.append(separator);
						needSeparator = false;
					}
					sb.append(current);
					needSeparator = true;
				}
			}
		}
		return sb.toString();
	}

}
