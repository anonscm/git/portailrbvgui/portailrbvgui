package fr.obsmip.sedoo.server.service.dtotool;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.metadata.client.service.ThesaurusService;
import fr.sedoo.metadata.server.service.ThesaurusServiceImpl;
import fr.sedoo.rbv.geonetwork.request.SearchCriteria;

public class SearchCriteriaDTOTools {

	private static ThesaurusService thesaurusService = new ThesaurusServiceImpl();

	public static SearchCriteria fromDTO(SearchCriteriaDTO dto) {
		SearchCriteria searchCriteria = new SearchCriteria();
		if (dto != null) {
			searchCriteria.setBoundingBox(GeographicBoundingBoxDTOTools
					.fromDto(dto.getBoundingBoxDTO()));
			searchCriteria.setKeywords(dto.getKeywords());
			searchCriteria.setKeywordsLogicalLink(dto.getKeywordsLogicalLink());
			searchCriteria.setEndDate(dto.getEndDate());
			searchCriteria.setStartDate(dto.getStartDate());
			searchCriteria.setObservatories(dto.getObservatories());
			searchCriteria.setIncludesDatasets(dto.isIncludesDatasets());
			searchCriteria.setIncludesExperimentalSites(dto
					.isIncludesExperimentalSites());
			searchCriteria.setIncludesObservatories(dto
					.isIncludesObservatories());
			searchCriteria.setThesaurusKeywords(dto.getThesaurusKeywords());

		}
		return searchCriteria;
	}
}
