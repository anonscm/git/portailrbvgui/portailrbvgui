package fr.obsmip.sedoo.server.service.dtotool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.metadata.utils.domain.Contact;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;

public class MetadataContactDTOTools 
{
	private MetadataContactDTOTools()
	{
		
	}

	public static List<Contact> toContactList(List<MetadataContactDTO> dto)
	{
		List<Contact> result = new ArrayList<Contact>();
		
		if (dto != null)
		{
			Iterator<MetadataContactDTO> iterator = dto.iterator();
			while (iterator.hasNext()) {
				MetadataContactDTO current = iterator.next();
				result.add(fromMetadataContactDTO(current));
			}
		}
		
		return result;
	}

	private static Contact fromMetadataContactDTO(MetadataContactDTO dto) 
	{
		Contact contact = new Contact();
		if (dto != null)
		{
			contact.setEmailAddress(dto.getEmail());
			contact.setIndividualName(dto.getPersonName());
			contact.setOrganisationName(dto.getOrganisationName());
			contact.setRole(dto.getRoles());
			contact.setAddress(dto.getAddress());
			contact.setCity(dto.getCity());
			contact.setCountry(dto.getCountry());
			contact.setZipCode(dto.getZipCode());
		}
		return contact;
	}
	public static List<MetadataContactDTO> fromContactList(List<Contact> resourceContacts) 
	{
		List<MetadataContactDTO> result = new ArrayList<MetadataContactDTO>();
		if (resourceContacts != null)
		{
			Iterator<Contact> iterator = resourceContacts.iterator();
			long id=1L;
			while (iterator.hasNext()) {
				Contact current = iterator.next();
				MetadataContactDTO dto = fromContact(current);
				dto.setUuid(""+id);
				id++;
				result.add(dto);
			}
		}
		return result;
	}

	private static MetadataContactDTO fromContact(Contact contact) 
	{
		MetadataContactDTO dto = new MetadataContactDTO();
		if (contact != null)
		{
			dto.setEmail(contact.getEmailAddress());
			dto.setPersonName(contact.getIndividualName());
			dto.setOrganisationName(contact.getOrganisationName());
			dto.setRoles(contact.getRole());
			dto.setAddress(contact.getAddress());
			dto.setZipCode(contact.getZipCode());
			dto.setCountry(contact.getCountry());
			dto.setCity(contact.getCity());
		}
		return dto;
	}
	
}
