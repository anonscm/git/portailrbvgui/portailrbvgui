package fr.obsmip.sedoo.server.service;

import java.util.HashMap;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.SystemService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.shared.domain.ParameterConstant;
import fr.sedoo.commons.web.dao.ParameterDAO;
import fr.sedoo.commons.web.dao.VersionDAO;

public class SystemServiceImpl extends RemoteServiceServlet implements
SystemService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8099324566476135745L;

	@Override
	public String getApplicationVersion() 
	{
		VersionDAO dao = (VersionDAO) RBVApplication.getInstance().getBeanFactory().getBeanByName(VersionDAO.VERSION_DAO_BEAN_NAME);
		return dao.getVersion();
	}

	@Override
	public String getJavaVersion() {
		return System.getProperty("java.version");
	}

	@Override
	public String getParameter(String parameterName) {
		ParameterDAO dao = (ParameterDAO) RBVApplication.getInstance().getBeanFactory().getBeanByName(ParameterDAO.PARAMETER_DAO_BEAN_NAME);
		return dao.getParameter(parameterName);
	}

	@Override
	public HashMap<String, String> getParameters() {
		ParameterDAO dao = (ParameterDAO) RBVApplication.getInstance().getBeanFactory().getBeanByName(ParameterDAO.PARAMETER_DAO_BEAN_NAME);
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(ParameterConstant.PRINT_SERVICE_URL_PARAMETER_NAME, dao.getParameter(ParameterConstant.PRINT_SERVICE_URL_PARAMETER_NAME));
		result.put(ParameterConstant.XML_SERVICE_URL_PARAMETER_NAME, dao.getParameter(ParameterConstant.XML_SERVICE_URL_PARAMETER_NAME));
		return result;
	}

	@Override
	public String getDatabaseInformations() 
	{
		DriverManagerDataSource dataSource = (DriverManagerDataSource) RBVApplication.getInstance().getBeanFactory().getBeanByName(ParameterConstant.DATA_SOURCE_BEAN_NAME);
		return dataSource.getUrl();
	}
	
}

