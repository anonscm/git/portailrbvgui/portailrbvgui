package fr.obsmip.sedoo.server.service.dtotool;

import java.util.ArrayList;
import java.util.Iterator;

import fr.obsmip.sedoo.core.domain.GeonetworkObservatory;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;

public class GeonetworkObservatoryDTOTools {
	public static GeonetworkObservatoryDTO toDTO(
			GeonetworkObservatory observatory) {
		GeonetworkObservatoryDTO dto = new GeonetworkObservatoryDTO();
		dto.setId(observatory.getId());
		dto.setName(observatory.getName());
		dto.setColor(observatory.getColor());
		return dto;
	}

	public static GeonetworkObservatory fromDTO(GeonetworkObservatoryDTO dto) {
		GeonetworkObservatory observatory = new GeonetworkObservatory();
		observatory.setId(dto.getId());
		observatory.setName(dto.getName());
		observatory.setColor(dto.getColor());
		return observatory;
	}

	public static ArrayList<GeonetworkObservatoryDTO> toDTOList(
			ArrayList<GeonetworkObservatory> observatories) {
		ArrayList<GeonetworkObservatoryDTO> result = new ArrayList<GeonetworkObservatoryDTO>();
		Iterator<GeonetworkObservatory> iterator = observatories.iterator();
		while (iterator.hasNext()) {
			GeonetworkObservatory current = iterator.next();
			result.add(toDTO(current));
		}
		java.util.Collections.sort(result);
		return result;
	}
}
