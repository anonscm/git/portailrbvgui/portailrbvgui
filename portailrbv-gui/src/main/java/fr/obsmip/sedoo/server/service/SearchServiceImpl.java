package fr.obsmip.sedoo.server.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.obsmip.sedoo.client.service.SearchService;
import fr.obsmip.sedoo.core.RBVApplication;
import fr.obsmip.sedoo.core.dao.GeonetworkObservatoryDAO;
import fr.obsmip.sedoo.core.dao.GeonetworkUserDAO;
import fr.obsmip.sedoo.core.domain.GeonetworkObservatory;
import fr.obsmip.sedoo.server.service.dtotool.MetadataDTOTools;
import fr.obsmip.sedoo.server.service.dtotool.SearchCriteriaDTOTools;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.metadata.server.service.thesaurus.ThesauriFactory;
import fr.sedoo.rbv.geonetwork.request.SearchRequest;
import fr.sedoo.rbv.thesauri.RBVThesauriFactory;

public class SearchServiceImpl extends RemoteServiceServlet implements
		SearchService {

	private GeonetworkObservatoryDAO dao;

	protected void initDao() {
		if (dao == null) {
			dao = RBVApplication.getInstance().getGeonetworkObservatoryDAO();
		}
	}

	@Override
	public HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> getSummaries(
			SearchCriteriaDTO criteria, int position, int size,
			ArrayList<String> displayLanguages) throws Exception {
		initDao();
		addKeywordTranslations(criteria);
		HashMap<String, GeonetworkObservatoryDTO> obervatoryColors = new HashMap<>();
		ArrayList<GeonetworkObservatory> findAll = dao.findAll();
		for (GeonetworkObservatory geonetworkObservatory : findAll) {
			GeonetworkObservatoryDTO aux = new GeonetworkObservatoryDTO();
			aux.setName(geonetworkObservatory.getName());
			aux.setColor(geonetworkObservatory.getColor());
			obervatoryColors.put(geonetworkObservatory.getName(), aux);
		}

		HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> result = new HashMap<>();

		SearchCriteriaDTO aux = criteria;
		List<String> observatories = aux.getObservatories();

		SearchRequest request;
		for (String observatory : observatories) {
			request = new SearchRequest(
					GeonetworkUserDAO
							.getUserFromObservatoryShortLabel(observatory));

			request.setCriteria(SearchCriteriaDTOTools.fromDTO(aux));
			request.setPagePosition(position);
			request.setPageSize(size);
			request.setDisplayLanguages(displayLanguages);
			boolean isCorrectlyExecuted = request.fetchSummaries();
			if (isCorrectlyExecuted == false) {
				throw new Exception("Uncorrect execution");
			} else {
				ArrayList<SummaryDTO> observatoryResult = new ArrayList<SummaryDTO>();
				List<Summary> summaries = request.getSummaries();
				if (summaries != null) {
					Iterator<Summary> iterator = summaries.iterator();
					while (iterator.hasNext()) {
						Summary summary = iterator.next();
						observatoryResult.add(MetadataDTOTools.toSummaryDTO(
								summary, displayLanguages));
					}
				}
				result.put(obervatoryColors.get(observatory), observatoryResult);
			}
		}
		return result;
	}

	private void addKeywordTranslations(SearchCriteriaDTO criteria) {
		RBVThesauriFactory thesauriFactory = (RBVThesauriFactory) RBVApplication
				.getInstance().getBeanFactory()
				.getBeanByName(ThesauriFactory.BEAN_NAME);

		HashMap<String, ArrayList<String>> augmentedKeywords = new HashMap<>();

		HashMap<String, ArrayList<String>> oldThesaurusKeywords = criteria
				.getThesaurusKeywords();

		Set<String> thesaurusNames = oldThesaurusKeywords.keySet();

		for (String thesaurusName : thesaurusNames) {
			Set<String> aux = new HashSet<>();
			ArrayList<String> values = oldThesaurusKeywords.get(thesaurusName);
			for (String value : values) {
				aux.addAll(thesauriFactory.getTranslationsForLabel(value));
			}
			augmentedKeywords.put(thesaurusName, new ArrayList<>(aux));
		}

		criteria.setThesaurusKeywords(augmentedKeywords);
	}

	@Override
	public int getHits(SearchCriteriaDTO criteria) throws Exception {

		int result = 0;
		addKeywordTranslations(criteria);
		SearchCriteriaDTO aux = criteria;
		if (aux == null) {
			aux = new SearchCriteriaDTO();
		}

		List<String> observatories = aux.getObservatories();
		SearchRequest request;
		for (String observatory : observatories) {
			request = new SearchRequest(
					GeonetworkUserDAO
							.getUserFromObservatoryShortLabel(observatory));
			request.setCriteria(SearchCriteriaDTOTools.fromDTO(criteria));
			boolean isCorrectlyExecuted = request.calculateHits();
			if (isCorrectlyExecuted == false) {
				throw new Exception("Uncorrect execution");
			} else {
				result = result + request.getHits();
			}
		}
		return result;
	}

}
