package fr.obsmip.sedoo.server.service.dtotool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.obsmip.sedoo.core.domain.Log;
import fr.obsmip.sedoo.shared.domain.LogDTO;

public class LogDTOTools {
	
	public static LogDTO toDTO(Log log) 
	{
		LogDTO dto = new LogDTO();
		dto.setId(log.getId());
		dto.setCategory(log.getCategory());
		dto.setAction(log.getAction());
		dto.setDate(log.getDate());
		dto.setDetail(log.getDetail());
		dto.setUser(log.getUser());
		return dto;
	}
	
	public static Log fromDTO(LogDTO dto)
	{
		Log log = new Log();
		log.setId(dto.getId());
		log.setDate(dto.getDate());
		log.setDetail(dto.getDetail());
		log.setAction(dto.getAction());
		log.setCategory(dto.getCategory());
		log.setUser(dto.getUser());
		return log;
	}
	
	public static ArrayList<LogDTO> toDTOList(List<Log> logs)
	{
		ArrayList<LogDTO> result = new ArrayList<LogDTO>();
		if (logs != null)
		{
			Iterator<Log> iterator = logs.iterator();
			while (iterator.hasNext()) {
				result.add(toDTO(iterator.next()));
			}
		}
		return result;
	}
}
