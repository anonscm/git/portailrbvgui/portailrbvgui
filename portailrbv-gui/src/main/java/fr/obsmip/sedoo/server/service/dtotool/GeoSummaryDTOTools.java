package fr.obsmip.sedoo.server.service.dtotool;


import java.util.ArrayList;

import fr.obsmip.sedoo.shared.domain.AbstractGeoSummaryDTO;
import fr.obsmip.sedoo.shared.domain.DatasetGeoSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteGeoSummaryDTO;
import fr.sedoo.rbv.geonetwork.request.GeoSummary;

public class GeoSummaryDTOTools {
	
	public static AbstractGeoSummaryDTO toDTO(GeoSummary summary, AbstractGeoSummaryDTO dto, ArrayList<String> languages) 
	{
		if (summary.getMetadataLanguage() != null)
		{
			summary.addTitleTranslation(summary.getMetadataLanguage(),summary.getResourceTitle());
			summary.addAbstractTranslation(summary.getMetadataLanguage(),summary.getResourceAbstract());
		}
		summary.setResourceTitle(MetadataDTOTools.getValue(summary.getTitleTranslations(), summary.getResourceTitle(), languages));
		summary.setResourceAbstract(MetadataDTOTools.getValue(summary.getAbstractTrasnlations(), summary.getResourceAbstract(), languages));
		dto.setResourceTitle(summary.getResourceTitle());
		dto.setResourceAbstract(summary.getResourceAbstract());
		dto.setUuid(summary.getUuid());
		dto.setBox(GeographicBoundingBoxDTOTools.toDTO(summary.getBox()));
		
		return dto;
	}

	public static DatasetGeoSummaryDTO toDatasetDTO(GeoSummary geoSummary, ArrayList<String> languages) {
		return (DatasetGeoSummaryDTO) toDTO(geoSummary, new DatasetGeoSummaryDTO(), languages);
	}

	public static ExperimentalSiteGeoSummaryDTO toExperimentalSiteDTO(
			GeoSummary geoSummary, ArrayList<String> languages) {
		return (ExperimentalSiteGeoSummaryDTO) toDTO(geoSummary, new ExperimentalSiteGeoSummaryDTO(), languages);
	}
	
	
	
}

