package fr.obsmip.sedoo.server.service.dtotool;

import fr.obsmip.sedoo.core.domain.ObservatoryPerson;
import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;



public class ObservatoryPersonDTOTools {
	
	public static ObservatoryPersonDTO toDTO(ObservatoryPerson observatoryPerson)
	{
		ObservatoryPersonDTO result = new ObservatoryPersonDTO();
		result.setId(observatoryPerson.getId());
		result.setEmail(observatoryPerson.getEmail());
		result.setOrganisationName(observatoryPerson.getOrganisationName());
		result.setPersonName(observatoryPerson.getPersonName());
		result.setZipCode(observatoryPerson.getZipCode());
		result.setCountry(observatoryPerson.getCountry());
		result.setCity(observatoryPerson.getCity());
		result.setAddress(observatoryPerson.getAddress());
		result.setRoles(observatoryPerson.getRoles());
		result.setObservatoryId(observatoryPerson.getObservatory().getId());
		return result;
	}
	
	public static ObservatoryPerson fromDTO(ObservatoryPersonDTO dto)
	{
		ObservatoryPerson result = new ObservatoryPerson();
		result.setId(dto.getId());
		result.setEmail(dto.getEmail());
		result.setOrganisationName(dto.getOrganisationName());
		result.setPersonName(dto.getPersonName());
		result.setZipCode(dto.getZipCode());
		result.setCountry(dto.getCountry());
		result.setCity(dto.getCity());
		result.setAddress(dto.getAddress());
		result.setRoles(dto.getRoles());
		return result;
	}
	
//	public static PersonDTO toPersonDTO(Person person)
//	{
//		return toPersonDTO(person, null);
//	}
//	
//	public static PersonDTO toPersonDTO(Person person, PersonDTO target) 
//	{
//		PersonDTO dto = null;
//		if (target == null)
//		{
//			dto = new PersonDTO();
//		}
//		else
//		{
//			dto = target;
//		}
//		dto.setId(person.getId());
//		dto.setEmail(person.getEmail());
//		dto.setOrganisationName(person.getOrganisationName());
//		dto.setPersonName(person.getPersonName());
//		dto.setZipCode(person.getZipCode());
//		dto.setCountry(person.getCountry());
//		dto.setCity(person.getCity());
//		dto.setAddress(person.getAddress());
//		dto.setRoles(person.getRoles());
//		return dto;
//	}
//
//	public static Person fromDTO(PersonDTO dto,
//			Person target) {
//		
//		Person result = null;
//		if (target == null)
//		{
//			result = new Person();
//		}
//		else
//		{
//			result = target;
//		}
//		result.setEmail(dto.getEmail());
//		result.setOrganisationName(dto.getOrganisationName());
//		result.setPersonName(dto.getPersonName());
//		result.setAddress(dto.getAddress());
//		result.setCity(dto.getCity());
//		result.setCountry(dto.getCountry());
//		result.setZipCode(dto.getZipCode());
//		result.setRoles(dto.getRoles());
//		return result;
//	}

}
