package fr.obsmip.sedoo.shared.domain;

import fr.sedoo.commons.metadata.utils.domain.Summary;

public interface SummaryNameProvider {
	
	String getName(Summary summary);

}
