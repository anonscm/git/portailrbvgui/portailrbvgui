package fr.obsmip.sedoo.shared.domain;

import fr.sedoo.commons.metadata.utils.domain.Summary;

public class ExperimentalSiteSummaryNameProvider implements SummaryNameProvider{

	@Override
	public String getName(Summary summary) 
	{
		ExperimentalSiteDTO tmp = new ExperimentalSiteDTO();
		return tmp.getNameFromIdentifiers(summary.getIdentifiers());
	}

}
