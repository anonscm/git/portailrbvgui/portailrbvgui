package fr.obsmip.sedoo.shared.domain;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.shared.domain.geographicalextent.GeographicalExtentListDTO;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class SummaryDTO extends AbstractDTO implements HasIdentifier {

	private static final int MAX_ABSTRACT_LENGTH = 1000;
	private String resourceAbstract;
	private String resourceTitle;
	private String uuid;
	private String modificationDate;
	private Long id;
	private String state;
	private List<IdentifiedDescribedString> links;
	private boolean editable = false;
	private String hierarchyLevelName = "";
	private String shortName = "";
	private GeographicalExtentListDTO geographicalExtentListDTO;
	private String observatoryName;
	private String observatoryColor;

	public SummaryDTO() {
		setResourceAbstract("");
		setResourceTitle("");
		setUuid("");
		setModificationDate("");
		setLinks(new ArrayList<IdentifiedDescribedString>());
		setEditable(false);
		geographicalExtentListDTO = new GeographicalExtentListDTO();

	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public String getHash() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<IdentifiedDescribedString> getLinks() {
		return links;
	}

	public void setLinks(List<IdentifiedDescribedString> links) {
		this.links = links;
	}

	// public String getResourceTitleDisplay()
	// {
	// String displayValue =
	// LocaleTools.getDisplayValue(getResourceTitleEnglish(),
	// getResourceTitleFrench());
	// if (StringUtil.isEmpty(displayValue))
	// {
	// displayValue = StringUtil.trimToEmpty(getResourceTitle());
	// }
	//
	// return displayValue;
	// }
	//
	// public String getResourceAbstractDisplay()
	// {
	// String displayValue =
	// LocaleTools.getDisplayValue(getResourceAbstractEnglish(),
	// getResourceAbstractFrench());
	// if (StringUtil.isEmpty(displayValue))
	// {
	// displayValue = StringUtil.trimToEmpty(getResourceAbstract());
	// }
	// if (displayValue.length()> MAX_ABSTRACT_LENGTH)
	// {
	// return displayValue.substring(0, MAX_ABSTRACT_LENGTH)+"...";
	// }
	// else
	// {
	// return displayValue;
	// }
	// }

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	@Override
	public String getIdentifier() {
		return "" + id;
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public String getHierarchyLevelName() {
		return hierarchyLevelName;
	}

	public void setHierarchyLevelName(String hierarchyLevelName) {
		this.hierarchyLevelName = hierarchyLevelName;
	}

	public String getResourceAbstract() {
		return resourceAbstract;
	}

	public void setResourceAbstract(String resourceAbstract) {
		String aux = StringUtil.trimToEmpty(resourceAbstract);
		if (aux.length() > MAX_ABSTRACT_LENGTH) {
			this.resourceAbstract = aux.substring(0, MAX_ABSTRACT_LENGTH)
					+ "[...]";
		} else {
			this.resourceAbstract = aux;
		}
	}

	public String getResourceTitle() {
		return resourceTitle;
	}

	public void setResourceTitle(String resourceTitle) {
		this.resourceTitle = resourceTitle;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public GeographicalExtentListDTO getGeographicalExtentListDTO() {
		return geographicalExtentListDTO;
	}

	public void setGeographicalExtentListDTO(
			GeographicalExtentListDTO geographicalExtentListDTO) {
		this.geographicalExtentListDTO = geographicalExtentListDTO;
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}

	public String getObservatoryColor() {
		return observatoryColor;
	}

	public void setObservatoryColor(String observatoryColor) {
		this.observatoryColor = observatoryColor;
	}

}
