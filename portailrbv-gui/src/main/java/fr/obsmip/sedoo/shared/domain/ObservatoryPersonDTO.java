package fr.obsmip.sedoo.shared.domain;

import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public class ObservatoryPersonDTO extends PersonDTO{
	
	@Override
	public List<ValidationAlert> validate() 
	{
		List<ValidationAlert> alerts =super.validate();
		return MetadataContactDTO.addMetadataContactvalidation(this, alerts);
	}
	
	public Long getObservatoryId() {
		return observatoryId;
	}

	public void setObservatoryId(Long observatoryId) {
		this.observatoryId = observatoryId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Long observatoryId;
	
	private Long id;
	
	@Override
	public String getIdentifier() {
		return ""+id;
	}

}
