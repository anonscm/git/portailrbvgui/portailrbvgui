package fr.obsmip.sedoo.shared.util;

import fr.sedoo.commons.client.util.StringUtil;

public class HierachyLevelUtil 
{
	public static String OBSERVATORY_HIERARCHY_LEVEL_NAME="observatoryLevel";
	public static String EXPERIMENTAL_SITE_HIERARCHY_LEVEL_NAME="experimentalSiteLevel";
	public static String DATASET_HIERARCHY_LEVEL_NAME="datasetLevel";

	public static boolean isObservatory(String hierarchyLevelName) 
	{
		return (StringUtil.trimToEmpty(hierarchyLevelName).compareToIgnoreCase(OBSERVATORY_HIERARCHY_LEVEL_NAME)==0);
	}
	
	public static boolean isExperimentalSite(String hierarchyLevelName) 
	{
		return (StringUtil.trimToEmpty(hierarchyLevelName).compareToIgnoreCase(EXPERIMENTAL_SITE_HIERARCHY_LEVEL_NAME)==0);
	}
	
}
