package fr.obsmip.sedoo.shared.domain;

import fr.sedoo.commons.metadata.utils.domain.Summary;

public class ObservatorySummaryNameProvider implements SummaryNameProvider{

	@Override
	public String getName(Summary summary) 
	{
		ObservatoryDTO tmp = new ObservatoryDTO();
		return tmp.getNameFromIdentifiers(summary.getIdentifiers());
	}

}
