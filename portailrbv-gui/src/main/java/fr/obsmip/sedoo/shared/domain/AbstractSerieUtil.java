package fr.obsmip.sedoo.shared.domain;

import java.util.List;
import java.util.ListIterator;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.metadata.shared.ResourceIdentifier;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public class AbstractSerieUtil {
	
	public static List<IdentifiedResourceIdentifier> setNameToList(String name, String identifierNamespace, List<IdentifiedResourceIdentifier> list)
	{
		ListIterator<IdentifiedResourceIdentifier> iterator = list.listIterator();
		while (iterator.hasNext()) {
			IdentifiedResourceIdentifier current = iterator.next();
			if (isIdentifier(current, identifierNamespace)) {
				if (StringUtil.isEmpty(name)) {
					// No value indicated, we delete the line
					iterator.remove();
				} else {
					// We update the line
					current.setCode(StringUtil.trimToEmpty(name));
				}
				return list;
			}
		}
		// No existing identifier line
		if (StringUtil.isNotEmpty(name)) {
			IdentifiedResourceIdentifier aux = new IdentifiedResourceIdentifier(StringUtil.trimToEmpty(name), identifierNamespace);
			list.add(aux);
		}
		return list;
		
	}

	public static boolean isIdentifier(ResourceIdentifier identifier, String namespace) {
		return (StringUtil.trimToEmpty(identifier.getNameSpace()).compareToIgnoreCase(namespace) == 0);
	}

}
