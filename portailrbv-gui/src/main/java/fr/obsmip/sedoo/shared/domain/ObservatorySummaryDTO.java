package fr.obsmip.sedoo.shared.domain;

import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ObservatorySummaryDTO extends MetadataSummaryDTO implements IsObservatory{
	
}
