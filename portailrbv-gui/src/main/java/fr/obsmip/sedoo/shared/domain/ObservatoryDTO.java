package fr.obsmip.sedoo.shared.domain;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;

/**
 * Metadata entry relative to an observatory
 * @author francois
 *
 */
public class ObservatoryDTO extends AbstractSerieMetadata implements IsObservatory
{

	public final static String RBV_NAMESPACE = "http://portailrbvws.sedoo.fr/observatory/";
	
	public String getObservatoryName() {
		
		return getName();
	}
	
	public void setObservatoryName(String experimentalSiteName)
	{
		setName(experimentalSiteName);
	}

	@Override
	public String getIdentifierNamespace() {
		return RBV_NAMESPACE;
	}
	
	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		
//		List<ValidationAlert> identificationValidationAlerts = getIdentificationPart().validate();
//		if (StringUtil.isEmpty(getObservatoryName()))
//		{
//			identificationValidationAlerts.add(BEGIN_INDEX, new ValidationAlert(MetadataMessage.INSTANCE.metadataEditingObservatoryName(), MetadataMessage.INSTANCE.mandatoryData()));
//		}
//		
//		result.addAll(ValidationAlert.prefixField(getIdentificationPart().getTabName() + " - ", identificationValidationAlerts));
//		result.addAll(ValidationAlert.prefixField(getKeywordPart().getTabName() + " - ", getKeywordPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getGeographicalLocationPart().getTabName() + " - ", getGeographicalLocationPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getTemporalExtentPart().getTabName() + " - ", getTemporalExtentPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getConstraintPart().getTabName() + " - ", getConstraintPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getMetadataPart().getTabName() + " - ", getMetadataPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getOtherPart().getTabName() + " - ", getOtherPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getContactPart().getTabName() + " - ", getContactPart().validate()));
//		result.addAll(ValidationAlert.prefixField(getMeasurementPart().getTabName() + " - ", getMeasurementPart().validate()));
		return result;
	}

}
