package fr.obsmip.sedoo.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.message.Message;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.util.ValidationUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class RBVUserDTO implements HasIdentifier, Serializable, AuthenticatedUser {

	private String login;
	private boolean externalAuthentication;
	private String password;
	private Long id;
	private String observatories;
	private String name;
	private boolean admin = false;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public List<ValidationAlert> validate() 
	{
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		if (StringUtil.isEmpty(getLogin()))
		{
			result.add(new ValidationAlert(CommonMessages.INSTANCE.login(), Message.INSTANCE.mandatoryData()));
		}
		else
		{
			if (ValidationUtil.isValidEmail(StringUtil.trimToEmpty(getLogin()))==false)
			{
				result.add(new ValidationAlert(CommonMessages.INSTANCE.login(), Message.INSTANCE.emailData()));
			}
		}
		if (isExternalAuthentication() == false)
		{
			if (StringUtil.isEmpty(getName()))
			{
				result.add(new ValidationAlert(CommonMessages.INSTANCE.name(), Message.INSTANCE.mandatoryData()));
			}
			if (StringUtil.isEmpty(getPassword()))
			{
				result.add(new ValidationAlert(CommonMessages.INSTANCE.password(), Message.INSTANCE.mandatoryData()));
			}
		}
		return result;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String getIdentifier() {
		if (getId() == null)
		{
			return null;
		}
		else
		{
			return ""+getId();
		}
	}
	public String getObservatories() {
		return observatories;
	}
	public void setObservatories(String observatories) {
		this.observatories = observatories;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isAutorizedFor(String observatoryName) 
	{
		return (observatories.toLowerCase().indexOf(observatoryName.toLowerCase())>=0);
	}
	public boolean isExternalAuthentication() {
		return externalAuthentication;
	}
	public void setExternalAuthentication(boolean externalAuthentication) {
		this.externalAuthentication = externalAuthentication;
	}
	@Override
	public ArrayList<String> getRoles() {
		return new ArrayList();
	}
}
