package fr.obsmip.sedoo.shared.domain.geographicalextent;

import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.commons.util.StringUtil;

public class GeographicalBoxDTO implements GeographicalExtentDTO {

	private String northLatitude;
	private String southLatitude;
	private String eastLongitude;
	private String westLongitude;

	public GeographicalBoxDTO() {

	}

	public GeographicalBoxDTO(String northLatitude, String eastLongitude,
			String southLatitude, String westLongitude) {
		setNorthLatitude(northLatitude);
		setSouthLatitude(southLatitude);
		setEastLongitude(eastLongitude);
		setWestLongitude(westLongitude);
	}

	public String getNorthLatitude() {
		return northLatitude;
	}

	public void setNorthLatitude(String northLatitude) {
		this.northLatitude = northLatitude;
	}

	public String getSouthLatitude() {
		return southLatitude;
	}

	public void setSouthLatitude(String southLatitude) {
		this.southLatitude = southLatitude;
	}

	public String getEastLongitude() {
		return eastLongitude;
	}

	public void setEastLongitude(String eastLongitude) {
		this.eastLongitude = eastLongitude;
	}

	public String getWestLongitude() {
		return westLongitude;
	}

	public void setWestLongitude(String westLongitude) {
		this.westLongitude = westLongitude;
	}

	public GeographicBoundingBoxDTO toBoundingBoxDTO() {
		GeographicBoundingBoxDTO result = new GeographicBoundingBoxDTO();
		result.setEastBoundLongitude(getEastLongitude());
		result.setWestBoundLongitude(getWestLongitude());
		result.setNorthBoundLatitude(getNorthLatitude());
		result.setSouthBoundLatitude(getSouthLatitude());

		return result;
	}

	public boolean isPoint() {
		if (!StringUtil.trimToEmpty(getEastLongitude()).equalsIgnoreCase(
				getWestLongitude())) {
			return false;
		}
		if (!StringUtil.trimToEmpty(getSouthLatitude()).equalsIgnoreCase(
				getNorthLatitude())) {
			return false;
		}
		if (StringUtil.isEmpty(eastLongitude)) {
			return false;
		}
		if (StringUtil.isEmpty(northLatitude)) {
			return false;
		}
		return true;
	}
}
