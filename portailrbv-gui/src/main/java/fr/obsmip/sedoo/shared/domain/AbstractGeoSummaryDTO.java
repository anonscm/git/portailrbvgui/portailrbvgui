package fr.obsmip.sedoo.shared.domain;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

public class AbstractGeoSummaryDTO extends AbstractDTO
{

	protected GeographicBoundingBoxDTO box;
	protected String resourceAbstract;
	protected String resourceTitle;
	protected String uuid;
	private String observatoryName;

	public AbstractGeoSummaryDTO()
	{
	}

	public String getResourceTitle()
	{
		return resourceTitle;
	}

	public void setResourceTitle(String resourceTitle)
	{
		this.resourceTitle = resourceTitle;
	}

	public String getResourceAbstract()
	{
		return resourceAbstract;
	}
	
	public void setResourceAbstract(String resourceAbstract) {
		this.resourceAbstract = resourceAbstract;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public boolean isValid() {
		if (StringUtil.isEmpty(getUuid()))
		{
			return false;
		}
		if (StringUtil.isEmpty(getResourceAbstract()))
		{
			return false;
		}
		if (StringUtil.isEmpty(getResourceTitle()))
		{
			return false;
		}
		if (box.isEmpty())
		{
			return false;
		}
		if (box.validate().isEmpty() == false)
		{
			return false;
		}
		return true;
	}

	public String getHash() {
		return "";
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}
	
	public GeographicBoundingBoxDTO getBox() {
		return box;
	}

	public void setBox(GeographicBoundingBoxDTO box) {
		this.box = box;
	}

	
}
