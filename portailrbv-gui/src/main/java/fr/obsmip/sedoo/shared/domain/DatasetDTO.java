package fr.obsmip.sedoo.shared.domain;

import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;


/**
 * Metadata entry relative to an experimental site
 * @author francois
 *
 */
public class DatasetDTO extends MetadataDTO implements HasIdentifier
{
	private MetadataSummaryDTO observatorySummary = new MetadataSummaryDTO();
	private MetadataSummaryDTO experimentalSiteSummary = new MetadataSummaryDTO();
	
	@Override
	public String getIdentifier() {
		return getOtherPart().getUuid();
	}

	public MetadataSummaryDTO getObservatorySummary() {
		return observatorySummary;
	}

	public void setObservatorySummary(MetadataSummaryDTO observatorySummary) {
		this.observatorySummary = observatorySummary;
	}

	public MetadataSummaryDTO getExperimentalSiteSummary() {
		return experimentalSiteSummary;
	}

	public void setExperimentalSiteSummary(MetadataSummaryDTO experimentalSiteSummary) {
		this.experimentalSiteSummary = experimentalSiteSummary;
	}
	
}
