package fr.obsmip.sedoo.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class LogDTO implements HasIdentifier, Serializable{

	
	private Long id;
	private String user;
	private String category;
	private String action;
	private String detail;
	private Date date;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String getIdentifier() {
		if (getId() == null)
		{
			return null;
		}
		else
		{
			return ""+getId();
		}
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

}
