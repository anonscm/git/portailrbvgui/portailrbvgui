package fr.obsmip.sedoo.shared.domain.geographicalextent;


public class GeographicalPolygonDTO implements GeographicalExtentDTO {

	private String posList;

	public GeographicalPolygonDTO() {
	}

	public GeographicalPolygonDTO(String posList) {
		setPosList(posList);
	}

	public String getPosList() {
		return posList;
	}

	public void setPosList(String posList) {
		this.posList = posList;
	}

}
