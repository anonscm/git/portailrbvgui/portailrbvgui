package fr.obsmip.sedoo.shared.util;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.MenuItem;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.AdministrationMenu;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.util.StringUtil;

public class MenuUtil {

	public static final String RBV_ADMIN = "RBV_ADMIN";
	
	public static final String RBV_NEWS = "RBV_NEWS";
	
	static final String RBV_PREFIX = "RBV_";
	static final String SEPARATOR = "----";
	static final String CMS_PREFIX = "CMS";
	// static final String MENU_IMAGE = "menuImage";
	static final String KVP_SEPARATOR = "=";
	static final String SCREEN_NAME = "SCREEN_NAME";
	static final String SUB_MENU_PREFIX = "MENU";

	static final Map<String, MenuItem> RBVMenuItems = new HashMap<String, MenuItem>();

	// private static SafeHtml createSafeHtml(ImageResource resource,
	// String tootltip) {
	// Image aux = AbstractImagePrototype.create(resource).createImage();
	// aux.setTitle(tootltip);
	// return SafeHtmlUtils.fromTrustedString(aux.toString());
	// }

	public static String getSubMenuLabel(String line) {
		String aux = StringUtil.trimToEmpty(line);
		int parenthesisStart = aux.indexOf("(");
		int parenthesisEnd = aux.lastIndexOf(")");
		aux = aux.substring(parenthesisStart + 1, parenthesisEnd);
		String[] kvps = aux.split(SeparatorUtil.COMMA_SEPARATOR);
		String currentLanguage = LocaleUtil.getCurrentLanguage(PortailRBV
				.getClientFactory());
		for (int i = 0; i < kvps.length; i++) {
			String kvp = kvps[i];
			String[] split = kvp.split(KVP_SEPARATOR);
			String key = split[0].trim();
			String value = split[1];
			if (key.compareToIgnoreCase(currentLanguage) == 0) {
				return value;
			}
		}
		return null;
	}

	public static boolean isSubMenuLine(String line) {
		return line.trim().toUpperCase().startsWith(SUB_MENU_PREFIX);
	}

	public static boolean isSeparatorLine(String line) {
		return line.trim().toUpperCase().startsWith(SEPARATOR);
	}

	public static int getLineLevel(String line) {
		String ltrim = line.replaceAll("^\\s+", "");
		return line.length() - ltrim.length();
	}

	public static MenuItem createMenuItemFromString(String line,
			HashMap<String, String> screenTitles) {
		String aux = StringUtil.trimToEmpty(line);
		EventBus eventBus = PortailRBV.getClientFactory().getEventBus();
		if (StringUtil.isEmpty(aux)) {
			return null;
		} else if (aux.toUpperCase().equals(RBV_ADMIN)) {
			AdministrationMenu administrationMenu = new AdministrationMenu(
					PortailRBV.getClientFactory());
			MenuItem administrationMenuItem = new MenuItem(
					CommonMessages.INSTANCE.administration(),
					administrationMenu);
			return administrationMenuItem;
		}else if(aux.toUpperCase().equals(RBV_NEWS)){
			return new MenuItem(Message.INSTANCE.news(), new PlaceCommand(
					eventBus, new NewsArchivePlace()));
		} else if ((aux.toUpperCase().startsWith(CMS_PREFIX))
				|| ((aux.toUpperCase().startsWith(SUB_MENU_PREFIX)))) {
			int parenthesisStart = aux.indexOf("(");
			int parenthesisEnd = aux.lastIndexOf(")");
			aux = aux.substring(parenthesisStart + 1, parenthesisEnd);
			String[] kvps = aux.split(SeparatorUtil.COMMA_SEPARATOR);
			String label = "";
			String screenName = "";
			String currentLanguage = LocaleUtil.getCurrentLanguage(PortailRBV
					.getClientFactory());
			for (int i = 0; i < kvps.length; i++) {
				String kvp = kvps[i];
				String[] split = kvp.split(KVP_SEPARATOR);
				String key = split[0].trim();
				String value = split[1];
				if (key.compareToIgnoreCase(currentLanguage) == 0) {
					label = value;
				} else if (key.compareToIgnoreCase(SCREEN_NAME) == 0) {
					screenName = value;
				}
			}
			if (StringUtil.isNotEmpty(screenName)) {
				if (StringUtil.isNotEmpty(label)) {
					return new MenuItem(label, new PlaceCommand(eventBus,
							new CMSConsultPlace(screenName)));
				} else {
					String tmp = screenTitles.get(screenName);
					if (StringUtil.isNotEmpty(tmp)) {
						return new MenuItem(tmp, new PlaceCommand(eventBus,
								new CMSConsultPlace(screenName)));
					} else {
						return new MenuItem(screenName, new PlaceCommand(
								eventBus, new CMSConsultPlace(screenName)));
					}
				}
			}
		}

		return null;
	}
}
