package fr.obsmip.sedoo.shared.api;

public interface IsObservatory {

	String getName();
}
