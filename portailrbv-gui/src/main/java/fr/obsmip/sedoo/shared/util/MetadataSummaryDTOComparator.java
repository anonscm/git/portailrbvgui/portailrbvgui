package fr.obsmip.sedoo.shared.util;

import java.util.Comparator;

import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class MetadataSummaryDTOComparator implements Comparator<MetadataSummaryDTO> {

	@Override
	public int compare(MetadataSummaryDTO arg0, MetadataSummaryDTO arg1) {
		if (arg0 == null)
		{
			return -1;
		}
		if (arg1 == null)
		{
			return 1;
		}
		return arg0.getName().compareToIgnoreCase(arg1.getName());
	}
}
