package fr.obsmip.sedoo.shared.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.metadata.shared.ResourceIdentifier;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public abstract class AbstractSerieMetadata extends MetadataDTO implements HasIdentifier 
{
	
	@Override
	public String getIdentifier() {
		return getOtherPart().getUuid();
	}
	
	@Override
	public List<ValidationAlert> validate() {
		List<ValidationAlert> result = new ArrayList<ValidationAlert>();
		return result;
	}
	
	public String getName() {
		
		return getNameFromIdentifiers(getIdentificationPart().getResourceIdentifiers());
		
	}
	
	public String getNameFromIdentifiers(List<? extends ResourceIdentifier> identifiers)
	{
		Iterator<? extends ResourceIdentifier> iterator = identifiers.iterator();
		while (iterator.hasNext()) {
			ResourceIdentifier current = (ResourceIdentifier) iterator.next();
			if (isIdentifier(current)) {
				return current.getCode();
			}
		}
		return "";
	}
	
	public boolean isIdentifier(ResourceIdentifier identifier) {
		return AbstractSerieUtil.isIdentifier(identifier, getIdentifierNamespace());
	}
	
	public abstract String getIdentifierNamespace();
	
	
	public void setName(String name) {
		List<IdentifiedResourceIdentifier> aux = AbstractSerieUtil.setNameToList(name, getIdentifierNamespace(), getIdentificationPart().getResourceIdentifiers());
		getIdentificationPart().setResourceIdentifiers(aux);
	}
}
