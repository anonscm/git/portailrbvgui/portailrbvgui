package fr.obsmip.sedoo.shared.domain;

import java.io.Serializable;


public class ApplicationException extends Exception implements Serializable 
{
	private String message;
	public ApplicationException()
	{
		super();
	}
	
	public ApplicationException(String message)
	{
		super();
		setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
