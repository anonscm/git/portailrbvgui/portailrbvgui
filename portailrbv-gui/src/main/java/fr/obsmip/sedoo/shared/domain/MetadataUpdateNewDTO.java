package fr.obsmip.sedoo.shared.domain;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.shared.domain.New;

public class MetadataUpdateNewDTO implements New, IsSerializable {
	
	private SummaryDTO summaryDTO;

	public MetadataUpdateNewDTO() 
	{

	}
	
	public MetadataUpdateNewDTO(SummaryDTO summaryDTO) {
		this.summaryDTO = summaryDTO;
	}

	public SummaryDTO getSummaryDTO() {
		return summaryDTO;
	}

	public void setSummaryDTO(SummaryDTO summaryDTO) {
		this.summaryDTO = summaryDTO;
	}

	@Override
	public Date getDate() {
		return new Date(summaryDTO.getModificationDate());
	}

	@Override
	public String getUuid() {
		return summaryDTO.getUuid();
	}
	

}
