package fr.obsmip.sedoo.shared.constant;

public class LogConstant {

	public static final String SESSION_CATEGORY = "Session";
	public static final String CONNEXION_ACTION = "Connexion";
	public static final String DECONNEXION_ACTION = "Deconnexion";
	public static final String DELETION_ACTION = "Deletion";
	public static final String METADATA_CATEGORY = "Metadata";
	public static final String MODIFICATION_ACTION = "Modification";
}
