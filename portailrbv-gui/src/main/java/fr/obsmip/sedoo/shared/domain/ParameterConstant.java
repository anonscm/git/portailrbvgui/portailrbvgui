package fr.obsmip.sedoo.shared.domain;

public class ParameterConstant {

	public static final String PRINT_SERVICE_URL_PARAMETER_NAME = "printServiceURL";
	public static final String XML_SERVICE_URL_PARAMETER_NAME = "xmlServiceURL";
	public static final String DATA_SOURCE_BEAN_NAME = "dataSource";
	
}
