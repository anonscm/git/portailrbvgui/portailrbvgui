package fr.obsmip.sedoo.shared.exception;

import com.google.gwt.user.client.rpc.IsSerializable;

public class LoginException extends RuntimeException implements IsSerializable
{
	private String message;
	
	public LoginException() {
	}
	
	public LoginException(String message)
	{
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
