package fr.obsmip.sedoo.shared.domain;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.message.ValidationMessages;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class GeonetworkObservatoryDTO extends AbstractDTO implements
		HasIdentifier, IsObservatory, Comparable<GeonetworkObservatoryDTO> {
	private Long id;
	private String name;
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getIdentifier() {
		return "" + getId();
	}

	@Override
	public String getHash() {
		return StringUtil.trimToEmpty("");
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		if (StringUtil.isEmpty(getName())) {
			result.add(new ValidationAlert(CommonMessages.INSTANCE.name(),
					ValidationMessages.INSTANCE.mandatoryData()));
		}
		return result;
	}

	@Override
	public int compareTo(GeonetworkObservatoryDTO o) {
		return getName().compareTo(o.getName());
	}
}
