package fr.obsmip.sedoo.shared.util;

import java.util.Comparator;

import fr.obsmip.sedoo.shared.api.IsObservatory;

public class IsObservatoryComparator implements Comparator<IsObservatory> {

	@Override
	public int compare(IsObservatory arg0, IsObservatory arg1) {
		if (arg0 == null)
		{
			return -1;
		}
		if (arg1 == null)
		{
			return 1;
		}
		return arg0.getName().compareToIgnoreCase(arg1.getName());
	}
}
