package fr.obsmip.sedoo.shared.domain;

import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;


/**
 * Metadata entry relative to an experimental site
 * @author francois
 *
 */
public class ExperimentalSiteDTO extends AbstractSerieMetadata
{
	public final static String RBV_NAMESPACE = "http://portailrbvws.sedoo.fr/experimentalsite/";

	private MetadataSummaryDTO parentSummary = new MetadataSummaryDTO(); 
	
	public String getExperimentalSiteName() {
		
		return getName();
	}
	
	public void setExperimentalSiteName(String experimentalSiteName)
	{
		setName(experimentalSiteName);
	}

	@Override
	public String getIdentifierNamespace() {
		return RBV_NAMESPACE;
	}

	public MetadataSummaryDTO getParentSummary() {
		return parentSummary;
	}

	public void setParentSummary(MetadataSummaryDTO parentSummary) {
		this.parentSummary = parentSummary;
	}
	

}
