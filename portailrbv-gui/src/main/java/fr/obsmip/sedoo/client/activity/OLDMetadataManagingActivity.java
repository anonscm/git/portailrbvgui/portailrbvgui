package fr.obsmip.sedoo.client.activity;


public class OLDMetadataManagingActivity 
{
//	extends RBVAuthenticatedActivity implements MetadataManagingView.Presenter
//}
//{
//	private MetadataManagingView view;
//
//	private final ObservatoryServiceAsync observatoryService = GWT.create(ObservatoryService.class);
//	private final SearchServiceAsync searchService = GWT.create(SearchService.class);
//	private final MetadataServiceAsync metadataService = GWT.create(MetadataService.class);
//	public final static int DEFAULT_PAGE_SIZE = 10;
//	public final static int DEFAULT_POSITION = 1;
//	public int currentPosition = DEFAULT_POSITION;
//	private SearchCriteriaDTO criteria;
//
//	public MetadataManagingActivity(MetadataManagingPlace place, ClientFactory clientFactory)
//	{
//		super(clientFactory, place);
//	}
//
//	@Override
//	public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
//	{
//		if (isValidUser() == false)
//		{
//			goToLoginPlace();
//			return;
//		}
//		sendActivityStartEvent();
//		view = clientFactory.getMetadataManagingView();
//		view.setPresenter(this);
//		view.reset();
//		containerWidget.setWidget(view.asWidget());
//		broadcastActivityTitle(Message.INSTANCE.metadataProviderMenuManageMetadata());
//		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//		shortcuts.add(ShortcutFactory.getMetadataManagementShortcut());
//		clientFactory.getBreadCrumb().refresh(shortcuts);
//		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.loading(), ActionEventConstant.OBSERVATORIES_LOADING_EVENT, true);
//		clientFactory.getEventBus().fireEvent(e);
//
//		observatoryService.getObservatories(new AsyncCallback<List<ObservatoryDTO>>()
//		{
//
//			@Override
//			public void onSuccess(List<ObservatoryDTO> observatories)
//			{
//
//				view.setObservatoriesFromEntries(observatories);
//				ArrayList<String> observatoryNames = new ArrayList<String>();
//				if (observatories != null)
//				{
//					Iterator<ObservatoryDTO> iterator = observatories.iterator();
//					while (iterator.hasNext()) {
//						ObservatoryDTO observatoryDTO = (ObservatoryDTO) iterator.next();
//						observatoryNames.add(observatoryDTO.getShortLabel());
//					}
//				}
//				
//				
//				view.setObservatoriesFromEntries(filterByUser(observatoryNames, (RBVUserDTO) PortailRBV.getClientFactory().getUserManager().getUser()));
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//			}
//		});
//	}
//
//	
//	private ArrayList<String> filterByUser(ArrayList<String> observatoryNames, RBVUserDTO user) 
//	{
//		if (user.isAdmin())
//		{
//			return observatoryNames;
//		}
//		else
//		{
//			ArrayList<String> result = new ArrayList<String>();
//			Iterator<String> iterator = observatoryNames.iterator();
//			while (iterator.hasNext()) 
//			{
//				String observatoryName = iterator.next();
//				if (user.isAutorizedFor(observatoryName))
//				{
//					result.add(observatoryName);
//				}
//			}
//			return result;
//		}
//	}
//	
//	@Override
//	public void getEntries(final int position)
//	{
//
//		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.metadataSearchingViewSearchInProgress(), ActionEventConstant.METADATA_SEARCH_EVENT, true);
//		clientFactory.getEventBus().fireEvent(e);
//
//		searchService.getHits(criteria, new AsyncCallback<Integer>()
//		{
//
//			@Override
//			public void onSuccess(Integer result)
//			{
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				if (result <= 0)
//				{
//					DialogBoxTools.modalAlert(Message.INSTANCE.information(), Message.INSTANCE.metadataSearchingEmptyResult());
//				} else
//				{
//					view.setHits(result);
//					fetchSummaries(position, criteria);
//				}
//
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//			}
//		});
//	}
//
//	public void fetchSummaries(final int start, final SearchCriteriaDTO criteria)
//	{
//		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.metadataSearchingViewSearchInProgress(), ActionEventConstant.METADATA_SEARCH_EVENT, true);
//
//		searchService.getSummaries(criteria, start, DEFAULT_PAGE_SIZE, new AsyncCallback<ArrayList<SummaryDTO>>()
//		{
//
//			@Override
//			public void onSuccess(ArrayList<SummaryDTO> results)
//			{
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				currentPosition = start;
//				setCriteria(criteria);
//				view.setEntries(start, results);
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//			}
//		});
//	}
//
//	@Override
//	public void editMetadata(String metadataUuid, String observatoryShortLabel)
//	{
//		MetadataEditingPlace place = new MetadataEditingPlace();
//		place.setMode(Constants.MODIFY);
//		place.setMetadataUuid(metadataUuid);
//		place.setObservatoryShortLabel(observatoryShortLabel);
//		clientFactory.getPlaceController().goTo(place);
//	}
//
//	@Override
//	public void viewMetadata(String metadataUuid)
//	{
//
//		MetadataDisplayPlace place = new MetadataDisplayPlace();
//		place.setId(metadataUuid);
//		clientFactory.getPlaceController().goTo(place);
//	}
//
//	@Override
//	public void deleteMetadata(final String uuid, final String title)
//	{
//		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.deleting(), ActionEventConstant.METADATA_DELETING_EVENT, true);
//		clientFactory.getEventBus().fireEvent(e);
//		metadataService.deleteMetadataByUuid(uuid, new AsyncCallback<Boolean>()
//		{
//			@Override
//			public void onSuccess(Boolean result)
//			{
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				clientFactory.getEventBus().fireEvent(new NotificationEvent(Message.INSTANCE.deletedElement()));
//				Logger.addLog((RBVUserDTO) PortailRBV.getClientFactory().getUserManager().getUser(), LogConstant.METADATA_CATEGORY, LogConstant.DELETION_ACTION, "User "+PortailRBV.getClientFactory().getUserManager().getUser().getName()+" has deleted metadata of title : "+title+" - UUID : "+uuid);
//				fetchSummaries(currentPosition, getCriteria());
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//			}
//		});
//	}
//
//	public SearchCriteriaDTO getCriteria()
//	{
//		return criteria;
//	}
//
//	@Override
//	public void setCriteria(SearchCriteriaDTO criteria)
//	{
//		this.criteria = criteria;
//
//	}
//
	
}
