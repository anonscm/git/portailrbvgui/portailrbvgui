package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.event.ActivityChangeEventHandler;

public interface SectionHeaderView extends IsWidget, ActivityChangeEventHandler {

	public int getHeight();

}
