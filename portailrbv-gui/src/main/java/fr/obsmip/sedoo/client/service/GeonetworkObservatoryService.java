package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.util.ServiceException;

@RemoteServiceRelativePath("geonetworkobservatories")

public interface GeonetworkObservatoryService extends RemoteService {
        ArrayList<GeonetworkObservatoryDTO> findAll() throws ServiceException;
        void delete(GeonetworkObservatoryDTO observatory) throws ServiceException;
        GeonetworkObservatoryDTO edit(GeonetworkObservatoryDTO observatory) throws ServiceException;
        GeonetworkObservatoryDTO create(GeonetworkObservatoryDTO observatory) throws ServiceException;
}
