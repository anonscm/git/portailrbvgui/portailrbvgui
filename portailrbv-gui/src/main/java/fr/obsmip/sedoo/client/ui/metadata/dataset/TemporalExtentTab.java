package fr.obsmip.sedoo.client.ui.metadata.dataset;

import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.CreationDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.EndDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.LastRevisionDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.PublicationDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.StartDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.UpdateRythm;

public class TemporalExtentTab extends AbstractTab {

	
	public TemporalExtentTab() {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingCoveredPeriods());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.STARTING_DATE));
		addRightComponent(new StartDate());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.ENDING_DATE));
		addRightComponent(new EndDate());
		addSection(MetadataMessage.INSTANCE.metadataEditingTemporalMilestones());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.CREATION_DATE));
		addRightComponent(new CreationDate());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.LAST_REVISION_DATE));
		addRightComponent(new LastRevisionDate());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.PUBLICATION_DATE));
		addRightComponent(new PublicationDate());
		addSection(MetadataMessage.INSTANCE.metadataEditingMaintenance());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.UPDATE_RYTHM));
		addRightComponent(new UpdateRythm());

		reset();
	}


}
