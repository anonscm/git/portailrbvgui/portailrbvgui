package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class DatasetEntryCreationPlace extends Place implements
		AuthenticatedPlace {

	public static DatasetEntryCreationPlace instance;

	private String observatoryName;
	private String experimentalSiteName;

	public static class Tokenizer implements
			PlaceTokenizer<DatasetEntryCreationPlace> {
		@Override
		public String getToken(DatasetEntryCreationPlace place) {
			return "";
		}

		@Override
		public DatasetEntryCreationPlace getPlace(String token) {
			if (instance == null) {
				instance = new DatasetEntryCreationPlace();
			}
			return instance;
		}
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}

	public String getExperimentalSiteName() {
		return experimentalSiteName;
	}

	public void setExperimentalSiteName(String experimentalSiteName) {
		this.experimentalSiteName = experimentalSiteName;
	}

}
