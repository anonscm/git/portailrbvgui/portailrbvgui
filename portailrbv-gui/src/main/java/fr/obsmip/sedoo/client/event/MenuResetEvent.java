package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class MenuResetEvent extends GwtEvent<MenuResetEventHandler> {

	public static final Type<MenuResetEventHandler> TYPE = new Type<MenuResetEventHandler>();

	public MenuResetEvent() {
	}

	@Override
	protected void dispatch(MenuResetEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<MenuResetEventHandler> getAssociatedType() {
		return TYPE;
	}

}
