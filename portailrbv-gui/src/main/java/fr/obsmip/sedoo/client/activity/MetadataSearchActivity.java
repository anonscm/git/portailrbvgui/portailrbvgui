package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.place.MetadataResultPlace;
import fr.obsmip.sedoo.client.place.MetadataSearchPlace;
import fr.obsmip.sedoo.client.service.SearchService;
import fr.obsmip.sedoo.client.service.SearchServiceAsync;
import fr.obsmip.sedoo.client.ui.MetadataSearchView;
import fr.obsmip.sedoo.client.ui.MetadataSearchView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class MetadataSearchActivity extends RBVAbstractActivity implements Presenter, LoadCallBack<ArrayList<IsObservatory>>  {

	private final SearchServiceAsync searchService = GWT.create(SearchService.class);

	MetadataSearchPlace place;
	SearchCriteriaDTO criteria;

	private MetadataSearchView metadataSearchView;

	public MetadataSearchActivity(MetadataSearchPlace place, ClientFactory clientFactory) {
		super(clientFactory);
		this.place = place;
		criteria = MetadataSearchPlace.getCriteria();
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		metadataSearchView = clientFactory.getMetadataSearchView();
		metadataSearchView.setPresenter(this);
		containerWidget.setWidget(metadataSearchView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.metadataSearchingTitle());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getMetadataSearchShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		if (criteria != null)
		{
			metadataSearchView.update(criteria);
		}
		ClientObservatoryList.getObservatories(this);
	}


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void search(final SearchCriteriaDTO criteria) 
	{
		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.metadataSearchingViewSearchInProgress(), ActionEventConstant.METADATA_SEARCH_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);

		searchService.getHits(criteria, new AsyncCallback<Integer>() {

			@Override
			public void onSuccess(Integer result) {
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
				if (result <=0)
				{
					DialogBoxTools.modalAlert(CommonMessages.INSTANCE.information(), Message.INSTANCE.metadataSearchingEmptyResult());	
					clientFactory.getEventBus().fireEvent(e.getEndingEvent());
				}
				else
				{
					MetadataResultPlace place= new MetadataResultPlace();
					place.setCriteria(criteria);
					place.setHits(result);
					goTo(place);
				}

			}

			@Override
			public void onFailure(Throwable caught) {
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" "+caught.getMessage());
			}
		});
	}

	@Override
	public void postLoadProcess(ArrayList<IsObservatory> result) 
	{
		ArrayList<String> observatoryNames = new ArrayList<String>();
		if (result != null)
		{
			Iterator<IsObservatory> iterator = result.iterator();
			while (iterator.hasNext()) {
				observatoryNames.add(iterator.next().getName());
			}
		}
		metadataSearchView.setObservatories(observatoryNames);
		
	}
}