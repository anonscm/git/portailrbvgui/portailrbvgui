package fr.obsmip.sedoo.client.ui.searchcriteria;

import com.google.gwt.user.client.ui.IsWidget;

public interface CriteriaWidget extends IsWidget {

	public void reset();

}
