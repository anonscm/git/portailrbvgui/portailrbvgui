package fr.obsmip.sedoo.client.ui.search;

import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.SearchResultEvent;
import fr.obsmip.sedoo.client.event.SearchResultEventHandler;
import fr.obsmip.sedoo.client.event.SearchStartEvent;
import fr.obsmip.sedoo.client.event.SearchStartEventHandler;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.sedoo.commons.client.widget.map.impl.SingleAreaSelectorWidget;

public class SearchMap extends SingleAreaSelectorWidget implements
		SearchResultEventHandler, SearchStartEventHandler {

	private static final String FORM_CONTROL_STYLE_NAME = "form-control";
	private SearchMapSelector searchMapSelector;

	public SearchMap() {
		super();
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(SearchResultEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(SearchStartEvent.TYPE, this);
		getNorthLatitudeTextBox().removeStyleName(FORM_CONTROL_STYLE_NAME);
		getSouthLatitudeTextBox().removeStyleName(FORM_CONTROL_STYLE_NAME);
		getEastLongitudeTextBox().removeStyleName(FORM_CONTROL_STYLE_NAME);
		getWestLongitudeTextBox().removeStyleName(FORM_CONTROL_STYLE_NAME);

	}

	@Override
	protected void initAreaSelector(String mapLayer) {

		searchMapSelector = new SearchMapSelector(mapLayer);
		setAreaSelector(searchMapSelector);
	}

	@Override
	public void onNotification(SearchResultEvent event) {
		if (!event.getAllSummaryDTOs().isEmpty()){
			searchMapSelector.addSearchResult(event.getSummaryDTOs());
		}
	}

	@Override
	public void onNotification(SearchStartEvent event) {
		Place place = PortailRBV.getPresenter().getWhere();
		
		if (place != null
				&& ! (place instanceof WelcomePlace) ){
			PortailRBV.getPresenter().goTo(new WelcomePlace());
		}
		
		searchMapSelector.resetSearchResult();
	}
}
