package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.DateTools;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaBar;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;
import fr.sedoo.metadata.client.ui.widget.date.Iso19115DateBox;

public class DateWidget extends AbstractSearchWidget implements HasValidation,
		ClickHandler {

	protected Iso19115DateBox startDate;
	protected Iso19115DateBox endDate;
	protected CriteriaBar criteriaBar;
	private Image startDateToolTip;
	private Image endDateToolTip;

	public DateWidget() {
		super(Message.INSTANCE.metadataSearchingViewTimePeriod());
		startDate = new Iso19115DateBox();
		endDate = new Iso19115DateBox();
		init();
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	public void init() {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(5);

		HorizontalPanel startDatePanel = new HorizontalPanel();
		startDatePanel.setSpacing(3);
		startDatePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		HorizontalPanel endDatePanel = new HorizontalPanel();
		endDatePanel.setSpacing(3);
		endDatePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

		startDate.getElement().setAttribute("placeHolder",
				Message.INSTANCE.startDatePlaceholder());
		startDateToolTip = new Image(GlobalBundle.INSTANCE.help());
		startDateToolTip.getElement().getStyle().setMarginLeft(3, Unit.PX);
		startDateToolTip.getElement().getStyle()
				.setProperty("cursor", "pointer");
		startDateToolTip.setTitle(Message.INSTANCE.iso8601Format());
		startDatePanel.add(startDate);
		startDatePanel.add(startDateToolTip);

		endDate.getElement().setAttribute("placeHolder",
				Message.INSTANCE.endDatePlaceholder());
		endDateToolTip = new Image(GlobalBundle.INSTANCE.help());
		endDateToolTip.getElement().getStyle().setMarginLeft(3, Unit.PX);
		endDateToolTip.getElement().getStyle().setProperty("cursor", "pointer");
		endDateToolTip.setTitle(Message.INSTANCE.iso8601Format());
		endDatePanel.add(endDate);
		endDatePanel.add(endDateToolTip);
		verticalPanel.add(startDatePanel);
		verticalPanel.add(endDatePanel);

		panel.add(verticalPanel);
		panel.setOpen(false);
	}

	@Override
	public void reset() {
		startDate.setValue(null);
		endDate.setValue(null);
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {
		if (startDate.getValue() == null) {
			criteria.setEndDate(null);
		} else {
			criteria.setStartDate(DateTools.getRBVDateFormat().format(
					startDate.getValue()));
		}

		if (endDate.getValue() == null) {
			criteria.setEndDate(null);
		} else {
			criteria.setEndDate(DateTools.getRBVDateFormat().format(
					endDate.getValue()));
		}
		return criteria;
	}

	@Override
	public List<ValidationAlert> validate() {

		return null;
	}

	@Override
	public void onClick(ClickEvent event) {
		criteriaBar.validate();

	}

}