package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.activity.MetadataResultActivity;
import fr.obsmip.sedoo.client.ui.table.search.ResultList;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public class MetadataResultViewImpl extends AbstractSection implements
		MetadataResultView {

	private Presenter presenter;

	@UiField
	ResultList resultList;

	@UiField
	Element hitsLabel;

	@UiField
	Button backButton;

	@UiField(provided = true)
	SimplePager pager;

	@UiField
	HTMLPanel resultPanel;

	private int hits = 0;

	private static ResultViewImplUiBinder uiBinder = GWT
			.create(ResultViewImplUiBinder.class);

	interface ResultViewImplUiBinder extends
			UiBinder<Widget, MetadataResultViewImpl> {
	}

	public MetadataResultViewImpl() {
		super();
		GWT.<GlobalBundle> create(GlobalBundle.class).css().ensureInjected();
		pager = new SimplePager(TextLocation.CENTER, false, true);
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		dataProvider.addDataDisplay(resultList);
		pager.setDisplay(resultList);
		reset();
		// resultList.setVisibleRange(MetadataResultActivity.DEFAULT_POSITION,
		// MetadataResultActivity.DEFAULT_PAGE_SIZE);
	}

	@Override
	public void setResults(int position,
			HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> results) {
		ArrayList<SummaryDTO> aux = new ArrayList<>();

		Set<GeonetworkObservatoryDTO> keySet = results.keySet();
		for (GeonetworkObservatoryDTO observatoryResult : keySet) {
			aux.addAll(results.get(observatoryResult));
		}

		resultList.setRowData(position - 1, aux);
		resultList.setRowCount(hits);
		resultPanel.setVisible(true);
		// resultList.setRowCount(results.size());
	}

	@Override
	public void setPresenter(Presenter presenter) {

		this.presenter = presenter;
		resultList.setPresenter(presenter);

	}

	@Override
	public Presenter getPresenter() {

		return presenter;
	}

	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event) {
		presenter.back();
	}

	@Override
	public void setHits(int hits) {
		this.hits = hits;
		hitsLabel.setAttribute("visibility", "visible");
		hitsLabel.setInnerText("" + hits);
		resultPanel.setVisible(true);
	}

	AsyncDataProvider<SummaryDTO> dataProvider = new AsyncDataProvider<SummaryDTO>() {

		@Override
		protected void onRangeChanged(HasData<SummaryDTO> display) {

			final Range range = display.getVisibleRange();
			int start = range.getStart();
			if (presenter != null) {
				presenter.fetchSummaries(start + 1);
			}
		}
	};

	@Override
	public void reset() {
		pager.setPageSize(MetadataResultActivity.DEFAULT_PAGE_SIZE);
		pager.setPageStart(0);
		resultList.setRowData(0, new ArrayList<SummaryDTO>());
		resultList.setRowCount(0);
		resultPanel.setVisible(false);
	}

	@Override
	public void hideResultPanel() {
		resultPanel.setVisible(false);
	}

}
