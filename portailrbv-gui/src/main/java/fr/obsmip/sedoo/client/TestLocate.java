package fr.obsmip.sedoo.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;

import fr.obsmip.sedoo.client.mvp.AppActivityMapper;
import fr.obsmip.sedoo.client.mvp.AppPlaceHistoryMapper;
import fr.obsmip.sedoo.client.place.FilteringHistorian;
import fr.obsmip.sedoo.client.place.GeoSummaryPlace;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class TestLocate implements EntryPoint
{
	private SplitLayoutPanel split;


	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	/**
	 * This is the entry point method.
	 */

	//SimpleMapWidget mapWidget;

	@Override
	public void onModuleLoad()
	{
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		ClientFactory clientFactory = GWT.create(ClientFactory.class);
		EventBus eventBus = clientFactory.getEventBus();
		PlaceController placeController = clientFactory.getPlaceController();
		SimpleLayoutPanel centerPanel =  new SimpleLayoutPanel();
		
		RootPanel loadingMessage = RootPanel.get("loadingMessage");
		if (loadingMessage != null)
		{
			DOM.setInnerHTML(loadingMessage.getElement(), "");
		}
		
		// Start ActivityManager for the main widget with our ActivityMapper
	    ActivityMapper activityMapper = new AppActivityMapper(clientFactory);
		ActivityManager activityManager = new ActivityManager(activityMapper, eventBus);
		activityManager.setDisplay(centerPanel);

		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		AppPlaceHistoryMapper historyMapper= GWT.create(AppPlaceHistoryMapper.class);
		FilteringHistorian historian = new FilteringHistorian();
		String name = LanguageSwitchPlace.class.getName();
		historian.addToken(name.substring(name.lastIndexOf('.')+1));
		
		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper,historian);
		historyHandler.register(placeController, eventBus, new GeoSummaryPlace());
		
		activityManager.setDisplay(centerPanel);
		//split.add(mapWidget);
		//loadMapApi();
		
		RootLayoutPanel.get().add(centerPanel);
		
		
		Window.enableScrolling(false);
		Window.setMargin("0px");
		historyHandler.handleCurrentHistory();

	}

	
//	  private void loadMapApi() {
//		    boolean sensor = false;
//
//		    // load all the libs for use in the maps
//		    ArrayList<LoadLibrary> loadLibraries = new ArrayList<LoadApi.LoadLibrary>();
////		    loadLibraries.add(LoadLibrary.ADSENSE);
////		    loadLibraries.add(LoadLibrary.DRAWING);
//		    loadLibraries.add(LoadLibrary.GEOMETRY);
////		    loadLibraries.add(LoadLibrary.PANORAMIO);
////		    loadLibraries.add(LoadLibrary.PLACES);
////		    loadLibraries.add(LoadLibrary.WEATHER);
//		    loadLibraries.add(LoadLibrary.VISUALIZATION);
//
//		    Runnable onLoad = new Runnable() {
//		      @Override
//		      public void run() {
//		    	  //Create a MapWidget
//			        GeoSummaryMap mapWidget = new GeoSummaryMap("100%", "100%");	        
//			      
//			      split.add(mapWidget);
//			      split.forceLayout();
//		      }
//		    };
//
//		    LoadApi.go(onLoad, loadLibraries, sensor);
//		  }

	
	
}
