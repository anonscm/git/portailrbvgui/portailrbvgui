package fr.obsmip.sedoo.client.ui.metadata.common.contact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;

import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.metadata.utils.pdf.labelprovider.RoleLabelProvider;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;

public class RBVMetadataPointOfContacts extends RBVAbstractContactList implements ClickHandler, ConfirmCallBack
{
	public String getSingleRole() {
		return RoleLabelProvider.POINT_OF_CONTACT;
	}

	@Override
	public List<? extends HasIdentifier> getDatas(MetadataDTO metadata) {
		return metadata.getContactPart().getMetadataContactsByRole(getSingleRole());
	}

	@Override
	public void flush(MetadataDTO metadata) {
		List<MetadataContactDTO> aux = new ArrayList<MetadataContactDTO>();
		Iterator<? extends HasIdentifier> iterator = table.getModel().iterator();
		while (iterator.hasNext()) {
			aux.add((MetadataContactDTO) iterator.next());
		}
		metadata.getContactPart().addMetadataContacts(aux);
	}
}
