package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

@RemoteServiceRelativePath("directory")

public interface DirectoryService extends RemoteService {
		  Boolean deletePerson(String id) throws ServiceException;
		  ArrayList<PersonDTO> loadPersons(String observatoryId) throws ServiceException;
		  void savePerson(ObservatoryPersonDTO person) throws ServiceException;
//        ArrayList<GeonetworkObservatoryDTO> findAll() throws ServiceException;
//        void delete(GeonetworkObservatoryDTO observatory) throws ServiceException;
//        GeonetworkObservatoryDTO edit(GeonetworkObservatoryDTO observatory) throws ServiceException;
//        GeonetworkObservatoryDTO create(GeonetworkObservatoryDTO observatory) throws ServiceException;
}
