package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.misc.MetadataDisplayShortcut;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumb;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class MetadataDisplayActivityUtil {

	public static void addShortcut(BreadCrumb breadCrumb) {
		List<Shortcut> shortcuts = breadCrumb.getShortcuts();
		if (ListUtil.isEmpty(shortcuts)) {
			ArrayList<Shortcut> newShortcuts = new ArrayList<Shortcut>();
			newShortcuts.add(ShortcutFactory.getWelcomeShortcut());
			newShortcuts.add(new MetadataDisplayShortcut());
			breadCrumb.refresh(newShortcuts);
		} else {
			Shortcut shortcut = shortcuts.get(shortcuts.size() - 1);
			if (shortcut instanceof MetadataDisplayShortcut) {
				// On ne fait rien
				return;
			} else {
				shortcuts.add(new MetadataDisplayShortcut());
				breadCrumb.refresh(shortcuts);
			}
		}
	}

}
