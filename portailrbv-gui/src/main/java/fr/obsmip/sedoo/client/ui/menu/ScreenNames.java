package fr.obsmip.sedoo.client.ui.menu;

public class ScreenNames {

	public final static String HOME = "HOME";
	public final static String HUMANITY_CRITICAL_ZONE = "HUMANITY_CRITICAL_ZONE";
	public final static String LINKS = "LINKS";
	public static final String MULTIDISCIPLINARY_APPROACH = "MULTIDISCIPLINARY_APPROACH";
	public static final String NATURAL_LABORATORY = "NATURAL_LABORATORY";
	public static final String ELEMENTARY_OBSERVATORIES = "ELEMENTARY_OBSERVATORIES";
	public static final String LOCATION = "LOCATION";
	public static final String A_SCIENTIST_NETWORK = "A_SCIENTIST_NETWORK";
	public static final String STEERING_COMITEE = "STEERING_COMITEE";
	public static final String SCIENTIFIC_ORIENTATION_COMITTEE = "SCIENTIFIC_ORIENTATION_COMITTEE";
	public static final String COORDINATOR_AND_CONTACT = "COORDINATOR_AND_CONTACT";
	public static final String INSTRUMENTS_AND_MEASUREMENTS = "INSTRUMENTS_AND_MEASUREMENTS";
	public static final String OBSERVATION_PARAMETERS = "OBSERVATION_PARAMETERS";
	public static final String METADATA_PORTAL = "METADATA_PORTAL";
	public static final String RESULT_EXAMPLES = "RESULT_EXAMPLES";
	public static final String MISCELLANEOUS = "MISCELLANEOUS";
	public static final String ANIMATION = "ANIMATION";
	public static final String DOCUMENTS = "DOCUMENTS";
}
