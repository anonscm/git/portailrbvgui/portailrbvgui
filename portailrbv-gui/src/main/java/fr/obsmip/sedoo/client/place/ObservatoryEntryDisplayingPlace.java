package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;
import fr.sedoo.commons.client.util.StringUtil;

public class ObservatoryEntryDisplayingPlace extends Place implements AuthenticatedPlace
{

	private String uuid;
	
	public static ObservatoryEntryDisplayingPlace instance;
	
	public static class Tokenizer implements PlaceTokenizer<ObservatoryEntryDisplayingPlace>
	{
		@Override
		public String getToken(ObservatoryEntryDisplayingPlace place)
		{
			return StringUtil.trimToEmpty(place.getUuid());
		}

		@Override
		public ObservatoryEntryDisplayingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ObservatoryEntryDisplayingPlace();
			}
			if (StringUtil.isNotEmpty(token))
			{
				instance.setUuid(token);
			}
			return instance;
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
