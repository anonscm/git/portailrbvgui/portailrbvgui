package fr.obsmip.sedoo.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.MetadataPrintHandler;
import fr.obsmip.sedoo.client.misc.MetadataXmlHandler;
import fr.obsmip.sedoo.client.place.ObservatoryEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumb;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;

public class ObservatoryEntryDisplayingActivity extends RBVPublicActivity
		implements DisplayPresenter {

	private String observatoryUuid;
	private ObservatoryEntryDisplayingView view;

	public ObservatoryEntryDisplayingActivity(
			ObservatoryEntryDisplayingPlace place, ClientFactory clientFactory) {
		super(clientFactory);
		if (StringUtil.isNotEmpty(place.getUuid())) {
			observatoryUuid = place.getUuid();
		}
	}

	private static final MetadataServiceAsync METADATA_SERVICE = GWT
			.create(MetadataService.class);

	@Override
	public void start(final AcceptsOneWidget containerWidget, EventBus eventBus) {

		// The uuid is not present - we go to the welcome page
		if (StringUtil.isEmpty(observatoryUuid)) {
			goToWelcomePlace();
			return;
		}
		sendActivityStartEvent();
		containerWidget.setWidget(clientFactory.getProgressView());
		broadcastActivityTitle(Message.INSTANCE.observatoryDisplayingTitle());
		ActionStartEvent startEvent = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.OBSERVATORY_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		METADATA_SERVICE.getObservatoryByUuid(observatoryUuid, clientFactory
				.getMetadataLanguages(), LocaleUtil
				.getCurrentLanguage(clientFactory),
				new DefaultAbstractCallBack<ObservatoryDTO>(startEvent,
						clientFactory.getEventBus()) {

					@Override
					public void onSuccess(ObservatoryDTO result) {
						super.onSuccess(result);
						view = clientFactory
								.getObservatoryEntryDisplayingView();
						((ObservatoryEntryDisplayingView) view)
								.setDisplayPresenter(ObservatoryEntryDisplayingActivity.this);
						containerWidget.setWidget(view.asWidget());
						view.display(result);

						BreadCrumb breadCrumb = clientFactory.getBreadCrumb();
						if (breadCrumb.getShortcuts().isEmpty()) {

						}
						MetadataDisplayActivityUtil.addShortcut(clientFactory
								.getBreadCrumb());
					}

				});
	}

	@Override
	public void back() {
		clientFactory.getBreadCrumb().back();
	}

	public void goToWelcomePlace() {
		clientFactory.getPlaceController().goTo(new WelcomePlace());
	}

	@Override
	public void print(String uuid) {
		MetadataPrintHandler.print(uuid);
	}

	@Override
	public void xml(String uuid) {
		MetadataXmlHandler.xml(uuid);
	}

}
