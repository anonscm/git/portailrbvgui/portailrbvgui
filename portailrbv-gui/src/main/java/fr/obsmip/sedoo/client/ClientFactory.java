package fr.obsmip.sedoo.client;

import java.util.ArrayList;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;

import fr.obsmip.sedoo.client.ui.BlankView;
import fr.obsmip.sedoo.client.ui.DatasetEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.DatasetEntryManagementView;
import fr.obsmip.sedoo.client.ui.DatasetEntryView;
import fr.obsmip.sedoo.client.ui.DirectoryManagementView;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryManagementView;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryView;
import fr.obsmip.sedoo.client.ui.GeoSummaryView;
import fr.obsmip.sedoo.client.ui.HarvestManagementView;
import fr.obsmip.sedoo.client.ui.HeaderView;
import fr.obsmip.sedoo.client.ui.LogConsultationView;
import fr.obsmip.sedoo.client.ui.LoginView;
import fr.obsmip.sedoo.client.ui.MenuView;
import fr.obsmip.sedoo.client.ui.MetadataManagingView;
import fr.obsmip.sedoo.client.ui.MetadataResultView;
import fr.obsmip.sedoo.client.ui.MetadataSearchView;
import fr.obsmip.sedoo.client.ui.NewsView;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryManagementView;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryView;
import fr.obsmip.sedoo.client.ui.ProgressView;
import fr.obsmip.sedoo.client.ui.SectionHeaderView;
import fr.obsmip.sedoo.client.ui.StatusBarView;
import fr.obsmip.sedoo.client.ui.SystemView;
import fr.obsmip.sedoo.client.ui.UserManagementView;
import fr.obsmip.sedoo.client.ui.WelcomeView;
import fr.obsmip.sedoo.client.ui.geonetworkobservatory.GeonetworkObservatoryManagementView;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryEntryCreationView;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumb;
import fr.obsmip.sedoo.shared.domain.DatasetDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.sedoo.commons.client.cms.mvp.CMSMenuClientFactory;
import fr.sedoo.commons.client.cms.ui.view.CMSMenuEditView;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingView;
import fr.sedoo.commons.client.mvp.CMSClientFactory;
import fr.sedoo.commons.client.mvp.CommonClientFactory;
import fr.sedoo.commons.client.news.mvp.MessageClientFactory;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.ui.MessageManageView;
import fr.sedoo.metadata.client.mvp.MetadataClientFactory;
import fr.sedoo.metadata.client.ui.view.FormatListView;
import fr.sedoo.metadata.client.ui.view.MetadataDisplayingView;
import fr.sedoo.metadata.client.ui.view.MetadataEditingView;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public interface ClientFactory extends CommonClientFactory, MessageClientFactory, MetadataClientFactory, NewsClientFactory, CMSClientFactory, CMSMenuClientFactory{

	EventBus getEventBus();

	PlaceController getPlaceController();

	WelcomeView getWelcomeView();

	MenuView getMenuView();

	HeaderView getHeaderView();

	SystemView getSystemView();
	
	NewsView getNewsView();
	
	ProgressView getProgressView();
	
	BlankView getBlankView();

	MetadataEditingView getMetadataEditingView();

	MetadataDisplayingView getMetadataDisplayView(MetadataDTO metadata);
	
	StatusBarView getStatusBarView();
	
	GeoSummaryView getGeoSummaryView();

	GlobalBundle getBundle();

	LanguageSwitchingView getLanguageSwitchingView();

	MetadataSearchView getMetadataSearchView();

	FormatListView getFormatListView();

	MetadataResultView getResultView();

	LoginView getLoginView();

	ObservatoryEntryManagementView getObservatoryManagementView();

	ObservatoryEntryView getObservatoryEditingView();

	SectionHeaderView getSectionHeaderView();

	MetadataManagingView getMetadataManagingView();
	
	UserManagementView getUserManagementView();
	
	MessageManageView getMessageManageView();
	
	LogConsultationView getLogConsultationView();
	
	GeonetworkObservatoryManagementView getGeonetworkObservatoryManagementView();
	
	BreadCrumb getBreadCrumb();
	
	ObservatoryDTO createObservatoryDTO(String observatoryName);

	ObservatoryEntryCreationView getObservatoryEntryCreationView();
	
	ExperimentalSiteEntryView getExperimentalSiteEntryEditingView();
	
	DatasetEntryCreationView getDatasetEntryCreationView();
	
	DatasetEntryManagementView getDatasetEntryManagementView();

	ExperimentalSiteDTO createExperimentalSiteDTO(String experimentalSiteName, MetadataSummaryDTO parentSummary);

	ExperimentalSiteEntryCreationView getExperimentalSiteEntryCreationView();

	ExperimentalSiteEntryManagementView getExperimentalSiteEntryManagementView();

	ObservatoryEntryDisplayingView getObservatoryEntryDisplayingView();
	
	ExperimentalSiteEntryDisplayingView getExperimentalSiteEntryDisplayingView();
	
	DatasetEntryDisplayingView getDatasetEntryDisplayingView();

	DatasetDTO createDatasetDTO(MetadataSummaryDTO observatorySummary,
			MetadataSummaryDTO experimentalSiteSummary);

	DatasetEntryView getDatasetEntryEditingView();

	ArrayList<String> getDisplayLanguages();

	DirectoryManagementView getDirectoryManagementView();

	HarvestManagementView getHarvestManagementView();
	
	CMSMenuEditView getCmsMenuEditView();

}
