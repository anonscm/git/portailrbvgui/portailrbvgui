package fr.obsmip.sedoo.client.ui.metadata.dataset;

import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class GeographicalLocationTab extends AbstractTab {

	public boolean created = false;
	private MetadataDTO metadata;
	private String mapLayer;

	public GeographicalLocationTab(String mapLayer) {
		super();
		this.mapLayer = mapLayer;
		addSection(MetadataMessage.INSTANCE
				.metadataEditingGeographicalLocationTabHeader());
		reset();
	}

	@Override
	public void isSelected(String mode) {
		super.isSelected(mode);
		if (!created) {
			PointOrRectangularGeographicalExtent aux = new PointOrRectangularGeographicalExtent(
					mapLayer);
			addFullLineComponent(aux);
			if (mode.compareTo(EDIT_MODE) == 0) {
				aux.edit(metadata);
			} else {
				aux.display(metadata);
			}
			created = true;
		}
	}

	@Override
	public void flush(MetadataDTO metadata) {
		if (created) {
			super.flush(metadata);
		} else {
			metadata.getGeographicalLocationPart().setBoxes(
					this.metadata.getGeographicalLocationPart().getBoxes());
		}
	}

	@Override
	public void edit(MetadataDTO metadata) {
		if (mapLayer.compareToIgnoreCase(AreaSelectorWidget.GOOGLE_LAYER) == 0) {
			this.metadata = metadata;
		} else {
			if (created == false) {
				PointOrRectangularGeographicalExtent aux = new PointOrRectangularGeographicalExtent(
						mapLayer);
				addFullLineComponent(aux);
				created = true;
			}
		}
		super.edit(metadata);
	}

	@Override
	public void display(MetadataDTO metadata) {
		if (mapLayer.compareToIgnoreCase(AreaSelectorWidget.GOOGLE_LAYER) == 0) {
			this.metadata = metadata;
		} else {
			if (created == false) {
				PointOrRectangularGeographicalExtent aux = new PointOrRectangularGeographicalExtent(
						mapLayer);
				addFullLineComponent(aux);
				created = true;
			}
		}
		super.display(metadata);
	}
}
