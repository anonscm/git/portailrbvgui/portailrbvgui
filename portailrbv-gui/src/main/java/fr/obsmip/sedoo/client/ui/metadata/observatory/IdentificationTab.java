package fr.obsmip.sedoo.client.ui.metadata.observatory;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.Identifiers;
import fr.sedoo.metadata.client.ui.widget.field.impl.InternetLinks;
import fr.sedoo.metadata.client.ui.widget.field.impl.Logo;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceAbstract;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceTitle;
import fr.sedoo.metadata.client.ui.widget.field.impl.Snapshots;
import fr.sedoo.metadata.client.ui.widget.field.impl.Status;
import fr.sedoo.metadata.client.ui.widget.field.impl.identifier.IdentifierComplementaryEditor;
import fr.sedoo.metadata.client.ui.widget.field.impl.snapshot.SnapshotComplementaryEditor;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class IdentificationTab extends AbstractTab {

	private HorizontalPanel descriptionPanel;
	private Label observatoryDescription = new Label();
	private ObservatoryName name;
	private Logo editLogo;
	private Label nameLabel;
	private Label logoLabel;
	private Label observatorySection;

	public IdentificationTab(ArrayList<String> displayLanguages) {
		super();
		
		descriptionPanel = createDescriptionPanel();
		mainPanel.add(descriptionPanel);
		observatorySection = addSection(MetadataMessage.INSTANCE.observatory());
		nameLabel = new Label(CommonMessages.INSTANCE.name());
		addLeftComponent(nameLabel);
		name = new ObservatoryName();
		addRightComponent(name);
		logoLabel = RBVLabelFactory.getLabelByKey(FieldConstant.LOGO);
		addLeftComponent(logoLabel);
		editLogo = new Logo();
		addRightComponent(editLogo);
		
		addSection(MetadataMessage.INSTANCE.metadataEditingGeneralInformations());
		
		List<IdentifierComplementaryEditor> identifiersComplementaryEditors = new ArrayList<IdentifierComplementaryEditor>();
		identifiersComplementaryEditors.add(name);
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.TITLE));
		addRightComponent(new ResourceTitle(displayLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.ABSTRACT));
		addRightComponent(new ResourceAbstract(displayLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.IDENTIFIERS));
		addRightComponent(new Identifiers(identifiersComplementaryEditors));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.STATUS));
		addRightComponent(new Status());
		addSection(MetadataMessage.INSTANCE.metadataEditingLinks());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.INTERNET_LINKS));
		addRightComponent(new InternetLinks());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.SNAPSHOTS));
		List<SnapshotComplementaryEditor> snapshotComplementaryEditors = new ArrayList<SnapshotComplementaryEditor>();
		snapshotComplementaryEditors.add(editLogo);
		addRightComponent(new Snapshots(snapshotComplementaryEditors));

		reset();
	}
	
	private HorizontalPanel createDescriptionPanel() {
		HorizontalPanel aux = new HorizontalPanel();
		aux.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		aux.add(observatoryDescription);
		Logo displayLogo = new Logo("30px","");
		addComponent(displayLogo);
		observatoryDescription.setStyleName("observatoryDescription");
		aux.add(displayLogo);
		return aux;
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.show(descriptionPanel);
		ElementUtil.hide(name);
		ElementUtil.hide(editLogo);
		ElementUtil.hide(nameLabel);
		ElementUtil.hide(logoLabel);
		ElementUtil.hide(observatorySection);
		observatoryDescription.setText(Message.INSTANCE.observatory()+" "+((ObservatoryDTO)metadata).getName());
		super.display(metadata);
	}

	@Override
	public void edit(MetadataDTO metadata) 
	{
		ElementUtil.hide(descriptionPanel);
		ElementUtil.show(name);
		ElementUtil.show(editLogo);
		ElementUtil.show(nameLabel);
		ElementUtil.show(logoLabel);
		ElementUtil.show(observatorySection);
		super.edit(metadata);
	}
}
