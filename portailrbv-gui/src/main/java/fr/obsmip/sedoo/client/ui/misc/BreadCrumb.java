package fr.obsmip.sedoo.client.ui.misc;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public interface BreadCrumb extends IsWidget {

	void setClientFactory(ClientFactory clientFactory);

	void refresh(List<Shortcut> shortcuts);

	void addShortcut(Shortcut shortcut);

	List<Shortcut> getShortcuts();

	void replaceLast(Shortcut lastShortcut);

	void back();

	int getHeight();

}
