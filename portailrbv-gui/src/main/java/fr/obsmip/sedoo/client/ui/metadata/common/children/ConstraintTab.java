package fr.obsmip.sedoo.client.ui.metadata.common.children;

import java.util.ArrayList;

import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.LabelFactory;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class ConstraintTab extends AbstractTab {

	private ChildrenUseConditions useConditions;
	private ChildrenPublicAccessLimitations publicAccessLimitations;
	

	public ConstraintTab(ArrayList<String> displayLanguages) {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingConstraintTabHeader());
		addLeftComponent(LabelFactory.getLabelByKey(FieldConstant.USE_CONDITIONS));
		useConditions = new ChildrenUseConditions(displayLanguages);
		addFullLineComponent(useConditions);
		addLeftComponent(LabelFactory.getLabelByKey(FieldConstant.PUBLIC_ACCESS_LIMITATIONS));
		publicAccessLimitations = new ChildrenPublicAccessLimitations(displayLanguages);
		addFullLineComponent(publicAccessLimitations);
		reset();
	}
	
	public void setPresenter(ComputeFromParentPresenter presenter) {
		useConditions.setPresenter(presenter);
		publicAccessLimitations.setPresenter(presenter);
	}

	public void setUseConditions(I18nString value) 
	{
		MetadataDTO dto = new MetadataDTO();
		dto.getConstraintPart().setUseConditions(value);
		useConditions.edit(dto);
	}
	
	public void setPublicAccessLimitations(I18nString value) 
	{
		MetadataDTO dto = new MetadataDTO();
		dto.getConstraintPart().setPublicAccessLimitations(value);
		publicAccessLimitations.edit(dto);
	}
	

}
