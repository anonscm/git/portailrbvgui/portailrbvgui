package fr.obsmip.sedoo.client.activity;


public class OLDObservatoryContactEditingActivity {
//	extends AbstractDTOEditingActivity implements Presenter{
//}
//
//	private Long id;
//	private Long observatoryId;
//	private String observatoryShortLabel="";
//	
//
//	public ObservatoryContactEditingActivity(ObservatoryContactEditingPlace place, ClientFactory clientFactory) {
//		super(clientFactory, place);
//		setMode(place.getMode());
//		if (place.getMode().compareTo(Constants.CREATE)==0)
//		{
//			observatoryId = place.getId();
//		}
//		else
//		{
//			id = place.getId();
//		}
//	}
//
//
//	private final ObservatoryServiceAsync observatoryService = GWT.create(ObservatoryService.class);
//
//
//	@Override
//	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
//		
//		if (isValidUser() == false)
//		{
//			goToLoginPlace();
//			return;
//		}
//		sendActivityStartEvent();
//		ObservatoryContactEditingView observatoryContactEditingView = clientFactory.getObservatoryContactEditingView();
//		view = observatoryContactEditingView;
//		observatoryContactEditingView.setPresenter(this);
//		containerWidget.setWidget(observatoryContactEditingView.asWidget());
//		if (getMode().compareTo(Constants.MODIFY) == 0)
//		{
//			observatoryService.getObservatoryContactById(id, new AsyncCallback<ObservatoryContactDTO>() {
//
//				@Override
//				public void onSuccess(ObservatoryContactDTO result) {
//					broadcastActivityTitle(Message.INSTANCE.observatoryContactEditingViewModificationHeader() +" ("+result.getPersonName()+")");
//					previousHash = result.getHash();
//					setObservatoryId(result.getObservatoryId());
//					List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//					shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
//			        observatoryShortLabel = result.getObservatoryShortLabel();
//			        shortcuts.add(ShortcutFactory.getObservatoryModificationShortcut(observatoryShortLabel, observatoryId));
//			        shortcuts.add(ShortcutFactory.getObservatoryContactModificationShortcut(result.getPersonName(), id));
//			        clientFactory.getBreadCrumb().refresh(shortcuts);
//					view.edit(result);
//					view.setMode(Constants.MODIFY);
//				}
//
//				@Override
//				public void onFailure(Throwable caught) {
//					DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened()+" : "+caught.getMessage());
//
//				}
//			});
//			
//			
//		}
//		else
//		//Creation
//		{
//			previousHash = "";
//			broadcastActivityTitle(Message.INSTANCE.observatoryContactEditingViewCreationHeader());
//			observatoryContactEditingView.edit(new ObservatoryContactDTO());
//			observatoryService.getObservatoryById(observatoryId, new AsyncCallback<ObservatoryDTO>() {
//				
//				@Override
//				public void onSuccess(ObservatoryDTO result) {
//					List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//					shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
//			        observatoryShortLabel = result.getShortLabel();
//			        shortcuts.add(ShortcutFactory.getObservatoryModificationShortcut(observatoryShortLabel, observatoryId));
//			        shortcuts.add(ShortcutFactory.getObservatoryContactCreationShortcut(id));
//			        clientFactory.getBreadCrumb().refresh(shortcuts);		
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// Nothing done ...
//					
//				}
//			});
//			view.setMode(Constants.CREATE);
//		}
//	}
//
//
//	
//	
//	
//
//	@Override
//	public void save(final ObservatoryContactDTO observatoryContactDTO) 
//	{
//		List<OldValidationAlert> result = observatoryContactDTO.validate();
//		 if (result.isEmpty() == false)
//		 {
//			 DialogBoxTools.popUp(Message.INSTANCE.error(), OldValidationAlert.toHTML(result), DialogBoxTools.HTML_MODE);
//			 return;
//		 }
//		//Modification mode
//		if (getMode().compareTo(Constants.MODIFY) == 0)
//		{
//			observatoryContactDTO.setId(id);
//		}
//		
//		ActionStartEvent startEvent = new ActionStartEvent(Message.INSTANCE.saving(), ActionEventConstant.OBSERVATORY_CONTACT_SAVING_EVENT, true);
//        clientFactory.getEventBus().fireEvent(startEvent);
//        if (getMode().compareTo(Constants.MODIFY) == 0)
//        {
//        	observatoryService.saveObservatoryContact(observatoryContactDTO, new AsyncCallback<Void>() {
//
//        		@Override
//        		public void onSuccess(Void result) 
//        		{
//        			previousHash = observatoryContactDTO.getHash();
//        			ActionEndEvent e = new ActionEndEvent(ActionEventConstant.OBSERVATORY_CONTACT_SAVING_EVENT);
//        			clientFactory.getEventBus().fireEvent(e);
//        			broadcastActivityTitle(Message.INSTANCE.observatoryContactEditingViewModificationHeader() +" ("+observatoryContactDTO.getPersonName()+")");
//        			view.setMode(Constants.MODIFY);
//        			clientFactory.getEventBus().fireEvent(new NotificationEvent(Message.INSTANCE.savedModifications()));
//        		}
//
//        		@Override
//        		public void onFailure(Throwable caught) {
//
//        			DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened()+" "+caught.getMessage());
//        			ActionEndEvent e = new ActionEndEvent(ActionEventConstant.OBSERVATORY_CONTACT_SAVING_EVENT);
//        			clientFactory.getEventBus().fireEvent(e);
//
//        		}
//        	});
//        }
//        else
//        {
//        	observatoryService.addObservatoryContact(observatoryContactDTO, observatoryId, new AsyncCallback<Long>() {
//        		@Override
//        		public void onSuccess(Long result) 
//        		{
//        			previousHash = observatoryContactDTO.getHash();
//        			ActionEndEvent e = new ActionEndEvent(ActionEventConstant.OBSERVATORY_CONTACT_SAVING_EVENT);
//        			clientFactory.getEventBus().fireEvent(e);
//        			broadcastActivityTitle(Message.INSTANCE.observatoryContactEditingViewModificationHeader() +" ("+observatoryContactDTO.getPersonName()+")");
//        			view.setMode(Constants.MODIFY);
//        			id = result;
//        			List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//					shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryModificationShortcut(observatoryShortLabel, observatoryId));
//			        shortcuts.add(ShortcutFactory.getObservatoryContactModificationShortcut(observatoryContactDTO.getPersonName(), id));
//			        clientFactory.getBreadCrumb().refresh(shortcuts);	
//			        clientFactory.getEventBus().fireEvent(new NotificationEvent(Message.INSTANCE.addedElement()));
//        		}
//
//        		@Override
//        		public void onFailure(Throwable caught) {
//
//        			DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened()+" "+caught.getMessage());
//        			ActionEndEvent e = new ActionEndEvent(ActionEventConstant.OBSERVATORY_CONTACT_SAVING_EVENT);
//        			clientFactory.getEventBus().fireEvent(e);
//
//        		}
//        	});
//        }
//	}
//
//
//
//
//
//
//	public Long getObservatoryId() {
//		return observatoryId;
//	}
//
//
//
//
//
//
//	public void setObservatoryId(Long observatoryId) {
//		this.observatoryId = observatoryId;
//	}
//
//
//
//	@Override
//	public void back() {
//		ObservatoryEditingPlace place = new ObservatoryEditingPlace();
//		place.setObservatoryId(getObservatoryId());
//		clientFactory.getPlaceController().goTo(place);	
//	}

}
