package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;

public interface ExperimentalSiteEntryManagementView extends IsWidget{
	
//	void init(List<ExperimentalSiteSummaryDTO> experimentalSiteSummaries);
	public void broadcastExperimentalSiteDeletion(String uuid);
	
	void setPresenter(Presenter presenter);
	
	public interface Presenter 
	 {
	        void deleteExperimentalSiteEntry(String uuid);
			void goToEditPlace(ExperimentalSiteSummaryDTO experimentalSiteSummaryDTO, ObservatorySummaryDTO observatorySummaryDTO);
			void goToObservatoryEntryCreationPlace();
			void loadExperimentalSites(String uuid);
			void goToExperimentalSiteEntryCreationPlace(String observatoryName);
	 }

	void reset();
	void setObservatories(ArrayList<ObservatorySummaryDTO> availableObservatories);
	void setExperimentalSites(ArrayList<ExperimentalSiteSummaryDTO> experimentalSites);
	

}
