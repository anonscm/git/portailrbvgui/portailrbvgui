package fr.obsmip.sedoo.client.ui.metadata.observatory;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.api.IsObservatory;

public interface ObservatoryEntryCreationView extends IsWidget
{
	void setObservatories(List<IsObservatory> filterByUser);
	void setPresenter(Presenter presenter);
	public interface Presenter 
	 {
	        void create(String observatoryName);
	 }
}
