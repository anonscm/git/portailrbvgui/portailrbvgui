package fr.obsmip.sedoo.client.ui.geonetworkobservatory;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.commons.client.widget.table.ImagedActionCell;

public class ColorCell<C> extends AbstractCell<C> {

	/**
	 * The delegate that will handle events from the cell.
	 * 
	 * @param <T>
	 *            the type that this delegate acts on
	 */
	public static interface Delegate<T> {
		/**
		 * Perform the desired action on the given object.
		 * 
		 * @param object
		 *            the object to be acted upon
		 */
		void execute(T object);
	}

	/**
	 * Construct a new {@link ImagedActionCell}.
	 * 
	 * @param message
	 *            the message to display on the button
	 * @param delegate
	 *            the delegate that will handle events
	 */
	public ColorCell() {
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			C value, SafeHtmlBuilder sb) {
		sb.append(new SafeHtmlBuilder().appendHtmlConstant(
				"<div style=\"width:35%;background-color:" + value
						+ "\" >&nbsp</div>").toSafeHtml());
	}

}