package fr.obsmip.sedoo.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.CellTable;

public interface GlobalBundle extends ClientBundle {
	@NotStrict
	@Source("carnavello.css")
	CssResource css();

	@Source("lightCross.png")
	ImageResource lightCross();

	@Source("darkCross.png")
	ImageResource darkCross();

	@Source("facebook-icon.png")
	ImageResource facebookLogo();

	@Source("google-icon.png")
	ImageResource googleLogo();

	@Source("flag-en.png")
	ImageResource english();

	@Source("flag-fr.png")
	ImageResource french();

	@Source("flag-fr-grey.png")
	ImageResource frenchGrey();

	@Source("flag-en-grey.png")
	ImageResource englishGrey();

	@Source("ajax-loader.gif")
	ImageResource loading();

	@Source("loading.gif")
	ImageResource bigLoading();

	// @Source("help.png")
	@Source("helpBW.png")
	ImageResource help();

	@Source("logo.png")
	ImageResource logo();

	@Source("print.png")
	ImageResource print();

	@Source("view.gif")
	ImageResource view();

	@Source("search.png")
	ImageResource search();

	@Source("upArrow.png")
	ImageResource upArrow();

	@Source("downArrow.png")
	ImageResource downArrow();

	@Source("bandeau.png")
	ImageResource bandeau();

	@Source("delete.png")
	ImageResource delete();

	@Source("delete-locked.png")
	ImageResource lockedDelete();

	@Source("edit.png")
	ImageResource edit();

	@Source("edit-locked.png")
	ImageResource lockedEdit();

	@Source("eye.png")
	ImageResource see();

	@Source("add.png")
	ImageResource add();

	@Source("ok.png")
	ImageResource ok();

	@Source("ko.png")
	ImageResource ko();

	@Source("minimize.png")
	ImageResource minimize();

	@Source("maximize.png")
	ImageResource maximize();

	@Source("puce_fleche_bleu.png")
	ImageResource menuDot();

	@Source("plus.png")
	ImageResource menuPlus();

	@Source("drag.png")
	ImageResource drag();

	@Source("compass.png")
	ImageResource guide();

	@Source("station.png")
	ImageResource marker();

	@Source("drainageBasinDraw.png")
	ImageResource drainageBasinDraw();

	@Source("siteDraw.png")
	ImageResource siteDraw();

	@Source("siteDelete.png")
	ImageResource siteDelete();

	@Source("drainageBasinDelete.png")
	ImageResource drainageBasinDelete();

	@Source("cellTable.css")
	CellTable.Style cellTableStyle();

	@Source("cellList.css")
	CellList.Resources cellListStyle();

	@Source("pdf.png")
	ImageResource pdf();

	@Source("word.png")
	ImageResource word();

	@Source("excel.png")
	ImageResource excel();

	@Source("document.png")
	ImageResource document();

	@Source("zip.png")
	ImageResource zip();
	
	public static final GlobalBundle INSTANCE = GWT.create(GlobalBundle.class);
	//

}