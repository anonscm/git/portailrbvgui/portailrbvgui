package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.misc.ObservatoryListUtil;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryManagementPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryManagementView;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryManagementView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class ExperimentalSiteEntryManagementActivity extends RBVAuthenticatedActivity implements Presenter, LoadCallBack<ArrayList<IsObservatory>> {


	public ExperimentalSiteEntryManagementActivity(ExperimentalSiteEntryManagementPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	private final static  MetadataServiceAsync METADATA_SERVICE = GWT.create(MetadataService.class);
	ExperimentalSiteEntryManagementView experimentalSiteManagementView;
	private AcceptsOneWidget containerWidget;


	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		this.containerWidget = containerWidget;
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		experimentalSiteManagementView = clientFactory.getExperimentalSiteEntryManagementView();
		experimentalSiteManagementView.setPresenter(this);
		containerWidget.setWidget(experimentalSiteManagementView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.experimentalSiteManagementViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getExperimentalSiteEntryManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		experimentalSiteManagementView.reset();
		ClientObservatoryList.getObservatories(this);
	}


	@Override
	public void deleteExperimentalSiteEntry(final String uuid) 
	{
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstant.EXPERIMENTAL_SITE_DELETING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		METADATA_SERVICE.deleteExperimentalSiteEntry(uuid, new DefaultAbstractCallBack<Boolean>(e, clientFactory.getEventBus()) 
		{
			@Override
			public void onSuccess(Boolean result) {
				super.onSuccess(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				experimentalSiteManagementView.broadcastExperimentalSiteDeletion(uuid);
			}
		});
	}

	@Override
	public void goToEditPlace(ExperimentalSiteSummaryDTO experimentalSiteSummaryDTO, ObservatorySummaryDTO observatorySummaryDTO) 
	{
		ExperimentalSiteEditingPlace place = new ExperimentalSiteEditingPlace();
		place.setExperimentalSiteName(experimentalSiteSummaryDTO.getName());
		place.setExperimentalSiteUuid(experimentalSiteSummaryDTO.getUuid());
		place.setParentSummary(observatorySummaryDTO);
		clientFactory.getPlaceController().goTo(place);
	}


	@Override
	public void postLoadProcess(ArrayList<IsObservatory> observatories) 
	{
		ObservatoryListUtil.getAvailableObservatories(observatories,clientFactory,METADATA_SERVICE, new OperationCallBack() {
			@Override
			public void postExecution(boolean result, Object aux) {
				experimentalSiteManagementView.setObservatories((ArrayList<ObservatorySummaryDTO>) aux);
				containerWidget.setWidget(experimentalSiteManagementView);
			}
		});
	}


	@Override
	public void goToObservatoryEntryCreationPlace() {
		clientFactory.getPlaceController().goTo(new ObservatoryEntryCreationPlace());
	}


	@Override
	public void loadExperimentalSites(String parentUuid) {
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.EXPERIMENTAL_SITE_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		METADATA_SERVICE.getExperimentalSitesSummaryFromParentUuid(parentUuid, clientFactory.getDisplayLanguages(), new DefaultAbstractCallBack<ArrayList<ExperimentalSiteSummaryDTO>>(e, clientFactory.getEventBus()) 
				{
					@Override
					public void onSuccess(ArrayList<ExperimentalSiteSummaryDTO> result) {
						super.onSuccess(result);
						experimentalSiteManagementView.setExperimentalSites(result);

					}
				});
	}


	@Override
	public void goToExperimentalSiteEntryCreationPlace(String observatoryName) 
	{
		ExperimentalSiteEntryCreationPlace place = new ExperimentalSiteEntryCreationPlace();
		place.setObservatoryName(observatoryName);
		clientFactory.getPlaceController().goTo(place);
	}

}
