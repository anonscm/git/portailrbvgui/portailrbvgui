package fr.obsmip.sedoo.client.ui.searchcriteria;

import java.util.Iterator;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class DisclosurePanelCustom extends Composite implements HasWidgets {

	private DisclosurePanel panel;

	public DisclosurePanelCustom(String title) {
		panel = new DisclosurePanel(title);
		panel.addStyleName("customDisclosurePanel");
		panel.setAnimationEnabled(true);
		panel.setWidth("100%");
		initWidget(panel);
		setOpen(false);
	}

	@Override
	public void add(Widget w) {
		panel.clear();
		panel.add(w);
	}

	@Override
	public void clear() {
		panel.clear();
	}

	@Override
	public Iterator<Widget> iterator() {
		return panel.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		return panel.remove(w);
	}

	public void setOpen(boolean open) {
		panel.setOpen(open);
	}

}
