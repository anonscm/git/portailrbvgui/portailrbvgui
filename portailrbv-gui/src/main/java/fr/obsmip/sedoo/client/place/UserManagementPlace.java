package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class UserManagementPlace extends Place implements AuthenticatedPlace
{

	public static UserManagementPlace instance;

	public static class Tokenizer implements PlaceTokenizer<UserManagementPlace>
	{
		@Override
		public String getToken(UserManagementPlace place)
		{
			return "";
		}

		@Override
		public UserManagementPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new UserManagementPlace();
			}
			return instance;
		}
	}
	
}
