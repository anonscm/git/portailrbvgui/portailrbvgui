package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public interface SearchServiceAsync {

	void getSummaries(
			SearchCriteriaDTO criteria,
			int position,
			int size,
			ArrayList<String> languages,
			AsyncCallback<HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>>> callback);

	void getHits(SearchCriteriaDTO criteria, AsyncCallback<Integer> callback);

}
