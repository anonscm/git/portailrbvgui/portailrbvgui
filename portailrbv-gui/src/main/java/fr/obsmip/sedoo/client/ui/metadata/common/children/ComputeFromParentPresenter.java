package fr.obsmip.sedoo.client.ui.metadata.common.children;

public interface ComputeFromParentPresenter {

	void getParentUseConditions();
	void getParentPublicAccessLimitations();

}
