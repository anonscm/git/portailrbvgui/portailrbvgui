package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class DatasetEditingPlace extends Place implements AuthenticatedPlace
{
	private String datasetUuid;
	private MetadataSummaryDTO observatorySummary;
	private MetadataSummaryDTO experimentalSiteSummary;
	
	public static DatasetEditingPlace instance;

	public static class Tokenizer implements PlaceTokenizer<DatasetEditingPlace>
	{
		@Override
		public String getToken(DatasetEditingPlace place)
		{
			return StringUtil.trimToEmpty(place.getDatasetUuid());
		}

		@Override
		public DatasetEditingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new DatasetEditingPlace();
			}
			if (StringUtil.isNotEmpty(token))
			{
				instance.setDatasetUuid(token);
			}
			return instance;
		}
	}

	public String getDatasetUuid() {
		return datasetUuid;
	}

	public void setDatasetUuid(String datasetUuid) {
		this.datasetUuid = datasetUuid;
	}

	public MetadataSummaryDTO getObservatorySummary() {
		return observatorySummary;
	}

	public void setObservatorySummary(MetadataSummaryDTO observatorySummary) {
		this.observatorySummary = observatorySummary;
	}

	public MetadataSummaryDTO getExperimentalSiteSummary() {
		return experimentalSiteSummary;
	}

	public void setExperimentalSiteSummary(MetadataSummaryDTO experimentalSiteSummary) {
		this.experimentalSiteSummary = experimentalSiteSummary;
	}

	



	
	
}
