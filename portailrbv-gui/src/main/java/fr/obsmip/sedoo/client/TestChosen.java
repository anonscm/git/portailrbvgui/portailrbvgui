package fr.obsmip.sedoo.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class TestChosen implements EntryPoint{
  /**
   * The message displayed to the user when the server cannot be reached or
   * returns an error.
   */
  	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		RootPanel loadingMessage = RootPanel.get("loadingMessage");
		if (loadingMessage != null)
		{
			DOM.setInnerHTML(loadingMessage.getElement(), "");
		}
		
		DockLayoutPanel centeringPanel = new DockLayoutPanel(Unit.EM);
		centeringPanel.add(new Label("coo"));
		RootLayoutPanel.get().add(centeringPanel);
		Window.enableScrolling(false);
	    Window.setMargin("0px");
		
	}

	
	
	

  
}
