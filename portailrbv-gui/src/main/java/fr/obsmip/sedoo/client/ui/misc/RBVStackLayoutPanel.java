package fr.obsmip.sedoo.client.ui.misc;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.StackLayoutPanel;

public class RBVStackLayoutPanel extends StackLayoutPanel 
{
	private Integer previousWidgetIndex;
	@Override
	public void add(IsWidget widget, IsWidget header, double headerSize) {
		// TODO Auto-generated method stub
		super.add(widget, header, headerSize);
//		PortailRBV.getClientFactory().getEventBus().addHandler(AdministrationActivityStartEvent.TYPE, this);
	}
	
	public RBVStackLayoutPanel(Unit unit) {
		super(unit);
	}
	
	@Override
	public void showWidget(int index) {
		Element elementById = DOM.getElementById("stackHeader-"+index);
		if (previousWidgetIndex != null)
		{
			elementById = DOM.getElementById("stackHeader-"+previousWidgetIndex);
			if (elementById != null)
			{
				elementById.getStyle().setProperty("visibility","visible");
			}
		}
		previousWidgetIndex=index;
		super.showWidget(index);
		elementById = DOM.getElementById("stackHeader-"+index);
		if (elementById != null)
		{
			elementById.getStyle().setProperty("visibility","hidden");
		}
		
	}
	
	/**
	 * Cette méthode masque la pile d'index passé en paramètre.
	 * Il n'existe pas actuellement de methode native dans le composant
	 * L'implémentation est donc un peu frustre et masque directement de grand-parent du composant identifié.
	 * @param index
	 */
	public void hideStack(int index)
	{
		Element elementById = DOM.getElementById("stackHeader-"+index);
		if (elementById != null)
		{
			com.google.gwt.dom.client.Element parentElement = elementById.getParentElement().getParentElement();
			parentElement.getStyle().setProperty("visibility","hidden");
		}
	}
	
	public void showStack(int index)
	{
		Element elementById = DOM.getElementById("stackHeader-"+index);
		if (elementById != null)
		{
		com.google.gwt.dom.client.Element parentElement = elementById.getParentElement().getParentElement();
		parentElement.getStyle().setProperty("visibility","visible");
		}
	}

}
