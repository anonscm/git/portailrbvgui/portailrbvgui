package fr.obsmip.sedoo.client.activity;

import com.google.gwt.event.shared.EventBus;

import fr.sedoo.commons.client.event.ActivityChangeEvent;

public class ActivityHelper {
	
	public static void broadcastActivityTitle(EventBus eventBus, String title)
	{
		eventBus.fireEvent(new ActivityChangeEvent(title));
	}
	

}
