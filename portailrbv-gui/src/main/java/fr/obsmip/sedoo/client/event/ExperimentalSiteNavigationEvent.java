package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ExperimentalSiteNavigationEvent extends GwtEvent<ExperimentalSiteNavigationEventHandler>{

	public static final Type<ExperimentalSiteNavigationEventHandler> TYPE = new Type<ExperimentalSiteNavigationEventHandler>();
	private String experimentalSiteName;
	private String mode;
	private String experimentalSiteUuid;
	

	public ExperimentalSiteNavigationEvent(String experimentalSiteName, String mode, String experimentalSiteUuid)
	{
		this.experimentalSiteName = experimentalSiteName;
		this.setMode(mode);
		this.experimentalSiteUuid = experimentalSiteUuid;
	}

	@Override
	protected void dispatch(ExperimentalSiteNavigationEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ExperimentalSiteNavigationEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getExperimentalSiteName() {
		return experimentalSiteName;
	}

	public String getExperimentalSiteUuid() {
		return experimentalSiteUuid;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
}
