package fr.obsmip.sedoo.client.ui.misc;


/**
 * Widget affichant une carte et des zones de texte Nord, Est, Sud, Ouest <br>
 * - la carte permet le dessin/effacement d'une unique zone rectangulaire et le
 * déplacement de la carte<br>
 * - Les zones sont synchronisées avec le rectangle<br>
 * - Un mode "display" transforme les zones de saisie en zones d'affichage
 * 
 * 
 * @author andre
 * 
 */
public class OLDAreaSelectorWidget

{
//	extends EditDisplayComposite implements ClickHandler
//}
//{
//
//	private static MapWidgetUiBinder uiBinder = GWT.create(MapWidgetUiBinder.class);
//
//	protected static final Projection DEFAULT_PROJECTION = new Projection("EPSG:4326");
//	protected static final String ID_ATTRIBUTE = "localId";
//
//	@UiField
//	TextBox northBoundLatitude;
//
//	@UiField
//	TextBox southBoundLatitude;
//
//	@UiField
//	TextBox eastBoundLongitude;
//
//	@UiField
//	TextBox westBoundLongitude;
//
//	@UiField
//	Label northBoundLatitudeDisplay;
//
//	@UiField
//	Label southBoundLatitudeDisplay;
//
//	@UiField
//	Label eastBoundLongitudeDisplay;
//
//	@UiField
//	Label westBoundLongitudeDisplay;
//
//	protected Map map;
//
//	private VerticalPanel mapPanel;
//
//	private boolean autoswitchToDragControl = true;
//
//	private final List<RectangularAreaListener> listeners = new ArrayList<RectangularAreaListener>();
//
//	@UiField
//	VerticalPanel contentPanel;
//
//	ToggleButton drawRectangularAreaButton;
//	ToggleButton eraseRectangularAreaButton;
//	ToggleButton dragPanButton;
//
//	protected DrawFeature drawRectangularAreaControl = null;
//	private SelectFeature hoverRectangularAreaSelectFeature = null;
//
//	private final NumberFormat doubleFormatter = NumberFormat.getFormat("#.0000");
//
//	VectorFeature reactangularAreaFeature;
//	Vector rectangularAreaLayer;
//
//	private HorizontalPanel toolBar;
//
//	private String width = "500px";
//
//	private List<ToggleButton> buttonList;
//
//	interface MapWidgetUiBinder extends UiBinder<Widget, OLDAreaSelectorWidget>
//	{
//	}
//
//	@UiConstructor
//	public OLDAreaSelectorWidget(String width)
//	{
//		initWidget(uiBinder.createAndBindUi(this));
//		if ((width != null) && (width.trim().length() > 0))
//		{
//			this.width = width;
//		}
//		init();
//	}
//
//	protected void init()
//	{
//
//		editWidgets.add(northBoundLatitude);
//		editWidgets.add(southBoundLatitude);
//		editWidgets.add(eastBoundLongitude);
//		editWidgets.add(westBoundLongitude);
//
//		displayWidgets.add(northBoundLatitudeDisplay);
//		displayWidgets.add(southBoundLatitudeDisplay);
//		displayWidgets.add(eastBoundLongitudeDisplay);
//		displayWidgets.add(westBoundLongitudeDisplay);
//
//		MapOptions defaultMapOptions = new MapOptions();
//		defaultMapOptions.setDisplayProjection(new Projection("EPSG:4326"));
//		defaultMapOptions.setNumZoomLevels(20);
//
//		MapWidget mapWidget = new MapWidget(width, width, defaultMapOptions);
//
//		map = mapWidget.getMap();
//
//		// addGoogleLayers(map);
//		addOSMLayers(map);
//
//		// Lets add some default controls to the map
//		// map.addControl(new LayerSwitcher()); // + sign in the upperright
//		// corner
//		// to display the layer switcher
//		map.addControl(new OverviewMap()); // + sign in the lowerright to
//		// display the overviewmap
//		map.addControl(new ScaleLine()); // Display the scaleline
//		map.addControl(new Navigation());
//		map.addControl(new DragPan());
//
//		MousePositionOutput mpOut = new MousePositionOutput()
//		{
//			@Override
//			public String format(LonLat lonLat, Map map)
//			{
//				String out = "";
//				out += "<span style=\"color:white;background-color:black\"><b>Longitude </b> ";
//				out += doubleFormatter.format(lonLat.lon());
//				out += "<b>, Latitude</b> ";
//				out += doubleFormatter.format(lonLat.lat());
//				out += "</span>";
//				return out;
//			}
//		};
//
//		MousePositionOptions mpOptions = new MousePositionOptions();
//		mpOptions.setFormatOutput(mpOut); // rename to setFormatOutput
//
//		map.addControl(new MousePosition(mpOptions));
//
//		rectangularAreaLayer = new Vector("RectangularAreaLayer");
//		map.addLayer(rectangularAreaLayer);
//
//		RectangularAreaLayerListener rectangularAreaLayerListener = new RectangularAreaLayerListener();
//
//		// Contrôle gérant le dessin du rectangle
//		DrawFeatureOptions rectangularAreaOptions = new DrawFeatureOptions();
//		rectangularAreaOptions.onFeatureAdded(rectangularAreaLayerListener);
//		RegularPolygonHandlerOptions regularPolygonHandlerOptions = new RegularPolygonHandlerOptions();
//		regularPolygonHandlerOptions.setSides(4);
//		regularPolygonHandlerOptions.setIrregular(true);
//		rectangularAreaOptions.setHandlerOptions(regularPolygonHandlerOptions);
//		drawRectangularAreaControl = new DrawFeature(rectangularAreaLayer, new RegularPolygonHandler(), rectangularAreaOptions);
//
//		// Contrôle gérant la selection du bassin versant
//		SelectFeatureOptions drainageBasinHoverSelectFeatureOptions = new SelectFeatureOptions();
//		drainageBasinHoverSelectFeatureOptions.setHover();
//		drainageBasinHoverSelectFeatureOptions.clickFeature(rectangularAreaLayerListener);
//		hoverRectangularAreaSelectFeature = new SelectFeature(rectangularAreaLayer, drainageBasinHoverSelectFeatureOptions);
//
//		drawRectangularAreaControl.deactivate();
//		hoverRectangularAreaSelectFeature.deactivate();
//
//		map.addControl(drawRectangularAreaControl);
//		map.addControl(hoverRectangularAreaSelectFeature);
//
//		// final DockLayoutPanel dock = new DockLayoutPanel(Unit.PX);
//		// dock.addNorth(map, 500);
//
//		// force the map to fall behind popups
//		mapWidget.getElement().getFirstChildElement().getStyle().setZIndex(0);
//
//		mapPanel = new VerticalPanel();
//		mapPanel.add(mapWidget);
//
//		mapWidget.addDomHandler(new MapMouseOverHandler(), MouseOverEvent.getType());
//		toolBar = new HorizontalPanel();
//		toolBar.setStylePrimaryName("map-selector-toolbar");
//		toolBar.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
//		toolBar.setSpacing(3);
//		drawRectangularAreaButton = new ToggleButton(new Image(GlobalBundle.INSTANCE.drainageBasinDraw()));
//		drawRectangularAreaButton.setTitle(Message.INSTANCE.drawRectangularAreaTooltip());
//		eraseRectangularAreaButton = new ToggleButton(new Image(GlobalBundle.INSTANCE.drainageBasinDelete()));
//		eraseRectangularAreaButton.setTitle(Message.INSTANCE.eraseRectangularAreaTooltip());
//		dragPanButton = new ToggleButton(new Image(GlobalBundle.INSTANCE.drag()));
//		dragPanButton.setTitle(Message.INSTANCE.dragPanButtonTooltip());
//		initToolbar();
//
//		dragPanButton.setDown(true);
//		contentPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
//		contentPanel.add(toolBar);
//		contentPanel.add(mapPanel);
//	}
//
//	public void setDrawRectangularAreaTooltip(String text)
//	{
//		drawRectangularAreaButton.setTitle(text);
//	}
//
//	public void setEraseRectangularAreaTooltip(String text)
//	{
//		eraseRectangularAreaButton.setTitle(text);
//	}
//
//	private void addOSMLayers(Map map)
//	{
//		OSM osm_1 = OSM.Mapnik("Mapnik");
//		//OSM osm_2 = OSM.CycleMap("CycleMap");
//		osm_1.setIsBaseLayer(true);
//		//osm_2.setIsBaseLayer(true);
//		
//		map.addLayer(osm_1);
//		//map.addLayer(osm_2);
//	}
//
//	protected List<ToggleButton> getToolBarButtons()
//	{
//		if (buttonList == null)
//		{
//			buttonList = new ArrayList<ToggleButton>();
//			buttonList.add(drawRectangularAreaButton);
//			buttonList.add(eraseRectangularAreaButton);
//			buttonList.add(dragPanButton);
//		}
//		return buttonList;
//	}
//
//	private void addGoogleLayers(Map map)
//	{
//
//		GoogleV3Options gHybridOptions = new GoogleV3Options();
//		gHybridOptions.setIsBaseLayer(true);
//		gHybridOptions.setType(GoogleV3MapType.G_HYBRID_MAP);
//		GoogleV3 gHybrid = new GoogleV3("Google Hybrid", gHybridOptions);
//
//		GoogleV3Options gNormalOptions = new GoogleV3Options();
//		gNormalOptions.setIsBaseLayer(true);
//		gNormalOptions.setType(GoogleV3MapType.G_NORMAL_MAP);
//		GoogleV3 gNormal = new GoogleV3("Google Normal", gNormalOptions);
//
//		GoogleV3Options gSatelliteOptions = new GoogleV3Options();
//		gSatelliteOptions.setIsBaseLayer(true);
//		gSatelliteOptions.setType(GoogleV3MapType.G_SATELLITE_MAP);
//		GoogleV3 gSatellite = new GoogleV3("Google Satellite", gSatelliteOptions);
//
//		GoogleV3Options gTerrainOptions = new GoogleV3Options();
//		gTerrainOptions.setIsBaseLayer(true);
//		gTerrainOptions.setType(GoogleV3MapType.G_TERRAIN_MAP);
//		GoogleV3 gTerrain = new GoogleV3("Google Terrain", gTerrainOptions);
//
//		map.addLayer(gHybrid);
//		map.addLayer(gNormal);
//		map.addLayer(gSatellite);
//		map.addLayer(gTerrain);
//
//	}
//
//	protected void initToolbar()
//	{
//		Iterator<ToggleButton> iterator = getToolBarButtons().iterator();
//		while (iterator.hasNext())
//		{
//			ToggleButton toggleButton = iterator.next();
//			toggleButton.addClickHandler(this);
//			toolBar.add(toggleButton);
//
//		}
//	}
//
//	/**
//	 * This handler force the updateSize commande whenever the mouse enters the
//	 * map. It avoids the offset between the mouse pointer and drawn feature in
//	 * case of an inner scrolled div
//	 * 
//	 * @author francois
//	 * 
//	 */
//	class MapMouseOverHandler implements MouseOverHandler
//	{
//
//		@Override
//		public void onMouseOver(MouseOverEvent event)
//		{
//			map.updateSize();
//		}
//
//	}
//
//	protected void resetTextFields() {
//		northBoundLatitude.setText("");
//		southBoundLatitude.setText("");
//		eastBoundLongitude.setText("");
//		westBoundLongitude.setText("");
//	}
//
//	
//	class RectangularAreaLayerListener implements ClickFeatureListener, FeatureAddedListener
//	{
//
//		@Override
//		public void onFeatureClicked(VectorFeature vectorFeature)
//		{
//			vectorFeature.destroy();
//			resetTextFields();
//			reactangularAreaFeature = null;
//		}
//
//		
//		@Override
//		public void onFeatureAdded(VectorFeature vectorFeature)
//		{
//			if (reactangularAreaFeature != null)
//			{
//				reactangularAreaFeature.destroy();
//			}
//			Bounds bounds = vectorFeature.getGeometry().getBounds();
//			LonLat lowerLeft = new LonLat(bounds.getLowerLeftX(), bounds.getLowerLeftY());
//			LonLat upperRight = new LonLat(bounds.getUpperRightX(), bounds.getUpperRightY());
//			lowerLeft.transform(map.getProjection(), DEFAULT_PROJECTION.getProjectionCode());
//			upperRight.transform(map.getProjection(), DEFAULT_PROJECTION.getProjectionCode());
//			northBoundLatitude.setText(format(upperRight.lat()));
//			eastBoundLongitude.setText(format(upperRight.lon()));
//			southBoundLatitude.setText(format(lowerLeft.lat()));
//			westBoundLongitude.setText(format(lowerLeft.lon()));
//			reactangularAreaFeature = vectorFeature;
//
//			// On rebascule en mode "drag" afin d'éviter une fausse manip de
//			// l'utilisateur
//
//			if (isAutoswitchToDragControl())
//			{
//				drawRectangularAreaControl.deactivate();
//				dragPanButton.setDown(true);
//				drawRectangularAreaButton.setDown(false);
//			}
//
//			Iterator<RectangularAreaListener> iterator = listeners.iterator();
//			while (iterator.hasNext())
//			{
//				iterator.next().onRectangleAdded();
//			}
//
//		}
//	}
//
//	String format(double value)
//	{
//		return doubleFormatter.format(value);
//	}
//
//	public GeographicBoundingBoxDTO getGeographicBoundingBoxDTO()
//	{
//		GeographicBoundingBoxDTO result = new GeographicBoundingBoxDTO();
//		result.setEastBoundLongitude(eastBoundLongitude.getText());
//		result.setWestBoundLongitude(westBoundLongitude.getText());
//		result.setSouthBoundLatitude(southBoundLatitude.getText());
//		result.setNorthBoundLatitude(northBoundLatitude.getText());
//		return result;
//	}
//
//	public void setGeographicBoundingBoxDTO(GeographicBoundingBoxDTO box)
//	{
//		reset();
//
//		northBoundLatitude.setText(box.getNorthBoundLatitude());
//		northBoundLatitudeDisplay.setText(box.getNorthBoundLatitude());
//		southBoundLatitude.setText(box.getSouthBoundLatitude());
//		southBoundLatitudeDisplay.setText(box.getSouthBoundLatitude());
//		eastBoundLongitude.setText(box.getEastBoundLongitude());
//		eastBoundLongitudeDisplay.setText(box.getEastBoundLongitude());
//		westBoundLongitude.setText(box.getWestBoundLongitude());
//		westBoundLongitudeDisplay.setText(box.getWestBoundLongitude());
//
//		center(box, true);
//	}
//
//	public void reset()
//	{
//
//		// RAZ des zones de textes
//		resetTextFields();
//
//		// RAZ des zones d'affichage
//		northBoundLatitudeDisplay.setText("");
//		southBoundLatitudeDisplay.setText("");
//		eastBoundLongitudeDisplay.setText("");
//		westBoundLongitudeDisplay.setText("");
//
//		// Suppression du rectange existant
//		rectangularAreaLayer.destroyFeatures();
//
//		deactivateAllControls();
//	}
//
//	public void center(GeographicBoundingBoxDTO box, boolean displayDefault)
//	{
//
//		LonLat rightLowerDisplay;
//		LonLat leftUpperDisplay;
//
//		boolean correctBox = false;
//
//		if (box.validate().isEmpty())
//		{
//			correctBox = true;
//			LonLat rightLower = box.getRightLowerCorner();
//			LonLat leftUpper = box.getLeftUpperCorner();
//
//			rightLowerDisplay = box.getRightLowerDisplayCorner();
//			leftUpperDisplay = box.getLeftUpperDisplayCorner();
//
//			rightLower.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
//			leftUpper.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
//
//			Bounds drainageBasinBounds = new Bounds();
//			drainageBasinBounds.extend(rightLower);
//			drainageBasinBounds.extend(leftUpper);
//			reactangularAreaFeature = new VectorFeature(drainageBasinBounds.toGeometry());
//			rectangularAreaLayer.addFeature(reactangularAreaFeature);
//		} else
//		{
//			// On affiche une carte complète du monde par défaut
//			rightLowerDisplay = new LonLat(-170, -80);
//			leftUpperDisplay = new LonLat(170, 80);
//		}
//
//		rightLowerDisplay.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
//		leftUpperDisplay.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
//
//		Bounds displayBounds = new Bounds();
//		displayBounds.extend(rightLowerDisplay);
//		displayBounds.extend(leftUpperDisplay);
//		if ((correctBox == true) || (displayDefault == true))
//		{
//			map.zoomToExtent(displayBounds, true);
//		}
//	}
//
//	public void activateDrawBasinControl()
//	{
//		Iterator<ToggleButton> iterator = getToolBarButtons().iterator();
//		while (iterator.hasNext())
//		{
//			ToggleButton toggleButton = iterator.next();
//			toggleButton.setDown(false);
//		}
//		deactivateAllControls();
//		drawRectangularAreaButton.setDown(true);
//		drawRectangularAreaControl.activate();
//	}
//
//	@Override
//	public void onClick(ClickEvent event)
//	{
//		if (event.getSource() instanceof ToggleButton)
//		{
//			Iterator<ToggleButton> iterator = getToolBarButtons().iterator();
//			while (iterator.hasNext())
//			{
//				ToggleButton toggleButton = iterator.next();
//				toggleButton.setDown(false);
//			}
//			((ToggleButton) event.getSource()).setDown(true);
//
//			deactivateAllControls();
//
//			if (event.getSource() == drawRectangularAreaButton)
//			{
//				drawRectangularAreaControl.activate();
//			}
//
//			if (event.getSource() == eraseRectangularAreaButton)
//			{
//				hoverRectangularAreaSelectFeature.activate();
//			}
//
//		}
//	}
//
//	protected void deactivateAllControls()
//	{
//		drawRectangularAreaControl.deactivate();
//		hoverRectangularAreaSelectFeature.deactivate();
//	}
//
//	@Override
//	public void enableDisplayMode()
//	{
//		changeButtonVisibility(false);
//		super.enableDisplayMode();
//	}
//
//	@Override
//	public void enableEditMode()
//	{
//		changeButtonVisibility(true);
//		super.enableEditMode();
//	}
//
//	protected void changeButtonVisibility(boolean visible)
//	{
//		Iterator<ToggleButton> iterator = getToolBarButtons().iterator();
//		while (iterator.hasNext())
//		{
//			ToggleButton toggleButton = iterator.next();
//			toggleButton.setVisible(visible);
//		}
//	}
//
//	@UiHandler(value = { "northBoundLatitude", "southBoundLatitude", "eastBoundLongitude", "westBoundLongitude" })
//	void onBoxChanged(ChangeEvent event)
//	{
//		rectangularAreaLayer.destroyFeatures();
//		Iterator<RectangularAreaListener> iterator = listeners.iterator();
//		while (iterator.hasNext())
//		{
//			iterator.next().onRectangleChanged();
//		}
//		center(getGeographicBoundingBoxDTO(), false);
//
//	}
//
//	public boolean isAutoswitchToDragControl()
//	{
//		return autoswitchToDragControl;
//	}
//
//	public void setAutoswitchToDragControl(boolean autoswitchToDragControl)
//	{
//		this.autoswitchToDragControl = autoswitchToDragControl;
//	}
//
//	public void addListener(RectangularAreaListener listener)
//	{
//		listeners.add(listener);
//	}

}
