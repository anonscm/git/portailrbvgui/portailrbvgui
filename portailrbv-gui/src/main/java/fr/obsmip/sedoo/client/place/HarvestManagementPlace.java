package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class HarvestManagementPlace extends Place implements AuthenticatedPlace
{

	public static HarvestManagementPlace instance;

	public static class Tokenizer implements PlaceTokenizer<HarvestManagementPlace>
	{
		@Override
		public String getToken(HarvestManagementPlace place)
		{
			return "";
		}

		@Override
		public HarvestManagementPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new HarvestManagementPlace();
			}
			return instance;
		}
	}
	
}
