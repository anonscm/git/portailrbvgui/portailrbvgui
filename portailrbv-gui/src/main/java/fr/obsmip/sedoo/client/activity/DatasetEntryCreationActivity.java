package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.misc.ObservatoryListUtil;
import fr.obsmip.sedoo.client.place.DatasetEditingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetEntryCreationView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class DatasetEntryCreationActivity extends RBVAuthenticatedActivity
		implements Presenter, LoadCallBack<ArrayList<IsObservatory>> {

	private final static MetadataServiceAsync METADATA_SERVICE = GWT
			.create(MetadataService.class);

	private DatasetEntryCreationView view;

	private AcceptsOneWidget containerWidget;

	private String observatoryName;

	private String experimentalSiteName;

	public DatasetEntryCreationActivity(DatasetEntryCreationPlace place,
			ClientFactory clientFactory) {
		super(clientFactory, place);
		observatoryName = place.getObservatoryName();
		experimentalSiteName = place.getExperimentalSiteName();
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		this.containerWidget = containerWidget;
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		broadcastActivityTitle(Message.INSTANCE.datasetEntryCreationHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getDatasetEntryCreationShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		containerWidget.setWidget(clientFactory.getProgressView());
		view = ((ClientFactory) clientFactory).getDatasetEntryCreationView();
		view.setPresenter(this);
		view.reset();
		ClientObservatoryList.getObservatories(this);
	}

	@Override
	public void postLoadProcess(ArrayList<IsObservatory> observatories) {
		ObservatoryListUtil.getAvailableObservatories(observatories,
				clientFactory, METADATA_SERVICE, new OperationCallBack() {
					@Override
					public void postExecution(boolean result, Object aux) {
						if (StringUtil.isNotEmpty(observatoryName)) {
							view.setObservatories(
									(List<ObservatorySummaryDTO>) aux,
									observatoryName);
						} else {
							view.setObservatories((List<ObservatorySummaryDTO>) aux);
						}
						containerWidget.setWidget(view);
					}
				});

	}

	@Override
	public void create(ObservatorySummaryDTO observatorySummary,
			ExperimentalSiteSummaryDTO experimentalSiteSummary) {
		DatasetEditingPlace place = new DatasetEditingPlace();
		place.setObservatorySummary(observatorySummary);
		place.setExperimentalSiteSummary(experimentalSiteSummary);
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void goToObservatoryEntryCreationPlace() {
		clientFactory.getPlaceController().goTo(
				new ObservatoryEntryCreationPlace());
	}

	@Override
	public void goToExperimentalSiteEntryCreationPlace(String observatoryName) {
		ExperimentalSiteEntryCreationPlace place = new ExperimentalSiteEntryCreationPlace();
		place.setObservatoryName(observatoryName);
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void loadExperimentalSiteFromObservatoryUuid(String observatoryUuid) {
		List<ExperimentalSiteSummaryDTO> result = new ArrayList<ExperimentalSiteSummaryDTO>();
		ActionStartEvent startEvent = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.EXPERIMENTAL_SITE_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		METADATA_SERVICE
				.getExperimentalSitesSummaryFromParentUuid(
						observatoryUuid,
						clientFactory.getDisplayLanguages(),
						new DefaultAbstractCallBack<ArrayList<ExperimentalSiteSummaryDTO>>(
								startEvent, clientFactory.getEventBus()) {

							@Override
							public void onSuccess(
									ArrayList<ExperimentalSiteSummaryDTO> result) {
								super.onSuccess(result);
								view.setExperimentalSites(result);
								if (StringUtil.isNotEmpty(experimentalSiteName)) {
									view.selectExperimentalSiteFromUuid(experimentalSiteName);
									experimentalSiteName = "";
								}
							}

						});
	}

}