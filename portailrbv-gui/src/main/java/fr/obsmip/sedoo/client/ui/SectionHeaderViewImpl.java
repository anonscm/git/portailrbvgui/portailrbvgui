package fr.obsmip.sedoo.client.ui;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.cms.bundle.MenuMessages;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.util.StringUtil;

public class SectionHeaderViewImpl extends Composite implements
		SectionHeaderView {

	private static SectionHeaderViewImplUiBinder uiBinder = GWT
			.create(SectionHeaderViewImplUiBinder.class);

	interface SectionHeaderViewImplUiBinder extends
			UiBinder<Widget, SectionHeaderViewImpl> {
	}

	@UiField
	Label sectionTitle;

	public SectionHeaderViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		sectionTitle.setText("");
	}

	@Override
	public void onNotification(ActivityChangeEvent event) {
		String screenTitle = null;
		HashMap<String, String> screenTitles = PortailRBV.getClientFactory()
				.getScreenNames();
		if (screenTitles != null) {
			screenTitle = screenTitles.get(event.getActivityTitle());
		}
		if (screenTitle != null && StringUtil.isNotEmpty(screenTitle)) {
			sectionTitle.setText(screenTitle);
		} else if (!event.getActivityTitle().equals(
				CMSMessages.INSTANCE.screenManagement())
				&& !event.getActivityTitle().equals(
						MenuMessages.INSTANCE.menuEdition())) {
			sectionTitle.setText(event.getActivityTitle());
		} else {
			sectionTitle.setText("");
		}
	}

	@Override
	public int getHeight() {
		return 50;
	}

}
