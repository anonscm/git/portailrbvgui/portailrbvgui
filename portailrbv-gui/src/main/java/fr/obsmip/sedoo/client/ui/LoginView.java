package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;

public interface LoginView extends IsWidget {

	void setPresenter(Presenter presenter);
	void updateSucces();
	void updateFailure();
	void setLoginFirstMessageVisible(boolean b);
	void reset();
	
	public interface Presenter 
	 {
	        void login(String login, String password);
			void loginWithGoogle();
			void loginWithLinkedIn();
			void loginWithFacebook();
	 }

	void activateLoginButtons();

	
}
