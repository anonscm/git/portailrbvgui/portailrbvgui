package fr.obsmip.sedoo.client.ui.searchcriteria;

import java.util.ArrayList;

import com.google.gwt.event.shared.GwtEvent;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;


public class ObservatoryCatalogsLoadedEvent extends GwtEvent<ObservatoryCatalogsLoadedEventHandler> {

	public static final Type<ObservatoryCatalogsLoadedEventHandler> TYPE = new Type<ObservatoryCatalogsLoadedEventHandler>();
	private ArrayList<GeonetworkObservatoryDTO> observatoryCatalogs;

	public ObservatoryCatalogsLoadedEvent(ArrayList<GeonetworkObservatoryDTO> observatoryCatalogs) {
		this.setobservatoryCatalogs(observatoryCatalogs);
	}

	@Override
	protected void dispatch(ObservatoryCatalogsLoadedEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ObservatoryCatalogsLoadedEventHandler> getAssociatedType() {
		return TYPE;
	}

	public ArrayList<GeonetworkObservatoryDTO> getobservatoryCatalogs() {
		return observatoryCatalogs;
	}

	public void setobservatoryCatalogs(ArrayList<GeonetworkObservatoryDTO> observatoryCatalogs) {
		this.observatoryCatalogs = observatoryCatalogs;
	}

}