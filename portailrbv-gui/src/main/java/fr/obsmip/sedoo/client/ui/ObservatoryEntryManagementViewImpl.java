package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.CellTableResources;
import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.ui.table.ObservatoryEntrySummaryTable;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.obsmip.sedoo.shared.util.IsObservatoryComparator;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class ObservatoryEntryManagementViewImpl extends AbstractSection implements ObservatoryEntryManagementView
{

	private static ObservatoryManagementViewImplUiBinder uiBinder = GWT.create(ObservatoryManagementViewImplUiBinder.class);

	interface ObservatoryManagementViewImplUiBinder extends UiBinder<Widget, ObservatoryEntryManagementViewImpl>
	{
	}

	//	protected List<ObservatoryDTO> observatories;

	//	private CellTable<ObservatoryDTO> observatoryTable;

	@UiField
	ObservatoryEntrySummaryTable summaryTable;

	@UiField
	Alert noObservatoryCreated;
	
	@UiField
	VerticalPanel tablePanel;

	protected Image deleteImage = new Image(GlobalBundle.INSTANCE.delete());
	protected Image editImage = new Image(GlobalBundle.INSTANCE.edit());

	private Presenter presenter;

	public ObservatoryEntryManagementViewImpl()
	{
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		CellTableResources.INSTANCE.cellTableStyle().ensureInjected();
		summaryTable.init(new ArrayList<HasIdentifier>());
		summaryTable.hideToolBar();
	}

	//	ListDataProvider<ObservatoryDTO> dataProvider = new ListDataProvider<ObservatoryDTO>();

	//	@Override
	//	public void init(List<ObservatoryDTO> observatories)
	//	{
	//		this.observatories = observatories;
	//		observatoryPanel.clear();
	//		observatoryTable = new CellTable<ObservatoryDTO>(Integer.MAX_VALUE, CellTableResources.INSTANCE);
	//		if ((observatories == null) || (observatories.size() == 0))
	//		{
	//			emptyList.setVisible(true);
	//		} else
	//		{
	//			emptyList.setVisible(false);
	//		}
	//		observatoryTable.setWidth("100%", false);
	//
	//		Cell<ObservatoryDTO> deleteActionCell = new ImagedActionCell<ObservatoryDTO>(deleteAction, this.deleteImage, "Supprimer");
	//		Column<ObservatoryDTO, String> deleteColumn = new Column(deleteActionCell)
	//		{
	//
	//			@Override
	//			public ObservatoryDTO getValue(Object object)
	//			{
	//				return (ObservatoryDTO) object;
	//			}
	//		};
	//
	//		Cell<ObservatoryDTO> editActionCell = new ImagedActionCell<ObservatoryDTO>(editAction, this.editImage, "Editer");
	//		Column<ObservatoryDTO, String> editColumn = new Column(editActionCell)
	//		{
	//
	//			@Override
	//			public ObservatoryDTO getValue(Object object)
	//			{
	//				return (ObservatoryDTO) object;
	//			}
	//		};
	//
	//		TextColumn<ObservatoryDTO> nameColumn = new TextColumn<ObservatoryDTO>()
	//		{
	//			@Override
	//			public String getValue(ObservatoryDTO observatory)
	//			{
	//				return observatory.getShortLabel();
	//			}
	//		};
	//
	//		TextColumn<ObservatoryDTO> observationColumn = new TextColumn<ObservatoryDTO>()
	//		{
	//			@Override
	//			public String getValue(ObservatoryDTO observatory)
	//			{
	//				String aux = observatory.getDescription();
	//				// aux = aux.replace("\n", "<br />");
	//				if (aux.length() > 200)
	//				{
	//					return aux.substring(0, 200) + "...";
	//				} else
	//				{
	//					return aux;
	//				}
	//			}
	//		};
	//
	//		observatoryTable.addColumn(nameColumn, "Name");
	//		observatoryTable.addColumn(observationColumn, "Description");
	//		observatoryTable.addColumn(editColumn);
	//		observatoryTable.addColumn(deleteColumn);
	//
	//		observatoryTable.setColumnWidth(nameColumn, 100.0, Unit.PX);
	//		observatoryTable.setColumnWidth(observationColumn, 200.0, Unit.PX);
	//		observatoryTable.setColumnWidth(editColumn, 30.0, Unit.PX);
	//		observatoryTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);
	//
	//		dataProvider.addDataDisplay(observatoryTable);
	//		List<ObservatoryDTO> list = dataProvider.getList();
	//		list.clear();
	//		list.addAll(observatories);
	//		observatoryTable.setRowData(dataProvider.getList());
	//		observatoryPanel.add(observatoryTable);
	//
	//	}
	//
	//	Delegate<ObservatoryDTO> deleteAction = new Delegate<ObservatoryDTO>()
	//	{
	//
	//		@Override
	//		public void execute(final ObservatoryDTO observatoryDTO)
	//		{
	//
	//			Message message = GWT.create(Message.class);
	//			ConfirmCallBack callBack = new ConfirmCallBack()
	//			{
	//
	//				@Override
	//				public void confirm(boolean choice)
	//				{
	//					if (choice == true)
	//					{
	//						Long id = observatoryDTO.getId();
	//						ListIterator<ObservatoryDTO> listIterator = observatories.listIterator();
	//						while (listIterator.hasNext())
	//						{
	//							ObservatoryDTO aux = listIterator.next();
	//							if (id.compareTo(aux.getId()) == 0)
	//							{
	//								presenter.deleteObservatory(aux.getId());
	//								break;
	//							}
	//						}
	//					}
	//
	//				}
	//			};
	//
	//			DialogBoxTools.modalConfirm(message.confirm(), message.observatoryDeletionConfirmationMessage(), callBack).center();
	//
	//		}
	//
	//	};
	//
	//	Delegate<ObservatoryDTO> editAction = new Delegate<ObservatoryDTO>()
	//	{
	//
	//		@Override
	//		public void execute(ObservatoryDTO observatoryDTO)
	//		{
	//
	//			presenter.goToEditObservatoryActivity(observatoryDTO);
	//
	//		}
	//
	//	};
	//
	@Override
	public void setPresenter(Presenter presenter)
	{
		this.presenter = presenter;
		summaryTable.setPresenter(presenter);
	}

	@Override
	public void init(List<ObservatorySummaryDTO> observatorySummaries) {
		Collections.sort(observatorySummaries, new IsObservatoryComparator());
		summaryTable.init(observatorySummaries);
		ensureAlertVisibility();
	}

	/*
	 * We switch the visibility of the alert message if the table is empty
	 */
	private void ensureAlertVisibility() {
		noObservatoryCreated.setVisible(summaryTable.getModel().size()==0);
		if (summaryTable.getModel().size()==0)
		{
			ElementUtil.hide(tablePanel);
		}
		else
		{
			ElementUtil.show(tablePanel);
		}
	}

	@Override
	public void broadcastObservatoryDeletion(String uuid) 
	{
		summaryTable.removeRow(uuid);
		ensureAlertVisibility();
	}

	@UiHandler("observatoryEntryCreationLink")
	 void onSaveButtonClicked(ClickEvent event) 
	 {
		 presenter.goToObservatoryEntryCreationPlace();
	 }

}
