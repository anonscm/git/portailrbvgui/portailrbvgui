package fr.obsmip.sedoo.client.ui.menu;

import com.google.gwt.user.client.ui.VerticalPanel;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.GeonetworkObservatoryManagementPlace;
import fr.obsmip.sedoo.client.place.LogConsultationPlace;
import fr.obsmip.sedoo.client.place.SystemPlace;
import fr.obsmip.sedoo.client.place.UserManagementPlace;
import fr.sedoo.metadata.client.place.FormatListPlace;

public class AdministrationMenu extends AbstractMenu {
	
public AdministrationMenu() {
	
		VerticalPanel panel = getAdministrationPanel(); 
	    initWidget(panel);
	}

private VerticalPanel getAdministrationPanel() {
	VerticalPanel administrationPanel = new VerticalPanel();
	administrationPanel.setSpacing(4);
	
	administrationPanel.add(createMenuLink(Message.INSTANCE.observatoryManagement(), new GeonetworkObservatoryManagementPlace()));
	administrationPanel.add(createMenuLink(Message.INSTANCE.userManagementViewHeader(),new UserManagementPlace()));
	//administrationPanel.add(createMenuLink(Message.INSTANCE.messageManagementViewHeader(), new MessageManagePlace()));
	administrationPanel.add(createMenuLink(Message.INSTANCE.formatViewHeader(), new FormatListPlace()));
	administrationPanel.add(createMenuLink(Message.INSTANCE.systemViewHeader(), new SystemPlace()));
	administrationPanel.add(createMenuLink(Message.INSTANCE.logConsultationViewHeader(), new LogConsultationPlace()));
//	administrationPanel.add(createMenuLink(Message.INSTANCE.harvestManagement(), new HarvestManagementPlace()));
	return administrationPanel;
}

}
