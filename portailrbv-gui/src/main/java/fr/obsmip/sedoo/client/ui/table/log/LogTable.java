package fr.obsmip.sedoo.client.ui.table.log;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.obsmip.sedoo.client.activity.LogConsultationActivity;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.shared.domain.LogDTO;
import fr.sedoo.commons.client.longlist.widget.LongListTable;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class LogTable extends LongListTable{


	private static DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy - HH:mm");

	public LogTable() {
		super();
	}


	protected void initColumns() 
	{

		TextColumn<HasIdentifier> dateColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				LogDTO log = (LogDTO) aux;
				if ( (log != null) && (log.getDate() != null))
				{
					return fmt.format(log.getDate());
				}
				else
				{
					return "";
				}
			}
		};

		TextColumn<HasIdentifier> categoryColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				if (aux == null)
				{
					return "";
				}
				else
				{
					LogDTO log = (LogDTO) aux;
					return log.getCategory();
				}
			}
		};

		TextColumn<HasIdentifier> actionColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				if (aux == null)
				{
					return "";
				}
				else
				{
					LogDTO log = (LogDTO) aux;
					return log.getAction();
				}
			}
		};

		TextColumn<HasIdentifier> userColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				if (aux == null)
				{
					return "";
				}
				else
				{
					LogDTO log = (LogDTO) aux;
					return log.getUser();
				}
			}
		};

		TextColumn<HasIdentifier> detailColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				if (aux == null)
				{
					return "";
				}
				else
				{
					LogDTO log = (LogDTO) aux;
					return log.getDetail();
				}
			}
		};

		table.addColumn(dateColumn, CommonMessages.INSTANCE.date());
		table.setColumnWidth(dateColumn, 50.0, Unit.PX);
		table.addColumn(categoryColumn, Message.INSTANCE.logConsultationViewCategory());
		table.setColumnWidth(categoryColumn, 50.0, Unit.PX);
		table.addColumn(actionColumn, Message.INSTANCE.logConsultationViewAction());
		table.setColumnWidth(actionColumn, 70.0, Unit.PX);
		table.addColumn(userColumn, Message.INSTANCE.logConsultationViewUser());
		table.setColumnWidth(userColumn, 70.0, Unit.PX);
		table.addColumn(detailColumn, Message.INSTANCE.logConsultationViewDetail());
		table.setColumnWidth(detailColumn, 70.0, Unit.PX);
	}


	@Override
	protected int getPageSize() {
		return LogConsultationActivity.DEFAULT_PAGE_SIZE;
	}

}
