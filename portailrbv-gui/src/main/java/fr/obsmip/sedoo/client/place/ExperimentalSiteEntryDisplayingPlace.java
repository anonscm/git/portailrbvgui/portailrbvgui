package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;
import fr.sedoo.commons.client.util.StringUtil;

public class ExperimentalSiteEntryDisplayingPlace extends Place implements AuthenticatedPlace
{

	private String uuid;
	
	public static ExperimentalSiteEntryDisplayingPlace instance;
	
	public static class Tokenizer implements PlaceTokenizer<ExperimentalSiteEntryDisplayingPlace>
	{
		@Override
		public String getToken(ExperimentalSiteEntryDisplayingPlace place)
		{
			return StringUtil.trimToEmpty(place.getUuid());
		}

		@Override
		public ExperimentalSiteEntryDisplayingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ExperimentalSiteEntryDisplayingPlace();
			}
			if (StringUtil.isNotEmpty(token))
			{
				instance.setUuid(token);
			}
			return instance;
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
