package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Label;

import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.Identifiers;
import fr.sedoo.metadata.client.ui.widget.field.impl.InternetLinks;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceAbstract;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceTitle;
import fr.sedoo.metadata.client.ui.widget.field.impl.Snapshots;
import fr.sedoo.metadata.client.ui.widget.field.impl.Status;
import fr.sedoo.metadata.client.ui.widget.field.impl.identifier.IdentifierComplementaryEditor;
import fr.sedoo.metadata.client.ui.widget.field.impl.snapshot.SnapshotComplementaryEditor;

public class IdentificationTab extends AbstractTab {

	public IdentificationTab(ArrayList<String> displayLanguages) {
		super();
		addSection(MetadataMessage.INSTANCE.experimentalSite());
		addLeftComponent(new Label(CommonMessages.INSTANCE.name()));
		ExperimentalSiteName name = new ExperimentalSiteName();
		addRightComponent(name);
		
		addSection(MetadataMessage.INSTANCE.metadataEditingGeneralInformations());
		
		List<IdentifierComplementaryEditor> identifiersComplementaryEditors = new ArrayList<IdentifierComplementaryEditor>();
		identifiersComplementaryEditors.add(name);
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.TITLE));
		addRightComponent(new ResourceTitle(displayLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.ABSTRACT));
		addRightComponent(new ResourceAbstract(displayLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.IDENTIFIERS));
		addRightComponent(new Identifiers(identifiersComplementaryEditors));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.STATUS));
		addRightComponent(new Status());
		addSection(MetadataMessage.INSTANCE.metadataEditingLinks());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.INTERNET_LINKS));
		addRightComponent(new InternetLinks());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.SNAPSHOTS));
		List<SnapshotComplementaryEditor> snapshotComplementaryEditors = new ArrayList<SnapshotComplementaryEditor>();
		addRightComponent(new Snapshots(snapshotComplementaryEditors));

		reset();
	}


}
