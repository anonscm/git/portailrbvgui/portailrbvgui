package fr.obsmip.sedoo.client.activity;

import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.mvp.place.LoginPlace;

public abstract class RBVAuthenticatedActivity extends RBVAbstractActivity
{

	private Place wishedPlace;

	public RBVAuthenticatedActivity(ClientFactory clientFactory, Place wishedPlace) {
		super(clientFactory);
		this.wishedPlace = wishedPlace;
	}

	protected boolean isValidUser()
	{
		if (PortailRBV.getClientFactory().getUserManager().getUser() == null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	protected void goToLoginPlace()
	{
		LoginPlace loginPlace = new LoginPlace();
		loginPlace.setWishedPlace(wishedPlace);
		PortailRBV.getPresenter().goTo(loginPlace);
	}
	
	protected void sendActivityStartEvent()
	{
		clientFactory.getEventBus().fireEvent(new ActivityStartEvent(ActivityStartEvent.MEMBER_ACTIVITY));
	}
}
