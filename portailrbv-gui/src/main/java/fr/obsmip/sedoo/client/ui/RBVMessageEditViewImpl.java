package fr.obsmip.sedoo.client.ui;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.ui.TabPanel;

import fr.sedoo.commons.client.news.ui.MessageEditViewImpl;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class RBVMessageEditViewImpl extends MessageEditViewImpl{

	public RBVMessageEditViewImpl(String displayLanguage) {
		super(displayLanguage);
		getBackButton().removeStyleName("btn-primary");
		getSaveButton().removeStyleName("btn-primary");
		
	}

	@Override
	public void setContent(HashMap<String, TitledMessage> messages,
			List<String> languages, String externaImagelUrl,
			String illustrationImageType, String fileName) {
		super.setContent(messages, languages, externaImagelUrl, illustrationImageType,
				fileName);
		TabPanel panel = getTabPanel();
		int widgetCount = panel.getWidgetCount();
		System.out.println(widgetCount);
		//panel.remove(widgetCount-2);
		//panel.remove(widgetCount-2);

	}
}
