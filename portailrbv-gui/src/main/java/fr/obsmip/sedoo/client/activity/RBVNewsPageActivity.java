package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.place.NewsPlace;
import fr.obsmip.sedoo.client.ui.NewsView;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class RBVNewsPageActivity extends AbstractActivity implements NewsListPresenter {

	public final static MessageServiceAsync MESSAGE_SERVICE = GWT
			.create(MessageService.class);
	
	private NewsView newsView; 
	
	private ClientFactory clientFactory;
	private NewsPlace place;

	public RBVNewsPageActivity(NewsPlace place,	ClientFactory clientFactory) {
		this.place = place;
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, final EventBus eventBus) {
		newsView = clientFactory.getNewsView();
		newsView.setNewsListPresenter(this);
		containerWidget.setWidget(newsView.asWidget());
		broadcastActivityTitle(CommonMessages.INSTANCE.news());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory
				.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
		
		//if (newsView.areNewsLoaded() == false) {
			newsView.reset();
			final ActionStartEvent e = new ActionStartEvent(
					CommonMessages.INSTANCE.loading(),
					ActionEventConstants.NEWS_LOADING_EVENT, true);
			eventBus.fireEvent(e);
			MESSAGE_SERVICE.getLastTitledMessage(
					LocaleUtil.getClientLocaleLanguage(clientFactory
							.getDefaultLanguage()), clientFactory
							.getLanguages(),
					new DefaultAbstractCallBack<ArrayList<TitledMessage>>(e,
							eventBus) {
						@Override
						public void onSuccess(
								final ArrayList<TitledMessage> otherNews) {
							MESSAGE_SERVICE.getCarouselNews(LocaleUtil
											.getClientLocaleLanguage(clientFactory
													.getDefaultLanguage()),
									clientFactory.getLanguages(),
									new DefaultAbstractCallBack<ArrayList<TitledMessage>>(
											e, eventBus) {
										@Override
										public void onSuccess(
												ArrayList<TitledMessage> carouselNews) {
											super.onSuccess(otherNews);
											newsView.setNews(carouselNews,
													otherNews);
										}
									});
						}
					});
		//}
		
	}

	protected void broadcastActivityTitle(String title) {
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
	}
		
	@Override
	public void displayNew(String newsUuid) {
		clientFactory.getPlaceController().goTo(new NewDisplayPlace(newsUuid));
	}
	
	@Override
	public void loadPageEntries(int pageNumber) {
		// Nothing is done this way

	}

}
