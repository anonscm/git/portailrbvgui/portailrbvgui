package fr.obsmip.sedoo.client.ui.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.message.Message;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.component.api.HorizontalMenu;
import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.commons.client.event.UserLoginEvent;
import fr.sedoo.commons.client.event.UserLoginEventHandler;
import fr.sedoo.commons.client.event.UserLogoutEvent;
import fr.sedoo.commons.client.event.UserLogoutEventHandler;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.CommonClientFactory;

public class HorizontalMenuImpl extends Composite implements HorizontalMenu,
		UserLoginEventHandler, UserLogoutEventHandler {

	private static final String MENU_IMAGE = "menuImage";

	@UiField
	MenuBar menu;

	@UiField
	HorizontalPanel contentPanel;

	private static HorizontalMenuImplUiBinder uiBinder = GWT
			.create(HorizontalMenuImplUiBinder.class);

	private MenuItem administrationMenu;

	private SiteAdministrationMenu administrationMenuContent;

	interface HorizontalMenuImplUiBinder extends
			UiBinder<Widget, HorizontalMenuImpl> {
	}

	public HorizontalMenuImpl(CommonClientFactory clientFactory) {
		super();
		EventBus eventBus = clientFactory.getEventBus();
		// GWT.<GlobalBundle> create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));

		menu.addItem(CommonMessages.INSTANCE.home(), new PlaceCommand(eventBus,
				new CMSConsultPlace(ScreenNames.HOME)));
		menu.addItem(Message.INSTANCE.links(), new PlaceCommand(eventBus,
				new CMSConsultPlace(ScreenNames.LINKS)));
		// menu.addItem(ApplicationMessages.INSTANCE.menuStations(),new
		// PlaceCommand(eventBus, new StationsPlace()));

	}

	private SafeHtml createSafeHtml(ImageResource resource, String tootltip) {
		Image aux = AbstractImagePrototype.create(resource).createImage();
		aux.setTitle(tootltip);
		return SafeHtmlUtils.fromTrustedString(aux.toString());
	}

	@Override
	public double getPreferredHeight() {
		return 30;
	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		administrationMenu.setVisible(false);
	}

	@Override
	public void onNotification(UserLoginEvent event) {
		administrationMenuContent.updateContent();
		administrationMenu.setVisible(true);
	}

}
