package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public class SearchEvent extends GwtEvent<SearchEventHandler> {

	public static final Type<SearchEventHandler> TYPE = new Type<SearchEventHandler>();
	private SearchCriteriaDTO criteria;

	public SearchCriteriaDTO getCriteria() {
		return criteria;
	}

	public void setCriteria(SearchCriteriaDTO criteria) {
		this.criteria = criteria;
	}

	public SearchEvent(SearchCriteriaDTO criteria) {
		this.setCriteria(criteria);
	}

	@Override
	protected void dispatch(SearchEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SearchEventHandler> getAssociatedType() {
		return TYPE;
	}

}
