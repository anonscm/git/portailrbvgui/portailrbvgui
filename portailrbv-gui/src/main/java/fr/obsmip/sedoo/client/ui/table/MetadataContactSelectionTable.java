package fr.obsmip.sedoo.client.ui.table;


public class MetadataContactSelectionTable
{
//	extends MetadataContactTable {
//}
//
//	MultiSelectionModel<HasIdentifier> selectionModel;
//	Presenter presenter;
//	String role;
//	
//	public MetadataContactSelectionTable(String role)
//	{
//		super();
//		this.role = role;
//		if (role == null)
//		{
//			addRoleColumn();
//		}
//		toolBarPanel.setVisible(false);
//	}
//	
//	
//	public void setPresenter(Presenter presenter) {
//		this.presenter = presenter;
//	}
//	
//	public void refresh()
//	{
//		itemTable.setVisibleRangeAndClearData(itemTable.getVisibleRange(), true);
//		presenter.getObservatoryContacts(this);
//	}
//	
//	@Override
//	protected void initColumns() {
//		
//		itemTable.setSelectionModel(selectionModel, DefaultSelectionEventManager.<HasIdentifier> createCheckboxManager());
//		
//		 Column<HasIdentifier, Boolean> checkColumn = new Column<HasIdentifier, Boolean>(
//			        new CheckboxCell(true, false)) {
//			      @Override
//			      public Boolean getValue(HasIdentifier object) {
//			        // Get the value from the selection model.
//			        return selectionModel.isSelected(object);
//			      }
//			    };
//		
//		itemTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
//		itemTable.setColumnWidth(checkColumn, 40, Unit.PX);
//		
//		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getPersonName();
//			}
//		};
//		itemTable.addColumn(nameColumn, Message.INSTANCE.personPersonName());
//		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);
//		
//		TextColumn<HasIdentifier> emailColumn = new TextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getEmail();
//			}
//		};
//		itemTable.addColumn(emailColumn, Message.INSTANCE.personEmail());
//		itemTable.setColumnWidth(emailColumn, 100.0, Unit.PX);
//		
//		TextColumn<HasIdentifier> organisationColumn = new TextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getOrganisationName();
//			}
//		};
//		itemTable.addColumn(organisationColumn, Message.INSTANCE.personOrganisationName());
//		itemTable.setColumnWidth(organisationColumn, 100.0, Unit.PX);
//		
//		MultiLineTextColumn<HasIdentifier> adressColumn = new MultiLineTextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getCompleteAdress();
//			}
//		};
//		itemTable.addColumn(adressColumn, Message.INSTANCE.personAddress());
//		itemTable.setColumnWidth(adressColumn, 100.0, Unit.PX);
//		
//	}
//
//	
//	private void addRoleColumn()
//	{
//		MultiLineTextColumn<HasIdentifier> roleColumn = new MultiLineTextColumn<HasIdentifier>(RoleRenderer.getInstance()) {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getRoles();
//			}
//		};
//		itemTable.addColumn(roleColumn, Message.INSTANCE.personRoles());
//		itemTable.setColumnWidth(roleColumn, 100.0, Unit.PX);
//	}
//	
//	public void reset() {
//			if (selectionModel != null)
//			{
//				selectionModel.clear();
//			}
//			List<MetadataContactDTO> aux = new ArrayList<MetadataContactDTO>();
//			init(aux);
//	}
//	
//	public List<MetadataContactDTO> getSelectedItems()
//	{
//		Set<HasIdentifier> selectedSet = selectionModel.getSelectedSet();
//		List<MetadataContactDTO> selection = new ArrayList<MetadataContactDTO>();
//		Iterator<HasIdentifier> iterator = selectedSet.iterator();
//		while (iterator.hasNext()) 
//		{
//			selection.add((MetadataContactDTO) iterator.next());
//		}
//		return selection;
//	}
//	
//	@Override
//	public void init(List<? extends HasIdentifier> model) {
//		if (role == null)
//		{
//			//On affiche tous les utilisateurs quelque soit leur rôle
//			super.init(model);
//		}
//		else
//		{
//			//On filtre les éléments pour ne garder que les elements correspondant au rôle
//			List<MetadataContactDTO> filteredList = new ArrayList<MetadataContactDTO>();
//			if (model != null)
//			{
//				Iterator<? extends HasIdentifier> iterator = model.iterator();
//				while (iterator.hasNext()) {
//					MetadataContactDTO contact = (MetadataContactDTO) iterator.next();
//					if (contact.getRoles().contains(role))
//					{
//						filteredList.add(contact);
//					}
//				}
//			}
//			super.init(filteredList);
//		}
//	}
//
//
//	

}
