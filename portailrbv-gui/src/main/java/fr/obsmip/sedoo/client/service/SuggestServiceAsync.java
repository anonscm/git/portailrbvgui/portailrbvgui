package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SuggestServiceAsync {

	void getObservatoryNames(AsyncCallback<ArrayList<String>> callback);

}
