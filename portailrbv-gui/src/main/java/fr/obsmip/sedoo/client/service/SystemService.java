package fr.obsmip.sedoo.client.service;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("system")
public interface SystemService extends RemoteService {
	
	String getApplicationVersion();
	String getJavaVersion();
	String getDatabaseInformations();
	String getParameter(String parameterName);
	HashMap<String, String> getParameters();
	
}

