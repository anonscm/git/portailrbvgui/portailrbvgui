package fr.obsmip.sedoo.client.ui.searchcriteria;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

public abstract class AbstractSearchCriteria implements IsSerializable {

	protected GeographicBoundingBoxDTO geoBBDTO = new GeographicBoundingBoxDTO();
	public static String VALUE_SEPARATOR = SeparatorUtil.COMMA_SEPARATOR;
	public static final String BOX_PARAM_NAME = "box";
	public static final String CIRCLE_PARAM_NAME = "circle";
	public String PARAM_SEPARATOR = "=";

	public GeographicBoundingBoxDTO getGeoBBDTO() {
		return geoBBDTO;
	}

	public void setGeoBBDTO(GeographicBoundingBoxDTO geoBBDTO) {
		this.geoBBDTO = geoBBDTO;
	}

}
