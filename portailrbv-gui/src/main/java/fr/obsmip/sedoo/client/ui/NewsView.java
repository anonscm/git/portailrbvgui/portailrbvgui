package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.news.event.MessageChangeEventHandler;
import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.shared.domain.New;

public interface NewsView extends IsWidget, MessageChangeEventHandler{
	boolean areNewsLoaded();

	void reset();

	void setNews(ArrayList<? extends New> frontPageNews,
			ArrayList<? extends New> otherNews);

	void setNewsListPresenter(NewsListPresenter presenter);
}
