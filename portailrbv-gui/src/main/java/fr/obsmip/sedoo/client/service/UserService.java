package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.util.ServiceException;

@RemoteServiceRelativePath("user")
public interface UserService extends RemoteService {

	RBVUserDTO login(String login, String password) throws Exception;
	ArrayList<RBVUserDTO> findAll() throws ServiceException;
	void delete(RBVUserDTO user) throws ServiceException;
	RBVUserDTO edit(RBVUserDTO user) throws ServiceException;
	RBVUserDTO create(RBVUserDTO user) throws ServiceException;

}

