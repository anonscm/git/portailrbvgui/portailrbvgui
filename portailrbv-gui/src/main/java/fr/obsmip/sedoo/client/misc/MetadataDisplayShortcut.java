package fr.obsmip.sedoo.client.misc;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class MetadataDisplayShortcut extends Shortcut {

	public MetadataDisplayShortcut() {
		super(Message.INSTANCE.metadataConsultation(), new WelcomePlace());
	}

}
