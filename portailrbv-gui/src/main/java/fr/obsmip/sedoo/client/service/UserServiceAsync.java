package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.RBVUserDTO;

public interface UserServiceAsync {

	void login(String login, String password, AsyncCallback<RBVUserDTO> callback);

	void create(RBVUserDTO user, AsyncCallback<RBVUserDTO> callback);

	void edit(RBVUserDTO user, AsyncCallback<RBVUserDTO> callback);

	void findAll(AsyncCallback<ArrayList<RBVUserDTO>> callback);

	void delete(RBVUserDTO user, AsyncCallback<Void> callback);


}
