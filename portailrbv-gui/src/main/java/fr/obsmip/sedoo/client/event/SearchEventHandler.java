package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SearchEventHandler extends EventHandler {
	void onNotification(SearchEvent event);
}
