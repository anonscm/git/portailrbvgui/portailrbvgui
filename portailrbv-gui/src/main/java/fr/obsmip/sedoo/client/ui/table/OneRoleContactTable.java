package fr.obsmip.sedoo.client.ui.table;

import fr.sedoo.commons.client.widget.table.AbstractListTable;

public abstract class OneRoleContactTable extends AbstractListTable {

//	private ContactListDialogBoxContent listContent = null;
//	
//	private Image addFromDirectoryImage;
//	protected Label addFromDirectoryLabel;
//	
//	private Presenter presenter;
//	
//	private boolean editColumnInitialized = false;
//	private boolean displayColumnInitialized = false;
//	
//	private String role;
//	
//	public OneRoleContactTable()
//	{
//		super();
//		setAskDeletionConfirmation(false);
//		setAddButtonEnabled(true);
//		addDirectoryButton();
//	}
//	
//	private void addDirectoryButton() {
//		addFromDirectoryImage = new Image(GlobalBundle.INSTANCE.add());
//		addFromDirectoryImage.setStyleName("clickable");
//		toolBarPanel.add(addFromDirectoryImage);
//		addFromDirectoryLabel = new Label(Message.INSTANCE.metadataEditingAddContactFromDirectory());
//		addFromDirectoryLabel.setStyleName("clickable");
//		toolBarPanel.add(addFromDirectoryLabel);	
//		addFromDirectoryImage.addClickHandler(this);
//		addFromDirectoryLabel.addClickHandler(this);
//	}
//
//	@Override
//	public void addItem() {
//		
//		NoRoleContactDialogBoxContent content = new NoRoleContactDialogBoxContent();
//		ConfirmCallBack callBack = new CreateConfirmCallBack(content); 
//		content.setConfirmCallback(callBack);
//		MetadataContactDTO newContact = new MetadataContactDTO();
//		newContact.setRoles(getRole());
//		content.edit(newContact);
//		DialogBoxTools.popUp(Message.INSTANCE.metadataContactTableAddItemText(), content);
//	}
//	
//	
//	public void addItemFromDirectory() {
//		listContent = new ContactListDialogBoxContent(new ConfirmCallBack() {;
//		
//			@Override
//			public void confirm(boolean choice) {
//				if (choice == true)
//				{
//					List<MetadataContactDTO> resultList = getListContent().getResultList();
//					if (resultList.isEmpty()==false)
//					{
//						Long maxId = 0L;
//						List<MetadataContactDTO> newValues = new ArrayList<MetadataContactDTO>();
//						Iterator<? extends HasId> iterator = model.iterator();
//						while (iterator.hasNext()) 
//						{
//							MetadataContactDTO aux = (MetadataContactDTO) iterator.next();
//							newValues.add(aux);
//							if (aux.getId()> maxId)
//							{
//								maxId = aux.getId();
//							}
//						}
//						Long index = maxId+1;
//						Iterator<MetadataContactDTO> resultIterator = resultList.iterator();
//						while (resultIterator.hasNext()) 
//						{
//							MetadataContactDTO metadataContactDTO = (MetadataContactDTO) resultIterator.next();
//							metadataContactDTO.setId(index);
//							index++;
//							newValues.add(metadataContactDTO);
//						}
//						init(newValues);
//					}
//				}
//			}
//		},getPresenter(),getRole());
//		
//		DialogBoxTools.popUp(Message.INSTANCE.metadataEditingSelectContactFromList(), listContent,"800px","300px");
//	}
//
//	@Override
//	public String getAddItemText() {
//		return Message.INSTANCE.metadataContactTableAddItemText();
//	}
//
//	@Override
//	public void presenterDelete(HasIdentifier hasId) {
//		removeRow(hasId.getIdentifier());
//	}
//
//	@Override
//	public void presenterEdit(HasIdentifier hasId) {
//		final String id = hasId.getIdentifier();
////		NoRoleContactDialogBoxContent content = new NoRoleContactDialogBoxContent();
////		ConfirmCallBack callBack = new EditConfirmCallBack(id, content); 
////		content.setConfirmCallback(callBack);
////		content.edit((MetadataContactDTO)getItemById(id));
////		DialogBoxTools.popUp(Message.INSTANCE.metadataContactTableAddItemText(), content);		
//	}
//	
//	@Override
//	protected void initColumns() {
//		
//		
//	}
//
//	public ContactListDialogBoxContent getListContent() {
//		return listContent;
//	}
//
//	public void reset() {
//			List<IdentifiedString> aux = new ArrayList<IdentifiedString>();
//			init(aux);
//	}
//	
//	public void onClick(ClickEvent event)
//	{
//		super.onClick(event);
//		if ((event.getSource() == addFromDirectoryImage) || (event.getSource() == addFromDirectoryLabel))
//		{
//			addItemFromDirectory();
//		}
//	}
//
//	public Presenter getPresenter() {
//		return presenter;
//	}
//
//	public void setPresenter(Presenter presenter) {
//		this.presenter = presenter;
//	}
//	
//	public void enableEditMode()
//	{
//		toolBarPanel.setVisible(true);
//		initEditColumns();
//	}
//	
//	public void enableDisplayMode()
//	{
//		toolBarPanel.setVisible(false);
//		initDisplayColumns();
//	}
//	
//	protected void initEditColumns()
//	{
//		if (editColumnInitialized == false)
//		{
//			initLocalColumns();
//			super.initColumns();
//			editColumnInitialized = true;
//		}
//		setAddButtonEnabled(true);
//	}
//	
//	private void initLocalColumns()
//	{
//		TextColumn<HasId> nameColumn = new TextColumn<HasId>() {
//			@Override
//			public String getValue(HasId aux) {
//				return ((MetadataContactDTO) aux).getPersonName();
//			}
//		};
//		itemTable.addColumn(nameColumn, Message.INSTANCE.personPersonName());
//		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);
//		
//		TextColumn<HasIdentifier> emailColumn = new TextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getEmail();
//			}
//		};
//		itemTable.addColumn(emailColumn, Message.INSTANCE.personEmail());
//		itemTable.setColumnWidth(emailColumn, 100.0, Unit.PX);
//		
//		TextColumn<HasIdentifier> organisationColumn = new TextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getOrganisationName();
//			}
//		};
//		itemTable.addColumn(organisationColumn, Message.INSTANCE.personOrganisationName());
//		itemTable.setColumnWidth(organisationColumn, 100.0, Unit.PX);
//		
//		MultiLineTextColumn<HasIdentifier> adressColumn = new MultiLineTextColumn<HasIdentifier>() {
//			@Override
//			public String getValue(HasIdentifier aux) {
//				return ((MetadataContactDTO) aux).getCompleteAdress();
//			}
//		};
//		itemTable.addColumn(adressColumn, Message.INSTANCE.personAddress());
//		itemTable.setColumnWidth(adressColumn, 100.0, Unit.PX);
//		
//	}
//	
//	
//	protected void initDisplayColumns()
//	{
//		if (displayColumnInitialized == false)
//		{
//			initLocalColumns();
//			displayColumnInitialized = true;
//		}
//		
//		setAddButtonEnabled(false);
//	}
//
//	public void setRole(String role) {
//		this.role = role;
//	}
//
//	public String getRole() {
//		return role;
//	}
//	
//	
//	private class CreateConfirmCallBack implements ConfirmCallBack
//	{
//		MetadataContactDTOEditor editor;
//		
//		public CreateConfirmCallBack(MetadataContactDTOEditor editor) {
//			this.editor = editor;
//			}
//		
//		@Override
//		public void confirm(boolean choice) {
//			if (choice == true)
//			{
//				Long maxId = 0L;
//				MetadataContactDTO resultValue = editor.getValue();
//				List<MetadataContactDTO> newValues = new ArrayList<MetadataContactDTO>();
//				Iterator<? extends HasIdentifier> iterator = model.iterator();
//				while (iterator.hasNext()) 
//				{
//					MetadataContactDTO aux = (MetadataContactDTO) iterator.next();
//					newValues.add(aux);
//					if (aux.getId()> maxId)
//					{
//						maxId = aux.getId();
//					}
//				}
//				resultValue.setId(maxId+1);
//				newValues.add(resultValue);
//				init(newValues);
//			}
//		}
//	}
//	
//	
//	private class EditConfirmCallBack implements ConfirmCallBack {
//	
//	Long id;
//	MetadataContactDTOEditor editor;
//		
//	public EditConfirmCallBack(Long id, MetadataContactDTOEditor editor) {
//		this.id = id;
//		this.editor = editor;
//		}
//
//	@Override
//	public void confirm(boolean choice) {
//		if (choice == true)
//		{
//			MetadataContactDTO resultValue = editor.getValue();
//			resultValue.setId(id);
//			List<MetadataContactDTO> newValues = new ArrayList<MetadataContactDTO>();
//			Iterator<? extends HasIdentifier> iterator = model.listIterator();
//			while (iterator.hasNext()) 
//			{
//				MetadataContactDTO aux = (MetadataContactDTO) iterator.next();
//				if (aux.getId()==id)
//				{
//					newValues.add(resultValue);
//				}
//				else
//				{
//					newValues.add(aux);
//				}
//			}
//			init(newValues);
//		}
//	}
//}
}
