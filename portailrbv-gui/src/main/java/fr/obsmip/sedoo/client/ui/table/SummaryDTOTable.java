package fr.obsmip.sedoo.client.ui.table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.MetadataPrintHandler;
import fr.obsmip.sedoo.client.ui.MetadataManagingView.Presenter;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.table.AbstractPagedTable;
import fr.sedoo.commons.client.widget.table.FormattedCellTable;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.ImagedActionCell.Delegate;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public abstract class SummaryDTOTable extends AbstractPagedTable
{

//	protected static final int MAX_ABSTRACT_LENGTH = 150;
//	protected Column<HasIdentifier, String> viewColumn;
//	protected Column<HasIdentifier, String> printColumn;
//	protected Column<HasIdentifier, String> lockableEditColumn;
//	protected Column<HasIdentifier, String> lockableDeleteColumn;
//	protected final Image editImage = new Image(GlobalBundle.INSTANCE.edit());
//	protected final Image lockedEdit = new Image(GlobalBundle.INSTANCE.lockedEdit());
//	protected final Image deleteImage = new Image(GlobalBundle.INSTANCE.delete());
//	protected final Image lockedDelete = new Image(GlobalBundle.INSTANCE.lockedDelete());
//
//	private Presenter presenter;
//	private Long drainageBasinId;
//	private Long observatoryId;
//	private List<String> editableObservatories = new ArrayList<String>();
//
//	private List<SummaryDTO> currentDisplayedValues;
//
//	public SummaryDTOTable()
//	{
//		super();
//		setAddButtonEnabled(false);
//		setToolBarPanelVisible(false);
//
//		Cell<HasIdentifier> editActionCell = new LockableSummaryDTOActionCell<HasIdentifier>(editAction, editImage, lockedEdit, CommonMessages.INSTANCE.edit());
//		lockableEditColumn = new Column(editActionCell)
//		{
//
//			@Override
//			public HasIdentifier getValue(Object object)
//			{
//				return (SummaryDTO) object;
//			}
//			
//		};
//
//		Cell<HasIdentifier> deleteActionCell = new LockableSummaryDTOActionCell<HasIdentifier>(deleteAction, deleteImage, lockedDelete, CommonMessages.INSTANCE.delete());
//		lockableDeleteColumn = new Column(deleteActionCell)
//		{
//
//			@Override
//			public HasIdentifier getValue(Object object)
//			{
//				return (SummaryDTO) object;
//			}
//		};
//
//		Cell<HasIdentifier> viewActionCell = new ImagedActionCell<HasIdentifier>(viewAction, new Image(GlobalBundle.INSTANCE.view()), Message.INSTANCE.view());
//		viewColumn = new Column(viewActionCell)
//		{
//
//			@Override
//			public HasIdentifier getValue(Object object)
//			{
//				return (HasIdentifier) object;
//			}
//		};
//
//		Cell<HasIdentifier> printActionCell = new ImagedActionCell<HasIdentifier>(printAction, new Image(GlobalBundle.INSTANCE.print()), Message.INSTANCE.print());
//		printColumn = new Column(printActionCell)
//		{
//
//			@Override
//			public HasIdentifier getValue(Object object)
//			{
//				return (HasIdentifier) object;
//			}
//		};
//
//		localInitColumns();
//	}
//
//	@Override
//	public void addItem()
//	{
//
//	}
//
//	private boolean isEditable(SummaryDTO dto) 
//	{
//		String observatoryName = StringUtil.trimToEmpty(dto.g());
//		if (editableObservatories.contains(observatoryName))
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//	
//	@Override
//	public String getAddItemText()
//	{
//		return Message.INSTANCE.addMetadataEntry();
//	}
//
//	@Override
//	public void presenterDelete(HasIdentifier hasId)
//	{
//		SummaryDTO aux = (SummaryDTO) hasId;
//		presenter.deleteMetadata(aux.getUuid(), aux.getResourceTitleDisplay());
//	}
//
//	@Override
//	public void presenterEdit(HasIdentifier hasId)
//	{
//
//		presenter.editMetadata(((SummaryDTO) hasId).getUuid(), ((SummaryDTO) hasId).getObservatoryName());
//
//	}
//
//	@Override
//	public void reset()
//	{
//
//		currentDisplayedValues = new ArrayList<SummaryDTO>();
//		super.reset();
//	}
//
//	@Override
//	protected void initColumns()
//	{
//	}
//
//	private void localInitColumns()
//	{
//		TextColumn<HasIdentifier> titleColumn = new TextColumn<HasIdentifier>()
//		{
//			@Override
//			public String getValue(HasIdentifier aux)
//			{
//				SummaryDTO summary = (SummaryDTO) aux;
//				return summary.getResourceTitleDisplay();
//			}
//		};
//		itemTable.addColumn(titleColumn, Message.INSTANCE.metadataEditingTitle());
//		itemTable.setColumnWidth(titleColumn, 100.0, Unit.PX);
//
//		TextColumn<HasIdentifier> abstractColumn = new TextColumn<HasIdentifier>()
//		{
//			@Override
//			public String getValue(HasIdentifier aux)
//			{
//				SummaryDTO summary = (SummaryDTO) aux;
//				return summary.getResourceAbstractDisplay();
//			}
//				
//		};
//		itemTable.addColumn(abstractColumn, Message.INSTANCE.metadataEditingAbstract());
//		itemTable.setColumnWidth(abstractColumn, 100.0, Unit.PX);
//
//		TextColumn<HasIdentifier> lastModificationDateColumn = new TextColumn<HasIdentifier>()
//		{
//			@Override
//			public String getValue(HasIdentifier aux)
//			{
//				return ((SummaryDTO) aux).getModificationDate();
//			}
//		};
//		itemTable.addColumn(lastModificationDateColumn, Message.INSTANCE.metadataEditingLastModificationDate());
//		itemTable.setColumnWidth(lastModificationDateColumn, 40.0, Unit.PX);
//
//		
//		itemTable.addColumn(viewColumn);
//		itemTable.addColumn(lockableEditColumn);
//		itemTable.addColumn(printColumn);
//		itemTable.addColumn(lockableDeleteColumn);
//		itemTable.setColumnWidth(viewColumn, 30.0, Unit.PX);
//		itemTable.setColumnWidth(lockableEditColumn, 30.0, Unit.PX);
//		itemTable.setColumnWidth(printColumn, 30.0, Unit.PX);
//		itemTable.setColumnWidth(lockableDeleteColumn, 30.0, Unit.PX);
//	}
//
//	public Long getDrainageBasinId()
//	{
//		return drainageBasinId;
//	}
//
//	public void setDrainageBasinId(Long drainageBasinId)
//	{
//		this.drainageBasinId = drainageBasinId;
//	}
//
//	public Presenter getPresenter()
//	{
//		return presenter;
//	}
//
//	public void setPresenter(Presenter presenter)
//	{
//		this.presenter = presenter;
//	}
//
//	// private String getUuidFromId(Long id)
//	// {
//	// Iterator<? extends HasId> iterator = model.iterator();
//	// while (iterator.hasNext()) {
//	// SummaryDTO aux = (SummaryDTO) iterator.next();
//	// if (aux.getId() == id)
//	// {
//	// return aux.getUuid();
//	// }
//	// }
//	// return null;
//	// }
//	//
//	// private Long getIdFromUuid(String uuid) {
//	//
//	// Iterator<? extends HasId> iterator = model.iterator();
//	// while (iterator.hasNext()) {
//	// SummaryDTO aux = (SummaryDTO) iterator.next();
//	// if (aux.getUuid().compareTo(uuid)==0)
//	// {
//	// return aux.getId();
//	// }
//	// }
//	// return null;
//	// }
//
//	public Long getObservatoryId()
//	{
//		return observatoryId;
//	}
//
//	public void setObservatoryId(Long observatoryId)
//	{
//		this.observatoryId = observatoryId;
//	}
//
//	Delegate<HasIdentifier> viewAction = new Delegate<HasIdentifier>()
//	{
//
//		@Override
//		public void execute(final HasIdentifier hasId)
//		{
//			presenter.viewMetadata(((SummaryDTO) hasId).getUuid());
//		}
//
//	};
//
//	Delegate<HasIdentifier> printAction = new Delegate<HasIdentifier>()
//	{
//
//		@Override
//		public void execute(HasIdentifier hasCode)
//		{
//
//			presenterPrint(((SummaryDTO) hasCode).getUuid());
//
//		}
//
//		private void presenterPrint(String uuid)
//		{
//
//			MetadataPrintHandler.print(uuid);
//		}
//
//	};
//
//
//	public FormattedCellTable<HasIdentifier> getItemTable()
//	{
//		return itemTable;
//	}
//
//	@Override
//	protected AbstractDataProvider<HasIdentifier> getDataProvider()
//	{
//		AsyncDataProvider<HasIdentifier> dataProvider = new AsyncDataProvider<HasIdentifier>()
//		{
//
//			@Override
//			protected void onRangeChanged(HasData<HasIdentifier> display)
//			{
//
//				final Range range = display.getVisibleRange();
//				int start = range.getStart();
//				if (presenter != null)
//				{
//					presenter.getEntries(start + 1);
//				}
//			}
//		};
//
//		return dataProvider;
//	}
//
//	public void setDataPage(int i, List<SummaryDTO> values, Integer hits)
//	{
//		Iterator<SummaryDTO> iterator = values.iterator();
//		while (iterator.hasNext()) {
//			SummaryDTO dto = (SummaryDTO) iterator.next();
//			if (isEditable(dto))
//			{
//				dto.setEditable(true);
//			}
//			else
//			{
//				dto.setEditable(false);
//			}
//			
//		}
//		
//		
//		
//		currentDisplayedValues = values;
//		itemTable.setRowData(i, values);
//		itemTable.setRowCount(hits);
//		checkEmptyPanelVisibility();
//		if (values.size() > 0)
//		{
//			setHeaderPanelVisible(true);
//		}
//
//	}
//
//	private void removeRow(Long id)
//	{
//	}
//
//	@Override
//	public boolean isEmpty()
//	{
//		return ((currentDisplayedValues == null) || (currentDisplayedValues.isEmpty()));
//	}
//
//	public void setObservatories(List<ObservatoryDTO> observatories) {
//		editableObservatories.clear();
//		Iterator<ObservatoryDTO> iterator = observatories.iterator();
//		while (iterator.hasNext()) 
//		{
//			ObservatoryDTO observatoryDTO = iterator.next();
//			if (observatoryDTO.getDatasetEditable())
//			{
//				editableObservatories.add(observatoryDTO.getShortLabel());
//			}
//		}
//	}

}
