package fr.obsmip.sedoo.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.event.MenuResetEvent;
import fr.obsmip.sedoo.client.event.MenuSearchEvent;
import fr.obsmip.sedoo.client.event.UserLoginEvent;
import fr.obsmip.sedoo.client.event.UserLogoutEvent;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.obsmip.sedoo.client.ui.menu.AdministrationMenu;
import fr.obsmip.sedoo.client.ui.menu.MetadataProviderMenu;
import fr.obsmip.sedoo.client.ui.menu.MetadataUserMenu;
import fr.obsmip.sedoo.client.ui.misc.RBVStackLayoutPanel;

public class MenuViewImpl extends AbstractStyledResizeComposite implements
		MenuView, ClickHandler {

	@UiField
	AdministrationMenu administrationMenu;
	@UiField
	MetadataProviderMenu metadataProviderMenu;
	@UiField
	MetadataUserMenu metadataSearchMenu;
	@UiField
	RBVStackLayoutPanel stackPanel;
	@UiField
	DockLayoutPanel main;

	@UiField
	HTMLPanel searchHeader;

	private Button searchButton;
	private Button resetButton;
	VerticalPanel vp;

	private final static int USER_MENU = 0;
	private final static int PROVIDER_MENU = 1;
	private final static int ADMINISTATOR_MENU = 2;
	private static final String MENU_BUTTON_STYLE = "gwt-MenuButton";

	private static MenuViewImplUiBinder uiBinder = GWT
			.create(MenuViewImplUiBinder.class);

	interface MenuViewImplUiBinder extends UiBinder<Widget, MenuViewImpl> {
	}

	public MenuViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();

		vp = new VerticalPanel();
		vp.setWidth("100%");
		vp.getElement().getStyle().setPaddingLeft(3, Unit.PX);
		vp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);

		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_JUSTIFY);
		buttonPanel.setSpacing(8);

		metadataProviderMenu.setVisible(false);
		administrationMenu.setVisible(false);

		searchButton = new Button(Message.INSTANCE.search());
		searchButton.addClickHandler(this);
		resetButton = new Button(Message.INSTANCE.reset());
		resetButton.addClickHandler(this);
		searchButton.setStyleName(MENU_BUTTON_STYLE);
		resetButton.setStyleName(MENU_BUTTON_STYLE);
		buttonPanel.add(searchButton);
		buttonPanel.add(resetButton);

		vp.add(buttonPanel);

		main.addSouth(vp, 5);

		hideAuthenticatedMenus();

		searchHeader.addDomHandler(this, ClickEvent.getType());
	}

	@Override
	public void onNotification(UserLoginEvent event) {
		stackPanel.showStack(PROVIDER_MENU);
		metadataProviderMenu.setVisible(true);
		if (event.getUser().isAdmin()) {
			stackPanel.showStack(ADMINISTATOR_MENU);
			administrationMenu.setVisible(true);
		}
		stackPanel.showWidget(USER_MENU);
		main.remove(vp);

	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		hideAuthenticatedMenus();
	}

	private void hideAuthenticatedMenus() {
		stackPanel.showWidget(USER_MENU);
		stackPanel.hideStack(PROVIDER_MENU);
		stackPanel.hideStack(ADMINISTATOR_MENU);
		metadataProviderMenu.setVisible(false);
		administrationMenu.setVisible(false);
		main.addSouth(vp, 5);
	}

	@Override
	public void onNotification(ActivityStartEvent event) {
		if (event.getType() == ActivityStartEvent.ADMINISTRATOR_ACTIVITY) {
			stackPanel.showWidget(ADMINISTATOR_MENU);
		} else if (event.getType() == ActivityStartEvent.MEMBER_ACTIVITY) {
			stackPanel.showWidget(PROVIDER_MENU);
		} else {
			stackPanel.showWidget(USER_MENU);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == resetButton) {
			PortailRBV.getClientFactory().getEventBus()
					.fireEvent(new MenuResetEvent());
		} else if (event.getSource() == searchButton) {
			PortailRBV.getClientFactory().getEventBus()
					.fireEvent(new MenuSearchEvent());
		} else if (event.getSource() == searchHeader) {
			PortailRBV.getClientFactory().getPlaceController()
					.goTo(new WelcomePlace());
		}
	}

}
