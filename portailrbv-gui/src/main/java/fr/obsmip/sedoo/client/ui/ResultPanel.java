package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.Range;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ResetEvent;
import fr.obsmip.sedoo.client.event.ResetEventHandler;
import fr.obsmip.sedoo.client.event.SearchEvent;
import fr.obsmip.sedoo.client.event.SearchEventHandler;
import fr.obsmip.sedoo.client.event.SearchResultEvent;
import fr.obsmip.sedoo.client.event.SearchResultEventHandler;
import fr.obsmip.sedoo.client.misc.SummaryPresenter;
import fr.obsmip.sedoo.client.ui.table.search.ResultList;

public class ResultPanel extends VerticalPanel implements
		SearchResultEventHandler, SearchEventHandler, ResetEventHandler {

	private SummaryPresenter presenter;

	public ResultPanel() {
		super();
		setWidth("100%");
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(SearchResultEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(ResetEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(SearchEvent.TYPE, this);
	}

	public void reset() {
		clear();
	}

	public void setSummaryPresenter(SummaryPresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void onNotification(SearchResultEvent event) {
		clear();
		ResultList resultList = new ResultList();
		resultList.setPresenter(presenter);
		int length = event.getTotalResultLength();
		resultList.setVisibleRange(new Range(0, length));
		resultList.setRowCount(length);
		resultList.setRowData(0, event.getAllSummaryDTOs());
		add(resultList);

	}

	@Override
	public void onNotification(ResetEvent event) {
		reset();

	}

	@Override
	public void onNotification(SearchEvent event) {
		reset();

	}

}
