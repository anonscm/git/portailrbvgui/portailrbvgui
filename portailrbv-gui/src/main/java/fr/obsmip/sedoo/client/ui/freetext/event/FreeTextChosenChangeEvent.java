package fr.obsmip.sedoo.client.ui.freetext.event;

import com.google.gwt.event.shared.EventHandler;

import fr.obsmip.sedoo.client.ui.freetext.FreeTextChosenImpl;

public class FreeTextChosenChangeEvent extends FreeTextChosenEvent<FreeTextChosenChangeEvent.FreeTextChosenChangeHandler> {

	public interface FreeTextChosenChangeHandler extends EventHandler {
		void onChange(FreeTextChosenChangeEvent event);
	}
	public static Type<FreeTextChosenChangeHandler> TYPE = new Type<FreeTextChosenChangeHandler>();
	public static Type<FreeTextChosenChangeHandler> getType() {return TYPE;}

	private boolean selection;

	private String value;

	public FreeTextChosenChangeEvent(String value, boolean selected, FreeTextChosenImpl chosen) {
		super(chosen);
		this.value = value;
		this.selection = selected;
	}

	public FreeTextChosenChangeEvent(String value, FreeTextChosenImpl chosen) {
		this(value, true, chosen);
	}

	@Override
	public Type<FreeTextChosenChangeHandler> getAssociatedType() {
		return TYPE;
	}

	public String getValue() {
		return value;
	}

	public boolean isSelection() {
		return selection;
	}

	@Override
	protected void dispatch(FreeTextChosenChangeHandler handler) {
		handler.onChange(this);
	}

}
