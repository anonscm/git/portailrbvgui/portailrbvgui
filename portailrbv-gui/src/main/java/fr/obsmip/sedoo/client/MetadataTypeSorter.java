package fr.obsmip.sedoo.client;

import fr.sedoo.metadata.shared.domain.MetadataDTO;


public class MetadataTypeSorter {
	
	public final static int DATASET_TYPE=1;
	public final static int OBSERVATORY_TYPE=2;
	public final static int EXPERIMENTAL_SITE_TYPE=3;
	
	public int getTypeFromMetadata(MetadataDTO metadata)
	{
		return DATASET_TYPE;
	}

}
