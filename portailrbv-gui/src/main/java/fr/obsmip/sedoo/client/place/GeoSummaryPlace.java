package fr.obsmip.sedoo.client.place;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class GeoSummaryPlace extends Place implements PlaceConstants
{

	public static GeoSummaryPlace instance;
	
	private List<String> observatoryNames = new ArrayList<String>();

	public static class Tokenizer implements PlaceTokenizer<GeoSummaryPlace>
	{
		@Override
		public String getToken(GeoSummaryPlace place)
		{
			List<String> aux = place.getObservatoryNames();
			StringBuffer result = new StringBuffer();
			if (aux.isEmpty() == false)
			{
				Iterator<String> iterator = aux.iterator();
				while (iterator.hasNext()) {
					result.append(iterator.next());
					if (iterator.hasNext())
					{
						result.append(TOKEN_SEPARATOR);
					}
				}
			}
			return result.toString();
		}

		@Override
		public GeoSummaryPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new GeoSummaryPlace();
				if (token.trim().length()>0)
				{
					List<String> aux = new ArrayList<String>();
					String[] split = token.split(TOKEN_SEPARATOR);
					for (int i = 0; i < split.length; i++) {
						aux.add(split[i]);
					}
					instance.setObservatoryNames(aux);
				}
			}
			return instance;
		}
	}

	public List<String> getObservatoryNames() {
		return observatoryNames;
	}

	public void setObservatoryNames(List<String> observatoryNames) {
		this.observatoryNames = observatoryNames;
	}
	
}
