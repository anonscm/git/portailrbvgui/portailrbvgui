package fr.obsmip.sedoo.client.ui.metadata.observatory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryView.ObservatoryEntryPresenter;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.util.MetadataSummaryDTOComparator;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ExperimentalSiteTable extends AbstractSummaryTable {

	
	private ObservatoryEntryPresenter presenter;

	public ExperimentalSiteTable()
	{
		super();
		setAddButtonEnabled(true);
	}
	
	@Override
	public void presenterView(HasIdentifier hasId) {
		
		ExperimentalSiteEntryDisplayingPlace place = new ExperimentalSiteEntryDisplayingPlace();
		place.setUuid(hasId.getIdentifier());
		PortailRBV.getClientFactory().getPlaceController().goTo(place);
		
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter.deleteExperimentalSite(hasId.getIdentifier());
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		presenter.goToExperimentalSiteEditPlace((MetadataSummaryDTO) hasId);
		
	}

	@Override
	public String getAddItemText() {
		return Message.INSTANCE.addNewExperimentalSite();
	}
	
	@Override
	public void addItem() {
		presenter.addExperimentalSite();
	}

	public void setPresenter(ObservatoryEntryPresenter presenter) {
		this.presenter = presenter;
	}
	
	public String getDeleteItemConfirmationText() {
		return Message.INSTANCE.experimentalSiteDeletetionConfirmationText();
	}

	@Override
	public void init(List<? extends HasIdentifier> model) {
		ArrayList<MetadataSummaryDTO> sortedEntries = new ArrayList<MetadataSummaryDTO>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) 
		{
			sortedEntries.add((MetadataSummaryDTO) iterator.next());
		}
		Collections.sort(sortedEntries, new MetadataSummaryDTOComparator());
		super.init(sortedEntries);
	}
	
}
