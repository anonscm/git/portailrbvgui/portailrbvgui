package fr.obsmip.sedoo.client.ui.metadata.observatory;

import fr.obsmip.sedoo.client.ui.ObservatoryEntryView.ObservatoryEntryPresenter;
import fr.sedoo.metadata.client.ui.widget.field.impl.Children;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;

public class ExperimentalSites extends Children {

	private ExperimentalSiteTable table;

	public ExperimentalSites() {
	}
	
	@Override
	public AbstractSummaryTable createTable() {
		if (table == null)
		{
			table = new ExperimentalSiteTable();
		}
		return table;
	}

	public void setPresenter(ObservatoryEntryPresenter presenter) {
		table.setPresenter(presenter);
	}

	public void broadcastExperimentalSiteDeletion(String uuid) 
	{
		table.removeRow(uuid);
	}
}
