package fr.obsmip.sedoo.client.ui.metadata.dataset;

import java.util.List;

import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.Charset;
import fr.sedoo.metadata.client.ui.widget.field.impl.DataLanguages;
import fr.sedoo.metadata.client.ui.widget.field.impl.Epsg;
import fr.sedoo.metadata.client.ui.widget.field.impl.Format;
import fr.sedoo.metadata.client.ui.widget.field.impl.MetadataLanguage;
import fr.sedoo.metadata.client.ui.widget.field.impl.MetadataLastModificationDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.Uuid;

public class OtherTab extends AbstractTab {

	public OtherTab(List<String> metadataLanguages, List<String> dataLanguages) {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingTechnichalInformations());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.FORMAT));
		addRightComponent(new Format());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.CHARSET));
		addRightComponent(new Charset());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.COORDINATE_SYSTEM));
		addRightComponent(new Epsg());
		addSection(MetadataMessage.INSTANCE.metadataEditingLanguages());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.DATA_LANGUAGES));
		addRightComponent(new DataLanguages(dataLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.METADATA_MAIN_LANGUAGE));
		addRightComponent(new MetadataLanguage(metadataLanguages));
		addSection(MetadataMessage.INSTANCE.metadataEditingMetadataInformations());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.UUID));
		addRightComponent(new Uuid());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.METADATA_LAST_MODIFICATION_DATE));
		addRightComponent(new MetadataLastModificationDate());
		reset();
	}

}
