package fr.obsmip.sedoo.client.ui.metadata.dataset;

import java.util.ArrayList;

import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceGenealogy;

public class MeasurementTab extends AbstractTab {

	public MeasurementTab(ArrayList<String> alternateLanguages) {
		super();
		addSection(MetadataMessage.INSTANCE
				.metadataEditingGeneralInformations());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.GENEALOGY));
		addFullLineComponent(new ResourceGenealogy(alternateLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.VARIABLES));
		reset();
	}

}
