package fr.obsmip.sedoo.client.ui.menu;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.event.ActivityStartEventHandler;
import fr.obsmip.sedoo.client.event.MenuResetEvent;
import fr.obsmip.sedoo.client.event.MenuResetEventHandler;
import fr.obsmip.sedoo.client.event.MenuSearchEvent;
import fr.obsmip.sedoo.client.event.MenuSearchEventHandler;
import fr.obsmip.sedoo.client.event.ResetEvent;
import fr.obsmip.sedoo.client.event.SearchEvent;
import fr.obsmip.sedoo.client.event.UserLoginEvent;
import fr.obsmip.sedoo.client.event.UserLoginEventHandler;
import fr.obsmip.sedoo.client.event.UserLogoutEvent;
import fr.obsmip.sedoo.client.event.UserLogoutEventHandler;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.searchcriteria.widget.DateWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.widget.EntryTypeWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.widget.KeywordWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.widget.ObservatoryWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.widget.ThesaurusWidget;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.rbv.thesauri.RBVThesauriFactory;

public class MetadataUserMenu extends AbstractMenu implements ClickHandler,
		UserLoginEventHandler, UserLogoutEventHandler,
		ActivityStartEventHandler, MenuSearchEventHandler,
		MenuResetEventHandler {

	private static final String MENU_BUTTON_STYLE = "gwt-MenuButton";
	private VerticalPanel searchPanel;
	private HorizontalPanel buttonPanel;
	private Button searchButton;
	private Button resetButton;
	private KeywordWidget keywordWidget;
	private ObservatoryWidget observatoriesWidget;
	private EntryTypeWidget entryTypeWidget;
	private DateWidget dateWidget;
	private DockLayoutPanel container;

	MetadataUserSearchCriteria userSearchCriteria = new MetadataUserSearchCriteria();

	public MetadataUserSearchCriteria getUserSearchCriteria() {
		return userSearchCriteria;
	}

	public MetadataUserMenu() {
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(UserLoginEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(UserLogoutEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(ActivityStartEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(MenuSearchEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(MenuResetEvent.TYPE, this);
		DockLayoutPanel panel = getMetadataPanel();
		initWidget(panel);
		reset();

	}

	private DockLayoutPanel getMetadataPanel() {

		container = new DockLayoutPanel(Unit.PX);
		container.setWidth("100%");
		VerticalPanel metadataPanel = new VerticalPanel();
		metadataPanel.getElement().getStyle().setPaddingLeft(3, Unit.PX);
		metadataPanel.setWidth("100%");
		searchPanel = new VerticalPanel();
		searchPanel.setSpacing(5);

		buttonPanel = new HorizontalPanel();

		buttonPanel
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_JUSTIFY);
		buttonPanel.setSpacing(8);

		searchButton = new Button(Message.INSTANCE.search());
		searchButton.setStyleName(MENU_BUTTON_STYLE);
		searchButton.addClickHandler(this);
		resetButton = new Button(Message.INSTANCE.reset());
		resetButton.setStyleName(MENU_BUTTON_STYLE);
		resetButton.addClickHandler(this);

		buttonPanel.add(searchButton);
		buttonPanel.add(resetButton);

		observatoriesWidget = new ObservatoryWidget();
		searchPanel.add(observatoriesWidget);
		userSearchCriteria.add(observatoriesWidget);

		entryTypeWidget = new EntryTypeWidget();
		searchPanel.add(entryTypeWidget);
		userSearchCriteria.add(entryTypeWidget);

		ThesaurusWidget climateWidget = new ThesaurusWidget(
				RBVThesauriFactory.CLIMATE_THESAURUS,
				Message.INSTANCE.climate());
		searchPanel.add(climateWidget);
		userSearchCriteria.add(climateWidget);

		ThesaurusWidget geologyWidget = new ThesaurusWidget(
				RBVThesauriFactory.GEOLOGY_THESAURUS,
				Message.INSTANCE.geology());
		searchPanel.add(geologyWidget);
		userSearchCriteria.add(geologyWidget);
		
		ThesaurusWidget variableWidget = new ThesaurusWidget(
				RBVThesauriFactory.VARIABLE_THESAURUS,
				Message.INSTANCE.variable());
		searchPanel.add(variableWidget);
		userSearchCriteria.add(variableWidget);

		keywordWidget = new KeywordWidget();
		searchPanel.add(keywordWidget);
		userSearchCriteria.add(keywordWidget);

		dateWidget = new DateWidget();
		searchPanel.add(dateWidget);
		userSearchCriteria.add(dateWidget);

		searchPanel.add(buttonPanel);

		metadataPanel.add(searchPanel);
		container.add(new ScrollPanel(metadataPanel));

		return container;
	}

	public void reset() {
		userSearchCriteria.reset();
		PortailRBV.getClientFactory().getEventBus().fireEvent(new ResetEvent());
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == resetButton) {
			reset();
		} else if (event.getSource() == searchButton) {
			/*SearchCriteriaDTO criteria = userSearchCriteria
					.flush(new SearchCriteriaDTO());
			PortailRBV.getClientFactory().getEventBus()
					.fireEvent(new SearchEvent(criteria));*/
			search();
		}
	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		searchPanel.remove(buttonPanel);
	}

	@Override
	public void onNotification(UserLoginEvent event) {
		searchPanel.add(buttonPanel);
		searchButton.setVisible(true);
		resetButton.setVisible(true);
	}

	@Override
	public void onNotification(ActivityStartEvent event) {
		searchButton.setVisible(true);
		resetButton.setVisible(true);
	}

	@Override
	public void onNotification(MenuResetEvent event) {
		reset();
		search();
	}

	private void search(){
		SearchCriteriaDTO criteria = userSearchCriteria
				.flush(new SearchCriteriaDTO());
		PortailRBV.getClientFactory().getEventBus()
				.fireEvent(new SearchEvent(criteria));
	}
	
	@Override
	public void onNotification(MenuSearchEvent event) {
		search();
		/*SearchCriteriaDTO criteria = userSearchCriteria
				.flush(new SearchCriteriaDTO());
		PortailRBV.getClientFactory().getEventBus()
				.fireEvent(new SearchEvent(criteria));*/
	}
}
