package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaBar;
import fr.obsmip.sedoo.client.ui.searchcriteria.DisclosurePanelCustom;
import fr.obsmip.sedoo.client.ui.searchcriteria.MapCriteriaWidget;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;

public class EntryTypeWidget implements HasValidation, MapCriteriaWidget,
		ClickHandler {

	protected DisclosurePanelCustom panel;
	protected CriteriaBar criteriaBar;
	private CheckBox observatories;
	private CheckBox experimentalSites;
	private CheckBox dataSets;

	public EntryTypeWidget() {
		panel = new DisclosurePanelCustom(Message.INSTANCE.entryTypes());
		panel.setStyleName("customDisclosurePanel");
		panel.setWidth("100%");
		init();
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	public void init() {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(2);
		verticalPanel.getElement().getStyle().setMarginLeft(3, Unit.PX);
		observatories = createCheckBox(Message.INSTANCE.observatories(), false);
		verticalPanel.add(observatories);

		experimentalSites = createCheckBox(
				Message.INSTANCE.experimentalSites(), false);
		verticalPanel.add(experimentalSites);

		dataSets = createCheckBox(Message.INSTANCE.dataSets(), false);
		verticalPanel.add(dataSets);

		panel.add(verticalPanel);
		panel.setOpen(true);
	}

	private CheckBox createCheckBox(String label, boolean checked) {
		CheckBox result = new NormalFontCheckBox(label);
		result.setValue(checked);
		result.addClickHandler(this);
		return result;
	}

	@Override
	public void reset() {
		dataSets.setValue(false);
		experimentalSites.setValue(true);
		observatories.setValue(false);
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		return result;
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {
		criteria.setIncludesDatasets(dataSets.getValue());
		criteria.setIncludesObservatories(observatories.getValue());
		criteria.setIncludesExperimentalSites(experimentalSites.getValue());
		return criteria;
	}

	@Override
	public void onClick(ClickEvent event) {
		criteriaBar.validate();
	}

}