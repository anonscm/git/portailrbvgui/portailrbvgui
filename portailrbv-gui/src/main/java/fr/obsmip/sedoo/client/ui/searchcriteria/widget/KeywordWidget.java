package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaBar;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.message.ValidationMessages;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;
import fr.sedoo.commons.util.StringUtil;

public class KeywordWidget extends AbstractSearchWidget implements
		HasValidation {

	protected TextBox keyword;
	protected RadioButton allKeyword;
	protected RadioButton anyKeyword;
	private Image keywordToolTip;
	protected CriteriaBar criteriaBar;
	protected ArrayList<RadioButton> radioButtons = new ArrayList<RadioButton>();

	public KeywordWidget() {
		super(Message.INSTANCE.metadataSearchingViewKeywords());
		keyword = new TextBox();
		allKeyword = new NormalFontRadioButton("keywordToggle",
				Message.INSTANCE.metadataSearchingViewAllKeyword());
		anyKeyword = new NormalFontRadioButton("keywordToggle",
				Message.INSTANCE.metadataSearchingViewAnyKeyword());

		init();
	}

	public void init() {
		VerticalPanel searchPanel = new VerticalPanel();
		searchPanel.setSpacing(5);
		VerticalPanel metadataPanel = new VerticalPanel();
		metadataPanel.setSpacing(2);
		HorizontalPanel quickSearchPanel = new HorizontalPanel();
		quickSearchPanel.setSpacing(3);
		Label metadataSearchSubmenuTitle = new Label(Message.INSTANCE.search());
		metadataSearchSubmenuTitle.setStyleName("inactiveMenuLink");

		quickSearchPanel
				.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		metadataPanel.add(metadataSearchSubmenuTitle);

		keyword.getElement().setAttribute("placeHolder",
				Message.INSTANCE.metadataUserMenuQuickSearchPlaceholder());

		keywordToolTip = new Image(GlobalBundle.INSTANCE.help());
		keywordToolTip.getElement().getStyle().setMarginLeft(3, Unit.PX);
		keywordToolTip.getElement().getStyle().setProperty("cursor", "pointer");
		keywordToolTip.setTitle(Message.INSTANCE
				.metadataUserMenuKeywordTooltip());

		quickSearchPanel.add(keyword);
		quickSearchPanel.add(keywordToolTip);

		searchPanel.add(quickSearchPanel);
		searchPanel.add(allKeyword);
		searchPanel.add(anyKeyword);

		panel.add(searchPanel);
		panel.setOpen(true);
	}

	@Override
	public void reset() {
		keyword.setText("");
		allKeyword.setValue(true);
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {

		String[] split = keyword.getText().split(SeparatorUtil.COMMA_SEPARATOR);
		ArrayList<String> trimmedKeywords = new ArrayList<String>();
		for (int i = 0; i < split.length; i++) {
			if (StringUtil.isNotEmpty(split[i])) {
				trimmedKeywords.add(StringUtil.trimToEmpty(split[i]));
			}
		}

		if (ListUtil.isNotEmpty(trimmedKeywords)) {
			criteria.setKeywords(trimmedKeywords);
		}

		if (allKeyword.getValue() == true) {
			criteria.setKeywordsLogicalLink(SearchCriteriaDTO.AND);
		} else {
			criteria.setKeywordsLogicalLink(SearchCriteriaDTO.OR);
		}

		return criteria;
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		boolean atLeastOneSelected = false;
		for (RadioButton radioButton : radioButtons) {
			if (radioButton.getValue() == true) {
				atLeastOneSelected = true;
			}
		}
		if (atLeastOneSelected == false) {
			result.add(new ValidationAlert(Message.INSTANCE.observatories(),
					ValidationMessages.INSTANCE.atLeastOneElementNeeded()));
		}
		return result;
	}
}