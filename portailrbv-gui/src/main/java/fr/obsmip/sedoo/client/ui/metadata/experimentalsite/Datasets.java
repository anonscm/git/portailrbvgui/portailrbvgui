package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryView.ExperimentalSiteEntryPresenter;
import fr.sedoo.metadata.client.ui.widget.field.impl.Children;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;

public class Datasets extends Children {

	private DatasetTable table;

	public Datasets() {
	}
	
	@Override
	public AbstractSummaryTable createTable() {
		if (table == null)
		{
			table = new DatasetTable();
		}
		return table;
	}

	public void setPresenter(ExperimentalSiteEntryPresenter presenter) {
		table.setPresenter(presenter);
	}

	public void broadcastDatasetDeletion(String uuid) 
	{
		table.removeRow(uuid);
	}
}
