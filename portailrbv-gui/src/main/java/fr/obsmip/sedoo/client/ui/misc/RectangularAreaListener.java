package fr.obsmip.sedoo.client.ui.misc;

public interface RectangularAreaListener
{
	void onRectangleAdded();

	void onRectangleChanged();
}
