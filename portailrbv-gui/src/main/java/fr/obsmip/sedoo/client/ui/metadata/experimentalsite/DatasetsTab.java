package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryView.ExperimentalSiteEntryPresenter;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;

public class DatasetsTab extends AbstractTab {

	Datasets datasets;
	
	public DatasetsTab() {
		super();
		addSection(Message.INSTANCE.dataSets());
		datasets = new Datasets();
		addFullLineComponent(datasets);
		reset();
	}
	
	public void setPresenter(ExperimentalSiteEntryPresenter presenter)
	{
		datasets.setPresenter(presenter);
	}

	public void broadcastDatasetDeletion(String uuid) 
	{
		datasets.broadcastDatasetDeletion(uuid);
	}
	
}
