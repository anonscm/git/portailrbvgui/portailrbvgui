package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.misc.ObservatoryListUtil;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteEntryCreationView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class ExperimentalSiteEntryCreationActivity  extends RBVAuthenticatedActivity implements Presenter, LoadCallBack<ArrayList<IsObservatory>> {

	private final static  MetadataServiceAsync METADATA_SERVICE = GWT.create(MetadataService.class);

	private ExperimentalSiteEntryCreationView view;

	private AcceptsOneWidget containerWidget;

	private String observatoryName;
	
	public ExperimentalSiteEntryCreationActivity(ExperimentalSiteEntryCreationPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
		observatoryName = place.getObservatoryName();
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		this.containerWidget = containerWidget;
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		broadcastActivityTitle(Message.INSTANCE.experimentalSiteEditingViewCreationHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getExperimentaSiteEntryCreationShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		containerWidget.setWidget(clientFactory.getProgressView());
		view = ((ClientFactory) clientFactory).getExperimentalSiteEntryCreationView();
		view.setPresenter(this);
		view.reset();
		ClientObservatoryList.getObservatories(this);
	}

	@Override
	public void postLoadProcess(ArrayList<IsObservatory> observatories) 
	{
		ObservatoryListUtil.getAvailableObservatories(observatories,clientFactory,METADATA_SERVICE, new OperationCallBack() {
			@Override
			public void postExecution(boolean result, Object aux) {
				view.setObservatories((List<ObservatorySummaryDTO>) aux);
				if (StringUtil.isNotEmpty(observatoryName))
				{
					view.selectObservatory(observatoryName);
				}
				containerWidget.setWidget(view);
				
			}
		});
		
	}

	@Override
	public void create(ObservatorySummaryDTO parentSummary, String experimentalSiteName) 
	{
		ExperimentalSiteEditingPlace place = new ExperimentalSiteEditingPlace();
		place.setParentSummary(parentSummary);
		place.setExperimentalSiteName(experimentalSiteName);
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void goToObservatoryEntryCreationPlace() {
		clientFactory.getPlaceController().goTo(new ObservatoryEntryCreationPlace());
	}

}