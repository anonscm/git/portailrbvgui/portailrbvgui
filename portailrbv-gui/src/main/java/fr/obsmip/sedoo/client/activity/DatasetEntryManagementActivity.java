package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.misc.ObservatoryListUtil;
import fr.obsmip.sedoo.client.place.DatasetEditingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryManagementPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.DatasetEntryManagementView;
import fr.obsmip.sedoo.client.ui.DatasetEntryManagementView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.callback.OperationCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class DatasetEntryManagementActivity extends RBVAuthenticatedActivity
		implements Presenter, LoadCallBack<ArrayList<IsObservatory>> {

	public DatasetEntryManagementActivity(DatasetEntryManagementPlace place,
			ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	private final static MetadataServiceAsync METADATA_SERVICE = GWT
			.create(MetadataService.class);
	DatasetEntryManagementView datasetEntryManagementView;

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		datasetEntryManagementView = clientFactory
				.getDatasetEntryManagementView();
		datasetEntryManagementView.setPresenter(this);
		containerWidget.setWidget(datasetEntryManagementView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.datasetManagementViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getDatasetEntryManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		datasetEntryManagementView.reset();
		ClientObservatoryList.getObservatories(this);
	}

	@Override
	public void postLoadProcess(ArrayList<IsObservatory> observatories) {
		ObservatoryListUtil.getAvailableObservatories(observatories,
				clientFactory, METADATA_SERVICE, new OperationCallBack() {
					@Override
					public void postExecution(boolean result, Object aux) {
						datasetEntryManagementView
								.setObservatories((List<ObservatorySummaryDTO>) aux);
					}
				});

	}

	@Override
	public void deleteDatasetEntry(final String uuid) {
		final ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.deleting(),
				ActionEventConstant.OBSERVATORY_DELETING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		METADATA_SERVICE.deleteDatasetEntry(
				uuid,
				new DefaultAbstractCallBack<Boolean>(e, clientFactory
						.getEventBus()) {
					@Override
					public void onSuccess(Boolean result) {
						super.onSuccess(result);
						clientFactory.getEventBus().fireEvent(
								new NotificationEvent(CommonMessages.INSTANCE
										.deletedElement()));
						datasetEntryManagementView
								.broadcastDatasetDeletion(uuid);
					}
				});
	}

	@Override
	public void goToEditPlace(MetadataSummaryDTO observatoryDTO) {
		DatasetEditingPlace place = new DatasetEditingPlace();
		place.setDatasetUuid(StringUtil.trimToEmpty(observatoryDTO.getUuid()));
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void goToDatasetEntryCreationPlace() {
		clientFactory.getPlaceController()
				.goTo(new DatasetEntryCreationPlace());
	}

	@Override
	public void loadExperimentalSiteFromObservatoryUuid(String observatoryUuid) {
		List<ExperimentalSiteSummaryDTO> result = new ArrayList<ExperimentalSiteSummaryDTO>();
		ActionStartEvent startEvent = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.EXPERIMENTAL_SITE_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		METADATA_SERVICE
				.getExperimentalSitesSummaryFromParentUuid(
						observatoryUuid,
						clientFactory.getDisplayLanguages(),
						new DefaultAbstractCallBack<ArrayList<ExperimentalSiteSummaryDTO>>(
								startEvent, clientFactory.getEventBus()) {

							@Override
							public void onSuccess(
									ArrayList<ExperimentalSiteSummaryDTO> result) {
								super.onSuccess(result);
								datasetEntryManagementView
										.setExperimentalSites(result);
							}

						});
	}

	@Override
	public void goToObservatoryEntryCreationPlace() {
		clientFactory.getPlaceController().goTo(
				new ObservatoryEntryCreationPlace());
	}

	@Override
	public void goToExperimentalSiteEntryCreationPlace(String observatoryName) {
		ExperimentalSiteEntryCreationPlace place = new ExperimentalSiteEntryCreationPlace();
		place.setObservatoryName(observatoryName);
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void loadDatasetFromExperimentalSiteUuid(String uuid) {
		List<MetadataSummaryDTO> result = new ArrayList<MetadataSummaryDTO>();
		ActionStartEvent startEvent = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.METADATA_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		METADATA_SERVICE.getDatasetFromParentUuid(uuid, clientFactory
				.getDisplayLanguages(),
				new DefaultAbstractCallBack<ArrayList<MetadataSummaryDTO>>(
						startEvent, clientFactory.getEventBus()) {

					@Override
					public void onSuccess(ArrayList<MetadataSummaryDTO> result) {
						super.onSuccess(result);
						datasetEntryManagementView.setDatasets(result);
					}

				});

	}

	@Override
	public void goToDatasetEntryCreationPlace(String observatory,
			String experimentalSite) {
		DatasetEntryCreationPlace place = new DatasetEntryCreationPlace();
		place.setObservatoryName(observatory);
		place.setExperimentalSiteName(experimentalSite);
		clientFactory.getPlaceController().goTo(place);

	}

}
