package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.activity.MessageManageActivity;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVMessageManagementActivity extends MessageManageActivity {

	private MessageManagePlace place;

	public RBVMessageManagementActivity(MessageManagePlace place,
			ClientFactory clientFactory) {
		super(place, clientFactory);
		this.place = place;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		super.start(containerWidget, eventBus);
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		messageManageView.setTitleVisible(false);
		ActivityHelper.broadcastActivityTitle(clientFactory.getEventBus(),
				NewsMessages.INSTANCE.header());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		shortcuts.add(ShortcutFactory.getMessageManagementShortcut());
		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser()) {
			return ((AuthenticatedClientFactory) clientFactory)
					.getUserManager().getUser().isAdmin();
		} else {
			return false;
		}
	}

	public MessageManagePlace getPlace() {
		return place;
	}

}
