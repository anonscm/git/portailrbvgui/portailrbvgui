package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class MenuSearchEvent extends GwtEvent<MenuSearchEventHandler> {

	public static final Type<MenuSearchEventHandler> TYPE = new Type<MenuSearchEventHandler>();

	public MenuSearchEvent() {
	}

	@Override
	protected void dispatch(MenuSearchEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<MenuSearchEventHandler> getAssociatedType() {
		return TYPE;
	}

}
