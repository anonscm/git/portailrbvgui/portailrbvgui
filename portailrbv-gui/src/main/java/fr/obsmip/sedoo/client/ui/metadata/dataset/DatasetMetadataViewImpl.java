package fr.obsmip.sedoo.client.ui.metadata.dataset;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.DatasetEntryView;
import fr.obsmip.sedoo.client.ui.breadcrumb.MetadataBreadcrumb;
import fr.obsmip.sedoo.client.ui.metadata.common.ContactTab;
import fr.obsmip.sedoo.client.ui.metadata.common.KeywordTab;
import fr.obsmip.sedoo.client.ui.metadata.common.children.ConstraintTab;
import fr.obsmip.sedoo.shared.domain.DatasetDTO;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.AbstractDTOEditingView;
import fr.sedoo.metadata.client.ui.view.MetadataDisplayingView;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.view.common.MetadataTabPanel;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;
import fr.sedoo.rbv.thesauri.RBVThesauriFactory;

public class DatasetMetadataViewImpl extends AbstractDTOEditingView implements
		DatasetEntryView, MetadataDisplayingView {

	private static MetadataEditingViewImplUiBinder uiBinder = GWT
			.create(MetadataEditingViewImplUiBinder.class);

	private DatasetEntryPresenter presenter;

	ArrayList<AbstractTab> tabs = new ArrayList<AbstractTab>();

	interface MetadataEditingViewImplUiBinder extends
			UiBinder<Widget, DatasetMetadataViewImpl> {
	}

	@UiField
	Button backButton;

	@UiField
	Button printButton;

	@UiField
	Button xmlButton;

	@UiField
	Button saveButton;

	@UiField
	Button validateButton;

	@UiField
	MetadataBreadcrumb breadcrumb;

	@UiField(provided = true)
	MetadataTabPanel tabPanel;

	private String uuid;

	private String hierarchyLevel;

	private String metadataLastModificationDate;

	private MetadataSummaryDTO observatorySummary;

	private MetadataSummaryDTO experimentalSiteSummary;

	private ConstraintTab constraintTab;

	private ContactTab contactTab;

	public DatasetMetadataViewImpl(ArrayList<String> displayLanguages,
			List<String> dataLanguages) {
		super();
		tabPanel = new MetadataTabPanel();

		tabPanel.addTab(new IdentificationTab(displayLanguages),
				MetadataMessage.INSTANCE
						.metadataEditingIdentificationTabHeader());
		contactTab = new ContactTab();
		tabPanel.addTab(contactTab,
				MetadataMessage.INSTANCE.metadataEditingContactTabHeader());

		ArrayList<String> thesaurusName = new ArrayList<String>();
		thesaurusName.add(RBVThesauriFactory.SENSOR_THESAURUS);
		thesaurusName.add(RBVThesauriFactory.VARIABLE_THESAURUS);
		thesaurusName.add(RBVThesauriFactory.MEASUREMENT_TYPE_THESAURUS);

		tabPanel.addTab(new KeywordTab(thesaurusName),
				MetadataMessage.INSTANCE.metadataEditingKeywordTabHeader());
		tabPanel.addTab(new GeographicalLocationTab(
				AreaSelectorWidget.DEFAULT_MAP_LAYER), Message.INSTANCE
				.metadataEditingGeographicalLocationTabHeader());
		tabPanel.addTab(new TemporalExtentTab(),
				Message.INSTANCE.metadataEditingTemporalExtentHeader());
		tabPanel.addTab(new MeasurementTab(displayLanguages),
				MetadataMessage.INSTANCE.metadataEditingMeasurementTabHeader());
		constraintTab = new ConstraintTab(displayLanguages);
		tabPanel.addTab(constraintTab,
				MetadataMessage.INSTANCE.metadataEditingConstraintTabHeader());
		tabPanel.addTab(new OtherTab(displayLanguages, dataLanguages),
				Message.INSTANCE.metadataEditingOtherTabHeader());
		tabPanel.activateSelectionHandler();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		reset();

	}

	@Override
	public void reset() {
		breadcrumb.reset();
		breadcrumb.setLevel(MetadataBreadcrumb.DATASET_LEVEL);
		tabPanel.reset();
		ElementUtil.hide(xmlButton);
		ElementUtil.hide(printButton);
		ElementUtil.hide(saveButton);
		ElementUtil.hide(validateButton);
		uuid = "";
	}

	@Override
	public void edit(AbstractDTO dto) {
		reset();
		DatasetDTO metadata = (DatasetDTO) dto;
		breadcrumb.enableEditMode();
		breadcrumb.setObservatoryName(metadata.getObservatorySummary()
				.getName());
		breadcrumb.setObservatoryUuid(metadata.getObservatorySummary()
				.getUuid());
		breadcrumb.setExperimentalSiteName(metadata
				.getExperimentalSiteSummary().getName());
		breadcrumb.setExperimentalSiteUuid(metadata
				.getExperimentalSiteSummary().getUuid());
		breadcrumb.setMetadataTitle(metadata
				.getIdentificationPart()
				.getResourceTitle()
				.getDisplayValue(
						PortailRBV.getClientFactory().getDisplayLanguages()));
		tabPanel.edit(metadata);
		ElementUtil.hide(xmlButton);
		ElementUtil.hide(printButton);
		ElementUtil.show(saveButton);
		ElementUtil.show(validateButton);
		uuid = StringUtil.trimToEmpty(metadata.getOtherPart().getUuid());
		observatorySummary = metadata.getObservatorySummary();
		experimentalSiteSummary = metadata.getExperimentalSiteSummary();
		hierarchyLevel = metadata.getIdentificationPart().getHierarchyLevel();
		metadataLastModificationDate = StringUtil.trimToEmpty(metadata
				.getOtherPart().getMetadataLastModificationDate());
	}

	@Override
	public void display(AbstractDTO dto) {
		reset();
		DatasetDTO metadata = (DatasetDTO) dto;
		breadcrumb.enableDisplayMode();
		breadcrumb.setMetadataTitle(metadata
				.getIdentificationPart()
				.getResourceTitle()
				.getDisplayValue(
						PortailRBV.getClientFactory().getDisplayLanguages()));
		breadcrumb.setObservatoryName(metadata.getObservatorySummary()
				.getName());
		breadcrumb.setObservatoryUuid(metadata.getObservatorySummary()
				.getUuid());
		breadcrumb.setExperimentalSiteName(metadata
				.getExperimentalSiteSummary().getName());
		breadcrumb.setExperimentalSiteUuid(metadata
				.getExperimentalSiteSummary().getUuid());
		tabPanel.display(metadata);
		uuid = StringUtil.trimToEmpty(metadata.getOtherPart().getUuid());
		ElementUtil.show(xmlButton);
		ElementUtil.show(printButton);
		ElementUtil.hide(saveButton);
		ElementUtil.hide(validateButton);
	}

	@Override
	public MetadataDTO flush() {
		DatasetDTO metadataDTO = new DatasetDTO();
		tabPanel.flush(metadataDTO);
		metadataDTO.getIdentificationPart().setHierarchyLevel(hierarchyLevel);
		metadataDTO.getIdentificationPart().setHierarchyLevelName("");
		metadataDTO.getOtherPart().setUuid(uuid);
		metadataDTO.setObservatorySummary(observatorySummary);
		metadataDTO.setExperimentalSiteSummary(experimentalSiteSummary);
		metadataDTO.getOtherPart().setMetadataLastModificationDate(
				metadataLastModificationDate);
		return metadataDTO;
	}

	@UiHandler("validateButton")
	void onValidateButtonClicked(ClickEvent event) {
		MetadataDTO dto = flush();
		presenter.validate(dto);
	}

	@UiHandler("printButton")
	void onPrintButtonClicked(ClickEvent event) {
		if (StringUtil.isNotEmpty(uuid)) {
			presenter.print(uuid);
		}
	}

	@UiHandler("xmlButton")
	void onXmlButtonClicked(ClickEvent event) {
		if (StringUtil.isNotEmpty(uuid)) {
			presenter.xml(uuid);
		}
	}

	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event) {
		if (presenter != null) {
			presenter.back();
		} else if (presenter != null) {
			presenter.back();
		}
	}

	@UiHandler("saveButton")
	void onSaveButtonClicked(ClickEvent event) {
		MetadataDTO dto = flush();
		presenter.save(dto);
	}

	@Override
	public void setEditingPresenter(EditingPresenter editingPresenter) {
		// Nothing is done via this way
	}

	@Override
	public void setDisplayPresenter(DisplayPresenter displayPresenter) {
		// Nothing is done via this way
	}

	@Override
	public void setPresenter(DatasetEntryPresenter presenter) {
		this.presenter = presenter;
		constraintTab.setPresenter(presenter);
		contactTab.setPresenter(presenter);

	}

	@Override
	public void setUseConditions(I18nString result) {
		constraintTab.setUseConditions(result);
	}

	@Override
	public void setPublicAccessLimitations(I18nString result) {
		constraintTab.setPublicAccessLimitations(result);
	}

}
