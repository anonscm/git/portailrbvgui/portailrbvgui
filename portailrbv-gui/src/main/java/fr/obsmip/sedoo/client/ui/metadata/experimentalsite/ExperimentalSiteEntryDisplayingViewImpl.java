package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.breadcrumb.MetadataBreadcrumb;
import fr.obsmip.sedoo.client.ui.metadata.common.ConstraintTab;
import fr.obsmip.sedoo.client.ui.metadata.common.ContactTab;
import fr.obsmip.sedoo.client.ui.metadata.common.KeywordTab;
import fr.obsmip.sedoo.client.ui.metadata.common.serie.OtherTab;
import fr.obsmip.sedoo.client.ui.metadata.common.serie.TemporalExtendTab;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.AbstractDTOEditingView;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.view.common.MetadataTabPanel;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;

public class ExperimentalSiteEntryDisplayingViewImpl extends
		AbstractDTOEditingView implements ExperimentalSiteEntryDisplayingView {

	private static ExperimentalSiteEntryDisplayingViewImplUiBinder uiBinder = GWT
			.create(ExperimentalSiteEntryDisplayingViewImplUiBinder.class);

	private DisplayPresenter presenter;

	ArrayList<AbstractTab> tabs = new ArrayList<AbstractTab>();

	interface ExperimentalSiteEntryDisplayingViewImplUiBinder extends
			UiBinder<Widget, ExperimentalSiteEntryDisplayingViewImpl> {
	}

	@UiField
	Button backButton;

	@UiField
	Button printButton;

	@UiField
	Button xmlButton;

	@UiField
	MetadataBreadcrumb breadcrumb;

	@UiField(provided = true)
	MetadataTabPanel tabPanel;

	private String uuid;

	public ExperimentalSiteEntryDisplayingViewImpl(
			ArrayList<String> metadataLanguages, List<String> dataLanguages) {
		super();
		tabPanel = new MetadataTabPanel();

		tabPanel.addTab(new IdentificationTab(metadataLanguages),
				MetadataMessage.INSTANCE
						.metadataEditingIdentificationTabHeader());
		tabPanel.addTab(new ContactTab(),
				MetadataMessage.INSTANCE.metadataEditingContactTabHeader());
		tabPanel.addTab(new KeywordTab(new ArrayList<String>()),
				MetadataMessage.INSTANCE.metadataEditingKeywordTabHeader());
		tabPanel.addTab(new SingleGeographicalLocationTab(
				AreaSelectorWidget.DEFAULT_MAP_LAYER), Message.INSTANCE
				.metadataEditingGeographicalLocationTabHeader());
		tabPanel.addTab(new TemporalExtendTab(),
				Message.INSTANCE.metadataEditingTemporalExtentHeader());
		tabPanel.addTab(new ConstraintTab(metadataLanguages),
				MetadataMessage.INSTANCE.metadataEditingConstraintTabHeader());
		tabPanel.addTab(new OtherTab(metadataLanguages, dataLanguages),
				MetadataMessage.INSTANCE.metadataEditingOtherTabHeader());
		tabPanel.addTab(new DatasetsTab(), Message.INSTANCE.dataSets());
		tabPanel.activateSelectionHandler();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		reset();

	}

	@Override
	public void reset() {
		breadcrumb.reset();
		breadcrumb.setLevel(MetadataBreadcrumb.EXPERIMENTAL_SITE_LEVEL);
		tabPanel.reset();
		ElementUtil.hide(xmlButton);
		ElementUtil.hide(printButton);
		uuid = "";
	}

	@Override
	public void display(AbstractDTO dto) {
		reset();
		ExperimentalSiteDTO metadata = (ExperimentalSiteDTO) dto;
		breadcrumb.enableDisplayMode();
		breadcrumb.setObservatoryName(metadata.getParentSummary().getName());
		breadcrumb.setObservatoryUuid(metadata.getParentSummary().getUuid());
		breadcrumb.setExperimentalSiteName(metadata.getExperimentalSiteName());
		tabPanel.display(metadata);
		uuid = StringUtil.trimToEmpty(metadata.getOtherPart().getUuid());
		ElementUtil.show(xmlButton);
		ElementUtil.show(printButton);
	}

	@UiHandler("printButton")
	void onPrintButtonClicked(ClickEvent event) {
		if (StringUtil.isNotEmpty(uuid)) {
			presenter.print(uuid);
		}
	}

	@UiHandler("xmlButton")
	void onXmlButtonClicked(ClickEvent event) {
		if (StringUtil.isNotEmpty(uuid)) {
			presenter.xml(uuid);
		}
	}

	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event) {
		if (presenter != null) {
			presenter.back();
		}
	}

	@Override
	public void setDisplayPresenter(DisplayPresenter displayPresenter) {
		presenter = displayPresenter;
	}

}
