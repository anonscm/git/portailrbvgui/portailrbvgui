package fr.obsmip.sedoo.client.ui;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public interface MetadataSearchView extends IsWidget{
	
	public interface Presenter 
	 {
		void search(SearchCriteriaDTO criteria);
	 }
	
	void setPresenter(Presenter presenter);
	void update(SearchCriteriaDTO criteria);
	void setObservatories(List<String> observatories);
	
}
