package fr.obsmip.sedoo.client.activity;


public class OLDMetadataEditingActivity 
{
//	extends AbstractDTOEditingActivity implements EditingPresenter
//
//{
//
//	private final MetadataServiceAsync metadataService = GWT.create(MetadataService.class);
//	private final ObservatoryServiceAsync observatoryService = GWT.create(ObservatoryService.class);
//
//	private final String metadataUuid;
//	private final Long drainageBassinId;
//	private DrainageBasinDTO drainageBasinDTO;
//	private ObservatoryDTO observatoryDTO;
//	private String observatoryShortLabel;
//
//	public MetadataEditingActivity(MetadataEditingPlace place, ClientFactory clientFactory)
//	{
//		super(clientFactory, place);
//		metadataUuid = place.getMetadataUuid();
//		drainageBassinId = place.getDrainageBasinId();
//		observatoryShortLabel = place.getObservatoryShortLabel();
//		setMode(place.getMode());
//	}
//
//	@Override
//	public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
//	{
//		if (isValidUser() == false)
//		{
//			goToLoginPlace();
//			return;
//		}
//		sendActivityStartEvent();
//		containerWidget.setWidget(clientFactory.getProgressView());
//		view = clientFactory.getMetadataEditingView();
//		if (isCreateMode())
//		{
//			broadcastActivityTitle(Message.INSTANCE.metadataCreatingTitle());
//		} else
//		{
//			broadcastActivityTitle(Message.INSTANCE.metadataEditingTitle());
//		}
//		((MetadataEditingView) view).setPresenter(this);
//
//		if (drainageBassinId != null)
//		{
//			loadInfoStartingWithDrainageBasin(containerWidget);
//		}
//		else
//		{
//			loadInfoStartingWithMetadata(containerWidget);
//		}
//	}
//
//
//	private void loadInfoStartingWithMetadata(final AcceptsOneWidget containerWidget) 
//	{
//		metadataService.getMetadataByUuid(metadataUuid, new AsyncCallback<MetadataDTO>()
//				{
//
//			@Override
//			public void onSuccess(MetadataDTO result)
//			{
//				final MetadataDTO metadata = result;
//				
//				observatoryService.getObservatoryByShortLabel(observatoryShortLabel, new AsyncCallback<ObservatoryDTO>() {
//					
//					@Override
//					public void onSuccess(ObservatoryDTO result) {
//						observatoryDTO = result;
//						broadcastUiInfos(metadata);
//						view.setMode(Constants.MODIFY);
//						previousHash = metadata.getHash();
//						view.edit(metadata);
//						containerWidget.setWidget(view.asWidget());	
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						containerWidget.setWidget(clientFactory.getBlankView());
//						DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " : " + caught.getMessage());
//					}
//				});
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				containerWidget.setWidget(clientFactory.getBlankView());
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " : " + caught.getMessage());
//
//			}
//				});
//	}
//
//	private void loadInfoStartingWithDrainageBasin(final AcceptsOneWidget containerWidget) {
//		// On charge les infos du DrainageBasin
//		observatoryService.getDrainageBasinById(drainageBassinId, new AsyncCallback<DrainageBasinDTO>()
//				{
//
//			@Override
//			public void onSuccess(DrainageBasinDTO result)
//			{
//				drainageBasinDTO = result;
//
//				broadcastActivityTitle(Message.INSTANCE.metadataCreatingTitle() + " (" + drainageBasinDTO.getLabel() + ")");
//				List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//				shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//				
//				if (isCreateMode())
//				{
//					shortcuts.add(ShortcutFactory.getMetadataCreationShortcut());
//				} else
//				{
//					shortcuts.add(ShortcutFactory.getMetadataManagementShortcut());
//				}
//				
//				
//				shortcuts.add(ShortcutFactory.getMetadataCreatingShortcut(drainageBasinDTO.getLabel()));
//				clientFactory.getBreadCrumb().refresh(shortcuts);
//
//
//				// On charge les infos de l'observatoire
//				observatoryService.getObservatoryByDrainageBasinId(drainageBassinId, new AsyncCallback<ObservatoryDTO>()
//						{
//
//					@Override
//					public void onSuccess(ObservatoryDTO result)
//					{
//						observatoryDTO = result;
//
//						// On charge la fiche de métadonnées
//
//						if (getMode().compareTo(Constants.MODIFY) == 0)
//						{
//							metadataService.getMetadataByUuid(metadataUuid, new AsyncCallback<MetadataDTO>()
//									{
//
//								@Override
//								public void onSuccess(MetadataDTO result)
//								{
//									broadcastActivityTitle(Message.INSTANCE.drainageBasinEditingViewModificationHeader() + " (" + result.getIdentificationPart().getResourceTitleDisplay() + ")");
//									previousHash = result.getHash();
//									view.setMode(Constants.MODIFY);
//									previousHash = result.getHash();
//									view.edit(result);
//									containerWidget.setWidget(view.asWidget());
//								}
//
//								@Override
//								public void onFailure(Throwable caught)
//								{
//									containerWidget.setWidget(clientFactory.getBlankView());
//									DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " : " + caught.getMessage());
//
//								}
//									});
//						} else
//						{
//							previousHash = "";
//							view.setMode(Constants.CREATE);
//							((MetadataEditingView) view).edit(createNewMetadata());
//							containerWidget.setWidget(view.asWidget());
//						}
//
//					}
//
//					@Override
//					public void onFailure(Throwable caught)
//					{
//						containerWidget.setWidget(clientFactory.getBlankView());
//						DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " : " + caught.getMessage());
//
//					}
//						});
//
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				containerWidget.setWidget(clientFactory.getBlankView());
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " : " + caught.getMessage());
//
//			}
//				});
//
//	}
//
//
//
//	private String generatedXML = "";
//
//	private MetadataDTO createNewMetadata()
//	{
//		MetadataDTO aux = new MetadataDTO();
//
//		String loc = LocaleInfo.getCurrentLocale().getLocaleName();
//		if (loc.compareToIgnoreCase(Constants.FRENCH_LOCALE) == 0)
//		{
//			aux.getOtherPart().setMetadataLanguage(Constants.FRENCH);
//		} else
//		{
//			aux.getOtherPart().setMetadataLanguage(Constants.ENGLISH);
//		}
//
//		if (!StringUtil.isEmpty(observatoryDTO.getPublicAccessLimitations()))
//		{
//			aux.getConstraintPart().setPublicAccessLimitations(observatoryDTO.getPublicAccessLimitations().trim());
//		}
//		if (!StringUtil.isEmpty(observatoryDTO.getUseConditions()))
//		{
//			aux.getConstraintPart().setUseConditions(observatoryDTO.getUseConditions().trim());
//		}
//
//		if (!StringUtil.isEmpty(observatoryDTO.getDescription()))
//		{
//			aux.getObservatoryPart().setDescription(observatoryDTO.getDescription());
//		}
//
//		aux.getGeographicalLocationPart().setObservatoryName(observatoryDTO.getShortLabel());
//		aux.getGeographicalLocationPart().setDrainageBasinName(drainageBasinDTO.getLabel());
//
//		return aux;
//	}
//
//	@Override
//	public void generateXML(MetadataDTO metadata)
//	{
//		// TODO Auto-generated method stub
//		metadataService.toXML(metadata, (new AsyncCallback<String>()
//				{
//
//			@Override
//			public void onSuccess(String xml)
//			{
//				((MetadataEditingView) view).setGeneratedXML(xml);
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//			}
//				}));
//
//		/*
//		 * MetadataRequest metadataRequest =
//		 * clientFactory.getRbvRequestFactory().metadataRequest(); MetadataProxy
//		 * proxy = metadataRequest.create(MetadataProxy.class);
//		 * proxy.setResourceTitle("mon titre");
//		 * proxy.setResourceAbstract("mon resumé"); Request<String> xmlRequest =
//		 * metadataRequest.toXML(metadata);
//		 * 
//		 * xmlRequest.fire( new Receiver<String>() {
//		 * 
//		 * @Override public void onSuccess(String result) {
//		 * 
//		 * setGeneratedXML(result); } });
//		 * 
//		 * return getGeneratedXML();
//		 */
//	}
//
//	public String getGeneratedXML()
//	{
//		return generatedXML;
//	}
//
//	public void setGeneratedXML(String generatedXML)
//	{
//		this.generatedXML = generatedXML;
//	}
//
//	@Override
//	public void getObservatoryContacts(final MetadataContactSelectionTable table)
//	{
//		observatoryService.getObservatoryContactsByDrainageBasinId(drainageBassinId, new AsyncCallback<List<ObservatoryContactDTO>>()
//				{
//
//			@Override
//			public void onSuccess(List<ObservatoryContactDTO> result)
//			{
//				List<MetadataContactDTO> aux = new ArrayList<MetadataContactDTO>();
//				if (result != null)
//				{
//					Long index = 1L;
//					Iterator<ObservatoryContactDTO> iterator = result.iterator();
//					while (iterator.hasNext())
//					{
//						ObservatoryContactDTO current = iterator.next();
//						MetadataContactDTO contact = new MetadataContactDTO();
//						contact.setId(index);
//						contact.setEmail(current.getEmail());
//						contact.setOrganisationName(current.getOrganisationName());
//						contact.setRoles(current.getRoles());
//						contact.setPersonName(current.getPersonName());
//						contact.setCity(current.getCity());
//						contact.setCountry(current.getCountry());
//						contact.setZipCode(current.getZipCode());
//						contact.setAddress(current.getAddress());
//						aux.add(contact);
//						index++;
//					}
//
//				}
//				table.init(aux);
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " : " + caught.getMessage());
//
//			}
//				});
//	}
//
//	@Override
//	public void save(final MetadataDTO dto)
//	{
//
//		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.saving(), ActionEventConstant.METADATA_SAVING_EVENT, true);
//		clientFactory.getEventBus().fireEvent(e);
//		metadataService.save(dto, observatoryDTO.getId(), new AsyncCallback<Boolean>()
//				{
//
//			@Override
//			public void onSuccess(Boolean result)
//			{
//				previousHash = dto.getHash();
//				broadcastUiInfos(dto);
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//				clientFactory.getEventBus().fireEvent(new NotificationEvent(Message.INSTANCE.savedModifications()));
//				RBVUserDTO user = (RBVUserDTO) PortailRBV.getClientFactory().getUserManager().getUser();
//				Logger.addLog(user, LogConstant.METADATA_CATEGORY, LogConstant.MODIFICATION_ACTION, "User "+user.getName()+" has modified metadata of title : "+dto.getIdentificationPart().getResourceTitleDisplay()+" - UUID :"+dto.getOtherPart().getUuid());
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//			}
//				});
//
//	}
//	
//	private void broadcastUiInfos(MetadataDTO dto)
//	{
//		String aux = StringUtil.trimToEmpty(dto.getIdentificationPart().getResourceTitleDisplay());
//		broadcastActivityTitle(Message.INSTANCE.metadataEditingHeader() + " (" + aux + ")");
//		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//		if (isCreateMode())
//		{
//			shortcuts.add(ShortcutFactory.getMetadataCreationShortcut());
//		} else
//		{
//			shortcuts.add(ShortcutFactory.getMetadataManagementShortcut());
//		}
//		shortcuts.add(ShortcutFactory.getMetadataEditingShortcut(aux));
//		clientFactory.getBreadCrumb().refresh(shortcuts);
//	}
//
//	@Override
//	public void back()
//	{
//		List<Shortcut> shortcuts = clientFactory.getBreadCrumb().getShortcuts();
//		Shortcut shortcut = shortcuts.get(shortcuts.size() - 2);
//		clientFactory.getPlaceController().goTo(shortcut.getPlace());
//	}
//
//	@Override
//	public void validate(MetadataDTO dto)
//	{
//		List<OldValidationAlert> alerts = dto.validate();
//		if (alerts.isEmpty())
//		{
//			DialogBoxTools.popUp(Message.INSTANCE.metadataEditingNoValidationAlert());
//		} else
//		{
//			DialogBoxTools.popUp(Message.INSTANCE.error(), OldValidationAlert.toHTML(alerts), DialogBoxTools.HTML_MODE);
//		}
//
//	}
}
