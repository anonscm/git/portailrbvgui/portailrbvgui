package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.event.MenuSearchEvent;
import fr.obsmip.sedoo.client.event.SearchResultEvent;
import fr.obsmip.sedoo.client.event.SearchStartEvent;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.DatasetEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.obsmip.sedoo.client.service.GeonetworkObservatoryService;
import fr.obsmip.sedoo.client.service.GeonetworkObservatoryServiceAsync;
import fr.obsmip.sedoo.client.service.SearchService;
import fr.obsmip.sedoo.client.service.SearchServiceAsync;
import fr.obsmip.sedoo.client.ui.WelcomeView;
import fr.obsmip.sedoo.client.ui.WelcomeView.Presenter;
import fr.obsmip.sedoo.client.ui.searchcriteria.ObservatoryCatalogsLoadedEvent;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.obsmip.sedoo.shared.util.HierachyLevelUtil;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class WelcomeActivity extends RBVPublicActivity implements Presenter {

	public final static GeonetworkObservatoryServiceAsync OBSERVATORY_CATALOG_SERVICE = GWT
			.create(GeonetworkObservatoryService.class);

	public final static SearchServiceAsync SEARCH_SERVICE = GWT
			.create(SearchService.class);

	public final static int DEFAULT_PAGE_SIZE = 10;
	public final static int DEFAULT_POSITION = 1;

	SearchCriteriaDTO criteria;
	WelcomeView welcomeView;
	
	public WelcomeActivity(WelcomePlace place, ClientFactory clientFactory) {
		super(clientFactory);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		sendActivityStartEvent();
		welcomeView = clientFactory.getWelcomeView();
		welcomeView.setPresenter(this);
		containerWidget.setWidget(welcomeView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.headerViewCatalogLink());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);

		if (welcomeView.isFirstLoad()){
		final ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.NEWS_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		OBSERVATORY_CATALOG_SERVICE
				.findAll(new AsyncCallback<ArrayList<GeonetworkObservatoryDTO>>() {

					@Override
					public void onFailure(Throwable caught) {
						clientFactory.getEventBus().fireEvent(
								e.getEndingEvent());
						DialogBoxTools.modalAlert(
								CommonMessages.INSTANCE.error(),
								CommonMessages.INSTANCE.anErrorHasOccurred()
										+ " : " + caught.getMessage());

					}

					@Override
					public void onSuccess(
							ArrayList<GeonetworkObservatoryDTO> result) {
						clientFactory.getEventBus().fireEvent(
								e.getEndingEvent());
						clientFactory.getEventBus().fireEvent(
								new ObservatoryCatalogsLoadedEvent(result));
						
						if (welcomeView.isFirstLoad()){
							clientFactory.getEventBus().fireEvent(new MenuSearchEvent());
							welcomeView.setFirstLoad(false);
						}
												
					}
				});
		
		}
	}

	@Override
	public void search(SearchCriteriaDTO searchCriteria) {

		clientFactory.getEventBus().fireEvent(new SearchStartEvent());
		ActionStartEvent startEvent = new ActionStartEvent(
				Message.INSTANCE.metadataSearchingViewSearchInProgress(), "",
				true);
		clientFactory.getEventBus().fireEvent(startEvent);
		ArrayList<String> displayLanguages = clientFactory
				.getDisplayLanguages();
		SEARCH_SERVICE
				.getSummaries(
						searchCriteria,
						1,
						100000,
						displayLanguages,
						new DefaultAbstractCallBack<HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>>>(
								startEvent, clientFactory.getEventBus()) {
							@Override
							public void onSuccess(
									HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> result) {

								super.onSuccess(result);
								if (result.isEmpty()) {
									DialogBoxTools.modalAlert(
											CommonMessages.INSTANCE
													.information(),
											Message.INSTANCE
													.metadataSearchingEmptyResult());
								} else {
									clientFactory.getEventBus().fireEvent(
											new SearchResultEvent(result));
								}
							}
						});
	}

	@Override
	public void displayObservatoryEntryMetadata(String uuid) {
		ObservatoryEntryDisplayingPlace place = new ObservatoryEntryDisplayingPlace();
		place.setUuid(uuid);
		clientFactory.getPlaceController().goTo(place);

	}

	@Override
	public void displayExperimentalSiteEntryMetadata(String uuid) {
		ExperimentalSiteEntryDisplayingPlace place = new ExperimentalSiteEntryDisplayingPlace();
		place.setUuid(uuid);
		clientFactory.getPlaceController().goTo(place);

	}

	@Override
	public void displayDatasetEntryMetadata(String uuid) {
		DatasetEntryDisplayingPlace place = new DatasetEntryDisplayingPlace();
		place.setUuid(uuid);
		clientFactory.getPlaceController().goTo(place);
	}

	@Override
	public void displayEntryMetadata(String id, String level) {
		if (StringUtil.trimToEmpty(level).compareToIgnoreCase(
				HierachyLevelUtil.OBSERVATORY_HIERARCHY_LEVEL_NAME) == 0) {
			displayObservatoryEntryMetadata(id);
		} else if (StringUtil.trimToEmpty(level).compareToIgnoreCase(
				HierachyLevelUtil.EXPERIMENTAL_SITE_HIERARCHY_LEVEL_NAME) == 0) {
			displayExperimentalSiteEntryMetadata(id);
		} else {
			displayDatasetEntryMetadata(id);
		}

	}

}