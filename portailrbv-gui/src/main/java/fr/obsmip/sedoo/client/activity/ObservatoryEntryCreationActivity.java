package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.habilitation.HabilitationUtil;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.place.ObservatoryEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryEntryCreationView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ObservatoryEntryCreationActivity  extends RBVAuthenticatedActivity implements Presenter, LoadCallBack<ArrayList<IsObservatory>> {

	private final static  MetadataServiceAsync METADATA_SERVICE = GWT.create(MetadataService.class);

	private ObservatoryEntryCreationView view;

	private AcceptsOneWidget containerWidget;

	public ObservatoryEntryCreationActivity(ObservatoryEntryCreationPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		this.containerWidget = containerWidget;
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		broadcastActivityTitle(Message.INSTANCE.observatoryEntryCreation());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getObservatoryEntryCreationShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		containerWidget.setWidget(clientFactory.getProgressView());
		view = ((ClientFactory) clientFactory).getObservatoryEntryCreationView();
		view.setPresenter(this);
		ClientObservatoryList.getObservatories(this);
	}

	@Override
	public void postLoadProcess(ArrayList<IsObservatory> result) 
	{
		final List<IsObservatory> filterByUser = HabilitationUtil.filterByUser(result, (RBVUserDTO) clientFactory.getUserManager().getUser()); 
		if (filterByUser.isEmpty())
		{
			view.setObservatories(new ArrayList<IsObservatory>());
			containerWidget.setWidget(view);
		}
		else
		{
			final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.OBSERVATORIES_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(e);
			
			METADATA_SERVICE.getObservatorySummaries(clientFactory.getDisplayLanguages(), new DefaultAbstractCallBack<ArrayList<ObservatorySummaryDTO>>(e, clientFactory.getEventBus())	{
				@Override
				public void onSuccess(ArrayList<ObservatorySummaryDTO> result) {
					super.onSuccess(result);
					List<IsObservatory> aux = filterByUser;
					Iterator<ObservatorySummaryDTO> iterator = result.iterator();
					while (iterator.hasNext()) 
					{
						MetadataSummaryDTO current = (MetadataSummaryDTO) iterator.next();
						aux = removeElement(current, aux);
					}
					
					view.setObservatories(aux);
					containerWidget.setWidget(view);
				}

				private List<IsObservatory> removeElement(MetadataSummaryDTO current, List<IsObservatory> currentList)
				{
					String observatoryName = StringUtil.trimToEmpty(current.getName());
					ListIterator<IsObservatory> listIterator = currentList.listIterator();
					while (listIterator.hasNext()) {
						IsObservatory aux = (IsObservatory) listIterator.next();
						if (aux.getName().compareToIgnoreCase(observatoryName)==0)
						{
							listIterator.remove();
							break;
						}
					}
					return currentList;
				}
			});
		}
	}

	@Override
	public void create(String observatoryName) 
	{
		ObservatoryEditingPlace place = new ObservatoryEditingPlace();
		place.setObservatoryName(observatoryName);
		clientFactory.getPlaceController().goTo(place);
	}


}