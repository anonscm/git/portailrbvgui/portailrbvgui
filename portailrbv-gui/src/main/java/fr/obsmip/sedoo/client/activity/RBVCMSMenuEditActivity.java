package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.obsmip.sedoo.shared.util.MenuUtil;
import fr.sedoo.commons.client.cms.activity.CMSMenuEditActivity;
import fr.sedoo.commons.client.cms.bundle.MenuMessages;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.MenuEditPlace;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;

public class RBVCMSMenuEditActivity extends CMSMenuEditActivity {

	public RBVCMSMenuEditActivity(MenuEditPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		broadcastActivityTitle("");
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory
				.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));

		shortcuts.add(ShortcutFactory.getMenuEditionShortcut(place));

		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
	}

	@Override
	protected List<ValidationAlert> validate(String content) {
		String[] split = content.split("\n");
		for (int i = 0; i < split.length; i++) {
			String tmp = StringUtil.trimToEmpty(split[i]);
			if (tmp.compareToIgnoreCase(MenuUtil.RBV_ADMIN) == 0) {
				return new ArrayList<ValidationAlert>();
			}
		}
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		result.add(new ValidationAlert(MenuMessages.INSTANCE.menu(),
				Message.INSTANCE.adminMenuMandatory()));
		return result;

	}

	protected void broadcastActivityTitle(String title) {
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
	}

	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}

}
