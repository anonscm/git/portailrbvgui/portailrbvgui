
package fr.obsmip.sedoo.client.service;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SystemServiceAsync {

	void getApplicationVersion(AsyncCallback<String> callback);

	void getJavaVersion(AsyncCallback<String> callback);

	void getParameter(String parameterName, AsyncCallback<String> callback);

	void getParameters(AsyncCallback<HashMap<String, String>> callback);

	void getDatabaseInformations(AsyncCallback<String> callback);

}
