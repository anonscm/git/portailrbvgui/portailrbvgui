package fr.obsmip.sedoo.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.MetadataPrintHandler;
import fr.obsmip.sedoo.client.misc.MetadataXmlHandler;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryDisplayingView;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;

public class ExperimentalSiteEntryDisplayingActivity extends RBVPublicActivity
		implements DisplayPresenter {

	private String experimentalSiteUuid;
	private ExperimentalSiteEntryDisplayingView view;

	public ExperimentalSiteEntryDisplayingActivity(
			ExperimentalSiteEntryDisplayingPlace place,
			ClientFactory clientFactory) {
		super(clientFactory);
		if (StringUtil.isNotEmpty(place.getUuid())) {
			experimentalSiteUuid = place.getUuid();
		}
	}

	private static final MetadataServiceAsync METADATA_SERVICE = GWT
			.create(MetadataService.class);

	@Override
	public void start(final AcceptsOneWidget containerWidget, EventBus eventBus) {

		// The uuid is not present - we go to the welcome page
		if (StringUtil.isEmpty(experimentalSiteUuid)) {
			goToWelcomePlace();
			return;
		}
		sendActivityStartEvent();
		containerWidget.setWidget(clientFactory.getProgressView());
		broadcastActivityTitle(Message.INSTANCE
				.experimentalSiteDisplayingTitle());
		ActionStartEvent startEvent = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.OBSERVATORY_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		METADATA_SERVICE.getExperimentalSiteByUuid(experimentalSiteUuid,
				clientFactory.getMetadataLanguages(), LocaleUtil
						.getCurrentLanguage(clientFactory),
				new DefaultAbstractCallBack<ExperimentalSiteDTO>(startEvent,
						clientFactory.getEventBus()) {

					@Override
					public void onSuccess(ExperimentalSiteDTO result) {
						super.onSuccess(result);
						view = clientFactory
								.getExperimentalSiteEntryDisplayingView();
						((ExperimentalSiteEntryDisplayingView) view)
								.setDisplayPresenter(ExperimentalSiteEntryDisplayingActivity.this);
						containerWidget.setWidget(view.asWidget());
						view.display(result);
						MetadataDisplayActivityUtil.addShortcut(clientFactory
								.getBreadCrumb());
					}

				});
	}

	@Override
	public void back() {
		clientFactory.getBreadCrumb().back();
	}

	public void goToWelcomePlace() {
		clientFactory.getPlaceController().goTo(new WelcomePlace());
	}

	@Override
	public void print(String uuid) {
		MetadataPrintHandler.print(uuid);
	}

	@Override
	public void xml(String uuid) {
		MetadataXmlHandler.xml(uuid);
	}

}
