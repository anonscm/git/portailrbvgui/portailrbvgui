package fr.obsmip.sedoo.client.marker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface MarkerBundle extends ClientBundle
{
	@Source("red_Marker.png")
	ImageResource dataSetRedMarker();
	
	@Source("red_MarkerS.png")
	ImageResource experimentalSiteRedMarker();
	
	@Source("flesh_Marker.png")
	ImageResource dataSetFleshMarker();
	
	@Source("flesh_MarkerS.png")
	ImageResource experimentalSiteFleshMarker();
	
	@Source("lightBlue_Marker.png")
	ImageResource dataSetLightBlueMarker();
	
	@Source("lightBlue_MarkerS.png")
	ImageResource experimentalSiteLightBlueMarker();
	
	@Source("grey_Marker.png")
	ImageResource dataSetGreyMarker();
	
	@Source("grey_MarkerS.png")
	ImageResource experimentalSiteGreyMarker();
	
	@Source("darkPink_Marker.png")
	ImageResource dataSetDarkpinkMarker();
	
	@Source("darkPink_MarkerS.png")
	ImageResource experimentalSiteDarkpinkMarker();
	
	@Source("turquoise_Marker.png")
	ImageResource dataSetTurquoiseMarker();
	
	@Source("turquoise_MarkerS.png")
	ImageResource experimentalSiteTurquoiseMarker();
	
	@Source("blue_Marker.png")
	ImageResource dataSetBlueMarker();
	
	@Source("blue_MarkerS.png")
	ImageResource experimentalSiteBlueMarker();
	
	@Source("paleblue_Marker.png")
	ImageResource dataSetPaleBlueMarker();
	
	@Source("paleblue_MarkerS.png")
	ImageResource experimentalSitePaleBlueMarker();

	@Source("brown_Marker.png")
	ImageResource dataSetBrownMarker();
	
	@Source("brown_MarkerS.png")
	ImageResource experimentalSiteBrownMarker();
	
	@Source("green_Marker.png")
	ImageResource dataSetGreenMarker();
	
	@Source("green_MarkerS.png")
	ImageResource experimentalSiteGreenMarker();
	
	@Source("darkgreen_Marker.png")
	ImageResource dataSetDarkgreenMarker();
	
	@Source("darkgreen_MarkerS.png")
	ImageResource experimentalSiteDarkgreenMarker();
	
	@Source("orange_Marker.png")
	ImageResource dataSetOrangeMarker();
	
	@Source("orange_MarkerS.png")
	ImageResource experimentalSiteOrangeMarker();
	
	@Source("purple_Marker.png")
	ImageResource dataSetPurpleMarker();
	
	@Source("purple_MarkerS.png")
	ImageResource experimentalSitePurpleMarker();
	
	@Source("yellow_Marker.png")
	ImageResource dataSetYellowMarker();
	
	@Source("yellow_MarkerS.png")
	ImageResource experimentalSiteYellowMarker();
	
	@Source("pink_Marker.png")
	ImageResource dataSetPinkMarker();
	
	@Source("pink_MarkerS.png")
	ImageResource experimentalSitePinkMarker();
	
	public static final MarkerBundle INSTANCE = GWT.create(MarkerBundle.class);

}