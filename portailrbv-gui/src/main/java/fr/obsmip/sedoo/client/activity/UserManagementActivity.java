package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.place.UserManagementPlace;
import fr.obsmip.sedoo.client.service.UserService;
import fr.obsmip.sedoo.client.service.UserServiceAsync;
import fr.obsmip.sedoo.client.ui.UserManagementView;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.crud.widget.CrudPresenter;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class UserManagementActivity extends RBVAdministrationActivity implements CrudPresenter {
	
	private UserManagementView view = null; 
	
	private final static UserServiceAsync USER_SERVICE = GWT.create(UserService.class);
	
	public UserManagementActivity(UserManagementPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		view = clientFactory.getUserManagementView();
		view.setCrudPresenter(this);
		containerWidget.setWidget(view.asWidget());
		broadcastActivityTitle(Message.INSTANCE.userManagementViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getUserManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);

		final ActionStartEvent startEvent = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.OBSERVATORIES_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
        
		ClientObservatoryList.getObservatories(new LoadCallBack<ArrayList<IsObservatory>>() {
			
			@Override
			public void postLoadProcess(ArrayList<IsObservatory> result) {
				view.setObservatories(result);
				loadUsers();
				
			}
		});
	}
	
	
	private void loadUsers()
	{
		final ActionStartEvent startEvent = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.USER_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		USER_SERVICE.findAll(new AsyncCallback<ArrayList<RBVUserDTO>>() {

			@Override
			public void onSuccess(ArrayList<RBVUserDTO> users) 
			{
				view.setUsers(users);
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
			}
		});
	}

	@Override
	public void delete(final HasIdentifier hasIdentifier) {

		EventBus eventBus = clientFactory.getEventBus();
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstant.USER_DELETING_EVENT, true);
    	eventBus.fireEvent(e);
    	
    	USER_SERVICE.delete((RBVUserDTO) hasIdentifier, new AsyncCallback<Void>() {
    		
    		@Override
			public void onSuccess(Void result) 
			{
				view.broadcastDeletion(hasIdentifier);
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}
    		
		});
	}

	@Override
	public void create(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstant.USER_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	
    	USER_SERVICE.create((RBVUserDTO) hasIdentifier, new AsyncCallback<RBVUserDTO>() {
    		
    		@Override
			public void onSuccess(RBVUserDTO savedUser) 
			{
				view.broadcastCreation(savedUser);
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}
    		
		});
    	
	}

	@Override
	public void edit(HasIdentifier hasIdentifier) {

		EventBus eventBus = clientFactory.getEventBus();
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstant.USER_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	
    	USER_SERVICE.edit((RBVUserDTO) hasIdentifier, new AsyncCallback<RBVUserDTO>() {
    		
    		@Override
			public void onSuccess(RBVUserDTO savedUser) 
			{
				view.broadcastEdition(savedUser);
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}
    		
		});
	}

	
}
