package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ResetEvent extends GwtEvent<ResetEventHandler> {

	public static final Type<ResetEventHandler> TYPE = new Type<ResetEventHandler>();

	public ResetEvent() {
	}

	@Override
	protected void dispatch(ResetEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ResetEventHandler> getAssociatedType() {
		return TYPE;
	}

}
