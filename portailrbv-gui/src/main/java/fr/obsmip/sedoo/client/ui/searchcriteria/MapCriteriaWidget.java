package fr.obsmip.sedoo.client.ui.searchcriteria;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public interface MapCriteriaWidget extends CriteriaWidget {

	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria);

}
