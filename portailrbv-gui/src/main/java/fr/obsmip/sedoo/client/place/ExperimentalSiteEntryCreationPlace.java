package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class ExperimentalSiteEntryCreationPlace extends Place implements AuthenticatedPlace
{

	public static ExperimentalSiteEntryCreationPlace instance;
	
	private String observatoryName;

	public static class Tokenizer implements PlaceTokenizer<ExperimentalSiteEntryCreationPlace>
	{
		@Override
		public String getToken(ExperimentalSiteEntryCreationPlace place)
		{
			return "";
		}

		@Override
		public ExperimentalSiteEntryCreationPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ExperimentalSiteEntryCreationPlace();
			}
			return instance;
		}
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}
	
}
