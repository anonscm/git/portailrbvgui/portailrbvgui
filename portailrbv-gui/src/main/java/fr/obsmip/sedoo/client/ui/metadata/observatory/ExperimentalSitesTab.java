package fr.obsmip.sedoo.client.ui.metadata.observatory;

import fr.obsmip.sedoo.client.ui.ObservatoryEntryView.ObservatoryEntryPresenter;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;

public class ExperimentalSitesTab extends AbstractTab {

	ExperimentalSites experimentalSites;
	
	public ExperimentalSitesTab() {
		super();
		addSection(MetadataMessage.INSTANCE.experimentalSites());
		experimentalSites = new ExperimentalSites();
		addFullLineComponent(experimentalSites);
		reset();
	}
	
	public void setPresenter(ObservatoryEntryPresenter presenter)
	{
		experimentalSites.setPresenter(presenter);
	}

	public void broadcastExperimentalSiteDeletion(String uuid) 
	{
		experimentalSites.broadcastExperimentalSiteDeletion(uuid);
		
	}
	
}
