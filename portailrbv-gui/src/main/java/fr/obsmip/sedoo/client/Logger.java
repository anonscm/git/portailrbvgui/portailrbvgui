package fr.obsmip.sedoo.client;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.client.service.LocaleService;
import fr.obsmip.sedoo.client.service.LoggerService;
import fr.obsmip.sedoo.client.service.LoggerServiceAsync;
import fr.obsmip.sedoo.shared.domain.LogDTO;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;

public class Logger {

	private static final LoggerServiceAsync LOGGER_SERVICE = GWT.create(LoggerService.class);
	
	public static void addLog(RBVUserDTO user, String category, String action, String detail) 
	{
		LogDTO log = new LogDTO();
		log.setAction(action);
		log.setCategory(category);
		log.setDetail(detail);
		log.setDate(new Date());
		log.setUser(user.getName());
		LOGGER_SERVICE.add(log, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		}); 
	}

}
