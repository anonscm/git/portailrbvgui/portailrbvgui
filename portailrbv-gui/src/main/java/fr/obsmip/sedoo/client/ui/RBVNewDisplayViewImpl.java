package fr.obsmip.sedoo.client.ui;

import fr.sedoo.commons.client.news.widget.NewDisplayViewImpl;
import fr.sedoo.commons.client.print.PrintStyleProvider;

public class RBVNewDisplayViewImpl extends NewDisplayViewImpl{

	public RBVNewDisplayViewImpl(PrintStyleProvider printStyleProvider) {
		super(printStyleProvider);
		getBackButton().removeStyleName("btn-primary");
	}
	
}
