package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.news.bundle.NewsBundle;
import fr.sedoo.commons.client.news.event.MessageChangeEvent;
import fr.sedoo.commons.client.news.widget.CarrouselBlock;
import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.New;

public class NewsViewImpl extends AbstractView  implements NewsView {

	private static NewsViewImplUiBinder uiBinder = GWT.create(NewsViewImplUiBinder.class);

	interface NewsViewImplUiBinder extends UiBinder<Widget, NewsViewImpl> {
	}

	boolean loaded;
	
	@UiField(provided = true)
	Image rss;
	
	@UiField
	CarrouselBlock carrouselBlock;
	
	
	public NewsViewImpl() {
		GWT.<GlobalBundle> create(GlobalBundle.class).css().ensureInjected();
		rss = new Image(NewsBundle.INSTANCE.rss());
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}
	
	@Override
	public void onNotification(MessageChangeEvent event) {
		loaded = false;
	}

	@Override
	public void setNews(ArrayList<? extends New> frontPageNews, ArrayList<? extends New> otherNews) {
		carrouselBlock.setNews(frontPageNews, otherNews);
		loaded = true;
	}

	@Override
	public void setNewsListPresenter(NewsListPresenter presenter) {
		carrouselBlock.setPresenter(presenter);
	}
	
	@Override
	public void reset() {
		carrouselBlock.reset();
		loaded = false;
	}

	@Override
	public boolean areNewsLoaded() {
		return loaded;
	}
	
	@UiHandler("rss")
	void onRssButtonClicked(ClickEvent event) {
		String hostPageBaseURL = GWT.getHostPageBaseURL();
		String url = "";
		if (GWT.isProdMode()) {
			url = hostPageBaseURL + GWT.getModuleName() + "/rest/rss/feed/"
					+ LocaleUtil.getCurrentLanguage(PortailRBV.getClientFactory()).toLowerCase();
		} else {
			url = hostPageBaseURL + GWT.getModuleName() + "/rest/rss/feed/"
					+ LocaleUtil.getCurrentLanguage(PortailRBV.getClientFactory()).toLowerCase();
		}
		Window.open(url, "rss", "");
	}
	
	@Override
	public void onResize() {
		super.onResize();
		carrouselBlock.onResize();
	}
}
