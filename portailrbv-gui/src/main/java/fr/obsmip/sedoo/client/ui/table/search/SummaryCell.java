package fr.obsmip.sedoo.client.ui.table.search;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.obsmip.sedoo.client.misc.SummaryPresenter;
import fr.obsmip.sedoo.client.ui.table.summary.SummaryClickHandler;
import fr.obsmip.sedoo.client.ui.table.summary.SummaryRenderer;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public class SummaryCell extends AbstractCell<SummaryDTO> {

	private SummaryPresenter presenter;

	public SummaryCell() {
		// On ecoute seulement les click éventuels sur les boutons
		super("click");
	}

	@Override
	public void render(Context context, SummaryDTO value, SafeHtmlBuilder sb) {
		if (value == null) {
			return;
		} else {
			SummaryRenderer.render(context, value, sb);
			return;
		}
	}

	@Override
	public void onBrowserEvent(com.google.gwt.cell.client.Cell.Context context,
			Element parent, SummaryDTO value, NativeEvent event,
			ValueUpdater<SummaryDTO> valueUpdater) {
		super.onBrowserEvent(context, parent, value, event, valueUpdater);
		EventTarget eventTarget = event.getEventTarget();
		if (Element.is(eventTarget)) {
			Element src = Element.as(eventTarget);
			SummaryClickHandler.handleClick(src, value, presenter);
		}
	}

	private SummaryPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(SummaryPresenter presenter) {
		this.presenter = presenter;
	}

}
