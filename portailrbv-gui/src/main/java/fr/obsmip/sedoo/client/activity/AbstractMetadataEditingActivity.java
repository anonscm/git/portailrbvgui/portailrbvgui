package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.Constants;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.misc.MetadataEditionShortcut;
import fr.obsmip.sedoo.client.service.DirectoryService;
import fr.obsmip.sedoo.client.service.DirectoryServiceAsync;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumb;
import fr.obsmip.sedoo.client.ui.table.ObservatoryPersonTable;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.metadata.client.ui.view.DTOEditingView;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public abstract class AbstractMetadataEditingActivity extends
		RBVAuthenticatedActivity implements
		LoadCallBack<ArrayList<IsObservatory>> {

	public AbstractMetadataEditingActivity(ClientFactory clientFactory,
			Place place) {
		super(clientFactory, place);
	}

	protected DTOEditingView view;
	protected String previousHash = "#";
	private String mode = Constants.CREATE;
	private String observatoryName;

	private final static DirectoryServiceAsync DIRECTORY_SERVICE = GWT
			.create(DirectoryService.class);
	private ObservatoryPersonTable table;

	/**
	 * This function indicates if some modifications has been done and not saved
	 * 
	 * @return true: modifications needs to be saved false: no modifications
	 */
	protected boolean isDirty() {
		AbstractDTO current = view.flush();
		return (previousHash.compareTo(current.getHash()) != 0);
	}

	public String mayStop() {
		// On teste si la vue est nulle. Cela correspond à une redirection vers
		// la page de login - le maystop doit repondre null dans ce cas
		if (view == null) {
			return null;
		}
		if (isDirty()) {
			if (mode.compareTo(Constants.MODIFY) == 0) {
				return Message.INSTANCE.unsavedModificationsConfirmation();
			} else {
				return Message.INSTANCE.unsavedCreationConfirmation();
			}
		} else {
			return null;
		}
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public boolean isCreateMode() {
		return (mode.compareTo(Constants.CREATE) == 0);
	}

	public abstract String getObservatoryName();

	public void loadDirectoryPersonsByObservatoryName(String observatoryName,
			ObservatoryPersonTable table) {
		if (StringUtil.isNotEmpty(observatoryName)) {
			this.table = table;
			this.observatoryName = observatoryName;
			ClientObservatoryList.getObservatories(this);
		}
	}

	@Override
	public void postLoadProcess(ArrayList<IsObservatory> result) {
		String id = "";
		Iterator<IsObservatory> iterator = result.iterator();
		while (iterator.hasNext()) {
			IsObservatory isObservatory = iterator.next();
			if (isObservatory.getName().compareToIgnoreCase(observatoryName) == 0) {
				id = "" + ((GeonetworkObservatoryDTO) isObservatory).getId();
				break;
			}
		}
		if (StringUtil.isNotEmpty(id)) {
			final ActionStartEvent e = new ActionStartEvent(
					CommonMessages.INSTANCE.loading(),
					ActionEventConstant.BASIC_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(e);
			DIRECTORY_SERVICE.loadPersons(id,
					new DefaultAbstractCallBack<ArrayList<PersonDTO>>(e,
							clientFactory.getEventBus()) {
						@Override
						public void onSuccess(ArrayList<PersonDTO> result) {
							super.onSuccess(result);
							table.init(result);
						}
					});
		}
	}

	protected void addShortcut(BreadCrumb breadCrumb) {
		List<Shortcut> shortcuts = breadCrumb.getShortcuts();
		if (ListUtil.isEmpty(shortcuts)) {
			ArrayList<Shortcut> newShortcuts = new ArrayList<Shortcut>();
			newShortcuts.add(ShortcutFactory.getWelcomeShortcut());
			newShortcuts.add(new MetadataEditionShortcut());
			breadCrumb.refresh(newShortcuts);
		} else {
			Shortcut shortcut = shortcuts.get(shortcuts.size() - 1);
			if (shortcut instanceof MetadataEditionShortcut) {
				// On ne fait rien
				return;
			} else {
				shortcuts.add(new MetadataEditionShortcut());
				breadCrumb.refresh(shortcuts);
			}
		}
	}

}
