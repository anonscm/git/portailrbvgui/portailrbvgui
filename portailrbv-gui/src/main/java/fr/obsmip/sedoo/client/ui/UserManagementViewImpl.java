package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.ui.table.user.UserTable;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.crud.component.CrudViewImpl;
import fr.sedoo.commons.client.crud.widget.CrudTable;

public class UserManagementViewImpl extends CrudViewImpl implements UserManagementView {

	private static UserManagementViewImplUiBinder uiBinder = GWT
			.create(UserManagementViewImplUiBinder.class);

	interface UserManagementViewImplUiBinder extends UiBinder<Widget, UserManagementViewImpl> {
	}

	@UiField
	UserTable userTable;
	
	public UserManagementViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public CrudTable getCrudTable() {
		return userTable;
	}

	@Override
	public void setUsers(List<RBVUserDTO> users) {
		userTable.init(users);
	}

	@Override
	public void setObservatories(ArrayList<? extends IsObservatory> observatories) {
		userTable.setObservatories(observatories);
		
	}

	

}
