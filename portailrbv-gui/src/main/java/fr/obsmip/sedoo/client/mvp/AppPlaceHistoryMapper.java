package fr.obsmip.sedoo.client.mvp;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

import fr.obsmip.sedoo.client.place.DatasetEditingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryManagementPlace;
import fr.obsmip.sedoo.client.place.DirectoryManagementPlace;
import fr.obsmip.sedoo.client.place.DrainageBasinEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryManagementPlace;
import fr.obsmip.sedoo.client.place.GeoSummaryPlace;
import fr.obsmip.sedoo.client.place.GeonetworkObservatoryManagementPlace;
import fr.obsmip.sedoo.client.place.HarvestManagementPlace;
import fr.obsmip.sedoo.client.place.LogConsultationPlace;
import fr.obsmip.sedoo.client.place.MetadataEditingPlace;
import fr.obsmip.sedoo.client.place.MetadataManagingPlace;
import fr.obsmip.sedoo.client.place.MetadataResultPlace;
import fr.obsmip.sedoo.client.place.MetadataSearchPlace;
import fr.obsmip.sedoo.client.place.NewsPlace;
import fr.obsmip.sedoo.client.place.ObservatoryContactEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryManagementPlace;
import fr.obsmip.sedoo.client.place.SystemPlace;
import fr.obsmip.sedoo.client.place.UserManagementPlace;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.cms.place.MenuEditPlace;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.metadata.client.place.FormatListPlace;

/**
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a
 * separate TokenizerFactory.
 */
@WithTokenizers({ CMSConsultPlace.Tokenizer.class,
		CMSEditPlace.Tokenizer.class, CMSListPlace.Tokenizer.class,
		DatasetEntryManagementPlace.Tokenizer.class,
		NewDisplayPlace.Tokenizer.class,
		HarvestManagementPlace.Tokenizer.class,
		DirectoryManagementPlace.Tokenizer.class,
		DatasetEditingPlace.Tokenizer.class,
		DatasetEntryDisplayingPlace.Tokenizer.class,
		DatasetEntryCreationPlace.Tokenizer.class,
		ObservatoryEntryDisplayingPlace.Tokenizer.class,
		ExperimentalSiteEntryDisplayingPlace.Tokenizer.class,
		ExperimentalSiteEntryManagementPlace.Tokenizer.class,
		ExperimentalSiteEntryCreationPlace.Tokenizer.class,
		ExperimentalSiteEditingPlace.Tokenizer.class,
		GeonetworkObservatoryManagementPlace.Tokenizer.class,
		LogConsultationPlace.Tokenizer.class,
		UserManagementPlace.Tokenizer.class, MessageEditPlace.Tokenizer.class,
		MessageManagePlace.Tokenizer.class, GeoSummaryPlace.Tokenizer.class,
		FormatListPlace.Tokenizer.class, MetadataManagingPlace.Tokenizer.class,
		SystemPlace.Tokenizer.class, DrainageBasinEditingPlace.Tokenizer.class,
		WelcomePlace.Tokenizer.class, MetadataEditingPlace.Tokenizer.class,
		LanguageSwitchPlace.Tokenizer.class,
		MetadataResultPlace.Tokenizer.class,
		MetadataSearchPlace.Tokenizer.class, LoginPlace.Tokenizer.class,
		NewsArchivePlace.Tokenizer.class,
		NewDisplayPlace.Tokenizer.class,
		NewsPlace.Tokenizer.class,
		ObservatoryEntryManagementPlace.Tokenizer.class,
		ObservatoryEntryCreationPlace.Tokenizer.class,
		ObservatoryContactEditingPlace.Tokenizer.class,
		ObservatoryEditingPlace.Tokenizer.class,
		MenuEditPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
