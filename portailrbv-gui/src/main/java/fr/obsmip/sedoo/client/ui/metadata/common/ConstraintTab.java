package fr.obsmip.sedoo.client.ui.metadata.common;

import java.util.ArrayList;

import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.LabelFactory;
import fr.sedoo.metadata.client.ui.widget.field.impl.PublicAccessLimitations;
import fr.sedoo.metadata.client.ui.widget.field.impl.UseConditions;

public class ConstraintTab extends AbstractTab {


	public ConstraintTab(ArrayList<String> displayLanguages) {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingConstraintTabHeader());
		addLeftComponent(LabelFactory.getLabelByKey(FieldConstant.USE_CONDITIONS));
		addFullLineComponent(new UseConditions(displayLanguages));
		addLeftComponent(LabelFactory.getLabelByKey(FieldConstant.PUBLIC_ACCESS_LIMITATIONS));
		addFullLineComponent(new PublicAccessLimitations(displayLanguages));
		reset();
	}

}
