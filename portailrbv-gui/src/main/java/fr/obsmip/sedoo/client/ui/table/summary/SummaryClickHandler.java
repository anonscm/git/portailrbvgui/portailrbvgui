package fr.obsmip.sedoo.client.ui.table.summary;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.MetadataPrintHandler;
import fr.obsmip.sedoo.client.misc.MetadataXmlHandler;
import fr.obsmip.sedoo.client.misc.SummaryPresenter;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.obsmip.sedoo.shared.util.HierachyLevelUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;

public class SummaryClickHandler
{

	public static void handleClick(Element src, SummaryDTO summary, SummaryPresenter presenter)
	{
		String id = src.getId();
		if (id != null)
		{
			if (id.startsWith(SummaryRenderer.SEE_METADATA))
			{
				if (HierachyLevelUtil.isObservatory(summary.getHierarchyLevelName()))
				{
					presenter.displayObservatoryEntryMetadata(summary.getUuid());
					return;
				}
				else if (HierachyLevelUtil.isExperimentalSite(summary.getHierarchyLevelName()))
				{
					presenter.displayExperimentalSiteEntryMetadata(summary.getUuid());
					return;
				}
				else
				{
					presenter.displayDatasetEntryMetadata(summary.getUuid());
					return;
				}
				
			}
			if (id.startsWith(SummaryRenderer.PDF_METADATA))
			{
				String uuid = summary.getUuid();
				MetadataPrintHandler.print(uuid);
				return;
			}
			if (id.startsWith(SummaryRenderer.XML_METADATA))
			{
				String uuid = summary.getUuid();
				MetadataXmlHandler.xml(uuid);
				return;
			}
			if (SummaryRenderer.isSnapshotSrcId(id))
			{
				String contentId= SummaryRenderer.getContentIdFromSrcId(id);
				int index = 0;
				Element content = null;
				while ((content = DOM.getElementById(contentId+index)) != null)
				{
					if (StringUtil.isEmpty(content.getStyle().getProperty("display")))
					{
						content.getStyle().setProperty("display", "none");
						
					}
					else
					{
						content.getStyle().setProperty("display", "");
					}
					index++;
				}
				
				String closeId= SummaryRenderer.getClosedImgId(id);
				String openId= SummaryRenderer.getOpenedImgId(id);
				
				Element closeImg = DOM.getElementById(closeId);
				Element openImg = DOM.getElementById(openId);
				System.out.println(openImg.getStyle().getVisibility());
				if (StringUtil.isEmpty(openImg.getStyle().getProperty("display")))
				{
					closeImg.getStyle().setProperty("display", "");
					openImg.getStyle().setProperty("display", "none");
				}
				else
				{
					closeImg.getStyle().setProperty("display", "none");
					openImg.getStyle().setProperty("display", "");
				}
			}
		}
		if (src.getNodeName().compareToIgnoreCase("img") == 0)
		{
			if (SummaryRenderer.isSnapshotSrcId(id)== false)
			{
				DialogBoxTools.popUpImage(Message.INSTANCE.view(), src.getAttribute("src"));
			}
		}

	}

}
