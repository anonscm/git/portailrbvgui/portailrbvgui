package fr.obsmip.sedoo.client.ui;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.UserLoginEvent;
import fr.obsmip.sedoo.client.event.UserLogoutEvent;
import fr.obsmip.sedoo.client.mvp.Presenter;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.obsmip.sedoo.shared.util.MenuUtil;
import fr.sedoo.commons.client.cms.bundle.MenuMessages;
import fr.sedoo.commons.client.cms.event.MenuChangeEvent;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.service.CMSMenuService;
import fr.sedoo.commons.client.cms.service.CMSMenuServiceAsync;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class HeaderViewImpl extends ResizeComposite implements HeaderView {

	private final ClientFactory clientFactory = PortailRBV.getClientFactory();
	private Presenter presenter;
	private static HeaderViewImplUiBinder uiBinder = GWT
			.create(HeaderViewImplUiBinder.class);

	interface HeaderViewImplUiBinder extends UiBinder<Widget, HeaderViewImpl> {
	}

	@UiField
	Image englishLanguageImage;
	@UiField
	Image frenchLanguageImage;
	@UiField
	Anchor signOutLink;
	@UiField
	Anchor signInLink;
	@UiField
	Element userName;
	@UiField
	Label notConnectedMessage;

	@UiField
	InlineLabel informationSiteLink;

	@UiField
	InlineLabel catalogSiteLink;

	@UiField(provided = true)
	MenuBar menuBar;

	@UiField
	DockLayoutPanel container;

	public final static CMSMenuServiceAsync CMS_MENU_SERVICE = GWT
			.create(CMSMenuService.class);

	private static MenuItem administrationItem;

	private static final int INFORMATION_SITE = 1;
	private static final int CATALOG_SITE = 0;
	private int currentSite = INFORMATION_SITE;

	public HeaderViewImpl() {

		initSubmenu();
		initWidget(uiBinder.createAndBindUi(this));
		if (clientFactory.getUserManager().getUser() != null) {
			signOutLink.setVisible(true);
			signInLink.setVisible(false);
		}
		clientFactory.getEventBus().addHandler(MenuChangeEvent.TYPE, this);
	}

	@UiHandler("englishLanguageImage")
	void onEnglishLanguageButtonClicked(ClickEvent event) {
		getPresenter().goTo(new LanguageSwitchPlace(LocaleUtil.ENGLISH));
	}

	@UiHandler("frenchLanguageImage")
	void onFrenchLanguageButtonClicked(ClickEvent event) {
		getPresenter().goTo(new LanguageSwitchPlace(LocaleUtil.FRENCH));
	}

	@UiHandler("signInLink")
	void onSignInClicked(ClickEvent event) {
		Presenter presenter = PortailRBV.getPresenter();
		presenter.goTo(new LoginPlace());
	}

	private void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}

	@UiHandler(value = { "informationSiteLink" })
	void onCatalogSiteLinkClicked(ClickEvent event) {
		currentSite = INFORMATION_SITE;
		toggleMenu();
		goTo(new CMSConsultPlace(ScreenNames.HOME));
	}

	private void toggleMenu() {
		informationSiteLink.getElement().toggleClassName("clickable");
		informationSiteLink.getElement().getParentElement()
				.toggleClassName("selectedMenuItem");
		informationSiteLink.getElement().getParentElement()
				.toggleClassName("unselectedMenuItem");
		catalogSiteLink.getElement().toggleClassName("clickable");
		catalogSiteLink.getElement().getParentElement()
				.toggleClassName("selectedMenuItem");
		catalogSiteLink.getElement().getParentElement()
				.toggleClassName("unselectedMenuItem");
	}

	// @UiHandler("welcomeLink")
	// void onWelcomeClicked(ClickEvent event) {
	//
	// getPresenter().goTo(new WelcomePlace());
	// }

	@UiHandler("catalogSiteLink")
	void onInformationSiteLinkClicked(ClickEvent event) {
		currentSite = CATALOG_SITE;
		toggleMenu();
		goTo(new WelcomePlace());
	}

	@UiHandler("collaborativeSiteLink")
	void onCollaborativeSiteLinkClicked(ClickEvent event) {
		Window.open("https://extra.core-cloud.net/projets/soere-rbv/",
				"_blank", "");
	}

	@UiHandler("signOutLink")
	void onSignOutClicked(ClickEvent event) {
		PortailRBV.logout();
	}

	public Presenter getPresenter() {
		if (presenter == null) {
			presenter = PortailRBV.getPresenter();
		}
		return presenter;
	}

	@Override
	public void onNotification(UserLoginEvent event) {
		RBVUserDTO user = (RBVUserDTO) clientFactory.getUserManager().getUser();
		notConnectedMessage.setVisible(false);
		userName.setInnerText(CommonMessages.INSTANCE.loggedAs() + " "
				+ user.getName());
		signOutLink.setVisible(true);
		signInLink.setVisible(false);

		if (administrationItem != null) {
			loadMenu(MenuMessages.INSTANCE.refreshingMenu());
		}

	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		notConnectedMessage.setVisible(true);
		userName.setInnerText("");
		signOutLink.setVisible(false);
		signInLink.setVisible(true);

		if (administrationItem != null) {
			loadMenu(MenuMessages.INSTANCE.refreshingMenu());
		}

	}

	@Override
	public void enableInformationSiteMenu() {
		if (currentSite == CATALOG_SITE) {
			currentSite = INFORMATION_SITE;
			toggleMenu();
		}

	}

	@Override
	public void enableCatalogSiteMenu() {
		if (currentSite == INFORMATION_SITE) {
			currentSite = CATALOG_SITE;
			toggleMenu();
		}

	}

	@Override
	public int getBannerHeight() {
		return 147;
	}

	@Override
	public int getSubMenuHeight() {
		return 30;
	}

	@Override
	public MenuBar getSubmenu() {
		return menuBar;
	}

	@Override
	public void initSubmenu() {
		menuBar = new MenuBar();
		menuBar.setAutoOpen(true);
		loadMenu(MenuMessages.INSTANCE.loadingMenu());
	}

	private void loadMenu(final String message) {
		menuBar.clearItems();
		final EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(message,
				ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		CMS_MENU_SERVICE.getContent(new DefaultAbstractCallBack<String>(e,
				eventBus) {

			@Override
			public void onSuccess(final String menuContent) {
				super.onSuccess(menuContent);

				ActionStartEvent e = new ActionStartEvent(message,
						ActionEventConstants.BASIC_LOADING_EVENT, true);
				eventBus.fireEvent(e);

				String currentLanguage = LocaleUtil
						.getCurrentLanguage(clientFactory);
				CMS_MENU_SERVICE.getScreenTitleByLanguage(currentLanguage,
						new DefaultAbstractCallBack<HashMap<String, String>>(e,
								eventBus) {

							@Override
							public void onSuccess(HashMap<String, String> result) {
								super.onSuccess(result);
								clientFactory.getScreenNames().clear();
								clientFactory.getScreenNames().putAll(result);
								parseMenu(menuContent);
								refreshTitle();
							}
						});

			}
		});

	}

	/*
	 * mise à jour du shortcut, du sectionHeaderView et du menu
	 */
	private void refreshTitle() {
		List<Shortcut> shorts = clientFactory.getBreadCrumb().getShortcuts();
		String screenName = shorts.get(shorts.size() - 1).getLabel();
		if (clientFactory.getScreenNames().get(screenName) != null) {
			shorts.get(shorts.size() - 1).setLabel(
					clientFactory.getScreenNames().get(screenName));
			clientFactory.getBreadCrumb().refresh(shorts);
		}
		clientFactory.getEventBus().fireEvent(
				new ActivityChangeEvent(screenName));
	}

	private void parseMenu(String menuDefinition) {
		HashMap<Integer, MenuBar> submenus = new HashMap<Integer, MenuBar>();
		submenus.put(0, menuBar);
		String aux = StringUtil.trimToEmpty(menuDefinition);
		String[] lines = aux.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (StringUtil.isEmpty(line)) {
				continue;
			} else {

				if (MenuUtil.isSubMenuLine(line)) {
					MenuBar newSubMenu = new MenuBar(true);
					int subMenuLevel = MenuUtil.getLineLevel(line) + 1;
					MenuBar parentMenu = submenus.get(subMenuLevel - 1);
					parentMenu.addItem(MenuUtil.getSubMenuLabel(line),
							newSubMenu);
					submenus.put(subMenuLevel, newSubMenu);
				} else {
					if (MenuUtil.isSeparatorLine(line)) {
						int lineLevel = MenuUtil.getLineLevel(line);
						MenuBar lineSubMenu = submenus.get(lineLevel);
						lineSubMenu.addSeparator();
					} else {
						MenuItem tmp = MenuUtil.createMenuItemFromString(line,
								clientFactory.getScreenNames());
						if (tmp != null) {
							int lineLevel = MenuUtil.getLineLevel(line);
							MenuBar lineSubMenu = submenus.get(lineLevel);
							MenuItem resultItem = lineSubMenu.addItem(tmp);
							if (StringUtil.trimToEmpty(line)
									.compareToIgnoreCase(MenuUtil.RBV_ADMIN) == 0) {
								administrationItem = resultItem;

								if (((AuthenticatedClientFactory) clientFactory)
										.getUserManager().getUser() == null) {
									administrationItem.getParentMenu()
											.removeItem(administrationItem);
								}

							}

						}
					}
				}
			}
		}
	}

	public double getPreferredHeight() {
		return 30;
	}

	@Override
	public void onNotification(MenuChangeEvent event) {
		loadMenu(MenuMessages.INSTANCE.refreshingMenu());
	}
}
