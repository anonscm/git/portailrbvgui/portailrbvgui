package fr.obsmip.sedoo.client.ui.searchcriteria;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

public class SearchObservatoryCriteria {

	private GeographicBoundingBoxDTO boundingBox;

	private ArrayList<String> instrumentation = new ArrayList<String>();
	private ArrayList<String> datasets = new ArrayList<String>();
	private ArrayList<String> observatories = new ArrayList<String>();

	private boolean includesDatasets = false;
	private boolean includesExperimentalSites = false;
	private boolean includesObservatories = false;

	public GeographicBoundingBoxDTO getBoundingBox() {
		return boundingBox;
	}

	public void setBoundingBox(GeographicBoundingBoxDTO boundingBox) {
		this.boundingBox = boundingBox;
	}

	public List<String> getInstrumentation() {
		return instrumentation;
	}

	public void setInstrumentation(ArrayList<String> instrumentation) {
		this.instrumentation = instrumentation;
	}

	public List<String> getDatasets() {
		return datasets;
	}

	public void setDatasets(ArrayList<String> datasets) {
		this.datasets = datasets;
	}

	public List<String> getObservatories() {
		return observatories;
	}

	public void setObservatories(ArrayList<String> observatories) {
		this.observatories = observatories;
	}

	public void setIncludesObservatories(boolean value) {
		includesObservatories = value;
	}

	public void setIncludesDatasets(boolean value) {
		includesDatasets = value;
	}

	public void setIncludesExperimentalSites(boolean value) {
		includesExperimentalSites = value;
	}

	public boolean getIncludesObservatories() {
		return includesObservatories;
	}

	public boolean getIncludesDatasets() {
		return includesDatasets;
	}

	public boolean getIncludesExperimentalSites() {
		return includesExperimentalSites;
	}

	public String getRestUrl() {
		return "xxx";
	}
}
