package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public interface DatasetEntryManagementView extends IsWidget {

	void init(List<MetadataSummaryDTO> datasetSummaries);

	void broadcastDatasetDeletion(String uuid);

	void reset();

	void setPresenter(Presenter presenter);

	public interface Presenter {
		void deleteDatasetEntry(String uuid);

		void goToEditPlace(MetadataSummaryDTO datasetDTO);

		void goToDatasetEntryCreationPlace();

		void loadExperimentalSiteFromObservatoryUuid(String uuid);

		void goToExperimentalSiteEntryCreationPlace(String itemText);

		void goToObservatoryEntryCreationPlace();

		void loadDatasetFromExperimentalSiteUuid(String uuid);

		void goToDatasetEntryCreationPlace(String observatory,
				String experimentalSite);
	}

	void setObservatories(List<ObservatorySummaryDTO> filteredObservatories);

	void setExperimentalSites(ArrayList<ExperimentalSiteSummaryDTO> result);

	void setDatasets(ArrayList<MetadataSummaryDTO> result);

}
