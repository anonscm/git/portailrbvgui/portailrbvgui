package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class DirectoryManagementPlace extends Place implements AuthenticatedPlace
{

	public static DirectoryManagementPlace instance;

	public static class Tokenizer implements PlaceTokenizer<DirectoryManagementPlace>
	{
		@Override
		public String getToken(DirectoryManagementPlace place)
		{
			return "";
		}

		@Override
		public DirectoryManagementPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new DirectoryManagementPlace();
			}
			return instance;
		}
	}
	
}
