package fr.obsmip.sedoo.client.ui.table;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.DirectoryManagementView.Presenter;
import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.dialog.OkCancelDialog;
import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.client.widget.table.multiline.MultilineTextColumn;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.widget.dialog.contact.PersonDTOEditor;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;


public class ObservatoryPersonTable extends AbstractListTable {

	private Presenter presenter;
	private String observatoryId;

	public ObservatoryPersonTable() {
		setAddButtonEnabled(true);
	}

	@Override
	protected void initColumns() {
		
		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getPersonName();
			}
		};
		itemTable.addColumn(nameColumn, Message.INSTANCE.name());
		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);
		
		TextColumn<HasIdentifier> emailColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getEmail();
			}
		};
		itemTable.addColumn(emailColumn, MetadataMessage.INSTANCE.personEmail());
		itemTable.setColumnWidth(emailColumn, 100.0, Unit.PX);

		TextColumn<HasIdentifier> organisationColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getOrganisationName();
			}
		};
		itemTable.addColumn(organisationColumn, MetadataMessage.INSTANCE.personOrganisationName());
		itemTable.setColumnWidth(organisationColumn, 100.0, Unit.PX);

		MultilineTextColumn<HasIdentifier> adressColumn = new MultilineTextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getCompleteAdress();
			}
		};
		itemTable.addColumn(adressColumn, MetadataMessage.INSTANCE.personAddress());
		itemTable.setColumnWidth(adressColumn, 100.0, Unit.PX);

		super.initColumns();
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter.deletePerson(hasId.getIdentifier());
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		ArrayList<String> fakeRole = new ArrayList<String>();
		fakeRole.add("FAKE_ROLE");
		ObservatoryPersonDialogBoxContent content = new ObservatoryPersonDialogBoxContent(fakeRole);
		ConfirmCallBack callBack = new EditConfirmCallBack(hasId.getIdentifier(), content);
		content.setConfirmCallBack(callBack);
		content.edit((PersonDTO) hasId);
		OkCancelDialog dialog = new OkCancelDialog(Message.INSTANCE.addPerson(), content);
		dialog.show();
		
	}
	
	private class EditConfirmCallBack implements ConfirmCallBack {
		PersonDTOEditor editor;
		String id;

		public EditConfirmCallBack(String id, PersonDTOEditor editor) {
			this.editor = editor;
			this.id = id;
		}

		@Override
		public void confirm(boolean choice) {
			if (choice == true) {
				ObservatoryPersonDTO resultValue = (ObservatoryPersonDTO) editor.getValue();
				resultValue.setId(new Long(id));
				resultValue.setObservatoryId(new Long(getObservatoryId()));
				presenter.save(resultValue);
			}
		}
	}
	

	@Override
	public void addItem() {
		ArrayList<String> fakeRole = new ArrayList<String>();
		fakeRole.add("FAKE_ROLE");
		
		ObservatoryPersonDialogBoxContent content = new ObservatoryPersonDialogBoxContent(fakeRole);
		ConfirmCallBack callBack = new CreateConfirmCallBack(content);
		content.setConfirmCallBack(callBack);
		ObservatoryPersonDTO newPerson = new ObservatoryPersonDTO();
		content.edit(newPerson);
		OkCancelDialog dialog = new OkCancelDialog(Message.INSTANCE.addPerson(), content);
		dialog.show();
		
	}

	@Override
	public String getAddItemText() {
		return Message.INSTANCE.addPerson();
	}
	
	private class CreateConfirmCallBack implements ConfirmCallBack {
		PersonDTOEditor editor;

		public CreateConfirmCallBack(PersonDTOEditor editor) {
			this.editor = editor;
		}

		@Override
		public void confirm(boolean choice) {
			if (choice == true) {
				ObservatoryPersonDTO resultValue = (ObservatoryPersonDTO) editor.getValue();
				resultValue.setObservatoryId(new Long(getObservatoryId()));
				presenter.save(resultValue);
			}
		}
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		
	}

	public void setObservatoryId(String observatoryId) {
		this.observatoryId = observatoryId;
	}
	
	public String getObservatoryId() {
		return observatoryId;
	}
}
