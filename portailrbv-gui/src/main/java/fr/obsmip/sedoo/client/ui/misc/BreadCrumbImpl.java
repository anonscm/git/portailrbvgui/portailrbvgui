package fr.obsmip.sedoo.client.ui.misc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.VerticalAlign;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class BreadCrumbImpl extends Composite implements BreadCrumb {

	private static BreadCrumbUiBinder uiBinder = GWT
			.create(BreadCrumbUiBinder.class);

	// @UiField SpanElement tooltipContent;

	@UiField
	HorizontalPanel contentPanel;

	@UiField
	Image minimizeImage;

	@UiField
	Image maximizeImage;

	private ClientFactory clientFactory;

	List<Shortcut> currentShortcuts = new ArrayList<Shortcut>();

	interface BreadCrumbUiBinder extends UiBinder<Widget, BreadCrumbImpl> {
	}

	public BreadCrumbImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		minimizeImage.setVisible(false);
		maximizeImage.getElement().getStyle()
				.setVerticalAlign(VerticalAlign.MIDDLE);
		minimizeImage.getElement().getStyle()
				.setVerticalAlign(VerticalAlign.MIDDLE);
	}

	public void refresh(List<Shortcut> shortcuts) {
		contentPanel.clear();
		InlineLabel inlineLabel = new InlineLabel(
				CommonMessages.INSTANCE.breadCrumbIntroductionText());
		inlineLabel.setStyleName("breadCrumbIntroductionText");
		contentPanel.add(inlineLabel);
		Iterator<Shortcut> iterator = shortcuts.iterator();
		while (iterator.hasNext()) {
			final Shortcut shortcut = iterator.next();
			if (iterator.hasNext()) {
				Anchor aux = new Anchor();
				aux.setText(shortcut.getLabel());
				aux.setStyleName("breadCrumbInnerToken");
				aux.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						getClientFactory().getPlaceController().goTo(
								shortcut.getPlace());
					}
				});
				contentPanel.add(aux);
				Label separator = new Label(" > ");
				separator.setStyleName("breadCrumbSeparator");
				contentPanel.add(separator);
			} else {
				Label aux = new Label();
				aux.setText(shortcut.getLabel());
				aux.setStyleName("breadCrumbLastToken");
				contentPanel.add(aux);
			}

		}
		currentShortcuts = shortcuts;
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	public void setClientFactory(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@UiHandler("maximizeImage")
	void onMaximizeImageClicked(ClickEvent event) {
		clientFactory.getEventBus().fireEvent(new MaximizeEvent());
		maximizeImage.setVisible(false);
		minimizeImage.setVisible(true);
	}

	@UiHandler("minimizeImage")
	void onMinimizeImageClicked(ClickEvent event) {
		clientFactory.getEventBus().fireEvent(new MinimizeEvent());
		maximizeImage.setVisible(true);
		minimizeImage.setVisible(false);
	}

	@Override
	public void addShortcut(Shortcut shortcut) {
		currentShortcuts.add(shortcut);
		refresh(currentShortcuts);
	}

	@Override
	public List<Shortcut> getShortcuts() {
		return currentShortcuts;
	}

	@Override
	public void replaceLast(Shortcut lastShortcut) {
		currentShortcuts.remove(currentShortcuts.size() - 1);
		currentShortcuts.add(lastShortcut);
		refresh(currentShortcuts);
	}

	@Override
	public void back() {
		if ((currentShortcuts != null) && (currentShortcuts.size() > 1)) {
			// We isolate the target shortcut
			Shortcut shortcut = currentShortcuts
					.get(currentShortcuts.size() - 2);

			// We suppress the last item of the list
			currentShortcuts.remove(currentShortcuts.size() - 1);
			refresh(currentShortcuts);

			// We navigate to the last place
			clientFactory.getPlaceController().goTo(shortcut.getPlace());
		} else {
			clientFactory.getPlaceController().goTo(new WelcomePlace());
		}
	}

	@Override
	public int getHeight() {
		return 24;
	}

}
