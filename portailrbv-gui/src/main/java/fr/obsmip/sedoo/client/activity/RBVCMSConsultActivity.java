package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.sedoo.commons.client.cms.activity.CMSConsultActivity;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.CMSClientFactory;
import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVCMSConsultActivity extends CMSConsultActivity {

	private ClientFactory clientFactory;

	public RBVCMSConsultActivity(CMSConsultPlace place,
			CMSClientFactory clientFactory) {
		super(place, clientFactory);
		this.clientFactory = (ClientFactory) clientFactory;
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory
				.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		if (place.getScreenName().compareToIgnoreCase(ScreenNames.HOME) != 0) {
			shortcuts.add(ShortcutFactory.getCMSConsultShortcut(place));
			broadcastActivityTitle(clientFactory.getCMSLabelProvider().getLabelByScreenName(place.getScreenName()));
		}else{
			broadcastActivityTitle("");
		}
		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
	}

	protected void broadcastActivityTitle(String title) {
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
	}

	@Override
	protected boolean isEditor() {
		AuthenticatedUser user = ((AuthenticatedClientFactory) PortailRBV
				.getClientFactory()).getUserManager().getUser();
		if (user == null) {
			return false;
		} else {
			return user.isAdmin();
		}
	}
}
