package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import java.util.List;
import java.util.ListIterator;

import fr.obsmip.sedoo.shared.domain.AbstractSerieUtil;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.metadata.shared.ResourceIdentifier;
import fr.sedoo.metadata.client.ui.widget.field.impl.identifier.IdentifierComplementaryEditor;
import fr.sedoo.metadata.client.ui.widget.field.primitive.LabelField;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.dto.IdentifiedResourceIdentifier;

public class ExperimentalSiteName extends LabelField implements IdentifierComplementaryEditor {

	public ExperimentalSiteName() {
		super();
	}

	@Override
	public void flush(MetadataDTO metadata) {
		// Nothing is flushed this way

	}

	@Override
	public void edit(MetadataDTO metadata) {
		display(((ExperimentalSiteDTO) metadata).getExperimentalSiteName());
	}

	@Override
	public void display(MetadataDTO metadata) {
		display(((ExperimentalSiteDTO) metadata).getExperimentalSiteName());
	}


	@Override
	public List<IdentifiedResourceIdentifier> filter(List<IdentifiedResourceIdentifier> identifiers) {
		ListIterator<IdentifiedResourceIdentifier> iterator = identifiers.listIterator();
		while (iterator.hasNext()) {
			ResourceIdentifier current = (ResourceIdentifier) iterator.next();
			if (AbstractSerieUtil.isIdentifier(current, ExperimentalSiteDTO.RBV_NAMESPACE)) {
				iterator.remove();
			}
		}
		return identifiers;
	}

	@Override
	public List<IdentifiedResourceIdentifier> flush(List<IdentifiedResourceIdentifier> identifiers) {
		String name = StringUtil.trimToEmpty(label.getText());
		return AbstractSerieUtil.setNameToList(name, ExperimentalSiteDTO.RBV_NAMESPACE, identifiers);
	}

}
