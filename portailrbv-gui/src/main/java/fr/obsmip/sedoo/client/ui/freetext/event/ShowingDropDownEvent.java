package fr.obsmip.sedoo.client.ui.freetext.event;

import com.google.gwt.event.shared.EventHandler;

import fr.obsmip.sedoo.client.ui.freetext.FreeTextChosenImpl;

public class ShowingDropDownEvent extends FreeTextChosenEvent<ShowingDropDownEvent.ShowingDropDownHandler> {

	  public interface ShowingDropDownHandler extends EventHandler {
	    void onShowingDropDown(ShowingDropDownEvent event);
	  }

	  public static Type<ShowingDropDownHandler> TYPE = new Type<ShowingDropDownHandler>();

	  public static Type<ShowingDropDownHandler> getType() {
	    return TYPE;
	  }

	  public ShowingDropDownEvent(FreeTextChosenImpl chosen) {
	    super(chosen);
	  }

	  @Override
	  public Type<ShowingDropDownHandler> getAssociatedType() {
	    return TYPE;
	  }

	  @Override
	  protected void dispatch(ShowingDropDownHandler handler) {
	    handler.onShowingDropDown(this);
	  }
}
