package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class ObservatoryEntryCreationPlace extends Place implements AuthenticatedPlace
{

	public static ObservatoryEntryCreationPlace instance;
	
	public static class Tokenizer implements PlaceTokenizer<ObservatoryEntryCreationPlace>
	{
		@Override
		public String getToken(ObservatoryEntryCreationPlace place)
		{
			return "";
		}

		@Override
		public ObservatoryEntryCreationPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ObservatoryEntryCreationPlace();
			}
			return instance;
		}
	}

}
