package fr.obsmip.sedoo.client.ui.table;

import java.util.List;

import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;
import fr.sedoo.metadata.client.ui.widget.dialog.contact.PersonDialogBoxContent;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public class ObservatoryPersonDialogBoxContent extends PersonDialogBoxContent {

	public ObservatoryPersonDialogBoxContent(List<String> roles) {
		super(roles);
	}

	@Override
	public PersonDTO createDTO() {
		return new ObservatoryPersonDTO();
	}

}
