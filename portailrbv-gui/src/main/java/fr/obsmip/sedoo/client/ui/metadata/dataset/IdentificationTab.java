package fr.obsmip.sedoo.client.ui.metadata.dataset;

import java.util.ArrayList;

import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.Identifiers;
import fr.sedoo.metadata.client.ui.widget.field.impl.InternetLinks;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceAbstract;
import fr.sedoo.metadata.client.ui.widget.field.impl.ResourceTitle;
import fr.sedoo.metadata.client.ui.widget.field.impl.Snapshots;
import fr.sedoo.metadata.client.ui.widget.field.impl.Status;

public class IdentificationTab extends AbstractTab {

	public IdentificationTab(ArrayList<String> displayLanguages) {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingGeneralInformations());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.TITLE));
		addRightComponent(new ResourceTitle(displayLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.ABSTRACT));
		addRightComponent(new ResourceAbstract(displayLanguages));
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.IDENTIFIERS));
		addRightComponent(new Identifiers());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.STATUS));
		addRightComponent(new Status());
		addSection(MetadataMessage.INSTANCE.metadataEditingLinks());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.INTERNET_LINKS));
		addRightComponent(new InternetLinks());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.SNAPSHOTS));
		addRightComponent(new Snapshots());

		reset();
	}

}
