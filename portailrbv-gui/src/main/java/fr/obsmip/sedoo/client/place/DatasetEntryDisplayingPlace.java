package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;
import fr.sedoo.commons.client.util.StringUtil;

public class DatasetEntryDisplayingPlace extends Place implements AuthenticatedPlace
{

	private String uuid;
	
	public static DatasetEntryDisplayingPlace instance;
	
	public static class Tokenizer implements PlaceTokenizer<DatasetEntryDisplayingPlace>
	{
		@Override
		public String getToken(DatasetEntryDisplayingPlace place)
		{
			return StringUtil.trimToEmpty(place.getUuid());
		}

		@Override
		public DatasetEntryDisplayingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new DatasetEntryDisplayingPlace();
			}
			if (StringUtil.isNotEmpty(token))
			{
				instance.setUuid(token);
			}
			return instance;
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
