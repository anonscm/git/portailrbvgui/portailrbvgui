package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ResetEventHandler extends EventHandler {
	void onNotification(ResetEvent event);
}
