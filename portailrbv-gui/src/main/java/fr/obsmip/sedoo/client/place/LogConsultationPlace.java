package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class LogConsultationPlace extends Place implements AuthenticatedPlace
{

	public static LogConsultationPlace instance;

	public static class Tokenizer implements PlaceTokenizer<LogConsultationPlace>
	{
		@Override
		public String getToken(LogConsultationPlace place)
		{
			return "";
		}

		@Override
		public LogConsultationPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new LogConsultationPlace();
			}
			return instance;
		}
	}
	
}
