package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.news.activity.MessageManageActivity;
import fr.sedoo.commons.client.news.mvp.MessageClientFactory;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVNewsManagementActivity extends MessageManageActivity {

	private MessageManagePlace place;

	public RBVNewsManagementActivity(MessageManagePlace place, MessageClientFactory clientFactory) {
		super(place, clientFactory);
		this.place = place;
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(""));
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		//shortcuts.add(ShortcutFactory
		//		.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		shortcuts.add(ShortcutFactory.getMessageManagementShortcut());

		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
		
	}
	
	@Override
	protected boolean isValidUser() {
		return isLoggedUser();
	}

	@Override
	public Place getPlace() {
		return place;
	}


}
