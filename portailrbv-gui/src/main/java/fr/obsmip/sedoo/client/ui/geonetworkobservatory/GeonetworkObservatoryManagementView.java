package fr.obsmip.sedoo.client.ui.geonetworkobservatory;

import java.util.ArrayList;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.crud.component.CrudView;

public interface GeonetworkObservatoryManagementView extends CrudView {

	void setObservatories(ArrayList<GeonetworkObservatoryDTO> observatories);

	

}
