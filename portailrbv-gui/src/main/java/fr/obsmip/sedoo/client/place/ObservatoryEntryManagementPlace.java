package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class ObservatoryEntryManagementPlace extends Place implements AuthenticatedPlace
{

	public static ObservatoryEntryManagementPlace instance;

	public static class Tokenizer implements PlaceTokenizer<ObservatoryEntryManagementPlace>
	{
		@Override
		public String getToken(ObservatoryEntryManagementPlace place)
		{
			return "";
		}

		@Override
		public ObservatoryEntryManagementPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ObservatoryEntryManagementPlace();
			}
			return instance;
		}
	}
	
}
