package fr.obsmip.sedoo.client.activity;

import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public abstract class RBVAdministrationActivity extends RBVAuthenticatedActivity{

	public RBVAdministrationActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	@Override
	protected boolean isValidUser() {
		if (((AuthenticatedClientFactory) clientFactory).getUserManager().getUser() != null)
		{
			return ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser().isAdmin();
		}
		else
		{
			return false;
		}
	}
	
	protected void sendActivityStartEvent()
	{
		clientFactory.getEventBus().fireEvent(new ActivityStartEvent(ActivityStartEvent.ADMINISTRATOR_ACTIVITY));
	}
	
	
}
