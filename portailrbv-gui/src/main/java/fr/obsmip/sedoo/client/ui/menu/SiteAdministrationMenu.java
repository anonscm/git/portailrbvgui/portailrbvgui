package fr.obsmip.sedoo.client.ui.menu;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.MenuBar;

public class SiteAdministrationMenu extends MenuBar {

	private EventBus eventBus;

	public SiteAdministrationMenu(EventBus eventBus) {
		super(true);
		this.eventBus = eventBus;
		initContent();

	}

	private void initContent() {

	}

	public void updateContent() {
		clearItems();
		initContent();
	}

}
