package fr.obsmip.sedoo.client.ui.table.news;

import com.google.gwt.user.cellview.client.CellList;

import fr.obsmip.sedoo.client.CellListResources;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ui.WelcomeView.Presenter;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.New;

public class NewsList extends CellList<New>{

	NewsCell newsCell;
	
	public NewsList() 
	{
		super(new NewsCell(LocaleUtil.getClientLocaleLanguage(PortailRBV.getClientFactory().getDefaultLanguage())),CellListResources.INSTANCE);
		newsCell = (NewsCell) getCell();
		setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
	}
	
	public void setPresenter(Presenter presenter)
	{
		newsCell.setPresenter(presenter);
	}
	
	

}
