package fr.obsmip.sedoo.client.ui.search;

import org.gwtopenmaps.openlayers.client.format.GeoJSON;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.layer.VectorOptions;
import org.gwtopenmaps.openlayers.client.protocol.HTTPProtocol;
import org.gwtopenmaps.openlayers.client.protocol.HTTPProtocolOptions;
import org.gwtopenmaps.openlayers.client.strategy.FixedStrategy;
import org.gwtopenmaps.openlayers.client.strategy.FixedStrategyOptions;
import org.gwtopenmaps.openlayers.client.strategy.Strategy;

public class JsonLayer {

	public static Vector createLayerFromJson(String layerName, String url) {

		FixedStrategyOptions fOptions = new FixedStrategyOptions();
		FixedStrategy fStrategy = new FixedStrategy(fOptions);

		GeoJSON geoJson = new GeoJSON();

		HTTPProtocolOptions httpProtOptions = new HTTPProtocolOptions();
		httpProtOptions.setUrl(url);
		httpProtOptions.setFormat(geoJson);

		HTTPProtocol httpProt = new HTTPProtocol(httpProtOptions);

		VectorOptions options = new VectorOptions();
		options.setStrategies(new Strategy[] { fStrategy });
		options.setProtocol(httpProt);

		Vector result = new Vector(layerName, options);

		LayerLoader locationLoader = new LayerLoader();
		result.addLayerLoadStartListener(locationLoader);
		result.addLayerLoadEndListener(locationLoader);
		result.addLayerLoadCancelListener(locationLoader);

		return result;
	}

}
