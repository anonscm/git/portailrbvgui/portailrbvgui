package fr.obsmip.sedoo.client.activity;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;

public abstract class RBVPublicActivity extends RBVAbstractActivity 
{
	public RBVPublicActivity(ClientFactory clientFactory) {
		super(clientFactory);
	}
	
	protected void sendActivityStartEvent()
	{
		clientFactory.getEventBus().fireEvent(new ActivityStartEvent(ActivityStartEvent.PUBLIC_ACTIVITY));
	}
}
