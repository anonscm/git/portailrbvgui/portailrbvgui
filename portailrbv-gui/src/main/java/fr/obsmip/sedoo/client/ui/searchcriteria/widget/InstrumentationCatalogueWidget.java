package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaBar;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.DisclosurePanelCustom;
import fr.obsmip.sedoo.client.ui.searchcriteria.MapCriteriaWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.ObservatoryCatalogsLoadedEvent;
import fr.obsmip.sedoo.client.ui.searchcriteria.ObservatoryCatalogsLoadedEventHandler;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.message.ValidationMessages;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;

public class InstrumentationCatalogueWidget implements CriteriaWidget,
		HasValidation, MapCriteriaWidget, ClickHandler,
		ObservatoryCatalogsLoadedEventHandler {

	protected DisclosurePanelCustom panel;
	protected CriteriaBar criteriaBar;
	protected ArrayList<CheckBox> checkBoxes = new ArrayList<CheckBox>();

	public InstrumentationCatalogueWidget(CriteriaBar criteriaBar) {
		this.criteriaBar = criteriaBar;
		panel = new DisclosurePanelCustom(Message.INSTANCE.instrumentation());
		panel.setWidth("100%");
		init();
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(ObservatoryCatalogsLoadedEvent.TYPE, this);
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	public void init() {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(2);
		verticalPanel.add(new Label(CommonMessages.INSTANCE.loading()));
		panel.add(verticalPanel);
		panel.setOpen(false);
	}

	@Override
	public void reset() {
		for (CheckBox checkBox : checkBoxes) {
			checkBox.setValue(false);
		}
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		boolean atLeastOneSelected = false;
		for (CheckBox checkBox : checkBoxes) {
			if (checkBox.getValue() == true) {
				atLeastOneSelected = true;
			}
		}
		if (atLeastOneSelected == false) {
			result.add(new ValidationAlert(Message.INSTANCE.instrumentation(),
					ValidationMessages.INSTANCE.atLeastOneElementNeeded()));
		}
		return result;
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {
		ArrayList<String> instrumentation = new ArrayList<String>();
		for (CheckBox checkBox : checkBoxes) {
			if (checkBox.getValue() == true) {
				instrumentation.add(checkBox.getText());
			}
		}
		// criteria.setInstrumentation(instrumentation);;
		return criteria;
	}

	@Override
	public void onClick(ClickEvent event) {
		criteriaBar.validate();
	}

	@Override
	public void onNotification(ObservatoryCatalogsLoadedEvent event) {
		panel.clear();
		ArrayList<GeonetworkObservatoryDTO> observatoryCatalogs = event
				.getobservatoryCatalogs();
		checkBoxes.clear();
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(2);
		CheckBox checkAll = new CheckBox("ALL");
		checkAll.setValue(true);
		checkBoxes.add(checkAll);
		checkAll.addClickHandler(this);
		verticalPanel.add(checkAll);
		for (GeonetworkObservatoryDTO observatoryCatalog : observatoryCatalogs) {
			CheckBox checkBox = new CheckBox(observatoryCatalog.getName());
			checkBox.setValue(true);
			checkBoxes.add(checkBox);
			checkBox.addClickHandler(this);
			verticalPanel.add(checkBox);
		}
		panel.add(verticalPanel);
		criteriaBar.validate();
	}

}