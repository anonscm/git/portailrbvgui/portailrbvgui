package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

@RemoteServiceRelativePath("search")
public interface SearchService extends RemoteService {

	HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> getSummaries(
			SearchCriteriaDTO criteria, int position, int size,
			ArrayList<String> displayLanguages) throws Exception;

	int getHits(SearchCriteriaDTO criteria) throws Exception;

}
