package fr.obsmip.sedoo.client.ui.searchcriteria;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;

public class CriteriaBar implements IsWidget, CriteriaWidget {

	protected List<CriteriaWidget> criteriaWidgets = new LinkedList<CriteriaWidget>();
	private ErrorButtonContainer errorButtonContainer;

	VerticalPanel widgetPanel;
	DockLayoutPanel container;

	public CriteriaBar() {
		container = new DockLayoutPanel(Unit.PX);
		widgetPanel = new VerticalPanel();
		widgetPanel.setSpacing(5);
		widgetPanel.setWidth("100%");
		container.add(new ScrollPanel(widgetPanel));
	}

	public CriteriaBar(ErrorButtonContainer errorButtonContainer) {
		this();
		this.errorButtonContainer = errorButtonContainer;

	}

	@Override
	public Widget asWidget() {
		return container;
	}

	public void add(CriteriaWidget searchCriteriaWidget) {
		criteriaWidgets.add(searchCriteriaWidget);
		widgetPanel.add(searchCriteriaWidget);
	}

	@Override
	public void reset() {
		for (CriteriaWidget widget : criteriaWidgets) {
			widget.reset();
		}
	}

	public void validate() {
		ArrayList<ValidationAlert> alerts = new ArrayList<ValidationAlert>();
		for (CriteriaWidget widget : criteriaWidgets) {
			if (widget instanceof HasValidation)
				alerts.addAll(((HasValidation) widget).validate());
		}
		if (errorButtonContainer != null) {
			errorButtonContainer.setValidationAlert(alerts);
		}
	}
}
