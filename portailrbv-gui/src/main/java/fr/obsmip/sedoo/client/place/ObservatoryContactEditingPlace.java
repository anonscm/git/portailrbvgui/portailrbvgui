package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class ObservatoryContactEditingPlace extends AbstractEditingPlace implements AuthenticatedPlace
{

	public static ObservatoryContactEditingPlace instance;

	public static class Tokenizer implements PlaceTokenizer<ObservatoryContactEditingPlace>
	{
		@Override
		public String getToken(ObservatoryContactEditingPlace place)
		{
			return place.getTokenString();
		}

		@Override
		public ObservatoryContactEditingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ObservatoryContactEditingPlace();
				instance.initFromToken(token);
			}
			return instance;
		}
	}
	
	
}
