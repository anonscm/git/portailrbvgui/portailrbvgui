package fr.obsmip.sedoo.client.ui.metadata.common.contact;

import com.google.gwt.event.dom.client.ClickHandler;

import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.metadata.utils.pdf.labelprovider.RoleLabelProvider;

public class RBVResourcePointOfContacts extends RBVAbstractContactList implements ClickHandler, ConfirmCallBack
{
	public String getSingleRole() {
		return RoleLabelProvider.POINT_OF_CONTACT;
	}
}
