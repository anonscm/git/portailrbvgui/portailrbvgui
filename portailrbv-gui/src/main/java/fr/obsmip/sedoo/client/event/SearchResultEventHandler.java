package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SearchResultEventHandler extends EventHandler {
	void onNotification(SearchResultEvent event);
}
