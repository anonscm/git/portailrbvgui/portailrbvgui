package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;

public interface GeonetworkObservatoryServiceAsync {

	void create(GeonetworkObservatoryDTO observatory,
			AsyncCallback<GeonetworkObservatoryDTO> callback);

	void delete(GeonetworkObservatoryDTO observatory,
			AsyncCallback<Void> callback);

	void edit(GeonetworkObservatoryDTO observatory,
			AsyncCallback<GeonetworkObservatoryDTO> callback);

	void findAll(AsyncCallback<ArrayList<GeonetworkObservatoryDTO>> callback);

}
