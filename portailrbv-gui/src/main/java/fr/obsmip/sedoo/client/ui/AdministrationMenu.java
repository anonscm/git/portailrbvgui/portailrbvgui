package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.event.UserLoginEvent;
import fr.obsmip.sedoo.client.event.UserLoginEventHandler;
import fr.obsmip.sedoo.client.event.UserLogoutEvent;
import fr.obsmip.sedoo.client.event.UserLogoutEventHandler;
import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.cms.bundle.MenuMessages;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.cms.place.MenuEditPlace;
import fr.sedoo.commons.client.component.impl.menu.PlaceCommand;
import fr.sedoo.commons.client.news.place.MessageManagePlace;

public class AdministrationMenu extends MenuBar implements
		UserLoginEventHandler, UserLogoutEventHandler {

	private MenuItem menuItem;

	public AdministrationMenu(ClientFactory clientFactory) {
		super(true);

		MenuBar screenManagementBar = new MenuBar(true);

		screenManagementBar.addItem(CMSMessages.INSTANCE.allScreens(),
				new PlaceCommand(clientFactory.getEventBus(),
						new CMSListPlace()));
		screenManagementBar.addItem(MenuMessages.INSTANCE.menuEdition(),
				new PlaceCommand(clientFactory.getEventBus(),
						new MenuEditPlace()));

		addItem(new MenuItem(CMSMessages.INSTANCE.screenManagement(),
				screenManagementBar));

		addItem(new MenuItem("News", new PlaceCommand(clientFactory.getEventBus(), new MessageManagePlace())));
		
		clientFactory.getEventBus().addHandler(UserLoginEvent.TYPE, this);
		clientFactory.getEventBus().addHandler(UserLogoutEvent.TYPE, this);

	}

	@Override
	public void onNotification(UserLogoutEvent event) {
		setVisible(false);
	}

	@Override
	public void onNotification(UserLoginEvent event) {
		// updateContent();
		setVisible(true);
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

}
