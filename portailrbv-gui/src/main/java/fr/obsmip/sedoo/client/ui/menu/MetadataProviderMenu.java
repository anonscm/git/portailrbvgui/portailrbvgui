package fr.obsmip.sedoo.client.ui.menu;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryManagementPlace;
import fr.obsmip.sedoo.client.place.DirectoryManagementPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryManagementPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryManagementPlace;

public class MetadataProviderMenu extends AbstractMenu {
	
	
public MetadataProviderMenu() {
	
		Panel panel = getMetadataPanel(); 
	    initWidget(panel);
	}

private VerticalPanel getMetadataPanel() {
	VerticalPanel metadataPanel = new VerticalPanel();
	metadataPanel.getElement().getStyle().setPaddingLeft(3, Unit.PX);
	
	VerticalPanel observatoryPanel = new VerticalPanel();
	observatoryPanel.setSpacing(6);
	observatoryPanel.getElement().getStyle().setPaddingLeft(5, Unit.PX);
	observatoryPanel.add(createMenuLink(Message.INSTANCE.createNewEntry(), new ObservatoryEntryCreationPlace()));
	observatoryPanel.add(createMenuLink(Message.INSTANCE.manageExistingEntries(), new ObservatoryEntryManagementPlace()));
	metadataPanel.add(createInactiveMenuLink(Message.INSTANCE.observatoryMetadata()));
	metadataPanel.add(observatoryPanel);
	
	VerticalPanel experimentalSitePanel = new VerticalPanel();
	experimentalSitePanel.setSpacing(10);
	experimentalSitePanel.getElement().getStyle().setPaddingLeft(5, Unit.PX);
	experimentalSitePanel.add(createMenuLink(Message.INSTANCE.createNewEntry(), new ExperimentalSiteEntryCreationPlace()));
	experimentalSitePanel.add(createMenuLink(Message.INSTANCE.manageExistingEntries(), new ExperimentalSiteEntryManagementPlace()));
	metadataPanel.add(createInactiveMenuLink(Message.INSTANCE.experimentalSiteMetadata()));
	metadataPanel.add(experimentalSitePanel);
	
	VerticalPanel dataSetSitePanel = new VerticalPanel();
	dataSetSitePanel.setSpacing(10);
	dataSetSitePanel.getElement().getStyle().setPaddingLeft(5, Unit.PX);
	dataSetSitePanel.add(createMenuLink(Message.INSTANCE.createNewEntry(), new DatasetEntryCreationPlace()));
	dataSetSitePanel.add(createMenuLink(Message.INSTANCE.manageExistingEntries(), new DatasetEntryManagementPlace()));
	metadataPanel.add(createInactiveMenuLink(Message.INSTANCE.dataSetMetadata()));
	metadataPanel.add(dataSetSitePanel);
	
	VerticalPanel directoryPanel = new VerticalPanel();
	directoryPanel.setSpacing(10);
	directoryPanel.getElement().getStyle().setPaddingLeft(5, Unit.PX);
	directoryPanel.add(createMenuLink(Message.INSTANCE.directoryManagement(), new DirectoryManagementPlace()));
	metadataPanel.add(createInactiveMenuLink(Message.INSTANCE.directory()));
	metadataPanel.add(directoryPanel);
	
	return metadataPanel;
}

	


}
