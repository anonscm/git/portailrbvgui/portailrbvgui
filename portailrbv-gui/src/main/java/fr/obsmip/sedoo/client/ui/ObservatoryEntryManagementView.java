package fr.obsmip.sedoo.client.ui;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public interface ObservatoryEntryManagementView extends IsWidget{
	
	void init(List<ObservatorySummaryDTO> observatorySummaries);
	public void broadcastObservatoryDeletion(String uuid);
	
	void setPresenter(Presenter presenter);
	
	public interface Presenter 
	 {
	        void deleteObservatoryEntry(String uuid);
			void goToEditPlace(MetadataSummaryDTO observatorySummaryDTO);
			void goToObservatoryEntryCreationPlace();
	 }
	

}
