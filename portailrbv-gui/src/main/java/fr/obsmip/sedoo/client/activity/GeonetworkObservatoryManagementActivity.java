package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.place.GeonetworkObservatoryManagementPlace;
import fr.obsmip.sedoo.client.service.GeonetworkObservatoryService;
import fr.obsmip.sedoo.client.service.GeonetworkObservatoryServiceAsync;
import fr.obsmip.sedoo.client.ui.geonetworkobservatory.GeonetworkObservatoryManagementView;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.crud.activity.CrudActivity;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class GeonetworkObservatoryManagementActivity  extends CrudActivity {

    public final static GeonetworkObservatoryServiceAsync GEONETWORK_OBSERVATORY_SERVICE = GWT.create(GeonetworkObservatoryService.class);

	private GeonetworkObservatoryManagementView view;

    public GeonetworkObservatoryManagementActivity(GeonetworkObservatoryManagementPlace place, ClientFactory clientFactory) {
    	super(clientFactory, place);
    }
    
    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
    	if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
        view = ((ClientFactory) clientFactory).getGeonetworkObservatoryManagementView();
        view.setCrudPresenter(this);
        containerWidget.setWidget(view.asWidget());
        view.setCrudPresenter(this);
        List<Shortcut> shortcuts = new ArrayList<Shortcut>();
        shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(Message.INSTANCE.observatoryManagement()));
        ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
    	eventBus.fireEvent(e);
        
    	GEONETWORK_OBSERVATORY_SERVICE.findAll(new DefaultAbstractCallBack<ArrayList<GeonetworkObservatoryDTO>>(e, eventBus) {

			@Override
			public void onSuccess(ArrayList<GeonetworkObservatoryDTO> result) {
				super.onSuccess(result);
				view.setObservatories(result);
			}
			
		});
        
    }

	@Override
	public void delete(final HasIdentifier hasIdentifier) 
	{
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_DELETING_EVENT, true);
    	eventBus.fireEvent(e);
    	GEONETWORK_OBSERVATORY_SERVICE.delete((GeonetworkObservatoryDTO) hasIdentifier, new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				view.broadcastDeletion(hasIdentifier);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				ClientObservatoryList.updateObservatories();
			}
		});
		
	}

	@Override
	public void create(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	GEONETWORK_OBSERVATORY_SERVICE.create((GeonetworkObservatoryDTO) hasIdentifier, new DefaultAbstractCallBack<GeonetworkObservatoryDTO>(e, eventBus) {

			@Override
			public void onSuccess(GeonetworkObservatoryDTO result) {
				super.onSuccess(result);
				view.broadcastCreation(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
				ClientObservatoryList.updateObservatories();
			}
		});	}

	@Override
	public void edit(HasIdentifier hasIdentifier) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
    	eventBus.fireEvent(e);
    	GEONETWORK_OBSERVATORY_SERVICE.edit((GeonetworkObservatoryDTO) hasIdentifier, new DefaultAbstractCallBack<GeonetworkObservatoryDTO>(e, eventBus) {

			@Override
			public void onSuccess(GeonetworkObservatoryDTO result) {
				super.onSuccess(result);
				view.broadcastEdition(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
				ClientObservatoryList.updateObservatories();
			}
		});	
	
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser())
		{
			return ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser().isAdmin();
		}
		else
		{
			return false;
		}
	}

}