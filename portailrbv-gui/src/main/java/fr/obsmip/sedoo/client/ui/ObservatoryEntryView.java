package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;

import fr.obsmip.sedoo.client.ui.metadata.common.contact.DirectoryPresenter;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.client.ui.view.MetadataEditingView;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public interface ObservatoryEntryView extends MetadataEditingView {
	
	void setPresenter(ObservatoryEntryPresenter presenter);
	
	public interface ObservatoryEntryPresenter extends DirectoryPresenter, EditingPresenter
	{
		void addExperimentalSite();
		void deleteExperimentalSite(String uuid);
		void goToExperimentalSiteEditPlace(MetadataSummaryDTO hasId);
		void goToExperimentalSiteDisplayPlace(String uuid);
		void computeGeographicalBoxes();
	}

	void broadcastExperimentalSiteDeletion(String uuid);

	void setGeographicalBoxes(ArrayList<GeographicBoundingBoxDTO> boxes);
	
}
