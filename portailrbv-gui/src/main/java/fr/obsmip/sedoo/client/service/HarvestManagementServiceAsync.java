package fr.obsmip.sedoo.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface HarvestManagementServiceAsync {

	void execute(AsyncCallback<Boolean> callback);


}
