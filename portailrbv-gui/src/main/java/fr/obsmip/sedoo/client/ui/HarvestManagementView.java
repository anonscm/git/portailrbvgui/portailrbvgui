package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;

public interface HarvestManagementView extends IsWidget {

	void displaySuccessMessage();
	void displayFailureMessage();
	void reset();
	void setPresenter(Presenter presenter);
	
	public interface Presenter 
	 {
	        void launch();
	 }

}
