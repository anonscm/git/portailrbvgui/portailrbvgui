package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.LogDTO;
import fr.sedoo.commons.client.util.ServiceException;

@RemoteServiceRelativePath("logger")
public interface LoggerService extends RemoteService {
	
	 ArrayList<LogDTO> findPage(int pageNumber, int pageSize) throws ServiceException;
	 int getHits() throws ServiceException;
     void add(LogDTO message) throws ServiceException;
}

