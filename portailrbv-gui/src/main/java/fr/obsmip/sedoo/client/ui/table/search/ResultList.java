package fr.obsmip.sedoo.client.ui.table.search;

import com.google.gwt.user.cellview.client.CellList;

import fr.obsmip.sedoo.client.CellListResources;
import fr.obsmip.sedoo.client.misc.SummaryPresenter;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public class ResultList extends CellList<SummaryDTO> {

	SummaryCell summaryCell;

	public ResultList() {
		super(new SummaryCell(), CellListResources.INSTANCE);
		summaryCell = (SummaryCell) getCell();
		setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		// addRowCountChangeHandler(new Handler() {
		//
		// @Override
		// public void onRowCountChange(RowCountChangeEvent event) {
		// Window.alert("" + event.getNewRowCount());
		// setVisibleRange(new Range(0, event.getNewRowCount() - 1));
		// }
		// });
	}

	public void setPresenter(SummaryPresenter presenter) {
		summaryCell.setPresenter(presenter);
	}

}
