package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class GeonetworkObservatoryManagementPlace extends Place
{
	public static GeonetworkObservatoryManagementPlace instance;
	
	public GeonetworkObservatoryManagementPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<GeonetworkObservatoryManagementPlace>
	{
		
		public GeonetworkObservatoryManagementPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new GeonetworkObservatoryManagementPlace();
			}
			return instance;
		}

		public String getToken(GeonetworkObservatoryManagementPlace place) {
			return "";
		}

		
	}
}