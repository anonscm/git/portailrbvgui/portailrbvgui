package fr.obsmip.sedoo.client.ui.search;

import org.gwtopenmaps.openlayers.client.event.LayerLoadCancelListener;
import org.gwtopenmaps.openlayers.client.event.LayerLoadEndListener;
import org.gwtopenmaps.openlayers.client.event.LayerLoadStartListener;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;

public class LayerLoader implements LayerLoadStartListener,
		LayerLoadEndListener, LayerLoadCancelListener {

	private ActionStartEvent currentEvent;
	private String message;

	public LayerLoader(String message) {
		this.message = message;
	}

	public LayerLoader() {
		this(CommonMessages.INSTANCE.loading());
	}

	@Override
	public void onLoadCancel(LoadCancelEvent eventObject) {
		if (currentEvent != null) {
			PortailRBV.getClientFactory().getEventBus()
					.fireEvent(currentEvent.getEndingEvent());
			currentEvent = null;
		}

	}

	@Override
	public void onLoadEnd(LoadEndEvent eventObject) {
		if (currentEvent != null) {
			PortailRBV.getClientFactory().getEventBus()
					.fireEvent(currentEvent.getEndingEvent());
			currentEvent = null;
		}

	}

	@Override
	public void onLoadStart(LoadStartEvent eventObject) {
		currentEvent = new ActionStartEvent(message, "", true);
		PortailRBV.getClientFactory().getEventBus().fireEvent(currentEvent);
	}

}
