package fr.obsmip.sedoo.client.ui.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryManagementView.Presenter;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.obsmip.sedoo.shared.util.MetadataSummaryDTOComparator;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;


public class ExperimentalSiteEntrySummaryTable extends AbstractSummaryTable {

	private Presenter presenter;
	private ObservatorySummaryDTO observatorySummaryDTO;

	public ExperimentalSiteEntrySummaryTable() {
		super();
		enableEditMode();
	}	
	@Override
	public void addItem() {
		//Nothing is done via this method
	}

	@Override
	public String getAddItemText() {
		return Message.INSTANCE.observatoryTableAddItem();
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter.deleteExperimentalSiteEntry(((MetadataSummaryDTO) hasId).getUuid());
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		presenter.goToEditPlace((ExperimentalSiteSummaryDTO) hasId, observatorySummaryDTO);
	}

	@Override
	public String getDeleteItemConfirmationText() {
		return Message.INSTANCE.experimentalSiteDeletetionConfirmationText();
	}

	@Override
	protected void initColumns() {
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	
	@Override
	public void presenterView(HasIdentifier hasId) {
		//Nothing is done via this method
	}
	public ObservatorySummaryDTO getObservatorySummaryDTO() {
		return observatorySummaryDTO;
	}
	public void setObservatorySummaryDTO(ObservatorySummaryDTO observatorySummaryDTO) {
		this.observatorySummaryDTO = observatorySummaryDTO;
	}

	public void init(List<? extends HasIdentifier> model) 
	{
		ArrayList<ExperimentalSiteSummaryDTO> sortedEntries = new ArrayList<ExperimentalSiteSummaryDTO>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) 
		{
			sortedEntries.add((ExperimentalSiteSummaryDTO) iterator.next());
		}
		Collections.sort(sortedEntries, new MetadataSummaryDTOComparator());
		super.init(sortedEntries);
	}
}
