package fr.obsmip.sedoo.client.ui.menu;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.mvp.Presenter;
import fr.obsmip.sedoo.client.ui.misc.MenuLink;

public class AbstractMenu extends Composite {

	protected Presenter presenter;

	protected Presenter getPresenter() {
		if (presenter == null) {
			presenter = PortailRBV.getPresenter();
		}
		return presenter;
	}

	protected Label createInactiveMenuLink(String title) {
		Label label = new Label(title);
		label.setStyleName("inactiveMenuLink");
		return label;
	}

	protected MenuLink createMenuLink(String label, final Place place) {
		MenuLink link = new MenuLink(label);
		link.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				getPresenter().goTo(place);
			}
		});
		return link;
	}

}
