package fr.obsmip.sedoo.client.ui.table.summary;

import java.util.Iterator;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.Image;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.obsmip.sedoo.shared.util.HierachyLevelUtil;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.ui.widget.field.impl.Snapshots;
import fr.sedoo.metadata.shared.domain.misc.IdentifiedDescribedString;

public class SummaryRenderer {

	public final static String SEE_METADATA = "SEE";
	public final static String PDF_METADATA = "PDF";
	public final static String XML_METADATA = "XML";

	private final static String SNAPSHOT_FOLDER_PREFIX = "FOLDER@";
	private final static String SNAPSHOT_CLOSED_IMG_PREFIX = "CLOSED_IMG@";
	private final static String SNAPSHOT_OPENED_IMG_PREFIX = "OPEN_IMG@";
	private final static String SNAPSHOT_CONTENT_PREFIX = "CONTENT@";

	public static void render(Context context, SummaryDTO value,
			SafeHtmlBuilder sb) {
		if (value == null) {
			return;
		}
		sb.appendHtmlConstant("<div width=\"100%\"  class=\"summaryContainer\">");
		sb.appendHtmlConstant("<table width=\"100%\" style=\"border-bottom: 1px solid #EEEEEE;padding:5px;cursor:default;border-collapse:collapse\" class=\"localCellList\">");
		sb.appendHtmlConstant("<col width=\"0%\"><col width=\"100%\">");
		sb.appendHtmlConstant("<tr><td colspan=\"2\"><span style=\"font-weight:bold;\">");

		if (HierachyLevelUtil.isObservatory(value.getHierarchyLevelName())) {
			String aux = "";
			if (StringUtil.isNotEmpty(value.getShortName())) {
				aux = " : " + value.getShortName().toUpperCase();
			}
			sb.appendHtmlConstant("<span class=\"green-small-button\">"
					+ Message.INSTANCE.observatory().toUpperCase() + aux
					+ "</span>&nbsp");
		} else if (HierachyLevelUtil.isExperimentalSite(value
				.getHierarchyLevelName())) {
			sb.appendHtmlConstant("<span class=\"green-small-button\">"
					+ Message.INSTANCE.experimentalSite().toUpperCase()
					+ "</span>&nbsp");
		} else {
			sb.appendHtmlConstant("<span class=\"green-small-button\">"
					+ Message.INSTANCE.dataSet().toUpperCase() + "</span>&nbsp");
		}

		sb.appendHtmlConstant("<span class=\"green-small-button\" style=\"background-color:"
				+ value.getObservatoryColor()
				+ "\">"
				+ value.getObservatoryName() + "</span>&nbsp");

		sb.appendEscaped(StringUtil.trimToEmpty(value.getResourceTitle()));
		sb.appendHtmlConstant("</span>");
		if (value.getModificationDate() != null) {
			sb.appendHtmlConstant("<i> ("
					+ Message.INSTANCE.welcomeAddedOrModified() + " "
					+ value.getModificationDate() + ")</i>");
		}
		sb.appendHtmlConstant("</td></tr>");

		sb.appendHtmlConstant("<tr>");

		sb.appendHtmlConstant("<td width=\"100%\" align=\"left\" valign=\"top\" style=\"text-align:justify;padding:5px;\"> ");
		sb.appendEscapedLines(value.getResourceAbstract());
		sb.appendHtmlConstant("</td>");

		sb.appendHtmlConstant("<td width=\"0%\" align=\"right\" valign=\"top\" style=\"padding-right: 5px;\">");
		sb.appendHtmlConstant("<table width=\"1%\"><tr>");
		sb.appendHtmlConstant("<td  ><div id=\""
				+ SEE_METADATA
				+ "-"
				+ value.getUuid()
				+ "\" class=\"news-button\" style=\"margin-top: 2px;\">"
				+ Message.INSTANCE.view().toUpperCase()
				+ "</div></td></tr><tr><td><div class=\"news-button\" style=\"margin-top: 2px;\" id=\""
				+ PDF_METADATA
				+ "-"
				+ value.getUuid()
				+ "\">"
				+ Message.INSTANCE.print().toUpperCase()
				+ "</div></td></tr><tr><td><div class=\"news-button\" style=\"margin-bottom: 2px;margin-top: 2px;\" id=\""
				+ XML_METADATA + "-" + value.getUuid()
				+ "\">XML</div></td></tr></table></td>");

		sb.appendHtmlConstant("</tr>");
		List<IdentifiedDescribedString> filteredSnapshots = Snapshots
				.removeBadUrl(value.getLinks());
		if (ListUtil.isNotEmpty(filteredSnapshots)) {
			String uuid = value.getUuid();
			String folderId = SNAPSHOT_FOLDER_PREFIX + uuid;
			String closedImageId = SNAPSHOT_CLOSED_IMG_PREFIX + uuid;
			String openedImageId = SNAPSHOT_OPENED_IMG_PREFIX + uuid;
			String contentId = SNAPSHOT_CONTENT_PREFIX + uuid;
			Image dot = new Image(CommonBundle.INSTANCE.menuDot());
			dot.getElement().setId(openedImageId);
			dot.getElement().getStyle().setProperty("cursor", "pointer");

			Image verticalDot = new Image(
					CommonBundle.INSTANCE.verticalMenuDot());
			verticalDot.getElement().setId(closedImageId);
			verticalDot.getElement().getStyle()
					.setProperty("cursor", "pointer");
			verticalDot.getElement().getStyle().setProperty("display", "none");

			sb.appendHtmlConstant("<tr><td colspan=\"2\">"
					+ dot.toString()
					+ verticalDot.toString()
					+ "<span style=\"margin-left:5px;cursor:pointer;font-style:italic;font-weight:normal\" id=\""
					+ folderId + "\">"
					+ Message.INSTANCE.metadataEditingSnapshots()
					+ "</span></td></tr>");

			int index = 0;
			int count = 0;
			sb.appendHtmlConstant("<tr style=\"display:none\" id=\""
					+ (contentId + count) + "\"><td colspan=\"2\">");
			boolean open = true;
			Iterator<IdentifiedDescribedString> iterator = filteredSnapshots
					.iterator();
			while (iterator.hasNext()) {

				IdentifiedDescribedString current = iterator.next();
				if (current.getLink() != null) {
					sb.appendHtmlConstant("<img style=\"cursor:pointer\" height=\"110\" width=\"110\" src=\""
							+ current.getLink()
							+ "\" title=\""
							+ current.getDescription()
							+ "\" alt=\"loading...\" />&nbsp;");
					index++;
					if ((index % 5) == 0) {
						sb.appendHtmlConstant("</td></tr>");
						if (iterator.hasNext()) {
							count++;
							sb.appendHtmlConstant("<tr style=\"display:none\" id=\""
									+ (contentId + count)
									+ "\"><td colspan=\"2\">");
							open = true;
						} else {
							open = false;
						}
					}
				}
			}
			if (open) {
				sb.appendHtmlConstant("</td></tr>");
			}
		}
		sb.appendHtmlConstant("</table>");
		sb.appendHtmlConstant("</div>");
	}

	public static final String getContentIdFromSrcId(String id) {
		String[] split = id.split("@");
		return SNAPSHOT_CONTENT_PREFIX + split[1];
	}

	public static final String getOpenedImgId(String id) {
		String[] split = id.split("@");
		return SNAPSHOT_OPENED_IMG_PREFIX + split[1];
	}

	public static final String getClosedImgId(String id) {
		String[] split = id.split("@");
		return SNAPSHOT_CLOSED_IMG_PREFIX + split[1];
	}

	public static boolean isSnapshotSrcId(String id) {
		if ((id.startsWith(SNAPSHOT_FOLDER_PREFIX))
				|| (id.startsWith(SNAPSHOT_OPENED_IMG_PREFIX))
				|| (id.startsWith(SNAPSHOT_CLOSED_IMG_PREFIX))) {
			return true;
		} else {
			return false;
		}
	}

}
