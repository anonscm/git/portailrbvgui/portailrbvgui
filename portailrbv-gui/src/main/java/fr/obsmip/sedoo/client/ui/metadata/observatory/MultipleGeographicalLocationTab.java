package fr.obsmip.sedoo.client.ui.metadata.observatory;

import java.util.ArrayList;

import org.gwtbootstrap3.client.ui.Button;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryView.ObservatoryEntryPresenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.impl.MultipleGeographicalExtent;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class MultipleGeographicalLocationTab extends AbstractTab implements
		ClickHandler {

	public boolean created = false;
	private MetadataDTO metadata;
	private String mapLayer;
	private Button computeFromExperimentalSitesButton;
	private MultipleGeographicalExtent multipleGeographicalExtent;
	private ObservatoryEntryPresenter presenter;

	public MultipleGeographicalLocationTab(String mapLayer) {
		super();
		this.mapLayer = mapLayer;
		addSection(MetadataMessage.INSTANCE
				.metadataEditingGeographicalLocationTabHeader());
		reset();
	}

	@Override
	public void reset() {
		super.reset();
		ElementUtil.hide(computeFromExperimentalSitesButton);
	}

	public Button createComputeFromExperimentalSitesButton() {
		if (computeFromExperimentalSitesButton == null) {
			computeFromExperimentalSitesButton = new Button(
					Message.INSTANCE
							.computeGeographicalExtentFromExperimentalSites());
			computeFromExperimentalSitesButton.addClickHandler(this);
		}
		return computeFromExperimentalSitesButton;
	}

	@Override
	public void isSelected(String mode) {
		super.isSelected(mode);
		if (!created) {
			multipleGeographicalExtent = new MultipleGeographicalExtent(
					mapLayer);
			addFullLineComponent(multipleGeographicalExtent);
			mainPanel.add(createComputeFromExperimentalSitesButton());
			if (mode.compareTo(EDIT_MODE) == 0) {
				multipleGeographicalExtent.edit(metadata);
			} else {
				multipleGeographicalExtent.display(metadata);
			}
			created = true;
		}
	}

	@Override
	public void flush(MetadataDTO metadata) {
		if (created) {
			super.flush(metadata);
		} else {
			metadata.getGeographicalLocationPart().setBoxes(
					this.metadata.getGeographicalLocationPart().getBoxes());
		}
	}

	@Override
	public void edit(MetadataDTO metadata) {
		if (mapLayer.compareToIgnoreCase(AreaSelectorWidget.GOOGLE_LAYER) == 0) {
			this.metadata = metadata;
		} else {
			if (created == false) {
				MultipleGeographicalExtent aux = new MultipleGeographicalExtent(
						mapLayer);
				addFullLineComponent(aux);
				mainPanel.add(createComputeFromExperimentalSitesButton());
				created = true;
			}
		}
		ElementUtil.show(computeFromExperimentalSitesButton);
		super.edit(metadata);
	}

	@Override
	public void display(MetadataDTO metadata) {
		ElementUtil.hide(computeFromExperimentalSitesButton);
		if (mapLayer.compareToIgnoreCase(AreaSelectorWidget.GOOGLE_LAYER) == 0) {
			this.metadata = metadata;
		} else {
			if (created == false) {
				MultipleGeographicalExtent aux = new MultipleGeographicalExtent(
						mapLayer);
				addFullLineComponent(aux);
				created = true;
			}
		}

		super.display(metadata);
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == computeFromExperimentalSitesButton) {

			ConfirmCallBack callBack = new ConfirmCallBack() {

				@Override
				public void confirm(boolean choice) {
					if (choice == true) {
						presenter.computeGeographicalBoxes();
					}
				}
			};

			if (multipleGeographicalExtent.getBoxes().isEmpty()) {
				callBack.confirm(true);
			} else {
				DialogBoxTools.modalConfirm(CommonMessages.INSTANCE.confirm(),
						Message.INSTANCE.deleteExistingValuesWarning(),
						callBack).center();
			}
		}
	}

	public void setPresenter(ObservatoryEntryPresenter presenter) {
		this.presenter = presenter;

	}

	public void setBoxes(ArrayList<GeographicBoundingBoxDTO> boxes) {
		multipleGeographicalExtent.setBoxes(boxes);
	}

	@Override
	public void onResize() {
		super.onResize();
		if (multipleGeographicalExtent != null) {
			multipleGeographicalExtent.onResize();
		}
	}
}
