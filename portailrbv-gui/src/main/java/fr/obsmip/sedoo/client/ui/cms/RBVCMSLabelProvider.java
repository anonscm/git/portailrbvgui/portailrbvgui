package fr.obsmip.sedoo.client.ui.cms;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.sedoo.commons.client.cms.ui.CMSLabelProvider;
import fr.sedoo.commons.client.message.CommonMessages;

public class RBVCMSLabelProvider implements CMSLabelProvider {

	@Override
	public String getLabelByScreenName(String screenName) {
		if (screenName.compareTo(ScreenNames.HOME) == 0) {
			//return Message.INSTANCE.headerViewInformationSiteLink();
			return CommonMessages.INSTANCE.home();
		}

		else if (screenName.compareTo(ScreenNames.HUMANITY_CRITICAL_ZONE) == 0) {
			return Message.INSTANCE.humanityCriticalZone();
		}

		else if (screenName.compareTo(ScreenNames.MULTIDISCIPLINARY_APPROACH) == 0) {
			return Message.INSTANCE.multidisciplinaryApproach();
		}

		else if (screenName.compareTo(ScreenNames.NATURAL_LABORATORY) == 0) {
			return Message.INSTANCE.naturalLaboratory();
		}

		else if (screenName.compareTo(ScreenNames.ELEMENTARY_OBSERVATORIES) == 0) {
			return Message.INSTANCE.elementaryObservatories();
		}

		else if (screenName.compareTo(ScreenNames.LOCATION) == 0) {
			return Message.INSTANCE.location();
		}

		else if (screenName.compareTo(ScreenNames.A_SCIENTIST_NETWORK) == 0) {
			return Message.INSTANCE.aScientistNetwork();
		}

		else if (screenName.compareTo(ScreenNames.STEERING_COMITEE) == 0) {
			return Message.INSTANCE.steeringCommittee();
		}

		else if (screenName
				.compareTo(ScreenNames.SCIENTIFIC_ORIENTATION_COMITTEE) == 0) {
			return Message.INSTANCE.scientificOrientationCommittee();
		}

		else if (screenName.compareTo(ScreenNames.COORDINATOR_AND_CONTACT) == 0) {
			return Message.INSTANCE.coordinatorAndContact();
		}

		else if (screenName.compareTo(ScreenNames.INSTRUMENTS_AND_MEASUREMENTS) == 0) {
			return Message.INSTANCE.instrumentsAndMeasurements();
		}

		else if (screenName.compareTo(ScreenNames.OBSERVATION_PARAMETERS) == 0) {
			return Message.INSTANCE.observationParameters();
		}

		else if (screenName.compareTo(ScreenNames.METADATA_PORTAL) == 0) {
			return Message.INSTANCE.metadataPortal();
		}

		else if (screenName.compareTo(ScreenNames.RESULT_EXAMPLES) == 0) {
			return Message.INSTANCE.resultExamples();
		}

		else if (screenName.compareTo(ScreenNames.MISCELLANEOUS) == 0) {
			return Message.INSTANCE.miscellaneous();
		}

		else if (screenName.compareTo(ScreenNames.ANIMATION) == 0) {
			return Message.INSTANCE.animation();
		}

		else if (screenName.compareTo(ScreenNames.DOCUMENTS) == 0) {
			return Message.INSTANCE.documents();
		}

		else if (screenName.compareTo(ScreenNames.LINKS) == 0) {
			return Message.INSTANCE.links();
		} else if (screenName != null) {
			return screenName;
		} else {
			return "";
		}

	}
}
