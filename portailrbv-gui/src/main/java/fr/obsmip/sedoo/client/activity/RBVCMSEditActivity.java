package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.sedoo.commons.client.cms.activity.CMSEditActivity;
import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.CMSClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVCMSEditActivity extends CMSEditActivity {

	private final CMSEditPlace place;

	public RBVCMSEditActivity(CMSEditPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		this.place = place;
		broadcastActivityTitle(CMSMessages.INSTANCE.screenModification()
				+ " : "
				+ ((CMSClientFactory) clientFactory).getCMSLabelProvider()
						.getLabelByScreenName(place.getScreenName()));
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory
				.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		if (place.getScreenName().compareToIgnoreCase(ScreenNames.HOME) != 0) {
			shortcuts.add(ShortcutFactory.getCMSEditShortcut(place));
		}
		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
	}

	protected void broadcastActivityTitle(String title) {
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser()) {
			return ((AuthenticatedClientFactory) clientFactory)
					.getUserManager().getUser().isAdmin();
		} else {
			return false;
		}
	}

	@Override
	public Place getPlace() {
		return place;
	}
}