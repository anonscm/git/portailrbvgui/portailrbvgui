package fr.obsmip.sedoo.client.ui.searchcriteria;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public class MapCriteriaBar extends CriteriaBar implements MapCriteriaWidget {

	public MapCriteriaBar() {
		super();
		// ObservatoryWidget observatoriesWidget = new ObservatoryWidget(this);
		// add(observatoriesWidget);
		//
		// EntryTypeWidget entryTypeWidget = new EntryTypeWidget(this);
		// add(entryTypeWidget);
		//
		// KeywordWidget keywordWidget = new KeywordWidget(this);
		// add(keywordWidget);
		//
		// DateWidget dateWidget = new DateWidget(this);
		// add(dateWidget);

	}

	public MapCriteriaBar(ErrorButtonContainer errorButtonContainer) {
		super(errorButtonContainer);
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {
		SearchCriteriaDTO aux = criteria;
		for (CriteriaWidget widget : criteriaWidgets) {
			aux = ((MapCriteriaWidget) widget).flush(aux);
		}
		return aux;
	}
}
