package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class ExperimentalSiteEntryManagementPlace extends Place implements AuthenticatedPlace
{

	public static ExperimentalSiteEntryManagementPlace instance;

	public static class Tokenizer implements PlaceTokenizer<ExperimentalSiteEntryManagementPlace>
	{
		@Override
		public String getToken(ExperimentalSiteEntryManagementPlace place)
		{
			return "";
		}

		@Override
		public ExperimentalSiteEntryManagementPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ExperimentalSiteEntryManagementPlace();
			}
			return instance;
		}
	}
	
}
