package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.user.client.ui.CheckBox;

public class NormalFontCheckBox extends CheckBox {

	public NormalFontCheckBox(String value) {
		super(value);
		NodeList<Element> labels = getElement().getElementsByTagName("label");
		int labelsCount = labels.getLength();

		for (int i = 0; i < labelsCount; i++) {
			labels.getItem(i).getStyle().setFontWeight(FontWeight.NORMAL);
		}
	}

}
