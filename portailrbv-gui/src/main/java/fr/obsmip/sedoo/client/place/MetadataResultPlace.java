package fr.obsmip.sedoo.client.place;

import java.util.ArrayList;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public class MetadataResultPlace extends Place
{
	public static MetadataResultPlace instance;
	
	private ArrayList<SummaryDTO> summaries;
	private int hits = -1;
	private SearchCriteriaDTO criteria;
	
	public MetadataResultPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<MetadataResultPlace>
	{
		
		public MetadataResultPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new MetadataResultPlace();
			}
			instance.setCriteria(SearchCriteriaDTO.fromToken(token));
			if (MetadataSearchPlace.getCriteria() == null)
			{
				MetadataSearchPlace.setCriteria(SearchCriteriaDTO.fromToken(token));
			}
			return instance;
		}

		public String getToken(MetadataResultPlace place) {
			if (place.getCriteria() != null)
			{
				return place.getCriteria().toToken();
			}
			return "";
		}
	}

	public void setSummaries(ArrayList<SummaryDTO> summaries) 
	{
		this.summaries = summaries;
	}
	
	public ArrayList<SummaryDTO> getSummaries() {
		return summaries;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public SearchCriteriaDTO getCriteria() {
		return criteria;
	}

	public void setCriteria(SearchCriteriaDTO criteria) {
		this.criteria = criteria;
	}

	
}
