package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ObservatoryNavigationEvent extends GwtEvent<ObservatoryNavigationEventHandler>{

	public static final Type<ObservatoryNavigationEventHandler> TYPE = new Type<ObservatoryNavigationEventHandler>();
	private String observatoryName;
	private String mode;
	private String observatoryUuid;
	

	public ObservatoryNavigationEvent(String observatoryName, String mode, String observatoryUuid)
	{
		this.observatoryName = observatoryName;
		this.setObservatoryUuid(observatoryUuid);
		this.setMode(mode);
		
	}

	@Override
	protected void dispatch(ObservatoryNavigationEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ObservatoryNavigationEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getObservatoryUuid() {
		return observatoryUuid;
	}

	public void setObservatoryUuid(String observatoryUuid) {
		this.observatoryUuid = observatoryUuid;
	}
	
	
}
