package fr.obsmip.sedoo.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

public class HarvestManagementViewImpl extends AbstractSection implements HarvestManagementView {

	private static HarvestManagementViewImplUiBinder uiBinder = GWT
			.create(HarvestManagementViewImplUiBinder.class);

	interface HarvestManagementViewImplUiBinder extends UiBinder<Widget, HarvestManagementViewImpl> {
	}

	@UiField
	Button launchButton;
	private Presenter presenter; 

	public HarvestManagementViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public void displaySuccessMessage() {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayFailureMessage() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reset() {
		launchButton.setEnabled(true);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@UiHandler("launchButton")
	void onLaunchButtonClicked(ClickEvent event) 
	{
		launchButton.setEnabled(false);
		presenter.launch();
		launchButton.setEnabled(true);
	}

}
