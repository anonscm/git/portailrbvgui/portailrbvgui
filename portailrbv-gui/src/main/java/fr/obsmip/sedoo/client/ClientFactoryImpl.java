package fr.obsmip.sedoo.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.event.UserLoginEvent;
import fr.obsmip.sedoo.client.event.UserLogoutEvent;
import fr.obsmip.sedoo.client.print.RBVPrintStyleProvider;
import fr.obsmip.sedoo.client.ui.BlankView;
import fr.obsmip.sedoo.client.ui.BlankViewImpl;
import fr.obsmip.sedoo.client.ui.DatasetEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.DatasetEntryManagementView;
import fr.obsmip.sedoo.client.ui.DatasetEntryManagementViewImpl;
import fr.obsmip.sedoo.client.ui.DatasetEntryView;
import fr.obsmip.sedoo.client.ui.DirectoryManagementView;
import fr.obsmip.sedoo.client.ui.DirectoryManagementViewImpl;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryManagementView;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryManagementViewImpl;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryView;
import fr.obsmip.sedoo.client.ui.GeoSummaryView;
import fr.obsmip.sedoo.client.ui.GeoSummaryViewImpl;
import fr.obsmip.sedoo.client.ui.HarvestManagementView;
import fr.obsmip.sedoo.client.ui.HarvestManagementViewImpl;
import fr.obsmip.sedoo.client.ui.HeaderView;
import fr.obsmip.sedoo.client.ui.HeaderViewImpl;
import fr.obsmip.sedoo.client.ui.LogConsultationView;
import fr.obsmip.sedoo.client.ui.LogConsultationViewImpl;
import fr.obsmip.sedoo.client.ui.LoginView;
import fr.obsmip.sedoo.client.ui.LoginViewImpl;
import fr.obsmip.sedoo.client.ui.MenuView;
import fr.obsmip.sedoo.client.ui.MenuViewImpl;
import fr.obsmip.sedoo.client.ui.MetadataManagingView;
import fr.obsmip.sedoo.client.ui.MetadataResultView;
import fr.obsmip.sedoo.client.ui.MetadataResultViewImpl;
import fr.obsmip.sedoo.client.ui.MetadataSearchView;
import fr.obsmip.sedoo.client.ui.MetadataSearchViewImpl;
import fr.obsmip.sedoo.client.ui.NewsView;
import fr.obsmip.sedoo.client.ui.NewsViewImpl;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryDisplayingView;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryManagementView;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryManagementViewImpl;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryView;
import fr.obsmip.sedoo.client.ui.ProgressView;
import fr.obsmip.sedoo.client.ui.ProgressViewImpl;
import fr.obsmip.sedoo.client.ui.RBVMessageEditViewImpl;
import fr.obsmip.sedoo.client.ui.RBVNewDisplayViewImpl;
import fr.obsmip.sedoo.client.ui.RBVNewsArchiveViewImpl;
import fr.obsmip.sedoo.client.ui.SectionHeaderView;
import fr.obsmip.sedoo.client.ui.SectionHeaderViewImpl;
import fr.obsmip.sedoo.client.ui.StatusBarView;
import fr.obsmip.sedoo.client.ui.StatusBarViewImpl;
import fr.obsmip.sedoo.client.ui.SystemView;
import fr.obsmip.sedoo.client.ui.SystemViewImpl;
import fr.obsmip.sedoo.client.ui.UserManagementView;
import fr.obsmip.sedoo.client.ui.UserManagementViewImpl;
import fr.obsmip.sedoo.client.ui.WelcomeView;
import fr.obsmip.sedoo.client.ui.WelcomeViewImpl;
import fr.obsmip.sedoo.client.ui.cms.RBVCMSLabelProvider;
import fr.obsmip.sedoo.client.ui.geonetworkobservatory.GeonetworkObservatoryManagementView;
import fr.obsmip.sedoo.client.ui.geonetworkobservatory.GeonetworkObservatoryManagementViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetEntryCreationViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetEntryDisplayingViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.dataset.DatasetMetadataViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteEntryCreationViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteEntryDisplayingViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.experimentalsite.ExperimentalSiteMetadataViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryEntryCreationView;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryEntryCreationViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryEntryDisplayingViewImpl;
import fr.obsmip.sedoo.client.ui.metadata.observatory.ObservatoryMetadataViewImpl;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumb;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumbImpl;
import fr.obsmip.sedoo.shared.domain.DatasetDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.obsmip.sedoo.shared.util.HierachyLevelUtil;
import fr.sedoo.commons.client.cms.ui.CMSConsultView;
import fr.sedoo.commons.client.cms.ui.CMSConsultViewImpl;
import fr.sedoo.commons.client.cms.ui.CMSEditView;
import fr.sedoo.commons.client.cms.ui.CMSEditViewImpl;
import fr.sedoo.commons.client.cms.ui.CMSLabelProvider;
import fr.sedoo.commons.client.cms.ui.CMSListView;
import fr.sedoo.commons.client.cms.ui.CMSListViewImpl;
import fr.sedoo.commons.client.cms.ui.view.CMSMenuEditView;
import fr.sedoo.commons.client.cms.ui.view.CMSMenuEditViewImpl;
import fr.sedoo.commons.client.event.ActionEndEvent;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingView;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingViewImpl;
import fr.sedoo.commons.client.mvp.NavigationManager;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.ui.MessageEditView;
import fr.sedoo.commons.client.news.ui.MessageManageView;
import fr.sedoo.commons.client.news.ui.MessageManageViewImpl;
import fr.sedoo.commons.client.news.widget.NewDisplayView;
import fr.sedoo.commons.client.news.widget.NewsArchiveView;
import fr.sedoo.commons.client.user.UserManager;
import fr.sedoo.commons.client.user.UserManagerImpl;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.metadata.shared.constant.Iso19139Constants;
import fr.sedoo.metadata.client.mvp.MetadataClientFactoryFragment;
import fr.sedoo.metadata.client.ui.view.FormatListView;
import fr.sedoo.metadata.client.ui.view.FormatListViewImpl;
import fr.sedoo.metadata.client.ui.view.MetadataDisplayingView;
import fr.sedoo.metadata.client.ui.view.MetadataEditingView;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;
import fr.sedoo.metadata.shared.domain.misc.Iso6392LanguageProvider;

public class ClientFactoryImpl implements ClientFactory {

	private static final EventBus EVENT_BUS = new SimpleEventBus();
	private static final PlaceController PLACE_CONTROLLER = new AsynchronousPlaceController(
			EVENT_BUS);
	private static WelcomeView welcomeView;
	private static ProgressView progressView;
	private static BlankView blankView;
	private static ObservatoryEntryManagementView observatoryManagementView = null;
	private static ObservatoryEntryView observatoryEditingView = null;
	private static MetadataManagingView metadataManagingView = null;
	private static MetadataSearchView metadataSearchView = null;
	private static final BreadCrumb BREAD_CRUMB = new BreadCrumbImpl();
	private static FormatListView formatListView = null;
	private static MetadataResultView resultView = null;
	private static GeoSummaryView geoSummaryView = null;
	private static UserManagementView userManagementView = null;
	private static MessageManageView messageManagementView = null;
	private static MessageEditView messageEditView = null;
	private static LogConsultationView logConsultationView = null;
	private static GeonetworkObservatoryManagementView geonetworkObservatoryManagementView = null;
	private static ObservatoryEntryCreationView observatoryEntryCreationView = null;
	private static ExperimentalSiteEntryCreationView experimentalSiteEntryCreationView = null;
	private static ExperimentalSiteEntryView experimentalSiteEntryView = null;
	private static ExperimentalSiteEntryManagementView experimentalSiteEntryManagementView = null;
	private static ObservatoryEntryDisplayingView observatoryEntryDisplayingView = null;
	private static ExperimentalSiteEntryDisplayingView experimentalSiteEntryDisplayingView = null;
	private static DatasetEntryDisplayingView datasetEntryDisplayingView = null;
	private static DatasetEntryCreationView datasetEntryCreationView = null;
	private static DatasetEntryManagementView datasetEntryManagementView = null;
	private static DatasetEntryView datasetEntryView = null;
	private static DirectoryManagementView directoryManagementView = null;
	private static HarvestManagementView harvestManagementView = null;
	private static NewDisplayView newDisplayView = null;
	public static CMSConsultView cmsConsultView;
	public static CMSEditView cmsEditView;
	public static CMSListView cmsListView;
	public static CMSMenuEditView cmsMenuEditView;
	private static NewsView newsView; 
	private static NewsArchiveView newsArchiveView;

	public static CMSLabelProvider labelProvider = new RBVCMSLabelProvider();

	public static HashMap<String, String> SCREEN_NAMES = new HashMap<String, String>();

	private static UserManager USER_MANAGER = new UserManagerImpl();

	private static MetadataTypeSorter METADATA_TYPE_SORTER = new MetadataTypeSorter();

	private static final HeaderView HEADER_VIEW = new HeaderViewImpl();
	private static final SectionHeaderView SECTION_HEADER_VIEW = new SectionHeaderViewImpl();

	private final MetadataClientFactoryFragment metadataFragment = new MetadataClientFactoryFragment(
			this);

	static {
		EVENT_BUS.addHandler(UserLoginEvent.TYPE, HEADER_VIEW);
		EVENT_BUS.addHandler(UserLogoutEvent.TYPE, HEADER_VIEW);
	}

	private static final MenuView MENU_VIEW = new MenuViewImpl();
	static {
		EVENT_BUS.addHandler(UserLoginEvent.TYPE, MENU_VIEW);
		EVENT_BUS.addHandler(UserLogoutEvent.TYPE, MENU_VIEW);
		EVENT_BUS.addHandler(ActivityStartEvent.TYPE, MENU_VIEW);
	}

	private static SystemView SYSTEM_VIEW = null;
	// private static final StatusBarView STATUS_BAR_VIEW = new
	// DebugStatusBarViewImpl(EVENT_BUS);
	private static final StatusBarView STATUS_BAR_VIEW = new StatusBarViewImpl();
	private static LoginView LOGIN_VIEW = null;

	static {
		EVENT_BUS.addHandler(ActionStartEvent.TYPE, STATUS_BAR_VIEW);
		EVENT_BUS.addHandler(ActionEndEvent.TYPE, STATUS_BAR_VIEW);
		EVENT_BUS.addHandler(ActivityStartEvent.TYPE, STATUS_BAR_VIEW);
	}

	private static MetadataEditingView metadataEditingView = null;
	private static LanguageSwitchingView LANGUAGE_SWITCHING_VIEW = new LanguageSwitchingViewImpl();
	public static final GlobalBundle GLOBAL_BUNDLE = GWT
			.create(GlobalBundle.class);
	private ArrayList<String> displayLanguage;

	static {
		EVENT_BUS.addHandler(ActivityChangeEvent.TYPE, SECTION_HEADER_VIEW);
	}

	public ClientFactoryImpl() {
		BREAD_CRUMB.setClientFactory(this);
	}

	@Override
	public EventBus getEventBus() {
		return EVENT_BUS;
	}

	@Override
	public PlaceController getPlaceController() {
		return PLACE_CONTROLLER;
	}

	@Override
	public WelcomeView getWelcomeView() {
		if (welcomeView == null) {
			welcomeView = new WelcomeViewImpl();
		}
		return welcomeView;
	}

	@Override
	public MenuView getMenuView() {
		return MENU_VIEW;
	}

	@Override
	public HeaderView getHeaderView() {
		return HEADER_VIEW;
	}

	@Override
	public SystemView getSystemView() {
		if (SYSTEM_VIEW == null) {
			SYSTEM_VIEW = new SystemViewImpl();
		}
		return SYSTEM_VIEW;
	}

	@Override
	public MetadataEditingView getMetadataEditingView() {
		if (metadataEditingView == null) {
			// metadataEditingView = new MetadataEditingViewImpl();
		}
		return metadataEditingView;
	}

	@Override
	public StatusBarView getStatusBarView() {
		return STATUS_BAR_VIEW;
	}

	@Override
	public SectionHeaderView getSectionHeaderView() {
		return SECTION_HEADER_VIEW;
	}

	@Override
	public GlobalBundle getBundle() {
		return GLOBAL_BUNDLE;
	}

	@Override
	public LanguageSwitchingView getLanguageSwitchingView() {
		return LANGUAGE_SWITCHING_VIEW;
	}

	@Override
	public MetadataSearchView getMetadataSearchView() {

		if (metadataSearchView == null) {
			metadataSearchView = new MetadataSearchViewImpl();
		}
		return metadataSearchView;
	}

	@Override
	public ProgressView getProgressView() {

		if (progressView == null) {
			progressView = new ProgressViewImpl();
		}
		return progressView;
	}

	@Override
	public BlankView getBlankView() {

		if (blankView == null) {
			blankView = new BlankViewImpl();
		}
		return blankView;
	}

	@Override
	public GeoSummaryView getGeoSummaryView() {
		if (geoSummaryView == null) {
			geoSummaryView = new GeoSummaryViewImpl();
		}
		return geoSummaryView;
	}

	@Override
	public MetadataDisplayingView getMetadataDisplayView(MetadataDTO metadata) {

		// int metadataType =
		// METADATA_TYPE_SORTER.getTypeFromMetadata(metadata);
		// String currentLanguage =
		// Iso6392LanguageProvider.convertLocaleToIso6392(LocaleUtil.getCurrentLanguage(this));
		//
		// if (metadataType == METADATA_TYPE_SORTER.DATASET_TYPE)
		// {
		// if (dataSetMetadataDisplayView == null)
		// {
		// dataSetMetadataDisplayView = new
		// DatasetMetadataViewImpl(currentLanguage, getMetadataLanguages(),
		// getDataLanguages());
		// }
		// return dataSetMetadataDisplayView;
		// }
		// else //Le type n'a pas pu être évalué, on retourne la vue pour les
		// dataset
		// {
		// if (dataSetMetadataDisplayView == null)
		// {
		// dataSetMetadataDisplayView =new
		// DatasetMetadataViewImpl(currentLanguage, getMetadataLanguages(),
		// getDataLanguages());
		// }
		// return dataSetMetadataDisplayView;
		// }
		return null;

	}

	@Override
	public LoginView getLoginView() {
		if (LOGIN_VIEW == null) {
			LOGIN_VIEW = new LoginViewImpl();
		}
		return LOGIN_VIEW;
	}

	@Override
	public ObservatoryEntryManagementView getObservatoryManagementView() {
		if (observatoryManagementView == null) {
			observatoryManagementView = new ObservatoryEntryManagementViewImpl();
		}

		return observatoryManagementView;
	}

	@Override
	public ObservatoryEntryView getObservatoryEditingView() {
		if (observatoryEditingView == null) {
			observatoryEditingView = new ObservatoryMetadataViewImpl(
					getDisplayLanguages(), getDataLanguages());
		}
		return observatoryEditingView;
	}

	@Override
	public ExperimentalSiteEntryView getExperimentalSiteEntryEditingView() {
		if (experimentalSiteEntryView == null) {
			experimentalSiteEntryView = new ExperimentalSiteMetadataViewImpl(
					getDisplayLanguages(), getDataLanguages());
		}
		return experimentalSiteEntryView;
	}

	@Override
	public UserManagementView getUserManagementView() {

		if (userManagementView == null) {
			userManagementView = new UserManagementViewImpl();
		}
		return userManagementView;
	}

	@Override
	public MessageManageView getMessageManageView() {

		if (messageManagementView == null) {
			messageManagementView = new MessageManageViewImpl();
		}
		return messageManagementView;
	}

	@Override
	public LogConsultationView getLogConsultationView() {

		if (logConsultationView == null) {
			logConsultationView = new LogConsultationViewImpl();
		}
		return logConsultationView;
	}

	@Override
	public BreadCrumb getBreadCrumb() {
		return BREAD_CRUMB;
	}

	@Override
	public MetadataManagingView getMetadataManagingView() {
		if (metadataManagingView == null) {
			// metadataManagingView = new MetadataManagingViewImpl();
		}
		return metadataManagingView;
	}

	@Override
	public FormatListView getFormatListView() {

		if (formatListView == null) {
			formatListView = new FormatListViewImpl();
		}
		return formatListView;
	}

	@Override
	public MetadataResultView getResultView() {
		if (resultView == null) {
			resultView = new MetadataResultViewImpl();
		}
		return resultView;
	}

	@Override
	public UserManager getUserManager() {
		return USER_MANAGER;
	}

	@Override
	public LoginPlace getLoginPlace() {
		return new LoginPlace();
	}

	@Override
	public NavigationManager getNavigationManager() {
		return null;
	}

	@Override
	public String getDefaultLanguage() {
		return LocaleUtil.ENGLISH;
	}

	@Override
	public List<String> getLanguages() {
		ArrayList<String> languages = new ArrayList<String>();
		languages.add(LocaleUtil.ENGLISH);
		languages.add(LocaleUtil.FRENCH);
		return languages;
	}

	@Override
	public MessageEditView getMessageEditView() {
		if (messageEditView == null) {
			messageEditView = new RBVMessageEditViewImpl("");
		}
		return messageEditView;
	}

	@Override
	public GeonetworkObservatoryManagementView getGeonetworkObservatoryManagementView() {
		if (geonetworkObservatoryManagementView == null) {
			geonetworkObservatoryManagementView = new GeonetworkObservatoryManagementViewImpl();
		}
		return geonetworkObservatoryManagementView;
	}

	@Override
	public ObservatoryEntryCreationView getObservatoryEntryCreationView() {
		if (observatoryEntryCreationView == null) {
			observatoryEntryCreationView = new ObservatoryEntryCreationViewImpl();
		}
		return observatoryEntryCreationView;
	}

	@Override
	public ExperimentalSiteEntryCreationView getExperimentalSiteEntryCreationView() {
		if (experimentalSiteEntryCreationView == null) {
			experimentalSiteEntryCreationView = new ExperimentalSiteEntryCreationViewImpl();
		}
		return experimentalSiteEntryCreationView;
	}

	@Override
	public ArrayList<String> getMetadataLanguages() {
		return getDisplayLanguages();
	}

	@Override
	public ArrayList<String> getDataLanguages() {
		ArrayList<String> languages = new ArrayList<String>();
		languages.add(Iso6392LanguageProvider.ENGLISH);
		languages.add(Iso6392LanguageProvider.FRENCH);
		languages.add(Iso6392LanguageProvider.PORTUGUESE);
		languages.add(Iso6392LanguageProvider.SPANISH);
		return languages;
	}

	@Override
	public MetadataDTO createMetadata() {
		return metadataFragment.createMetadata();
	}

	@Override
	public ObservatoryDTO createObservatoryDTO(String observatoryName) {
		ObservatoryDTO dto = new ObservatoryDTO();
		I18nString resourceTitle = new I18nString();
		HashMap<String, String> resourceTitleValue = new HashMap<String, String>();
		resourceTitleValue.put(Iso6392LanguageProvider.ENGLISH,
				"Observations and measurements made by observatory "
						+ observatoryName);
		resourceTitleValue.put(Iso6392LanguageProvider.FRENCH,
				"Observations et mesures réalisées par l'observatoire "
						+ observatoryName);
		resourceTitle.setI18nValues(resourceTitleValue);
		dto.setObservatoryName(observatoryName);
		dto.getIdentificationPart().setResourceTitle(resourceTitle);
		dto.getIdentificationPart().setHierarchyLevel(Iso19139Constants.SERIES);
		dto.getIdentificationPart().setHierarchyLevelName(
				HierachyLevelUtil.OBSERVATORY_HIERARCHY_LEVEL_NAME);
		String currentLanguage = Iso6392LanguageProvider
				.convertLocaleToIso6392(LocaleUtil.getCurrentLanguage(this));
		dto.getOtherPart().setMetadataLanguage(currentLanguage);
		return dto;
	}

	@Override
	public LanguageSwitchPlace getLanguageSwitchPlace(String language) {
		return new LanguageSwitchPlace(language);
	}

	@Override
	public ExperimentalSiteDTO createExperimentalSiteDTO(
			String experimentalSiteName, MetadataSummaryDTO parentSummary) {
		ExperimentalSiteDTO dto = new ExperimentalSiteDTO();
		I18nString resourceTitle = new I18nString();
		HashMap<String, String> resourceTitleValue = new HashMap<String, String>();
		resourceTitleValue.put(Iso6392LanguageProvider.ENGLISH,
				"Observations and measurements made by experimental site "
						+ experimentalSiteName);
		resourceTitleValue.put(Iso6392LanguageProvider.FRENCH,
				"Observations et mesures réalisées par le site expérimental "
						+ experimentalSiteName);
		resourceTitle.setI18nValues(resourceTitleValue);
		dto.setExperimentalSiteName(experimentalSiteName);
		dto.setParentSummary(parentSummary);
		dto.getIdentificationPart().setResourceTitle(resourceTitle);
		dto.getIdentificationPart().setHierarchyLevel(Iso19139Constants.SERIES);
		dto.getIdentificationPart().setHierarchyLevelName(
				HierachyLevelUtil.EXPERIMENTAL_SITE_HIERARCHY_LEVEL_NAME);
		String currentLanguage = Iso6392LanguageProvider
				.convertLocaleToIso6392(LocaleUtil.getCurrentLanguage(this));
		dto.getOtherPart().setMetadataLanguage(currentLanguage);
		return dto;
	}

	@Override
	public DatasetDTO createDatasetDTO(MetadataSummaryDTO observatorySummary,
			MetadataSummaryDTO experimentalSiteSummary) {
		DatasetDTO dto = new DatasetDTO();
		dto.setObservatorySummary(observatorySummary);
		dto.setExperimentalSiteSummary(experimentalSiteSummary);
		dto.getIdentificationPart()
				.setHierarchyLevel(Iso19139Constants.DATASET);
		dto.getIdentificationPart().setHierarchyLevelName(
				HierachyLevelUtil.DATASET_HIERARCHY_LEVEL_NAME);
		String currentLanguage = Iso6392LanguageProvider
				.convertLocaleToIso6392(LocaleUtil.getCurrentLanguage(this));
		dto.getOtherPart().setMetadataLanguage(currentLanguage);
		return dto;
	}

	@Override
	public ExperimentalSiteEntryManagementView getExperimentalSiteEntryManagementView() {
		if (experimentalSiteEntryManagementView == null) {
			experimentalSiteEntryManagementView = new ExperimentalSiteEntryManagementViewImpl();
		}
		return experimentalSiteEntryManagementView;
	}

	@Override
	public ObservatoryEntryDisplayingView getObservatoryEntryDisplayingView() {
		if (observatoryEntryDisplayingView == null) {
			observatoryEntryDisplayingView = new ObservatoryEntryDisplayingViewImpl(
					getMetadataLanguages(), getDataLanguages());
		}
		return observatoryEntryDisplayingView;
	}

	@Override
	public ExperimentalSiteEntryDisplayingView getExperimentalSiteEntryDisplayingView() {
		if (experimentalSiteEntryDisplayingView == null) {
			experimentalSiteEntryDisplayingView = new ExperimentalSiteEntryDisplayingViewImpl(
					getDisplayLanguages(), getDataLanguages());
		}
		return experimentalSiteEntryDisplayingView;
	}

	@Override
	public DatasetEntryDisplayingView getDatasetEntryDisplayingView() {
		if (datasetEntryDisplayingView == null) {
			datasetEntryDisplayingView = new DatasetEntryDisplayingViewImpl(
					getDisplayLanguages(), getDataLanguages());
		}
		return datasetEntryDisplayingView;
	}

	@Override
	public DatasetEntryCreationView getDatasetEntryCreationView() {
		if (datasetEntryCreationView == null) {
			datasetEntryCreationView = new DatasetEntryCreationViewImpl();
		}
		return datasetEntryCreationView;
	}

	@Override
	public DatasetEntryView getDatasetEntryEditingView() {
		if (datasetEntryView == null) {
			datasetEntryView = new DatasetMetadataViewImpl(
					getDisplayLanguages(), getDataLanguages());
		}
		return datasetEntryView;
	}

	/**
	 * Return the list of display languages in Iso6392 format The language
	 * corresponding to the current language is returned first The other
	 * languages correspond to the other GUI languages
	 */
	@Override
	public ArrayList<String> getDisplayLanguages() {
		if (displayLanguage == null) {
			displayLanguage = new ArrayList<String>();
			LinkedHashSet<String> aux = new LinkedHashSet<String>();
			aux.add(Iso6392LanguageProvider.convertLocaleToIso6392(LocaleUtil
					.getCurrentLanguage(this)));
			for (int i = 0; i < getLanguages().size(); i++) {
				aux.add(Iso6392LanguageProvider
						.convertLocaleToIso6392(getLanguages().get(i)));
			}
			displayLanguage.addAll(aux);
		}
		return displayLanguage;
	}

	@Override
	public DirectoryManagementView getDirectoryManagementView() {
		if (directoryManagementView == null) {
			directoryManagementView = new DirectoryManagementViewImpl();
		}
		return directoryManagementView;
	}

	@Override
	public HarvestManagementView getHarvestManagementView() {
		if (harvestManagementView == null) {
			harvestManagementView = new HarvestManagementViewImpl();
		}
		return harvestManagementView;
	}

	@Override
	public IsWidget getUnAuthorizedUserView() {
		return null;
	}

	@Override
	public NewDisplayView getNewDisplayView() {
		if (newDisplayView == null) {
			newDisplayView = new RBVNewDisplayViewImpl(
					new RBVPrintStyleProvider());
		}
		return newDisplayView;

	}

	
	
	@Override
	public NewsArchiveView getNewsArchiveView() {
		if (newsArchiveView == null) {
			newsArchiveView = new RBVNewsArchiveViewImpl();
		}
		return newsArchiveView;
	}

	@Override
	public DatasetEntryManagementView getDatasetEntryManagementView() {
		if (datasetEntryManagementView == null) {
			datasetEntryManagementView = new DatasetEntryManagementViewImpl();
		}
		return datasetEntryManagementView;
	}

	@Override
	public HashMap<String, String> getScreenNames() {
		return SCREEN_NAMES;
	}

	@Override
	public CMSMenuEditView getCmsMenuEditView() {
		if (cmsMenuEditView == null) {
			cmsMenuEditView = new CMSMenuEditViewImpl();
		}
		return cmsMenuEditView;
	}

	@Override
	public CMSConsultView getCmsConsultView() {
		if (cmsConsultView == null) {
			cmsConsultView = new CMSConsultViewImpl(
					new RBVPrintStyleProvider(), false);
		}
		return cmsConsultView;
	}

	@Override
	public CMSEditView getCmsEditView() {
		if (cmsEditView == null) {
			cmsEditView = new CMSEditViewImpl(
					LocaleUtil.getCurrentLanguage(this), false);
			((CMSEditViewImpl) cmsEditView).getSaveButton().removeStyleName(
					"btn-primary");
			((CMSEditViewImpl) cmsEditView).getViewButton().removeStyleName(
					"btn-primary");
		}
		return cmsEditView;
	}

	@Override
	public CMSListView getCmsListView() {
		if (cmsListView == null) {
			cmsListView = new CMSListViewImpl();
		}
		return cmsListView;
	}

	@Override
	public CMSLabelProvider getCMSLabelProvider() {
		return labelProvider;
	}

	@Override
	public Shortcut getWelcomeShortcut() {
		return ShortcutFactory.getWelcomeShortcut();
	}

	@Override
	public NewsView getNewsView() {
		if (newsView == null) {
			newsView = new NewsViewImpl();
		}
		return newsView;
	}

}
