package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.DatasetEntryDisplayingPlace;
import fr.obsmip.sedoo.client.ui.ExperimentalSiteEntryView.ExperimentalSiteEntryPresenter;
import fr.obsmip.sedoo.shared.util.MetadataSummaryDTOComparator;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class DatasetTable extends AbstractSummaryTable {

	
	private ExperimentalSiteEntryPresenter presenter;

	public DatasetTable()
	{
		super();
		setAddButtonEnabled(true);
	}
	
	@Override
	public void presenterView(HasIdentifier hasId) {
		
		DatasetEntryDisplayingPlace place = new DatasetEntryDisplayingPlace();
		place.setUuid(hasId.getIdentifier());
		PortailRBV.getClientFactory().getPlaceController().goTo(place);
		
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter.deleteDataset(hasId.getIdentifier());
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		presenter.goToDatasetEditPlace((MetadataSummaryDTO) hasId);
		
	}

	@Override
	public String getAddItemText() {
		return Message.INSTANCE.addNewDataset();
	}
	
	@Override
	public void addItem() {
		presenter.addDataset();
	}

	public void setPresenter(ExperimentalSiteEntryPresenter presenter) {
		this.presenter = presenter;
	}
	
	public String getDeleteItemConfirmationText() {
		return CommonMessages.INSTANCE.deletionConfirmMessage();
	}

	@Override
	public void init(List<? extends HasIdentifier> model) {
		ArrayList<MetadataSummaryDTO> sortedEntries = new ArrayList<MetadataSummaryDTO>();
		Iterator<? extends HasIdentifier> iterator = model.iterator();
		while (iterator.hasNext()) 
		{
			sortedEntries.add((MetadataSummaryDTO) iterator.next());
		}
		Collections.sort(sortedEntries, new MetadataSummaryDTOComparator());
		super.init(sortedEntries);
	}
	
	public void enableEditMode() {
		clearColumns();
		addNameColumn();
		addEditColumn();
		addDeleteColumn();
	}

	public void enableDisplayMode() {
		clearColumns();
		addNameColumn();
		addViewColum();
	}
}
