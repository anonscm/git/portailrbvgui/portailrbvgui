package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;

public interface ExperimentalSiteEntryCreationView extends IsWidget
{
	void setObservatories(List<ObservatorySummaryDTO> observatories);
	void setPresenter(Presenter presenter);
	public interface Presenter 
	 {
	        void create(ObservatorySummaryDTO parentSummary, String experimentalSiteName);

			void goToObservatoryEntryCreationPlace();
	 }
	void reset();
	void selectObservatory(String observatoryName);
}
