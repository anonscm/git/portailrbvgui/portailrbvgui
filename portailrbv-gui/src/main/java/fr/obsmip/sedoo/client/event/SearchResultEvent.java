package fr.obsmip.sedoo.client.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.google.gwt.event.shared.GwtEvent;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public class SearchResultEvent extends GwtEvent<SearchResultEventHandler> {

	public static final Type<SearchResultEventHandler> TYPE = new Type<SearchResultEventHandler>();
	private HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> summaryDTOs;

	public SearchResultEvent(
			HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> summaryDTOs) {
		this.setSummaryDTOs(summaryDTOs);
	}

	@Override
	protected void dispatch(SearchResultEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SearchResultEventHandler> getAssociatedType() {
		return TYPE;
	}

	public HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> getSummaryDTOs() {
		return summaryDTOs;
	}

	public void setSummaryDTOs(
			HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> summaryDTOs) {
		this.summaryDTOs = summaryDTOs;
		initAllSummaryDTOs();
	}

	private ArrayList<SummaryDTO> allSummaryDTOs = new ArrayList<SummaryDTO>();
	
	
	public ArrayList<SummaryDTO> getAllSummaryDTOs() {
		return allSummaryDTOs;
	}
	
	private void initAllSummaryDTOs() {

		this.allSummaryDTOs = new ArrayList<SummaryDTO>();

		Set<GeonetworkObservatoryDTO> keySet = summaryDTOs.keySet();
		for (GeonetworkObservatoryDTO geonetworkObservatoryDTO : keySet) {
			ArrayList<SummaryDTO> aux = summaryDTOs
					.get(geonetworkObservatoryDTO);
			for (SummaryDTO summaryDTO : aux) {
				summaryDTO.setObservatoryName(geonetworkObservatoryDTO
						.getName());
				summaryDTO.setObservatoryColor(geonetworkObservatoryDTO
						.getColor());
				allSummaryDTOs.add(summaryDTO);
			}
		}
		
	}

	public int getTotalResultLength() {
		int result = 0;

		Set<GeonetworkObservatoryDTO> keySet = summaryDTOs.keySet();
		for (GeonetworkObservatoryDTO geonetworkObservatoryDTO : keySet) {
			ArrayList<SummaryDTO> aux = summaryDTOs
					.get(geonetworkObservatoryDTO);
			if (aux != null) {
				result = result + aux.size();
			}
		}
		return result;
	}

}
