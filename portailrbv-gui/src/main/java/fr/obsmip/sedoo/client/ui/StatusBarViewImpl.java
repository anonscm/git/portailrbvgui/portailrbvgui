package fr.obsmip.sedoo.client.ui;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.service.SystemService;
import fr.obsmip.sedoo.client.service.SystemServiceAsync;
import fr.sedoo.commons.client.event.ActionEndEvent;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;

public class StatusBarViewImpl extends AbstractStyledResizeComposite implements StatusBarView {

	private final SystemServiceAsync versionService = GWT.create(SystemService.class);

	@UiField Label message;

	@UiField Element version;

	@UiField Image loading;
	
	LinkedList<String> messages = new LinkedList<String>();

	private static StatusBarViewImplUiBinder uiBinder = GWT
			.create(StatusBarViewImplUiBinder.class);

	interface StatusBarViewImplUiBinder extends UiBinder<Widget, StatusBarViewImpl> {
	}

	public StatusBarViewImpl() {
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		updateDisplay();


		versionService.getApplicationVersion(new AsyncCallback<String>() {

			@Override
			public void onSuccess(String applicationVersion) {
				setVersion(applicationVersion);
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
			}});
	}

	@Override
	public void setVersion(String version) {
		this.version.setInnerText(version);		
	}

	@Override
	public void onNotification(ActionEndEvent event) {

		messages.remove(event.getMessage());
		updateDisplay();
	}

	private void updateDisplay()
	{
		if (messages.isEmpty())
		{
			ElementUtil.hide(loading);
			message.setText(" ");
		}
		else
		{
			ElementUtil.show(loading);
			message.setText(messages.get(ListUtil.FIRST_INDEX));
			System.out.println("Message de progression "+messages.get(ListUtil.FIRST_INDEX));
		}
	}



	@Override
	public void onNotification(ActionStartEvent event) {

		messages.add(event.getMessage());
		updateDisplay();
	}

	@Override
	public double getHeight() {
		return 22;
	}

	@Override
	public void onNotification(ActivityStartEvent event) {
		reset();
		
	}

	private void reset() {
		messages.clear();
		updateDisplay();
		
	}

}



