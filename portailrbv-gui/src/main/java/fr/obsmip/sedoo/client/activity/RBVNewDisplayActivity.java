package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.news.activity.NewDisplayActivity;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVNewDisplayActivity extends NewDisplayActivity{

	public RBVNewDisplayActivity(NewDisplayPlace place,
			NewsClientFactory clientFactory) {
		super(place, clientFactory);
	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		PortailRBV.getClientFactory().getEventBus().fireEvent(new ActivityChangeEvent(""));
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
	//	shortcuts.add(ShortcutFactory
	//			.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		shortcuts.add(ShortcutFactory.getNewsDisplayShortcut());
		PortailRBV.getClientFactory().getBreadCrumb().refresh(shortcuts);
		PortailRBV.getClientFactory().getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
		
		
		super.start(containerWidget, eventBus);
		
		/*PortailRBV.getClientFactory().getEventBus().fireEvent(new ActivityChangeEvent(NewsMessages.INSTANCE.newsDisplayTitle()));
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getNewsDisplayShortcut());
		PortailRBV.getClientFactory().getBreadCrumb().refresh(shortcuts);
		PortailRBV.getClientFactory().getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));*/
	}
	

	@Override
	public void back() {
		PortailRBV.getClientFactory().getPlaceController().goTo(new NewsArchivePlace());
	}

}
