package fr.obsmip.sedoo.client.ui.search;

import static com.google.gwt.query.client.GQuery.$;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.ClickFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.SelectFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.UnselectFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeatureOptions;
import org.gwtopenmaps.openlayers.client.event.MapMoveEndListener;
import org.gwtopenmaps.openlayers.client.event.MapZoomListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.popup.FramedCloud;
import org.gwtopenmaps.openlayers.client.popup.Popup;
import org.gwtopenmaps.openlayers.client.util.Attributes;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.WelcomeView.Presenter;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.obsmip.sedoo.shared.domain.geographicalextent.GeographicalBoxDTO;
import fr.obsmip.sedoo.shared.domain.geographicalextent.GeographicalExtentDTO;
import fr.obsmip.sedoo.shared.domain.geographicalextent.GeographicalExtentListDTO;
import fr.obsmip.sedoo.shared.util.HierachyLevelUtil;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MaximizeEventHandler;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.event.MinimizeEventHandler;
import fr.sedoo.commons.client.util.UuidUtil;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.client.widget.map.impl.FeatureUtil;
import fr.sedoo.commons.client.widget.map.impl.MapConstants;
import fr.sedoo.commons.client.widget.map.impl.RectangularFeatureUtil;
import fr.sedoo.commons.client.widget.map.impl.RectangularSelectionAdder;
import fr.sedoo.commons.util.StringUtil;

public class SearchMapSelector extends AreaSelectorWidget implements
		MinimizeEventHandler, MaximizeEventHandler {

	private static final int MARKER_Y_OFFSET = 13;
	private static final String UUID = "UUID";
	private static final String LEVEL = "LEVEL";
	private static final String TITLE = "TITLE";
	private static final String SHORTNAME = "SHORTNAME";
	private static final String OBSERVATORY_NAME = "OBSERVATORY_NAME";
	private static final String OBSERVATORY_COLOR = "OBSERVATORY_COLOR";

	private SelectFeature hoverExtentFeature;
	private SearchMapAdder searchMapAdder = new SearchMapAdder();
	private Popup popup;
	private Presenter presenter;
	private String level;

	public final static int GRAPHIC_OFFSET_X = -10;
	public final static int GRAPHIC_OFFSET_Y = -34;
	public final static int GRAPHIC_WIDTH = 20;
	public final static int GRAPHIC_HEIGHT = 34;

	private String uuid;
	private Style EXPERIMENTAL_SITE_STYLE;

	private HashMap<String, Style> markerStyles = new HashMap<>();

	public SearchMapSelector(String mapLayer) {
		super(mapLayer);
		initFeatureLayer();

		EXPERIMENTAL_SITE_STYLE = new Style();
		EXPERIMENTAL_SITE_STYLE.setFillOpacity(1.0);
		EXPERIMENTAL_SITE_STYLE
				.setExternalGraphic("http://www.aeris-data.fr/portail/rest/image/storedById/555");
		EXPERIMENTAL_SITE_STYLE.setGraphicSize(GRAPHIC_WIDTH, GRAPHIC_HEIGHT);
		EXPERIMENTAL_SITE_STYLE.setGraphicOffset(GRAPHIC_OFFSET_X,
				GRAPHIC_OFFSET_Y);
		EXPERIMENTAL_SITE_STYLE.setCursor("pointer");

		EXPERIMENTAL_SITE_STYLE = new Style();
		EXPERIMENTAL_SITE_STYLE.setFillOpacity(1.0);
		EXPERIMENTAL_SITE_STYLE
				.setExternalGraphic("http://www.aeris-data.fr/portail/rest/image/storedById/555");
		EXPERIMENTAL_SITE_STYLE.setGraphicSize(GRAPHIC_WIDTH, GRAPHIC_HEIGHT);
		EXPERIMENTAL_SITE_STYLE.setGraphicOffset(GRAPHIC_OFFSET_X,
				GRAPHIC_OFFSET_Y);

		getMap().addMapZoomListener(new MapZoomListener() {

			@Override
			public void onMapZoom(MapZoomEvent eventObject) {
				activateMarkers();
			}
		});

		getMap().addMapMoveEndListener(new MapMoveEndListener() {

			@Override
			public void onMapMoveEnd(MapMoveEndEvent eventObject) {
				activateMarkers();

			}
		});

		PortailRBV.getClientFactory().getEventBus()
				.addHandler(MaximizeEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(MinimizeEvent.TYPE, this);

	}

	@Override
	public void reset() {
		super.reset();
		removePopup();
		destroyAllFeatures();

	}

	public void resetSearchResult() {
		removePopup();
		clearExtents();
	}

	private void clearExtents() {
		deleteAllFeatureByValue(SEDOO_TYPE, SearchMapAdder.SEARCH_MAP_ADDER, "");
	}

	protected void initFeatureLayer() {

		if (hoverExtentFeature != null) {
			map.removeControl(hoverExtentFeature);
		}

		SelectFeatureOptions hoverExtentOptions = new SelectFeatureOptions();
		hoverExtentOptions.setHover();
		hoverExtentOptions.onSelect(new SelectFeatureListener() {

			@Override
			public void onFeatureSelected(VectorFeature vectorFeature) {
				if (FeatureUtil.hasValue(vectorFeature, SEDOO_TYPE,
						RectangularSelectionAdder.SEDOO_RECTANGULAR_SELECTION)) {
					if (hoverExtentFeature != null) {
						// // On ne selectionne pas les éléments de sélection
						// avec
						// // ce controle
						hoverExtentFeature.unSelect(vectorFeature);
					}
				} else {
					if (vectorFeature.getStyle() != null) {
						vectorFeature.getStyle().setCursor("pointer");
						vectorFeature.redrawParent();
					}
				}
			}
		});

		hoverExtentOptions.onUnSelect(new UnselectFeatureListener() {

			@Override
			public void onFeatureUnselected(VectorFeature vectorFeature) {
				if (vectorFeature.getStyle() != null) {
					vectorFeature.getStyle().setCursor("default");
					vectorFeature.redrawParent();
				}
			}
		});

		hoverExtentOptions.clickFeature(new ExtentClickListener());
		hoverExtentFeature = new SelectFeature(getFeatureLayer(),
				hoverExtentOptions);
		hoverExtentOptions.setHover();
		map.addControl(hoverExtentFeature);
		hoverExtentFeature.activate();

	}

	private class ExtentClickListener implements ClickFeatureListener {

		@Override
		public void onFeatureClicked(VectorFeature vectorFeature) {
			showPopup(vectorFeature);

		}
	}

	public void addSearchResult(
			HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> results) {
		// TODO : vider les resultats précédents
		// List<SummaryDTO> aux = summaryDTOs.subList(0, 1);
		
		//Remplacé
		//Bounds bounds = new Bounds();
		Set<GeonetworkObservatoryDTO> observatories = results.keySet();
		int index = 0;
		for (GeonetworkObservatoryDTO observatory : observatories) {

			ArrayList<SummaryDTO> aux = results.get(observatory);

			for (final SummaryDTO summaryDTO : aux) {
				index++;
				uuid = summaryDTO.getUuid();
				//PortailRBV.consoleLog("index :" + index);
				//PortailRBV.consoleLog("uuid : " + uuid);

				//PortailRBV.consoleLog("titre : "
				//		+ summaryDTO.getResourceTitle());
				level = StringUtil.trimToEmpty(summaryDTO
						.getHierarchyLevelName());
				String levelLabel = "";
				if (HierachyLevelUtil.isExperimentalSite(level)) {
					levelLabel = Message.INSTANCE.experimentalSite()
							.toUpperCase();
				} else if (HierachyLevelUtil.isObservatory(level)) {
					levelLabel = Message.INSTANCE.observatory().toUpperCase();
				} else {
					levelLabel = Message.INSTANCE.dataSet().toUpperCase();
				}
				GeographicalExtentListDTO geographicalExtentListDTO = summaryDTO
						.getGeographicalExtentListDTO();
				for (GeographicalExtentDTO geographicalExtentDTO : geographicalExtentListDTO) {
					if (geographicalExtentDTO instanceof GeographicalBoxDTO) {

						GeographicalBoxDTO box = (GeographicalBoxDTO) geographicalExtentDTO;
						VectorFeature feature = null;
						if (box.isPoint()) {

							feature = RectangularFeatureUtil
									.getFeatureFromBox(
											((GeographicalBoxDTO) geographicalExtentDTO)
													.toBoundingBoxDTO(), this
													.getMap());
							LonLat centerLonLat = feature.getGeometry()
									.getBounds().getCenterLonLat();
							Point p = new Point(centerLonLat.lon(),
									centerLonLat.lat());
							feature = new VectorFeature(p,
									getMarkerFromColor(observatory.getColor()));
							feature.setFeatureId(UuidUtil.uuid());
							FeatureUtil.setAttribute(feature, TITLE,
									summaryDTO.getResourceTitle());
							FeatureUtil.setAttribute(feature, SHORTNAME,
									summaryDTO.getResourceAbstract());
							FeatureUtil.setAttribute(feature,
									OBSERVATORY_COLOR, observatory.getColor());
							FeatureUtil.setAttribute(feature, OBSERVATORY_NAME,
									observatory.getName());

							FeatureUtil
									.setAttribute(feature, LEVEL, levelLabel);
							FeatureUtil.setAttribute(feature, UUID, uuid);

						} else {
							feature = RectangularFeatureUtil
									.getFeatureFromBox(
											((GeographicalBoxDTO) geographicalExtentDTO)
													.toBoundingBoxDTO(), this
													.getMap());
							//bounds.extend(feature.getGeometry().getBounds());
							feature.setFeatureId(UuidUtil.uuid());
							Style style = new Style();
							style.setFillOpacity(0.3);
							style.setStrokeColor(observatory.getColor());
							style.setStrokeWidth(1);
							style.setCursor("");
							style.setFillColor(observatory.getColor());
							feature.setStyle(style);

							FeatureUtil.setAttribute(feature, TITLE,
									summaryDTO.getResourceTitle());
							FeatureUtil.setAttribute(feature, SHORTNAME,
									summaryDTO.getResourceAbstract());

							FeatureUtil.setAttribute(feature, UUID, uuid);
							FeatureUtil
									.setAttribute(feature, LEVEL, levelLabel);
							FeatureUtil.setAttribute(feature,
									OBSERVATORY_COLOR, observatory.getColor());
							FeatureUtil.setAttribute(feature, OBSERVATORY_NAME,
									observatory.getName());

							// datasetLink = new
							// Label(Message.INSTANCE.dataSet());
							// datasetLink.addStyleName("rightPadding5px");
							// datasetLink.setTitle(Message.INSTANCE.dataSet());
							//
							// experimentalSiteLink = new Label(
							// Message.INSTANCE.experimentalSite());
							// experimentalSiteLink
							// .addStyleName("rightPadding5px");
							// experimentalSiteLink.setTitle(Message.INSTANCE
							// .experimentalSite());
							//
							// observatoryLink = new Label(
							// Message.INSTANCE.observatory());
							// observatoryLink.addStyleName("rightPadding5px");
							// observatoryLink.setTitle(Message.INSTANCE
							// .observatory());

							//if (level.compareToIgnoreCase(HierachyLevelUtil.EXPERIMENTAL_SITE_HIERARCHY_LEVEL_NAME) == 0) {

								LonLat centerLonLat = feature.getGeometry()
										.getBounds().getCenterLonLat();
								Point p = new Point(centerLonLat.lon(),
										centerLonLat.lat());
								VectorFeature experimentalSiteMarker = new VectorFeature(
										p,
										getMarkerFromColor(observatory
												.getColor()));
								experimentalSiteMarker.setFeatureId(UuidUtil
										.uuid());
								addFeature(experimentalSiteMarker,
										searchMapAdder);
								FeatureUtil.setAttribute(
										experimentalSiteMarker, TITLE,
										summaryDTO.getResourceTitle());
								FeatureUtil.setAttribute(
										experimentalSiteMarker, SHORTNAME,
										summaryDTO.getResourceAbstract());

								FeatureUtil.setAttribute(
										experimentalSiteMarker, UUID, uuid);
								FeatureUtil.setAttribute(
										experimentalSiteMarker, LEVEL,
										levelLabel);
								FeatureUtil.setAttribute(
										experimentalSiteMarker,
										OBSERVATORY_COLOR,
										observatory.getColor());
								FeatureUtil
										.setAttribute(experimentalSiteMarker,
												OBSERVATORY_NAME,
												observatory.getName());
							//}
						}

						addFeature(feature, searchMapAdder);
						if (getFeatureLayer() != null) {
							getFeatureLayer().redraw();
						}
						activateDragControl();
						hoverExtentFeature.activate();

					}
				}

				// For Polygon :
				// http://stackoverflow.com/questions/13939308/openlayers-how-do-i-draw-a-multipolygon
			}
		}
		if (results.isEmpty() == false) {
			//getMap().zoomToExtent(bounds);
			getMap().zoomToExtent(getFeatureLayer().getDataExtent());
		}
		getFeatureLayer().redraw();
		activateMarkers();
	}

	private void activateMarkers() {
		$("text[pointer-events='visible']").css("cursor", "pointer");

	}

	private Style getMarkerFromColor(String observatory) {
		Style result = markerStyles.get(observatory);
		if (result == null) {
			char c = '\uf041';
			result = new Style();
			result.setLabel("" + c);
			result.setFontFamily("FontAwesome");
			result.setFontSize("28");
			result.setFontColor(observatory);
			result.setLabelAlign("cm");
			result.setFontWeight("normal");
			result.setLabelSelect(true);
			result.setLabelOutlineWidth(2);
			result.setLabelOutlineColor("black");
			result.setLabelYOffset(MARKER_Y_OFFSET);
			result.setStrokeOpacity(0);
			result.setFill(false);
			result.setCursor("pointer");
			markerStyles.put(observatory, result);
		}
		return result;

	}

	public void removePopup() {
		if (popup != null) {
			getMap().removePopup(popup);
			popup.destroy();
			popup = null;
		}
	}

	private void showPopup(VectorFeature vectorFeature) {

		if (StringUtil.trimToEmpty(
				vectorFeature.getAttributes().getAttributeAsString(
						MapConstants.SEDOO_TYPE)).compareToIgnoreCase(
				SearchMapAdder.SEARCH_MAP_ADDER) != 0) {
			return;
		}
		if (popup != null) {
			getMap().removePopup(popup);
			vectorFeature.redrawParent();
		}

		popup = new FramedCloud("id1", vectorFeature.getCenterLonLat(), null,
				getPopupContent(vectorFeature), null, true);
		popup.setPanMapIfOutOfView(true); // this set the popup in a
		// strategic way, and pans the
		// map if needed.
		popup.setAutoSize(true);
		vectorFeature.setPopup(popup);

		// And attach the popup to the map
		getMap().addPopup(vectorFeature.getPopup());

		Element elementById = DOM
				.getElementById(getUuidFromVector(vectorFeature));
		Event.sinkEvents(elementById, Event.ONCLICK);
		Event.setEventListener(elementById, new EventListener() {

			@Override
			public void onBrowserEvent(Event event) {
				if (Event.ONCLICK == event.getTypeInt()) {

					Element target = event.getEventTarget().cast();
					String id = target.getAttribute("id");
					presenter = PortailRBV.getClientFactory().getWelcomeView()
							.getPresenter();
					presenter.displayEntryMetadata(id, level);
				}
			}
		});

	}

	private String getUuidFromVector(VectorFeature vectorFeature) {
		Attributes atributes = vectorFeature.getAttributes();
		return atributes.getAttributeAsString(UUID);
	}

	private String getPopupContent(VectorFeature vectorFeature) {

		StringBuilder sb = new StringBuilder();
		sb.append("<b><span class=\"green-small-button\">"
				+ vectorFeature.getAttributes().getAttributeAsString(LEVEL)
				+ "</span></b>");
		sb.append(" <b><span class=\"green-small-button\" style=\"background-color:"
				+ vectorFeature.getAttributes().getAttributeAsString(
						OBSERVATORY_COLOR)
				+ "\">"
				+ vectorFeature.getAttributes().getAttributeAsString(
						OBSERVATORY_NAME) + "</span></b>");
		sb.append("<br><b><br>"
				+ vectorFeature.getAttributes().getAttributeAsString(TITLE)
				+ "</b>");
		sb.append("<br>"
				+ vectorFeature.getAttributes().getAttributeAsString(SHORTNAME));
		sb.append("<br><a id=\"" + getUuidFromVector(vectorFeature) + "\">"
				+ Message.INSTANCE.details() + "</a>");

		return sb.toString();

	}

	@Override
	public void onNotification(MaximizeEvent event) {
		getMapWidget().setHeight("700px");
		getMapWidget().setWidth("1024px");
	}

	@Override
	public void onNotification(MinimizeEvent event) {
		getMapWidget().setHeight("400px");
		getMapWidget().setWidth("512px");

	}

}
