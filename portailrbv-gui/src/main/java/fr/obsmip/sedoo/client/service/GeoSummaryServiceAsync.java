package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.AbstractGeoSummaryDTO;

public interface GeoSummaryServiceAsync {

	void getGeoSummaries(String observatoryName, ArrayList<String> languages,
			AsyncCallback<ArrayList<? extends AbstractGeoSummaryDTO>> callback);

}
