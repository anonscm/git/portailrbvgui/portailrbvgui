package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.Iterator;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.CellTableResources;
import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.ui.table.ExperimentalSiteEntrySummaryTable;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ExperimentalSiteEntryManagementViewImpl extends AbstractSection implements ExperimentalSiteEntryManagementView
{

	private static ExprimentalSiteManagementViewImplUiBinder uiBinder = GWT.create(ExprimentalSiteManagementViewImplUiBinder.class);

	interface ExprimentalSiteManagementViewImplUiBinder extends UiBinder<Widget, ExperimentalSiteEntryManagementViewImpl>
	{
	}

	@UiField
	ExperimentalSiteEntrySummaryTable summaryTable;

	@UiField
	Alert noObservatoryCreated;

	@UiField
	Alert noExperimentalSiteCreated;

	@UiField
	VerticalPanel tablePanel;

	@UiField
	HorizontalPanel observatoryPanel;

	@UiField
	ListBox observatories;

	protected Image deleteImage = new Image(GlobalBundle.INSTANCE.delete());
	protected Image editImage = new Image(GlobalBundle.INSTANCE.edit());

	private Presenter presenter;

	public ExperimentalSiteEntryManagementViewImpl()
	{
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		CellTableResources.INSTANCE.cellTableStyle().ensureInjected();
		summaryTable.init(new ArrayList<HasIdentifier>());
		summaryTable.hideToolBar();
	}


	@Override
	public void setPresenter(Presenter presenter)
	{
		this.presenter = presenter;
		summaryTable.setPresenter(presenter);
	}

	/*
	 * We switch the visibility of the alert message if the table is empty
	 */
	private void ensureAlertVisibility() {
		ElementUtil.show(tablePanel);
		if (summaryTable.getModel().size()==0)
		{
			ElementUtil.hide(summaryTable);
			ElementUtil.show(noExperimentalSiteCreated);
		}
		else
		{
			ElementUtil.show(summaryTable);
			ElementUtil.hide(noExperimentalSiteCreated);
		}
	}

	@Override
	public void broadcastExperimentalSiteDeletion(String uuid) {
		summaryTable.removeRow(uuid);
		ensureAlertVisibility();
	}


	@Override
	public void reset() {
		summaryTable.init(new ArrayList<HasIdentifier>());
		observatories.clear();
		ElementUtil.hide(observatoryPanel);
		ElementUtil.hide(tablePanel);
		ElementUtil.hide(noObservatoryCreated);
		ElementUtil.hide(noExperimentalSiteCreated);
	}

	@Override
	public void setObservatories(ArrayList<ObservatorySummaryDTO> observatoryList) {
		if (observatoryList.isEmpty() == false)
		{
			observatories.clear();
			Iterator<ObservatorySummaryDTO> iterator = observatoryList.iterator();
			while (iterator.hasNext()) {
				MetadataSummaryDTO current = iterator.next();
				observatories.addItem(current.getName(), current.getUuid());
			}
			ElementUtil.hide(noObservatoryCreated);
			ElementUtil.show(observatoryPanel);

			//We load the experimental sites
			ObservatorySummaryDTO observatorySummaryDTO = observatoryList.get(ListUtil.FIRST_INDEX);
			loadExperimentalSites(observatorySummaryDTO.getUuid());
		}
		else
		{
			observatories.clear();
			ElementUtil.show(noObservatoryCreated);
			ElementUtil.hide(observatoryPanel);
		}
	}

	private void loadExperimentalSites(String uuid) 
	{
		ElementUtil.hide(tablePanel);
		presenter.loadExperimentalSites(uuid);
	}

	@UiHandler("observatories")
	void onObservatoriesChanged(ChangeEvent event)
	{
		if (observatories.getItemCount()>0)
		{
			loadExperimentalSites(observatories.getValue(observatories.getSelectedIndex()));
		}
		else
		{
			ElementUtil.hide(tablePanel);
		}
	}

	@UiHandler("observatoryEntryCreationLink")
	void onObservatoryEntryCreationLinkClicked(ClickEvent event) 
	{
		presenter.goToObservatoryEntryCreationPlace();
	}
	
	@UiHandler("experimentalSiteEntryCreationLink")
	void onExperimentalSiteEntryCreationLinkClicked(ClickEvent event) 
	{
		presenter.goToExperimentalSiteEntryCreationPlace(observatories.getItemText(observatories.getSelectedIndex()));
	}


	@Override
	public void setExperimentalSites(
			ArrayList<ExperimentalSiteSummaryDTO> experimentalSites) {
		summaryTable.init(experimentalSites);
		ensureAlertVisibility();
	}

}
