package fr.obsmip.sedoo.client.ui.geonetworkobservatory;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.crud.widget.CrudConfirmCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.UpperCaseTextBox;
import fr.sedoo.commons.client.widget.dialog.DialogBoxContent;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;

public class GeonetworkObservatoryEditDialogContent extends EditDialogContent
		implements DialogBoxContent {

	private static AdministratorEditDialogContentUiBinder uiBinder = GWT
			.create(AdministratorEditDialogContentUiBinder.class);

	interface AdministratorEditDialogContentUiBinder extends
			UiBinder<Widget, GeonetworkObservatoryEditDialogContent> {
	}

	Long id;

	@UiField
	UpperCaseTextBox name;

	@UiField
	TextBox color;

	public GeonetworkObservatoryEditDialogContent(
			CrudConfirmCallBack confirmCallback,
			GeonetworkObservatoryDTO observatory) {
		super(confirmCallback);
		initWidget(uiBinder.createAndBindUi(this));
		edit(observatory);
	}

	private void edit(GeonetworkObservatoryDTO observatory) {
		id = observatory.getId();
		name.setText(StringUtil.trimToEmpty(observatory.getName()));
		color.setText(StringUtil.trimToEmpty(observatory.getColor()));
	}

	@Override
	public GeonetworkObservatoryDTO flush() {
		GeonetworkObservatoryDTO aux = new GeonetworkObservatoryDTO();
		aux.setId(id);
		aux.setName(name.getText());
		aux.setColor(color.getText());
		return aux;
	}

	@Override
	public String getPreferredHeight() {
		return "70px";
	}

}
