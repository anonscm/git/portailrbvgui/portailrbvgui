package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class DatasetEntryManagementPlace extends Place implements AuthenticatedPlace
{

	public static DatasetEntryManagementPlace instance;

	public static class Tokenizer implements PlaceTokenizer<DatasetEntryManagementPlace>
	{
		@Override
		public String getToken(DatasetEntryManagementPlace place)
		{
			return "";
		}

		@Override
		public DatasetEntryManagementPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new DatasetEntryManagementPlace();
			}
			return instance;
		}
	}
	
}
