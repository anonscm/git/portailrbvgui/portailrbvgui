package fr.obsmip.sedoo.client.ui.map;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.resources.client.ImageResource;

import fr.obsmip.sedoo.client.marker.MarkerBundle;

public class MarkerConstants {
	
	public static List<MarkerDefinition> markerDefinitions = new ArrayList<MarkerDefinition>();
	static
	{
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetPurpleMarker()), url(MarkerBundle.INSTANCE.experimentalSitePurpleMarker()), "#c89bff"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetYellowMarker()), url(MarkerBundle.INSTANCE.experimentalSiteYellowMarker()), "#ffed5c"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetRedMarker()), url(MarkerBundle.INSTANCE.experimentalSiteRedMarker()), "#ff776b"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetBlueMarker()), url(MarkerBundle.INSTANCE.experimentalSiteBlueMarker()), "#6b98ff"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetOrangeMarker()), url(MarkerBundle.INSTANCE.experimentalSiteOrangeMarker()), "#fd8d08"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetGreenMarker()), url(MarkerBundle.INSTANCE.experimentalSiteGreenMarker()), "#97ec7d"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetPinkMarker()), url(MarkerBundle.INSTANCE.experimentalSitePinkMarker()), "#fdabff"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetPaleBlueMarker()), url(MarkerBundle.INSTANCE.experimentalSitePaleBlueMarker()), "#bce3ff"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetDarkgreenMarker()), url(MarkerBundle.INSTANCE.experimentalSiteDarkgreenMarker()), "#01bf00"));		
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetBrownMarker()), url(MarkerBundle.INSTANCE.experimentalSiteBrownMarker()), "#cb9d7c"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetTurquoiseMarker()), url(MarkerBundle.INSTANCE.experimentalSiteTurquoiseMarker()), "#3e999a"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetDarkpinkMarker()), url(MarkerBundle.INSTANCE.experimentalSiteDarkpinkMarker()), "#940094"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetGreyMarker()), url(MarkerBundle.INSTANCE.experimentalSiteGreyMarker()), "#9a9a9a"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetFleshMarker()), url(MarkerBundle.INSTANCE.experimentalSiteFleshMarker()), "#fed8be"));
		markerDefinitions.add(new MarkerDefinition(url(MarkerBundle.INSTANCE.dataSetLightBlueMarker()), url(MarkerBundle.INSTANCE.experimentalSiteLightBlueMarker()), "#09c6fd"));
	}
	
	private static String url(ImageResource resource) {
		return resource.getSafeUri().asString();
	}
}
