package fr.obsmip.sedoo.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;

public class BlankViewImpl extends AbstractSection implements BlankView {

	
	private static BlankViewImplUiBinder uiBinder = GWT
			.create(BlankViewImplUiBinder.class);

	interface BlankViewImplUiBinder extends UiBinder<Widget, BlankViewImpl> {
	}

	public BlankViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

}
