package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.CellTableResources;
import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.ui.table.ObservatoryPersonTable;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public class DirectoryManagementViewImpl extends AbstractSection implements DirectoryManagementView
{

	private static ExprimentalSiteManagementViewImplUiBinder uiBinder = GWT.create(ExprimentalSiteManagementViewImplUiBinder.class);

	interface ExprimentalSiteManagementViewImplUiBinder extends UiBinder<Widget, DirectoryManagementViewImpl>
	{
	}

	@UiField
	ObservatoryPersonTable personTable;

	@UiField
	Alert noObservatoryCreated;

	@UiField
	VerticalPanel tablePanel;

	@UiField
	HorizontalPanel observatoryPanel;

	@UiField
	ListBox observatories;

	protected Image deleteImage = new Image(GlobalBundle.INSTANCE.delete());
	protected Image editImage = new Image(GlobalBundle.INSTANCE.edit());

	private Presenter presenter;

	public DirectoryManagementViewImpl()
	{
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		CellTableResources.INSTANCE.cellTableStyle().ensureInjected();
		personTable.init(new ArrayList<HasIdentifier>());
	}


	@Override
	public void setPresenter(Presenter presenter)
	{
		this.presenter = presenter;
		personTable.setPresenter(presenter);
	}

	@Override
	public void broadcastPersonDeletion(String id) {
		personTable.removeRow(id);
	}


	@Override
	public void reset() {
		personTable.init(new ArrayList<HasIdentifier>());
		observatories.clear();
		ElementUtil.hide(observatoryPanel);
		ElementUtil.hide(tablePanel);
		ElementUtil.hide(noObservatoryCreated);
	}

	@Override
	public void setObservatories(List<GeonetworkObservatoryDTO> observatoryList) {
		if (observatoryList.isEmpty() == false)
		{
			observatories.clear();
			Iterator<GeonetworkObservatoryDTO> iterator = observatoryList.iterator();
			while (iterator.hasNext()) {
				GeonetworkObservatoryDTO current = iterator.next();
				observatories.addItem(current.getName(), current.getIdentifier());
			}
			ElementUtil.hide(noObservatoryCreated);
			ElementUtil.show(observatoryPanel);

			//We load the experimental sites
			loadPersons(observatoryList.get(ListUtil.FIRST_INDEX).getIdentifier());
		}
		else
		{
			observatories.clear();
			ElementUtil.show(noObservatoryCreated);
			ElementUtil.hide(observatoryPanel);
		}
	}


	@Override
	public void setPersons(ArrayList<PersonDTO> persons) 
	{
		personTable.init(persons);
		personTable.setObservatoryId(observatories.getValue(observatories.getSelectedIndex()));
		ElementUtil.show(tablePanel);
	}

	
	@UiHandler("observatories")
	void onObservatoriesChanged(ChangeEvent event)
	{
		if (observatories.getItemCount()>0)
		{
			loadPersons(observatories.getValue(observatories.getSelectedIndex()));
		}
		else
		{
			ElementUtil.hide(tablePanel);
		}
	}
	
	private void loadPersons(String uuid) 
	{
		ElementUtil.hide(tablePanel);
		presenter.loadPersons(uuid);
	}

//	@UiHandler("observatoryEntryCreationLink")
//	void onObservatoryEntryCreationLinkClicked(ClickEvent event) 
//	{
//		presenter.goToObservatoryEntryCreationPlace();
//	}
//	
//	@UiHandler("experimentalSiteEntryCreationLink")
//	void onExperimentalSiteEntryCreationLinkClicked(ClickEvent event) 
//	{
//		presenter.goToExperimentalSiteEntryCreationPlace(observatories.getItemText(observatories.getSelectedIndex()));
//	}


//	@Override
//	public void setExperimentalSites(
//			ArrayList<ExperimentalSiteSummaryDTO> experimentalSites) {
//		personTable.init(experimentalSites);
//		ensureAlertVisibility();
//	}

}
