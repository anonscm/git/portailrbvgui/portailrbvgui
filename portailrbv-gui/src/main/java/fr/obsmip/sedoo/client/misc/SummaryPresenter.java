package fr.obsmip.sedoo.client.misc;

public interface SummaryPresenter {

	void displayObservatoryEntryMetadata(String uuid);

	void displayExperimentalSiteEntryMetadata(String uuid);
	
	void displayDatasetEntryMetadata(String uuid);
}
