package fr.obsmip.sedoo.client.ui.table.user;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.crud.widget.CreateConfirmCallBack;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.client.crud.widget.EditConfirmCallBack;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.commons.client.widget.table.multiline.MultilineRenderer;
import fr.sedoo.commons.client.widget.table.multiline.MultilineTextColumn;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class UserTable extends CrudTable{

	private ArrayList<? extends IsObservatory> observatories;

	public UserTable() {
		super();
		setAddButtonEnabled(true);
	}

	
	@Override
	public String getAddItemText() {
		return Message.INSTANCE.userTableAddItemText();
	}

	protected void initColumns() 
	{
		
		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				RBVUserDTO user = (RBVUserDTO) aux;
				if (user.isExternalAuthentication())
				{
					return "-";
				}
				else
				{
					return user.getName();
				}
			}
		};

		TextColumn<HasIdentifier> loginColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				RBVUserDTO user = (RBVUserDTO) aux;
				return user.getLogin();
			}
		};

		TextColumn<HasIdentifier> passwordColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) 
			{
				RBVUserDTO user = (RBVUserDTO) aux;
				if (user.isExternalAuthentication())
				{
					return "-";
				}
				else
				{
					return user.getPassword();
				}
			}
		};
		
		
		TextColumn<HasIdentifier> administratorColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				RBVUserDTO user = (RBVUserDTO) aux;
				if (user.isAdmin())
				{
					return Message.INSTANCE.administrator();
				}
				else
				{
					return Message.INSTANCE.dataProvider();
				}
			}
		};
		
		TextColumn<HasIdentifier> externalAuthenticationColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				RBVUserDTO user = (RBVUserDTO) aux;
				if (user.isExternalAuthentication())
				{
					return CommonMessages.INSTANCE.yes();
				}
				else
				{
					return CommonMessages.INSTANCE.no();
				}
			}
		};
		
		MultilineTextColumn<HasIdentifier> observatoryColumn = new MultilineTextColumn<HasIdentifier>(new MultilineRenderer(SeparatorUtil.AROBAS_SEPARATOR)) {
			@Override
			public String getValue(HasIdentifier aux) {
				RBVUserDTO user = (RBVUserDTO) aux;
				if (user.isAdmin())
				{
					return "-";
				}
				else
				{
					return user.getObservatories();
				}
			}
		};

		table.addColumn(loginColumn, CommonMessages.INSTANCE.login());
		table.setColumnWidth(loginColumn, 100.0, Unit.PX);
		table.addColumn(externalAuthenticationColumn, CommonMessages.INSTANCE.externalAuthentication());
		table.setColumnWidth(externalAuthenticationColumn, 20.0, Unit.PX);
		table.addColumn(nameColumn, CommonMessages.INSTANCE.name());
		table.setColumnWidth(nameColumn, 70.0, Unit.PX);
		table.addColumn(passwordColumn, CommonMessages.INSTANCE.password());
		table.setColumnWidth(passwordColumn, 70.0, Unit.PX);
		table.addColumn(administratorColumn, Message.INSTANCE.role());
		table.setColumnWidth(administratorColumn, 20.0, Unit.PX);
		table.addColumn(observatoryColumn, Message.INSTANCE.observatories());
		table.setColumnWidth(observatoryColumn, 100.0, Unit.PX);
		table.addColumn(editColumn);
		table.addColumn(deleteColumn);
		table.setColumnWidth(editColumn, 30.0, Unit.PX);
		table.setColumnWidth(deleteColumn, 30.0, Unit.PX);
	}

	@Override
	public String getEditDialogTitle() {
		return Message.INSTANCE.userTableEditDialogTitle();
	}

	@Override
	public String getCreateDialogTitle() {
		return Message.INSTANCE.userTableCreateDialogTitle();
	}
	
	@Override
	public EditDialogContent getEditDialogContent(EditConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new UserEditDialogContent(callBack, (RBVUserDTO) hasIdentifier, observatories);
	}

	@Override
	public EditDialogContent getCreateDialogContent(CreateConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new UserEditDialogContent(callBack, (RBVUserDTO) hasIdentifier, observatories);
	}

	@Override
	public HasIdentifier createNewItem() {
		
		return new RBVUserDTO();
	}

	public void setObservatories(ArrayList<? extends IsObservatory> observatories) 
	{
		this.observatories = observatories;
		
	}
}
