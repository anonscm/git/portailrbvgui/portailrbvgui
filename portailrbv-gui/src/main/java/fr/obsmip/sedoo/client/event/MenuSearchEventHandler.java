package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MenuSearchEventHandler extends EventHandler {
	void onNotification(MenuSearchEvent event);
}
