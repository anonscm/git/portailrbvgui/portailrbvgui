package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.activity.MessageEditActivity;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVMessageEditActivity extends MessageEditActivity{

	public RBVMessageEditActivity(MessageEditPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		PortailRBV.getClientFactory().getEventBus().fireEvent(new ActivityChangeEvent(NewsMessages.INSTANCE.editDialogTitle()));
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		shortcuts.add(ShortcutFactory.getMessageEditShortcut());
		PortailRBV.getClientFactory().getBreadCrumb().refresh(shortcuts);
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser())
		{
			return ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser().isAdmin();
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public void back() {
		super.back();
		PortailRBV.getClientFactory().getPlaceController().goTo(new NewsArchivePlace());
	}
	

}
