package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.ui.searchcriteria.DisclosurePanelCustom;
import fr.obsmip.sedoo.client.ui.searchcriteria.MapCriteriaWidget;

public abstract class AbstractSearchWidget implements MapCriteriaWidget {

	protected DisclosurePanelCustom panel;

	public AbstractSearchWidget(String title) {
		panel = new DisclosurePanelCustom(title);
		panel.setStyleName("customDisclosurePanel");
		panel.setWidth("100%");
		panel.getElement().getStyle().setColor("#000000");
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

}
