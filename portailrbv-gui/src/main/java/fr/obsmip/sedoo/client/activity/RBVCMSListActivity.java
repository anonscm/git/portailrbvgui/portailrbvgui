package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.sedoo.commons.client.cms.activity.CMSListActivity;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVCMSListActivity extends CMSListActivity {

	private CMSListPlace place;

	public RBVCMSListActivity(CMSListPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		this.place = place;
		broadcastActivityTitle("");
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory
				.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));

		shortcuts.add(ShortcutFactory.getCMSListShortcut());

		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
	}

	protected void broadcastActivityTitle(String title) {
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser()) {
			return ((AuthenticatedClientFactory) clientFactory)
					.getUserManager().getUser().isAdmin();
		} else {
			return false;
		}
	}

}
