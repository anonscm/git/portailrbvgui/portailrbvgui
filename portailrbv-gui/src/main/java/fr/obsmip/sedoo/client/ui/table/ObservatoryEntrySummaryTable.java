package fr.obsmip.sedoo.client.ui.table;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryManagementView.Presenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.table.children.AbstractSummaryTable;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ObservatoryEntrySummaryTable extends AbstractSummaryTable {

	private Presenter presenter;

	public ObservatoryEntrySummaryTable() {
		super();
		enableEditMode();
	}

	public void enableEditMode() {
		clearColumns();
		addNameColumn();
		addTitleColumn();
		addEditColumn();
		addDeleteColumn();
	}

	protected void addTitleColumn() {
		TextColumn<HasIdentifier> abstractColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((MetadataSummaryDTO) aux).getDisplayTitle();
			}
		};
		itemTable.addColumn(abstractColumn,
				CommonMessages.INSTANCE.description());
		itemTable.setColumnWidth(abstractColumn, 200.0, Unit.PX);
	}

	@Override
	public void addItem() {
		// Nothing is done via this method
	}

	@Override
	public String getAddItemText() {
		return Message.INSTANCE.observatoryTableAddItem();
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter
				.deleteObservatoryEntry(((MetadataSummaryDTO) hasId).getUuid());
	}

	@Override
	public void presenterEdit(HasIdentifier hasId) {
		presenter.goToEditPlace((MetadataSummaryDTO) hasId);
	}

	@Override
	public String getDeleteItemConfirmationText() {
		return Message.INSTANCE.observatoryDeletionConfirmationMessage();
	}

	@Override
	protected void initColumns() {
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void presenterView(HasIdentifier hasId) {
		// Nothing is done via this method
	}

}
