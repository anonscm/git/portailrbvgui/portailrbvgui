package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ActivityStartEvent extends GwtEvent<ActivityStartEventHandler> {

	public final static String ADMINISTRATOR_ACTIVITY = "ADMINISTRATOR";
	public final static String MEMBER_ACTIVITY = "MEMBER";
	public final static String PUBLIC_ACTIVITY = "DEFAULT_USER";
	public final static String CMS_ACTIVITY = "CMS";

	private String type = PUBLIC_ACTIVITY;

	public ActivityStartEvent(String type) {
		this.type = type;
	}

	public static final Type<ActivityStartEventHandler> TYPE = new Type<ActivityStartEventHandler>();

	@Override
	protected void dispatch(ActivityStartEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ActivityStartEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getType() {
		return type;
	}

}
