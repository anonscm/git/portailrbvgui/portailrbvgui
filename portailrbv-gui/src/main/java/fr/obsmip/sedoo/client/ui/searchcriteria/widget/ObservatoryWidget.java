package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaBar;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.DisclosurePanelCustom;
import fr.obsmip.sedoo.client.ui.searchcriteria.MapCriteriaWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.ObservatoryCatalogsLoadedEvent;
import fr.obsmip.sedoo.client.ui.searchcriteria.ObservatoryCatalogsLoadedEventHandler;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.message.ValidationMessages;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;

public class ObservatoryWidget implements CriteriaWidget, HasValidation,
		MapCriteriaWidget, ClickHandler, ObservatoryCatalogsLoadedEventHandler {

	protected DisclosurePanelCustom panel;
	protected CriteriaBar criteriaBar;
	protected ArrayList<CheckBox> checkBoxes = new ArrayList<CheckBox>();

	public ObservatoryWidget() {
		panel = new DisclosurePanelCustom(Message.INSTANCE.observatories());
		panel.setStyleName("customDisclosurePanel");
		panel.setWidth("100%");
		init();
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(ObservatoryCatalogsLoadedEvent.TYPE, this);
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	public void init() {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(2);
		verticalPanel.add(new Label(CommonMessages.INSTANCE.loading()));
		panel.add(verticalPanel);
		panel.setOpen(true);
	}

	public void selectAll() {
		for (CheckBox checkBox : checkBoxes) {
			checkBox.setValue(true);
		}
	}
	
	public void unselectAll() {
		for (CheckBox checkBox : checkBoxes) {
			checkBox.setValue(false);
		}
	}
	
	@Override
	public void reset() {
		for (CheckBox checkBox : checkBoxes) {
			checkBox.setValue(true);
		}
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		boolean atLeastOneSelected = false;
		for (CheckBox checkBox : checkBoxes) {
			if (checkBox.getValue() == true) {
				atLeastOneSelected = true;
			}
		}
		if (atLeastOneSelected == false) {
			result.add(new ValidationAlert(Message.INSTANCE.observatories(),
					ValidationMessages.INSTANCE.atLeastOneElementNeeded()));
		}
		return result;
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {
		ArrayList<String> observatories = new ArrayList<String>();
		for (CheckBox checkBox : checkBoxes) {
			if (checkBox.getValue() == true) {
				observatories.add(checkBox.getText());
			}
		}
		criteria.setObservatories(observatories);
		return criteria;
	}

	@Override
	public void onClick(ClickEvent event) {
		criteriaBar.validate();
	}

	@Override
	public void onNotification(ObservatoryCatalogsLoadedEvent event) {
		panel.clear();
		ArrayList<GeonetworkObservatoryDTO> observatoryCatalogs = event
				.getobservatoryCatalogs();
		checkBoxes.clear();
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(2);

		for (GeonetworkObservatoryDTO observatoryCatalog : observatoryCatalogs) {
			CheckBox checkBox = new NormalFontCheckBox(
					observatoryCatalog.getName());

			checkBox.setValue(true);
			checkBoxes.add(checkBox);
			checkBox.addClickHandler(this);

			HorizontalPanel panel = new HorizontalPanel();
			panel.setSpacing(2);
			panel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			Label color = new Label("_");
			color.getElement().getStyle().setColor("#ffffff");
			color.getElement().getStyle()
					.setBackgroundColor(observatoryCatalog.getColor());
			panel.add(color);
			color.getElement().getStyle().setMarginLeft(3, Unit.PX);
			color.getElement().getStyle().setMarginRight(3, Unit.PX);
			panel.add(checkBox);

			verticalPanel.add(panel);
		}

		VerticalPanel actionsPanel = new VerticalPanel();
		actionsPanel.setSpacing(2);
		actionsPanel.setWidth("100%");
		actionsPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
		HorizontalPanel actions = new HorizontalPanel();
		Button selectAll = new Button("Select all");
		//selectAll.addStyleName("btn-link btn-block");
		selectAll.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAll();
			}
		});
		Button unselectAll = new Button("Unselect all");
		//unselectAll.addStyleName("btn-link btn-block");
		unselectAll.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				unselectAll();
			}
		});
		
		actions.add(selectAll);
		actions.add(unselectAll);
		actionsPanel.add(actions);
		verticalPanel.add(actionsPanel);
		panel.add(verticalPanel);
		//criteriaBar.validate();
	}
}