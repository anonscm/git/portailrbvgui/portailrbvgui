package fr.obsmip.sedoo.client.ui;

import fr.obsmip.sedoo.client.ui.metadata.common.children.ComputeFromParentPresenter;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.DirectoryPresenter;
import fr.sedoo.metadata.client.ui.view.MetadataEditingView;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public interface ExperimentalSiteEntryView extends MetadataEditingView {
	
	void setPresenter(ExperimentalSiteEntryPresenter presenter);
	
	public interface ExperimentalSiteEntryPresenter extends DirectoryPresenter, EditingPresenter, DisplayPresenter, ComputeFromParentPresenter
	{
		void deleteDataset(String identifier);
		void goToDatasetEditPlace(MetadataSummaryDTO hasId);
		void addDataset();
	}
	
	void broadcastExperimentalSiteDeletion(String uuid);

	void setUseConditions(I18nString result);
	void setPublicAccessLimitations(I18nString result);
	
}
