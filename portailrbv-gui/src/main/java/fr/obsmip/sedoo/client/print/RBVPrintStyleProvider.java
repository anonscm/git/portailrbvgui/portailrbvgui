package fr.obsmip.sedoo.client.print;

import fr.sedoo.commons.client.print.PrintStyleProvider;

public class RBVPrintStyleProvider implements PrintStyleProvider
{

	@Override
	public String getPrintStyle() {
		String aux = "<style type='text/css'>table th {font-size: small;} table td {font-size: small;} .topMargin3px {margin-top: 3px;} .horizontalAlignCenter {text-align: center;} .verticalAlignTop {vertical-align: top;} .metadataLeftColumn {text-align: right;white-space: nowrap;width: 150px;font-weight: bold;padding-right: 5px;} .metadataRowSpacing {border-bottom: 2px solid white;} .observatoryName {font-size: 20px;color: #4a99c6;} .metadataSection {font-size: 17px; color: #4A99C6;} .observatoryDescription {color: #535548; text-align: justify; font-size: 13px; } .title1 { font-family:Arial; font-size: 32px; color: #4a99c6;} table { font-family:Arial; } </style>";
		return aux;
	}

}