package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.client.event.ActivityStartEventHandler;
import fr.sedoo.commons.client.event.ActionEndEventHandler;
import fr.sedoo.commons.client.event.ActionStartEventHandler;

public interface StatusBarView extends IsWidget, ActionEndEventHandler, ActionStartEventHandler, ActivityStartEventHandler {

	void setVersion(String version);

	double getHeight();
}
