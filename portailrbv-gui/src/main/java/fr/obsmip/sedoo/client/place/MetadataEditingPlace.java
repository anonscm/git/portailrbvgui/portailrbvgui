package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.PlaceTokenizer;

import fr.obsmip.sedoo.client.Constants;
import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;
import fr.sedoo.commons.client.util.StringUtil;

public class MetadataEditingPlace extends AbstractEditingPlace implements AuthenticatedPlace
{
	
	public MetadataEditingPlace() {
		super();
	}
	
	public Long getDrainageBasinId() {
		return drainageBasinId;
	}

	public void setDrainageBasinId(Long drainageBasinId) {
		this.drainageBasinId = drainageBasinId;
	}

	public String getMetadataUuid() {
		return metadataUuid;
	}

	public void setMetadataUuid(String metadataUuid) {
		this.metadataUuid = metadataUuid;
	}

	
	
	public String getObservatoryShortLabel() {
		return observatoryShortLabel;
	}

	public void setObservatoryShortLabel(String observatoryShortLabel) {
		this.observatoryShortLabel = observatoryShortLabel;
	}



	private Long drainageBasinId;
	private String metadataUuid;
	private String observatoryShortLabel;
	
	public static MetadataEditingPlace instance;

	public static class Tokenizer implements PlaceTokenizer<MetadataEditingPlace>
	{
		@Override
		public String getToken(MetadataEditingPlace place)
		{
			if (place.getMode().compareTo(Constants.CREATE)==0)
			{
				return place.getMode()+"@"+StringUtil.trimToEmpty(""+place.getDrainageBasinId());
			}
			else
			{
				return place.getMode()+"@"+StringUtil.trimToEmpty(""+place.getObservatoryShortLabel())+"@"+StringUtil.trimToEmpty(""+place.getMetadataUuid());
			}
			
		}

		@Override
		public MetadataEditingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new MetadataEditingPlace();
			}
			String[] split = token.split("@");
			if (split.length>=2)
			{
				instance.setMode(split[0]);
				
				if (instance.getMode().compareTo(Constants.CREATE)==0)
				{
					instance.setDrainageBasinId(new Long(split[1]));
				}
				else
				{
					instance.setObservatoryShortLabel(split[1]);
				}
			}
			if (split.length==3)
			{
				instance.setMetadataUuid(split[2]);
			}
			return instance;
		}
	}

	
	
}
