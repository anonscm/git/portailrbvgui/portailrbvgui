package fr.obsmip.sedoo.client.ui;

import fr.obsmip.sedoo.client.ui.metadata.common.children.ComputeFromParentPresenter;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.DirectoryPresenter;
import fr.sedoo.metadata.client.ui.view.MetadataEditingView;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public interface DatasetEntryView extends MetadataEditingView {
	
	void setPresenter(DatasetEntryPresenter presenter);
	
	public interface DatasetEntryPresenter extends DirectoryPresenter, EditingPresenter, DisplayPresenter,ComputeFromParentPresenter
	{
	}
	void setUseConditions(I18nString result);
	void setPublicAccessLimitations(I18nString result);
	
}
