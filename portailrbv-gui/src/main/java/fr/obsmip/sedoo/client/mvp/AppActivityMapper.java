package fr.obsmip.sedoo.client.mvp;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.activity.DatasetEntryCreationActivity;
import fr.obsmip.sedoo.client.activity.DatasetEntryDisplayingActivity;
import fr.obsmip.sedoo.client.activity.DatasetEntryEditingActivity;
import fr.obsmip.sedoo.client.activity.DatasetEntryManagementActivity;
import fr.obsmip.sedoo.client.activity.DirectoryManagementActivity;
import fr.obsmip.sedoo.client.activity.ExperimentalSiteEntryCreationActivity;
import fr.obsmip.sedoo.client.activity.ExperimentalSiteEntryDisplayingActivity;
import fr.obsmip.sedoo.client.activity.ExperimentalSiteEntryEditingActivity;
import fr.obsmip.sedoo.client.activity.ExperimentalSiteEntryManagementActivity;
import fr.obsmip.sedoo.client.activity.GeoSummaryActivity;
import fr.obsmip.sedoo.client.activity.GeonetworkObservatoryManagementActivity;
import fr.obsmip.sedoo.client.activity.HarvestManagementActivity;
import fr.obsmip.sedoo.client.activity.LogConsultationActivity;
import fr.obsmip.sedoo.client.activity.LoginActivity;
import fr.obsmip.sedoo.client.activity.MetadataResultActivity;
import fr.obsmip.sedoo.client.activity.MetadataSearchActivity;
import fr.obsmip.sedoo.client.activity.ObservatoryEntryCreationActivity;
import fr.obsmip.sedoo.client.activity.ObservatoryEntryDisplayingActivity;
import fr.obsmip.sedoo.client.activity.ObservatoryEntryEditingActivity;
import fr.obsmip.sedoo.client.activity.ObservatoryEntryManagementActivity;
import fr.obsmip.sedoo.client.activity.RBVCMSConsultActivity;
import fr.obsmip.sedoo.client.activity.RBVCMSEditActivity;
import fr.obsmip.sedoo.client.activity.RBVCMSListActivity;
import fr.obsmip.sedoo.client.activity.RBVCMSMenuEditActivity;
import fr.obsmip.sedoo.client.activity.RBVFormatManagementActivity;
import fr.obsmip.sedoo.client.activity.RBVNewDisplayActivity;
import fr.obsmip.sedoo.client.activity.RBVNewsArchiveActivity;
import fr.obsmip.sedoo.client.activity.RBVNewsEditActivity;
import fr.obsmip.sedoo.client.activity.RBVNewsManagementActivity;
import fr.obsmip.sedoo.client.activity.RBVNewsPageActivity;
import fr.obsmip.sedoo.client.activity.SystemActivity;
import fr.obsmip.sedoo.client.activity.UserManagementActivity;
import fr.obsmip.sedoo.client.activity.WelcomeActivity;
import fr.obsmip.sedoo.client.place.DatasetEditingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryManagementPlace;
import fr.obsmip.sedoo.client.place.DirectoryManagementPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryManagementPlace;
import fr.obsmip.sedoo.client.place.GeoSummaryPlace;
import fr.obsmip.sedoo.client.place.GeonetworkObservatoryManagementPlace;
import fr.obsmip.sedoo.client.place.HarvestManagementPlace;
import fr.obsmip.sedoo.client.place.LogConsultationPlace;
import fr.obsmip.sedoo.client.place.MetadataEditingPlace;
import fr.obsmip.sedoo.client.place.MetadataResultPlace;
import fr.obsmip.sedoo.client.place.MetadataSearchPlace;
import fr.obsmip.sedoo.client.place.NewsPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryManagementPlace;
import fr.obsmip.sedoo.client.place.SystemPlace;
import fr.obsmip.sedoo.client.place.UserManagementPlace;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.cms.place.MenuEditPlace;
import fr.sedoo.commons.client.language.activity.SwitchLanguageActivity;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.metadata.client.place.FormatListPlace;

public class AppActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	/**
	 * AppActivityMapper associates each Place with its corresponding
	 * {@link Activity}
	 * 
	 * @param clientFactory
	 *            Factory to be passed to activities
	 */
	public AppActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

	/**
	 * Map each Place to its corresponding Activity. This would be a great use
	 * for GIN.
	 */
	@Override
	public Activity getActivity(Place place) {
		if (place instanceof WelcomePlace) {
			return new WelcomeActivity((WelcomePlace) place, clientFactory);
			// return new GeoSummaryActivity(new GeoSummaryPlace(),
			// clientFactory);
		} else if (place instanceof SystemPlace) {
			return new SystemActivity((SystemPlace) place, clientFactory);
		} else if (place instanceof MetadataEditingPlace) {
			// return new MetadataEditingActivity((MetadataEditingPlace) place,
			// clientFactory);
		} else if (place instanceof LanguageSwitchPlace) {
			return new SwitchLanguageActivity((LanguageSwitchPlace) place,
					clientFactory);
		} else if (place instanceof MetadataSearchPlace) {
			return new MetadataSearchActivity((MetadataSearchPlace) place,
					clientFactory);
		} else if (place instanceof MetadataResultPlace) {
			return new MetadataResultActivity((MetadataResultPlace) place,
					clientFactory);
		} else if (place instanceof LoginPlace) {
			return new LoginActivity((LoginPlace) place, clientFactory);
		} else if (place instanceof ObservatoryEntryManagementPlace) {
			return new ObservatoryEntryManagementActivity(
					(ObservatoryEntryManagementPlace) place, clientFactory);
		} else if (place instanceof ExperimentalSiteEntryManagementPlace) {
			return new ExperimentalSiteEntryManagementActivity(
					(ExperimentalSiteEntryManagementPlace) place, clientFactory);
		} else if (place instanceof ObservatoryEntryCreationPlace) {
			return new ObservatoryEntryCreationActivity(
					(ObservatoryEntryCreationPlace) place, clientFactory);
		} else if (place instanceof ObservatoryEditingPlace) {
			return new ObservatoryEntryEditingActivity(
					(ObservatoryEditingPlace) place, clientFactory);
		} else if (place instanceof ExperimentalSiteEditingPlace) {
			return new ExperimentalSiteEntryEditingActivity(
					(ExperimentalSiteEditingPlace) place, clientFactory);
		} else if (place instanceof FormatListPlace) {
			return new RBVFormatManagementActivity((FormatListPlace) place,
					clientFactory);
		} else if (place instanceof GeoSummaryPlace) {
			return new GeoSummaryActivity((GeoSummaryPlace) place,
					clientFactory);
		} else if (place instanceof UserManagementPlace) {
			return new UserManagementActivity((UserManagementPlace) place,
					clientFactory);
		}else if (place instanceof LogConsultationPlace) {
			return new LogConsultationActivity((LogConsultationPlace) place,
					clientFactory);
		} else if (place instanceof GeonetworkObservatoryManagementPlace) {
			return new GeonetworkObservatoryManagementActivity(
					(GeonetworkObservatoryManagementPlace) place, clientFactory);
		} else if (place instanceof ExperimentalSiteEntryCreationPlace) {
			return new ExperimentalSiteEntryCreationActivity(
					(ExperimentalSiteEntryCreationPlace) place, clientFactory);
		} else if (place instanceof ObservatoryEntryDisplayingPlace) {
			return new ObservatoryEntryDisplayingActivity(
					(ObservatoryEntryDisplayingPlace) place, clientFactory);
		} else if (place instanceof ExperimentalSiteEntryDisplayingPlace) {
			return new ExperimentalSiteEntryDisplayingActivity(
					(ExperimentalSiteEntryDisplayingPlace) place, clientFactory);
		} else if (place instanceof DatasetEntryDisplayingPlace) {
			return new DatasetEntryDisplayingActivity(
					(DatasetEntryDisplayingPlace) place, clientFactory);
		} else if (place instanceof DatasetEntryCreationPlace) {
			return new DatasetEntryCreationActivity(
					(DatasetEntryCreationPlace) place, clientFactory);
		} else if (place instanceof DatasetEditingPlace) {
			return new DatasetEntryEditingActivity((DatasetEditingPlace) place,
					clientFactory);
		} else if (place instanceof DirectoryManagementPlace) {
			return new DirectoryManagementActivity(
					(DirectoryManagementPlace) place, clientFactory);
		} else if (place instanceof DirectoryManagementPlace) {
			return new DirectoryManagementActivity(
					(DirectoryManagementPlace) place, clientFactory);
		} else if (place instanceof HarvestManagementPlace) {
			return new HarvestManagementActivity(
					(HarvestManagementPlace) place, clientFactory);
		} 
		
		else if (place instanceof NewsPlace) {
			return new RBVNewsPageActivity((NewsPlace) place, clientFactory);
		}
	

		else if (place instanceof NewDisplayPlace) {
			return new RBVNewDisplayActivity((NewDisplayPlace) place,
					(NewsClientFactory) clientFactory);
		}

		else  if (place instanceof NewsArchivePlace) {
			return new RBVNewsArchiveActivity((NewsArchivePlace) place,
					(NewsClientFactory) clientFactory);
		} 

		else if (place instanceof MessageManagePlace) {
			return new RBVNewsManagementActivity((MessageManagePlace) place, clientFactory);
		} else if (place instanceof MessageEditPlace) {
			return new RBVNewsEditActivity((MessageEditPlace) place, clientFactory);
		} 
		
		/*else if (place instanceof MessageManagePlace) {
			return new RBVMessageManagementActivity((MessageManagePlace) place,
					clientFactory);
		} else if (place instanceof MessageEditPlace) {
			return new RBVMessageEditActivity((MessageEditPlace) place,
					clientFactory);
		} */
		
	/*	else if (place instanceof NewsArchivePlace) {
			return new RBVNewsArchiveActivity((NewsArchivePlace) place,
					(NewsClientFactory) clientFactory);
		}
		
		else if (place instanceof NewDisplayPlace) {
			return new RBVNewDisplayActivity((NewDisplayPlace) place,
					(NewsClientFactory) clientFactory);
		}*/

		else if (place instanceof DatasetEntryManagementPlace) {
			return new DatasetEntryManagementActivity(
					(DatasetEntryManagementPlace) place, clientFactory);
		}

		else if (place instanceof CMSConsultPlace) {
			return new RBVCMSConsultActivity((CMSConsultPlace) place,
					clientFactory);
		}

		else if (place instanceof CMSEditPlace) {
			return new RBVCMSEditActivity((CMSEditPlace) place, clientFactory);
		}

		else if (place instanceof CMSListPlace) {
			return new RBVCMSListActivity((CMSListPlace) place, clientFactory);
		}
		
		else if (place instanceof MenuEditPlace) {
			return new RBVCMSMenuEditActivity((MenuEditPlace) place, clientFactory);
		}
		
		
		return null;
	}

}
