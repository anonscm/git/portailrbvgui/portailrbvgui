package fr.obsmip.sedoo.client.ui.metadata.common.contact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;

import fr.obsmip.sedoo.client.message.Message;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.table.TableBundle;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.ui.widget.field.impl.PrincipalInvestigators;
import fr.sedoo.metadata.client.ui.widget.field.impl.contact.AbstractSingleRoleResourceContact;
import fr.sedoo.metadata.shared.domain.dto.MetadataContactDTO;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public abstract class RBVAbstractContactList extends AbstractSingleRoleResourceContact implements ClickHandler, ConfirmCallBack
{
	private DirectoryPresenter presenter;
	private ContactListDialogBoxContent content;

	public RBVAbstractContactList() {
		super();
		CommonBundle.INSTANCE.commonStyle().ensureInjected();
		addDirectoryButton(table.getToolBarPanel());
	}
	
	protected void addDirectoryButton(Panel panel)
	{
		Image image = new Image(TableBundle.INSTANCE.add());
		panel.add(image);
		Label label = new Label(Message.INSTANCE.metadataEditingAddContactFromDirectory());
		panel.add(label);
		label.setStyleName(CommonBundle.INSTANCE.commonStyle().clickable());
		image.setStyleName(CommonBundle.INSTANCE.commonStyle().clickable());
		label.addClickHandler(this);
		image.addClickHandler(this);
	}

	@Override
	public void onClick(ClickEvent event) {
		content = new ContactListDialogBoxContent(this);
		DialogBoxTools.popUp(Message.INSTANCE.directory(), content);
		presenter.loadDirectoryPersons(content.contactTable);
	}

	public void setPresenter(DirectoryPresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void confirm(boolean choice) 
	{
		if (choice == true)
		{
			List<? extends HasIdentifier> model = table.getModel();
			ArrayList<MetadataContactDTO> newModel = new ArrayList<MetadataContactDTO>();
			int i = 0;
			Iterator<? extends HasIdentifier> existingIterator = model.iterator();
			while (existingIterator.hasNext()) {
				MetadataContactDTO aux = (MetadataContactDTO) existingIterator.next();
				aux.setUuid(""+i);
				i++;
				newModel.add(aux);
			}
			List<PersonDTO> selection = content.getResultList();
			Iterator<PersonDTO> selectionIterator = selection.iterator();
			while (selectionIterator.hasNext()) 
			{
				PersonDTO personDTO = (PersonDTO) selectionIterator.next();
				MetadataContactDTO aux = new MetadataContactDTO();
				PersonDTO.clone(personDTO, aux);
				aux.setRoles(getSingleRole());
				aux.setUuid(""+i);
				i++;
				newModel.add(aux);
			}
			table.init(newModel);
		}
	}
}
