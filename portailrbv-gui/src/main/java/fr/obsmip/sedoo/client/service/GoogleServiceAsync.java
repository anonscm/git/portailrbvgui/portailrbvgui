package fr.obsmip.sedoo.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.RBVUserDTO;

public interface GoogleServiceAsync {

	void getUserFromToken(String token, AsyncCallback<RBVUserDTO> callback);

}
