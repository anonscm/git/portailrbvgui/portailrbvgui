package fr.obsmip.sedoo.client.ui;

import java.util.List;

import com.google.gwt.resources.client.CssResource;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.sedoo.commons.client.component.DefaultCssResourceProvider;
import fr.sedoo.commons.client.style.SedooStyleBundle;

public class RBVCssResourceProvider extends DefaultCssResourceProvider 
{
@Override
public List<CssResource> getCssResources() {
	List<CssResource> cssResources = super.getCssResources();
	cssResources.add(GlobalBundle.INSTANCE.css());
	cssResources.add(SedooStyleBundle.INSTANCE.sedooStyle());
	return cssResources;
}
}
