package fr.obsmip.sedoo.client.ui.metadata.experimentalsite;

import java.util.Iterator;
import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.AbstractSection;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.message.ValidationMessages;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.UpperCaseTextBox;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ExperimentalSiteEntryCreationViewImpl extends AbstractSection implements ExperimentalSiteEntryCreationView {

	@UiField
	Alert noObservatoryToCreate;

	@UiField
	ListBox observatories;

	@UiField
	UpperCaseTextBox experimentalSiteName;
	
	@UiField
	VerticalPanel creationPanel;

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	private Presenter presenter;

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, ExperimentalSiteEntryCreationViewImpl> {
	}

	public ExperimentalSiteEntryCreationViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		noObservatoryToCreate.setVisible(true);
		creationPanel.setVisible(false);
	}

	@Override
	public void setObservatories(List<ObservatorySummaryDTO> observatoryList) {
		if (observatoryList.isEmpty() == false)
		{
			observatories.clear();
			Iterator<ObservatorySummaryDTO> iterator = observatoryList.iterator();
			while (iterator.hasNext()) {
				MetadataSummaryDTO current = iterator.next();
				observatories.addItem(current.getName(), current.getUuid());
			}
			creationPanel.setVisible(true);
			noObservatoryToCreate.setVisible(false);		
		}
		else
		{
			observatories.clear();
			creationPanel.setVisible(false);
			noObservatoryToCreate.setVisible(true);
		}
	}

	@UiHandler("createButton")
	void onCreateButtonClicked(ClickEvent event) {
		String name = StringUtil.trimToEmpty(experimentalSiteName.getText());
		if (StringUtil.isEmpty(name))
		{
			DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(),Message.INSTANCE.experimentalSiteName()+" : "+ ValidationMessages.INSTANCE.mandatoryData());
		}
		else
		{
			ObservatorySummaryDTO aux = new ObservatorySummaryDTO();
			aux.setName(observatories.getItemText(observatories.getSelectedIndex()));
			aux.setUuid(observatories.getValue(observatories.getSelectedIndex()));
			presenter.create(aux, name);
		}
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void reset() {
		experimentalSiteName.setText("");
	}

	@UiHandler("observatoryEntryCreationLink")
	 void onSaveButtonClicked(ClickEvent event) 
	 {
		 presenter.goToObservatoryEntryCreationPlace();
	 }

	@Override
	public void selectObservatory(String observatoryName) 
	{
		int itemCount = observatories.getItemCount();
		for (int i = 0; i < itemCount; i++) 
		{
			String aux = observatories.getItemText(i);
			if (aux.compareToIgnoreCase(StringUtil.trimToEmpty(observatoryName))==0)
			{
				observatories.setSelectedIndex(i);
				break;
			}
		}
	}

}
