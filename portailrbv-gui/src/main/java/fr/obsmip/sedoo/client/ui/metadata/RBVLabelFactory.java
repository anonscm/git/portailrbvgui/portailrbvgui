package fr.obsmip.sedoo.client.ui.metadata;

import com.google.gwt.user.client.ui.Label;

import fr.sedoo.metadata.client.ui.widget.field.impl.LabelFactory;

public class RBVLabelFactory {

	public static Label getLabelByKey(String key)
	{
		Label aux = LabelFactory.getLabelByKey(key);
		aux.setText(aux.getText().replace(':', ' ').trim());
		return aux;
	}
}
