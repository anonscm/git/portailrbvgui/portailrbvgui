package fr.obsmip.sedoo.client.ui.menu;

import java.util.LinkedList;
import java.util.List;

import fr.obsmip.sedoo.client.ui.WelcomeView.Presenter;
import fr.obsmip.sedoo.client.ui.searchcriteria.CriteriaWidget;
import fr.obsmip.sedoo.client.ui.searchcriteria.MapCriteriaWidget;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public class MetadataUserSearchCriteria {

	private Presenter presenter;
	private List<CriteriaWidget> criteriaWidgets = new LinkedList<CriteriaWidget>();

	public void searchUserCriteria(SearchCriteriaDTO searchCriteria) {
		presenter.search(searchCriteria);
	}

	public void add(CriteriaWidget searchCriteriaWidget) {
		criteriaWidgets.add(searchCriteriaWidget);
	}

	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {
		SearchCriteriaDTO aux = criteria;
		for (CriteriaWidget widget : criteriaWidgets) {
			aux = ((MapCriteriaWidget) widget).flush(aux);
		}
		return aux;
	}

	public void reset() {
		for (CriteriaWidget widget : criteriaWidgets) {
			widget.reset();
		}
	}

}
