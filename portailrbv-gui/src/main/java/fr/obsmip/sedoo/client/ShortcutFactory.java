package fr.obsmip.sedoo.client;

import java.util.HashMap;

import com.google.gwt.place.shared.Place;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.MetadataEditionShortcut;
import fr.obsmip.sedoo.client.place.DatasetEditingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryManagementPlace;
import fr.obsmip.sedoo.client.place.DirectoryManagementPlace;
import fr.obsmip.sedoo.client.place.DrainageBasinEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEditingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryManagementPlace;
import fr.obsmip.sedoo.client.place.GeoSummaryPlace;
import fr.obsmip.sedoo.client.place.GeonetworkObservatoryManagementPlace;
import fr.obsmip.sedoo.client.place.HarvestManagementPlace;
import fr.obsmip.sedoo.client.place.MetadataEditingPlace;
import fr.obsmip.sedoo.client.place.MetadataManagingPlace;
import fr.obsmip.sedoo.client.place.MetadataResultPlace;
import fr.obsmip.sedoo.client.place.MetadataSearchPlace;
import fr.obsmip.sedoo.client.place.ObservatoryContactEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryManagementPlace;
import fr.obsmip.sedoo.client.place.SystemPlace;
import fr.obsmip.sedoo.client.place.UserManagementPlace;
import fr.obsmip.sedoo.client.place.WelcomePlace;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.cms.bundle.CMSMessages;
import fr.sedoo.commons.client.cms.bundle.MenuMessages;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.cms.place.CMSEditPlace;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.util.StringUtil;
import fr.sedoo.metadata.client.place.FormatListPlace;

public class ShortcutFactory {

	static public Shortcut getWelcomeShortcut() {
		return new Shortcut(Message.INSTANCE.headerViewCatalogLink(),
				new WelcomePlace());
	}

	public static Shortcut getSystemShortcut() {
		return new Shortcut(Message.INSTANCE.systemViewHeader(),
				new SystemPlace());
	}

	public static Shortcut getLoginShortcut() {
		return new Shortcut(Message.INSTANCE.loginViewHeader(),
				new LoginPlace());
	}

	public static Shortcut getObservatoryEntryManagementShortcut() {
		return new Shortcut(Message.INSTANCE.observatoryManagementViewHeader(),
				new ObservatoryEntryManagementPlace());
	}

	public static Shortcut getObservatoryManagementShortcut() {
		return new Shortcut(Message.INSTANCE.observatoryManagement(),
				new GeonetworkObservatoryManagementPlace());
	}

	public static Shortcut getMetadataManagementShortcut() {
		return new Shortcut(
				Message.INSTANCE.metadataProviderMenuManageMetadata(),
				new MetadataManagingPlace());
	}

	public static Shortcut getMetadataEditingShortcut(String drainageBasinName) {
		return new Shortcut(Message.INSTANCE.metadataEditingHeader() + " ("
				+ drainageBasinName + ")", new MetadataEditingPlace());
	}

	public static Shortcut getMetadataCreatingShortcut(String drainageBasinName) {
		return new Shortcut(Message.INSTANCE.metadataCreatingTitle() + " ("
				+ drainageBasinName + ")", new MetadataEditingPlace());
	}

	public static Shortcut getMetadataSearchShortcut() {
		return new Shortcut(Message.INSTANCE.metadataSearchingTitle(),
				new MetadataSearchPlace());
	}

	public static Shortcut getCMSListShortcut() {
		return new Shortcut(CMSMessages.INSTANCE.screenList(),
				new CMSListPlace());
	}

	public static Shortcut getMetadataSearchShortcut(SearchCriteriaDTO criteria) {
		MetadataSearchPlace metadataSearchPlace = new MetadataSearchPlace();
		metadataSearchPlace.setCriteria(criteria);
		return new Shortcut(Message.INSTANCE.metadataSearchingTitle(),
				metadataSearchPlace);
	}

	public static Shortcut getMetadataResultShortcut(SearchCriteriaDTO criteria) {
		MetadataResultPlace metadataResultPlace = new MetadataResultPlace();
		if (criteria != null) {
			metadataResultPlace.setCriteria(criteria);
		}
		return new Shortcut(Message.INSTANCE.resultViewHeader(),
				metadataResultPlace);
	}

	public static Shortcut getObservatoryModificationShortcut(String name,
			String uuid) {
		ObservatoryEditingPlace aux = new ObservatoryEditingPlace();
		aux.setObservatoryUuid(uuid);
		return new Shortcut(
				Message.INSTANCE.observatoryEditingViewModificationHeader()
						+ " (" + name + ")", aux);
	}

	public static Shortcut getObservatoryContactModificationShortcut(
			String name, Long id) {
		ObservatoryContactEditingPlace aux = new ObservatoryContactEditingPlace();
		aux.setMode(Constants.MODIFY);
		aux.setId(id);

		return new Shortcut(
				Message.INSTANCE.observatoryContactEditingViewModificationHeader()
						+ " (" + name + ")", aux);
	}

	public static Shortcut getObservatoryContactCreationShortcut(Long id) {
		ObservatoryContactEditingPlace aux = new ObservatoryContactEditingPlace();
		aux.setId(id);
		aux.setMode(Constants.CREATE);
		return new Shortcut(
				Message.INSTANCE.observatoryContactEditingViewCreationHeader(),
				aux);
	}

	public static Shortcut getObservatoryDrainageBasinCreationShortcut(Long id) {
		DrainageBasinEditingPlace aux = new DrainageBasinEditingPlace();
		aux.setId(id);
		aux.setMode(Constants.CREATE);
		return new Shortcut(
				Message.INSTANCE.drainageBasinEditingViewCreationHeader(), aux);
	}

	public static Shortcut getDrainageBasinModificationShortcut(String label,
			Long id) {
		DrainageBasinEditingPlace aux = new DrainageBasinEditingPlace();
		aux.setMode(Constants.MODIFY);
		aux.setId(id);
		return new Shortcut(
				Message.INSTANCE.drainageBasinEditingViewModificationHeader()
						+ " (" + label + ")", aux);
	}

	public static Shortcut getFormatListShortcut() {
		return new Shortcut(Message.INSTANCE.formatViewHeader(),
				new FormatListPlace());
	}

	public static Shortcut getGeoSummaryShortcut() {
		return new Shortcut(Message.INSTANCE.geoSummaryViewHeader(),
				new GeoSummaryPlace());
	}

	public static Shortcut getUserManagementShortcut() {
		return new Shortcut(Message.INSTANCE.userManagementViewHeader(),
				new UserManagementPlace());
	}

	public static Shortcut getMessageManagementShortcut() {
		return new Shortcut(NewsMessages.INSTANCE.header(),
				new MessageManagePlace());
	}

	public static Shortcut getMessageEditShortcut() {
		return new Shortcut(NewsMessages.INSTANCE.editDialogTitle(),
				new MessageEditPlace());
	}

	public static Shortcut getObservatoryEntryCreationShortcut() {
		return new Shortcut(Message.INSTANCE.observatoryEntryCreation(),
				new ObservatoryEntryCreationPlace());
	}

	public static Shortcut getExperimentalSiteCreationShortcut() {
		return new Shortcut(
				Message.INSTANCE.experimentalSiteEditingViewCreationHeader(),
				new ExperimentalSiteEditingPlace());
	}

	public static Shortcut getExperimentalSiteModificationShortcut(
			String experimentalSiteName, String experimentalSiteUuid) {
		ExperimentalSiteEditingPlace place = new ExperimentalSiteEditingPlace();
		place.setExperimentalSiteUuid(experimentalSiteUuid);
		return new Shortcut(
				Message.INSTANCE.experimentalSiteEditingViewModificationHeader()
						+ " (" + experimentalSiteName + ")", place);
	}

	public static Shortcut getExperimentaSiteEntryCreationShortcut() {
		return new Shortcut(
				Message.INSTANCE.experimentalSiteEditingViewCreationHeader(),
				new ExperimentalSiteEntryCreationPlace());
	}

	public static Shortcut getDatasetEntryCreationShortcut() {
		return new Shortcut(Message.INSTANCE.datasetEntryCreationHeader(),
				new DatasetEntryCreationPlace());
	}

	public static Shortcut getExperimentalSiteEntryManagementShortcut() {
		return new Shortcut(
				Message.INSTANCE.experimentalSiteManagementViewHeader(),
				new ExperimentalSiteEntryManagementPlace());
	}

	public static Shortcut getObservatoryEntryDisplayingShortcut(String uuid) {
		ObservatoryEntryDisplayingPlace place = new ObservatoryEntryDisplayingPlace();
		place.setUuid(uuid);
		return new Shortcut(Message.INSTANCE.observatoryDisplayingTitle(),
				place);
	}

	public static Shortcut getExperimentalSiteEntryDisplayingShortcut(
			String uuid) {
		ExperimentalSiteEntryDisplayingPlace place = new ExperimentalSiteEntryDisplayingPlace();
		place.setUuid(uuid);
		return new Shortcut(Message.INSTANCE.experimentalSiteDisplayingTitle(),
				place);
	}

	public static Shortcut getDatasetModificationShortcut(String datasetUuid) {
		DatasetEditingPlace place = new DatasetEditingPlace();
		place.setDatasetUuid(datasetUuid);
		return new Shortcut(
				Message.INSTANCE.datasetEditingViewModificationHeader(), place);
	}

	public static Shortcut getDirectoryManagementShortcut() {
		return new Shortcut(Message.INSTANCE.directoryManagementViewHeader(),
				new DirectoryManagementPlace());
	}

	public static Shortcut getHarvestManagementShortcut() {
		return new Shortcut(Message.INSTANCE.harvestManagement(),
				new HarvestManagementPlace());
	}

	

	public static Shortcut getDatasetEntryManagementShortcut() {
		return new Shortcut(Message.INSTANCE.datasetManagementViewHeader(),
				new DatasetEntryManagementPlace());
	}

	public static Shortcut getCMSConsultShortcut(CMSConsultPlace cmsConsultPlace) {
		String screenTitle = null;
		HashMap<String, String> screenNames = PortailRBV.getClientFactory()
				.getScreenNames();
		if (screenNames != null) {
			screenTitle = screenNames.get(cmsConsultPlace.getScreenName());
		}
		if (screenTitle != null && StringUtil.isNotEmpty(screenTitle)) {
			return new Shortcut(screenTitle, cmsConsultPlace);
		} else {
			return new Shortcut(PortailRBV.getClientFactory()
					.getCMSLabelProvider()
					.getLabelByScreenName(cmsConsultPlace.getScreenName()),
					cmsConsultPlace);
		}
	}

	public static Shortcut getCMSEditShortcut(CMSEditPlace cmsEditPlace) {
		return new Shortcut(CMSMessages.INSTANCE.screenModification()
				+ " : "
				+ PortailRBV.getClientFactory().getCMSLabelProvider()
						.getLabelByScreenName(cmsEditPlace.getScreenName()),
				cmsEditPlace);
	}

	public static Shortcut getMetadataEditingShortcut() {
		return new MetadataEditionShortcut();
	}

	public static Shortcut getMenuEditionShortcut(Place place) {
		return new Shortcut(MenuMessages.INSTANCE.menuEdition(), place);
	}
	
	public static Shortcut getNewsShortcut() {
		return new Shortcut(Message.INSTANCE.news(), new NewsArchivePlace());
	}
	public static Shortcut getNewsDisplayShortcut() {
		return new Shortcut(NewsMessages.INSTANCE.newsDisplayTitle(),
				new NewDisplayPlace());
	}
	/*public static Shortcut getNewsArchiveShortcut() {
		return new Shortcut(NewsMessages.INSTANCE.newsArchiveTitle(),
				new NewsArchivePlace());
	}*/
}
