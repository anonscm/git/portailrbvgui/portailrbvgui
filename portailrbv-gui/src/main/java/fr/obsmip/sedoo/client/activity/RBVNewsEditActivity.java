package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.activity.MessageEditActivity;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVNewsEditActivity extends MessageEditActivity{
	
	
	public RBVNewsEditActivity(MessageEditPlace place, AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
		
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(""));
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		//shortcuts.add(ShortcutFactory
		//		.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		shortcuts.add(ShortcutFactory.getMessageManagementShortcut());
		shortcuts.add(ShortcutFactory.getMessageEditShortcut());

		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
		clientFactory.getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
		
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser())	{
			return ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser().isAdmin();
		} else {
			return false;
		}
	}
	
	@Override
	public void back() {
		super.back();
		PortailRBV.getClientFactory().getPlaceController().goTo(new NewsArchivePlace());
	}
}
