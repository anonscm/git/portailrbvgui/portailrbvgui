package fr.obsmip.sedoo.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;

public class ProgressViewImpl extends AbstractSection implements ProgressView {

	
	private static ProgressViewImplUiBinder uiBinder = GWT
			.create(ProgressViewImplUiBinder.class);

	interface ProgressViewImplUiBinder extends UiBinder<Widget, ProgressViewImpl> {
	}

	public ProgressViewImpl() {
		super();
		GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

}
