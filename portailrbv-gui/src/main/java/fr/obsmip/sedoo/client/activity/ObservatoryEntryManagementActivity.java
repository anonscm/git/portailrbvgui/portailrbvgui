package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.habilitation.HabilitationUtil;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.ObservatoryEditingPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryCreationPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryManagementPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryManagementView;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryManagementView.Presenter;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ObservatoryEntryManagementActivity extends RBVAuthenticatedActivity implements Presenter {


	public ObservatoryEntryManagementActivity(ObservatoryEntryManagementPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	private final static  MetadataServiceAsync METADATA_SERVICE = GWT.create(MetadataService.class);
	ObservatoryEntryManagementView observatoryManagementView;


	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		observatoryManagementView = clientFactory.getObservatoryManagementView();
		observatoryManagementView.setPresenter(this);
		containerWidget.setWidget(observatoryManagementView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.observatoryManagementViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getObservatoryEntryManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);

		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.OBSERVATORIES_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);

		METADATA_SERVICE.getObservatorySummaries(clientFactory.getDisplayLanguages(), new AsyncCallback<ArrayList<ObservatorySummaryDTO>>() {

			@Override
			public void onSuccess(ArrayList<ObservatorySummaryDTO> observatorySummaries ) 
			{
				List<ObservatorySummaryDTO> filteredObservatories = HabilitationUtil.filterByUser(observatorySummaries, (RBVUserDTO) PortailRBV.getClientFactory().getUserManager().getUser());
				observatoryManagementView.init(filteredObservatories);
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
			}

			@Override
			public void onFailure(Throwable caught) {
				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
			}
		});
	}


	@Override
	public void deleteObservatoryEntry(final String uuid) 
	{
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstant.OBSERVATORY_DELETING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		METADATA_SERVICE.deleteObservatoryEntry(uuid, new DefaultAbstractCallBack<Boolean>(e, clientFactory.getEventBus()) 
		{
			@Override
			public void onSuccess(Boolean result) {
				super.onSuccess(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				observatoryManagementView.broadcastObservatoryDeletion(uuid);
			}
		});
	}

	@Override
	public void goToEditPlace(MetadataSummaryDTO observatoryDTO) {
		ObservatoryEditingPlace place = new ObservatoryEditingPlace();
		place.setObservatoryUuid(StringUtil.trimToEmpty(observatoryDTO.getUuid()));
		clientFactory.getPlaceController().goTo(place);
	}
	
	@Override
	public void goToObservatoryEntryCreationPlace() {
		clientFactory.getPlaceController().goTo(new ObservatoryEntryCreationPlace());
	}

}
