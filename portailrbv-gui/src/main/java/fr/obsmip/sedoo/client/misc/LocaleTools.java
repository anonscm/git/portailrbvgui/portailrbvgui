package fr.obsmip.sedoo.client.misc;

import com.google.gwt.i18n.client.LocaleInfo;

import fr.sedoo.commons.client.util.StringUtil;

public class LocaleTools {
	
	private static final String FRENCH_LOCALE = "FR";

	public static boolean isFrenchGUI()
	{
		if (LocaleInfo.getCurrentLocale().getLocaleName().toUpperCase().indexOf(FRENCH_LOCALE)>=0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static String getDisplayValue(String englishValue, String frenchValue)
	{
		if (isFrenchGUI())
		{
			if (StringUtil.isEmpty(frenchValue))
			{
				return StringUtil.trimToEmpty(englishValue);
			}
			else
			{
				return frenchValue;
			}
		}
		else
		{
			if (StringUtil.isEmpty(englishValue))
			{
				return StringUtil.trimToEmpty(frenchValue);
			}
			else
			{
				return englishValue;
			}
		}
	}

}
