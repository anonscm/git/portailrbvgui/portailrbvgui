package fr.obsmip.sedoo.client.place;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class OneLevelThesaurusPlace extends Place implements PlaceConstants {

	public static OneLevelThesaurusPlace instance;

	private List<String> selectedValues = new ArrayList<String>();
	private String thesaurusName;
	private final static String THESAURUS_SEPARATOR="@";  
	public final static String CLIMATE_THESAURUS="climate";
	public final static String GEOLOGY_THESAURUS="geology";


	public OneLevelThesaurusPlace(String thesaurusName) {
		this.thesaurusName = thesaurusName;
	}

	public static class Tokenizer implements PlaceTokenizer<OneLevelThesaurusPlace>
	{
		@Override
		public String getToken(OneLevelThesaurusPlace place)
		{
			StringBuffer result = new StringBuffer();
			result.append(place.getThesaurusName());
			List<String> aux = place.getSelectedValues();
			if (aux.isEmpty() == false)
			{
				result.append(THESAURUS_SEPARATOR);
				Iterator<String> iterator = aux.iterator();
				while (iterator.hasNext()) {
					result.append(iterator.next());
					if (iterator.hasNext())
					{
						result.append(TOKEN_SEPARATOR);
					}
				}
			}
			return result.toString();
		}

		@Override
		public OneLevelThesaurusPlace getPlace(String token)
		{
			if (instance == null)
			{
				String[] tmp = token.split(THESAURUS_SEPARATOR);
			
				instance = new OneLevelThesaurusPlace(tmp[0]);
				if (tmp.length>1)
				{
					List<String> aux = new ArrayList<String>();
					String[] split = tmp[1].split(TOKEN_SEPARATOR);
					for (int i = 0; i < split.length; i++) {
						aux.add(split[i]);
					}
					instance.setSelectedValues(aux);
				}
			}
			return instance;
		}
	}

	public List<String> getSelectedValues() {
		return selectedValues;
	}

	public void setSelectedValues(List<String> selectedValues) {
		this.selectedValues = selectedValues;
	}

	public String getThesaurusName() {
		return thesaurusName;
	}

	public void setThesaurusName(String thesaurusName) {
		this.thesaurusName = thesaurusName;
	}

}
