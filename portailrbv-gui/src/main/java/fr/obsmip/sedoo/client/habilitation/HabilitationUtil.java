package fr.obsmip.sedoo.client.habilitation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;

public class HabilitationUtil {
	
	public static<E extends IsObservatory> List<E> filterByUser(List<E> observatories, RBVUserDTO user) 
	{
		if (user.isAdmin())
		{
			return observatories;
		}
		else
		{
			ArrayList<E> result = new ArrayList<E>();
			Iterator<E> iterator = observatories.iterator();
			while (iterator.hasNext()) 
			{
				E observatory = iterator.next();
				if (user.isAutorizedFor(observatory.getName()))
				{
					result.add(observatory);
				}
			}
			return result;
		}
	}
}
