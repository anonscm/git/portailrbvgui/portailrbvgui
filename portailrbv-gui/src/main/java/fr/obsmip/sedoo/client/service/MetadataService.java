package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.ApplicationException;
import fr.obsmip.sedoo.shared.domain.DatasetDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

@RemoteServiceRelativePath("metadata")
public interface MetadataService extends RemoteService {

	String toXML(MetadataDTO metadata, ArrayList<String> alternateLanguages);

	// boolean save(MetadataDTO metadata, Long observatoryId) throws
	// ApplicationException;
	MetadataDTO saveObservatory(MetadataDTO metadata, String observatoryName,
			ArrayList<String> alternateLanguages, String currentLanguage)
			throws ApplicationException;

	MetadataDTO saveExperimentalSite(MetadataDTO metadata,
			String observatoryName, ArrayList<String> alternateLanguages,
			String currentLanguage) throws ApplicationException;

	MetadataDTO saveDataset(MetadataDTO metadata, String observatoryName,
			ArrayList<String> alternateLanguages, String currentLanguage)
			throws ApplicationException;

	MetadataDTO createDefaultMetadata();

	String getPDFURL(String metadataId) throws ApplicationException;

	MetadataDTO getMetadataByUuid(String uuid,
			ArrayList<String> alternateLanguages, String currentLanguage)
			throws ApplicationException;

	ObservatoryDTO getObservatoryByUuid(String uuid,
			ArrayList<String> alternateLanguages, String currentLanguage)
			throws ApplicationException;

	ExperimentalSiteDTO getExperimentalSiteByUuid(String uuid,
			ArrayList<String> alternateLanguages, String currentLanguage)
			throws ApplicationException;

	DatasetDTO getDatasetByUuid(String uuid,
			ArrayList<String> alternateLanguages, String currentLanguage)
			throws ApplicationException;

	boolean deleteMetadataByUuid(String uuid) throws ApplicationException;

	List<SummaryDTO> getSummariesByDrainageBasinId(Long id)
			throws ApplicationException;

	ArrayList<ObservatorySummaryDTO> getObservatorySummaries(
			ArrayList<String> displayLanguages) throws ApplicationException;

	boolean deleteObservatoryEntry(String uuid) throws ApplicationException;

	boolean deleteExperimentalSiteEntry(String uuid)
			throws ApplicationException;

	boolean deleteDatasetEntry(String uuid) throws ApplicationException;

	ArrayList<ExperimentalSiteSummaryDTO> getExperimentalSitesSummaryFromParentUuid(
			String parentUuid, ArrayList<String> displayLanguages)
			throws ApplicationException;

	ArrayList<GeographicBoundingBoxDTO> computeGeographicalBoxesFromExperimentalSites(
			String uuid) throws ApplicationException;

	I18nString computeUseConditionsFromParent(String parentUuid,
			ArrayList<String> alternateLanguages) throws ApplicationException;

	I18nString computePublicAccessLimitationsFromParent(String parentUuid,
			ArrayList<String> alternateLanguages) throws ApplicationException;

	String getExperimentalSiteUuidByDatasetUuid(String uuid)
			throws ApplicationException;

	String getObservatoryUuidByDatasetUuid(String uuid)
			throws ApplicationException;

	String getObservatoryUuidByExperimentalSiteUuid(String uuid)
			throws ApplicationException;

	ArrayList<MetadataSummaryDTO> getDatasetFromParentUuid(String uuid,
			ArrayList<String> displayLanguages) throws ApplicationException;
}
