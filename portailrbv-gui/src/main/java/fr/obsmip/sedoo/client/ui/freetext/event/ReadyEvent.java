package fr.obsmip.sedoo.client.ui.freetext.event;

import com.google.gwt.event.shared.EventHandler;

import fr.obsmip.sedoo.client.ui.freetext.FreeTextChosenImpl;

public class ReadyEvent extends FreeTextChosenEvent<ReadyEvent.ReadyHandler> {

	  public interface ReadyHandler extends EventHandler {
	    void onReady(ReadyEvent event);
	  }

	  public static Type<ReadyHandler> TYPE = new Type<ReadyHandler>();

	  public static Type<ReadyHandler> getType() {
	    return TYPE;
	  }

	  public ReadyEvent(FreeTextChosenImpl chosen) {
	    super(chosen);
	  }

	  @Override
	  public Type<ReadyHandler> getAssociatedType() {
	    return TYPE;
	  }

	  @Override
	  protected void dispatch(ReadyHandler handler) {
	    handler.onReady(this);
	  }

}
