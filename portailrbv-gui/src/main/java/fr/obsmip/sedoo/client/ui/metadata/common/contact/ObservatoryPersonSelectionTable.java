package fr.obsmip.sedoo.client.ui.metadata.common.contact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;

import fr.obsmip.sedoo.client.ui.table.ObservatoryPersonTable;
import fr.sedoo.commons.client.widget.table.multiline.MultilineTextColumn;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public class ObservatoryPersonSelectionTable extends ObservatoryPersonTable {

	MultiSelectionModel<HasIdentifier> selectionModel;
	EditingPresenter presenter;

	public ObservatoryPersonSelectionTable() {
		super();
		getToolBarPanel().setVisible(false);
	}

	public void setPresenter(EditingPresenter presenter) {
		this.presenter = presenter;
	}

	public void refresh() {
		itemTable.setVisibleRangeAndClearData(itemTable.getVisibleRange(), true);
	}

	@Override
	protected void initColumns() {

		selectionModel = new MultiSelectionModel<HasIdentifier>(AbstractDTO.KEY_PROVIDER);
		itemTable.setSelectionModel(selectionModel, DefaultSelectionEventManager.<HasIdentifier> createCheckboxManager());

		Column<HasIdentifier, Boolean> checkColumn = new Column<HasIdentifier, Boolean>(new CheckboxCell(true, false)) {
			@Override
			public Boolean getValue(HasIdentifier object) {
				// Get the value from the selection model.
				return selectionModel.isSelected(object);
			}
		};

		itemTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
		itemTable.setColumnWidth(checkColumn, 40, Unit.PX);

		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getPersonName();
			}
		};
		itemTable.addColumn(nameColumn, MetadataMessage.INSTANCE.personPersonName());
		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);

		TextColumn<HasIdentifier> emailColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getEmail();
			}
		};
		itemTable.addColumn(emailColumn, MetadataMessage.INSTANCE.personEmail());
		itemTable.setColumnWidth(emailColumn, 100.0, Unit.PX);

		TextColumn<HasIdentifier> organisationColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getOrganisationName();
			}
		};
		itemTable.addColumn(organisationColumn, MetadataMessage.INSTANCE.personOrganisationName());
		itemTable.setColumnWidth(organisationColumn, 100.0, Unit.PX);

		MultilineTextColumn<HasIdentifier> adressColumn = new MultilineTextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((PersonDTO) aux).getCompleteAdress();
			}
		};
		itemTable.addColumn(adressColumn, MetadataMessage.INSTANCE.personAddress());
		itemTable.setColumnWidth(adressColumn, 100.0, Unit.PX);

	}

	public void reset() {
		if (selectionModel != null) {
			selectionModel.clear();
		}
		List<PersonDTO> aux = new ArrayList<PersonDTO>();
		init(aux);
	}

	public List<PersonDTO> getSelectedItems() {
		Set<HasIdentifier> selectedSet = selectionModel.getSelectedSet();
		List<PersonDTO> selection = new ArrayList<PersonDTO>();
		Iterator<HasIdentifier> iterator = selectedSet.iterator();
		while (iterator.hasNext()) {
			selection.add((PersonDTO) iterator.next());
		}
		return selection;
	}

	

}
