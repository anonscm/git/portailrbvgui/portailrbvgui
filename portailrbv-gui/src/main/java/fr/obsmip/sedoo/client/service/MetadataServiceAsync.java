package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.DatasetDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteDTO;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public interface MetadataServiceAsync {

	void computeGeographicalBoxesFromExperimentalSites(String uuid,
			AsyncCallback<ArrayList<GeographicBoundingBoxDTO>> callback);

	void computePublicAccessLimitationsFromParent(String parentUuid,
			ArrayList<String> alternateLanguages,
			AsyncCallback<I18nString> callback);

	void computeUseConditionsFromParent(String parentUuid,
			ArrayList<String> alternateLanguages,
			AsyncCallback<I18nString> callback);

	void createDefaultMetadata(AsyncCallback<MetadataDTO> callback);

	void deleteExperimentalSiteEntry(String uuid,
			AsyncCallback<Boolean> callback);

	void deleteMetadataByUuid(String uuid, AsyncCallback<Boolean> callback);

	void deleteObservatoryEntry(String uuid, AsyncCallback<Boolean> callback);

	void getDatasetByUuid(String uuid, ArrayList<String> alternateLanguages,
			String currentLanguage, AsyncCallback<DatasetDTO> callback);

	void getExperimentalSiteByUuid(String uuid,
			ArrayList<String> alternateLanguages, String currentLanguage,
			AsyncCallback<ExperimentalSiteDTO> callback);

	void getExperimentalSiteUuidByDatasetUuid(String uuid,
			AsyncCallback<String> callback);

	void getExperimentalSitesSummaryFromParentUuid(String parentUuid,
			ArrayList<String> displayLanguages,
			AsyncCallback<ArrayList<ExperimentalSiteSummaryDTO>> callback);

	void getMetadataByUuid(String uuid, ArrayList<String> alternateLanguages,
			String currentLanguage, AsyncCallback<MetadataDTO> callback);

	void getObservatoryByUuid(String uuid,
			ArrayList<String> alternateLanguages, String currentLanguage,
			AsyncCallback<ObservatoryDTO> callback);

	void getObservatorySummaries(ArrayList<String> displayLanguages,
			AsyncCallback<ArrayList<ObservatorySummaryDTO>> callback);

	void getObservatoryUuidByDatasetUuid(String uuid,
			AsyncCallback<String> callback);

	void getObservatoryUuidByExperimentalSiteUuid(String uuid,
			AsyncCallback<String> callback);

	void getPDFURL(String metadataId, AsyncCallback<String> callback);

	void getSummariesByDrainageBasinId(Long id,
			AsyncCallback<List<SummaryDTO>> callback);

	void saveDataset(MetadataDTO metadata, String observatoryName,
			ArrayList<String> alternateLanguages, String currentLanguage,
			AsyncCallback<MetadataDTO> callback);

	void saveExperimentalSite(MetadataDTO metadata, String observatoryName,
			ArrayList<String> alternateLanguages, String currentLanguage,
			AsyncCallback<MetadataDTO> callback);

	void saveObservatory(MetadataDTO metadata, String observatoryName,
			ArrayList<String> alternateLanguages, String currentLanguage,
			AsyncCallback<MetadataDTO> callback);

	void toXML(MetadataDTO metadata, ArrayList<String> alternateLanguages,
			AsyncCallback<String> callback);

	void deleteDatasetEntry(String uuid, AsyncCallback<Boolean> callback);

	void getDatasetFromParentUuid(String uuid,
			ArrayList<String> displayLanguages,
			AsyncCallback<ArrayList<MetadataSummaryDTO>> callback);

}
