package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SearchStartEventHandler extends EventHandler {
	void onNotification(SearchStartEvent event);
}
