package fr.obsmip.sedoo.client.activity;

import fr.sedoo.commons.client.cms.activity.CMSListActivity;
import fr.sedoo.commons.client.cms.place.CMSListPlace;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public class ResifCMSListActivity extends CMSListActivity {

	public ResifCMSListActivity(CMSListPlace place,
			AuthenticatedClientFactory clientFactory) {
		super(place, clientFactory);
	}

	@Override
	protected boolean isValidUser() {
		if (isLoggedUser()) {
			return ((AuthenticatedClientFactory) clientFactory)
					.getUserManager().getUser().isAdmin();
		} else {
			return false;
		}
	}

}
