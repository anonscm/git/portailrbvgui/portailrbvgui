package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.HarvestManagementPlace;
import fr.obsmip.sedoo.client.service.HarvestManagementService;
import fr.obsmip.sedoo.client.service.HarvestManagementServiceAsync;
import fr.obsmip.sedoo.client.ui.HarvestManagementView;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class HarvestManagementActivity extends RBVAdministrationActivity implements HarvestManagementView.Presenter {
	
	private final HarvestManagementServiceAsync HARVEST_MANAGEMENT_SERVICE = GWT.create(HarvestManagementService.class);

	public HarvestManagementActivity(HarvestManagementPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		final HarvestManagementView view = clientFactory.getHarvestManagementView();
		view.reset();
		view.setPresenter(this);
		containerWidget.setWidget(view.asWidget());
		broadcastActivityTitle(Message.INSTANCE.harvestManagement());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getHarvestManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		
	}

	@Override
	public void launch() {
		
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.processing(), ActionEventConstant.HARVESTING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		HARVEST_MANAGEMENT_SERVICE.execute(new DefaultAbstractCallBack<Boolean>(e, clientFactory.getEventBus())
		{
				@Override
				public void onSuccess(Boolean result) {
					super.onSuccess(result);
				}
		}
		);
		
	}
}
