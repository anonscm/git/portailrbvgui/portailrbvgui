package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.domain.New;

public interface NewsServiceAsync {

	void getLatest(String preferredLanguage, List<String> alternateLanguages, ArrayList<String> displayLanguages, 
			AsyncCallback<ArrayList<New>> callback);

}
