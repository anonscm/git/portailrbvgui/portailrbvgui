package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ObservatoryNavigationEventHandler extends EventHandler {
    void onNotification(ObservatoryNavigationEvent event);
}

