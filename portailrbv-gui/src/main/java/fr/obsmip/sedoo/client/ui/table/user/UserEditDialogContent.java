package fr.obsmip.sedoo.client.ui.table.user;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.watopi.chosen.client.gwt.ChosenListBox;

import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.crud.widget.CrudConfirmCallBack;
import fr.sedoo.commons.client.util.SeparatorUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.dialog.DialogBoxContent;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;

public class UserEditDialogContent extends EditDialogContent implements DialogBoxContent {

	private static UserEditDialogContentUiBinder uiBinder = GWT
			.create(UserEditDialogContentUiBinder.class);

	interface UserEditDialogContentUiBinder extends
	UiBinder<Widget, UserEditDialogContent> {
	}

	Long id;
	
	@UiField
	TextBox name;
	
	@UiField
	TextBox login;

	@UiField
	TextBox password;
	
	@UiField
	RadioButton administrator;
	
	@UiField
	RadioButton dataProvider;
	
	@UiField
	CheckBox externalAuthentication;
	
	@UiField
	ChosenListBox observatories;

	public UserEditDialogContent(CrudConfirmCallBack confirmCallback, RBVUserDTO user, ArrayList< ? extends IsObservatory> observatoryList) {
		super(confirmCallback);
		initWidget(uiBinder.createAndBindUi(this));
		observatories.setMultipleSelect(true);
		observatories.setPlaceholderText("");
		
		Iterator<? extends IsObservatory> iterator = observatoryList.iterator();
		while (iterator.hasNext()) 
		{
			observatories.addItem(iterator.next().getName());
		}
		
		edit(user);
	}

	
	private void edit(RBVUserDTO user)
	{
		int itemCount = observatories.getItemCount();
		for (int i = 0; i < itemCount; i++) 
		{
			observatories.setItemSelected(i, false);	
		}

		id = user.getId();
		name.setText(StringUtil.trimToEmpty(user.getName()));
		login.setText(StringUtil.trimToEmpty(user.getLogin()));
		password.setText(StringUtil.trimToEmpty(user.getPassword()));
		administrator.setValue(user.isAdmin());
		dataProvider.setValue(!user.isAdmin());
		externalAuthentication.setValue(user.isExternalAuthentication());
		if (user.isExternalAuthentication())
		{
			onExternalAuthenticationClicked(null);
		}
		String aux = StringUtil.trimToEmpty(user.getObservatories());
		String[] split = aux.split(SeparatorUtil.AROBAS_SEPARATOR);
		for (int i = 0; i < split.length; i++) 
		{
			select(split[i]);
		}
		observatories.update();
		onRoleChanged(null);
	}
	
	
	private void select(String value)
	{
		if (StringUtil.isEmpty(value))
		{
			return;
		}
		else
		{
			int itemCount = observatories.getItemCount();
			for (int i = 0; i < itemCount; i++) 
			{
				if (observatories.getItemText(i).compareToIgnoreCase(value)==0)
				{
					observatories.setItemSelected(i, true);	
				}
			}
		}
	}

	@Override
	public RBVUserDTO flush() 
	{
		RBVUserDTO aux = new RBVUserDTO();
		aux.setId(id);
		aux.setLogin(login.getText());
		aux.setExternalAuthentication(externalAuthentication.getValue());
		aux.setAdmin(administrator.getValue());
		
		if (externalAuthentication.getValue() == false)
		{
			aux.setName(name.getText());
			aux.setPassword(password.getText());
		}

		if (dataProvider.getValue() == true)
		{
			StringBuffer sb = new StringBuffer();
			int itemCount = observatories.getItemCount();
			boolean needSeparator = false;
			for (int i = 0; i < itemCount; i++) 
			{
				if (observatories.isItemSelected(i))
				{
					if (needSeparator)
					{
						sb.append(SeparatorUtil.AROBAS_SEPARATOR);
					}
					sb.append(observatories.getItemText(i));
					needSeparator = true;
				}
			}
			aux.setObservatories(sb.toString());
		}
		else
		{
			aux.setObservatories("");
		}
		return aux;
	}


	@Override
	public String getPreferredHeight() {
		return "260px";
	}

	@UiHandler(value={"administrator", "dataProvider"})
	  void onRoleChanged(ClickEvent event) 
	  {
		  observatories.setEnabled(!administrator.getValue());
		  observatories.update();
	  }

	@UiHandler("externalAuthentication")
	  void onExternalAuthenticationClicked(ClickEvent event) 
	  {
		  name.setEnabled(!externalAuthentication.getValue());
		  password.setEnabled(!externalAuthentication.getValue());
	  }	
	

}
