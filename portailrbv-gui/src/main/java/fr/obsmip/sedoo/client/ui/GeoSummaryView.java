package fr.obsmip.sedoo.client.ui;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.AbstractGeoSummaryDTO;

public interface GeoSummaryView extends IsWidget {
	
	public void setObservatoryNames(List<String> observatoryList);
	public void displayGeoSummaries(List<? extends AbstractGeoSummaryDTO> summaries, String observatoryName);
	public void reset();
	
	public interface Presenter 
	 {
		void loadGeoSummaries(String observatoryName);

		void displayDataset(String uuid);
		void displayObservatory(String observatoryName);
		void loadAllGeoSummaries(List<String> observatoryNames);
		void displayExperimentalSite(String uuid);
		void displayExperimentalSiteFromDataset(String uuid);
		void displayObservatoryFromDataset(String uuid);
		void displayObservatoryFromExperimentalSite(String uuid);
	 }

	void setPresenter(Presenter presenter);
	public void ensureVisible();

}
