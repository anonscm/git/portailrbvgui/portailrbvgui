package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.client.misc.SummaryPresenter;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public interface MetadataResultView extends IsWidget {

	void setResults(int position,
			HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> results);

	void setHits(int hits);

	void hideResultPanel();

	void setPresenter(Presenter presenter);

	void reset();

	Presenter getPresenter();

	public interface Presenter extends SummaryPresenter {
		void back();

		void fetchSummaries(int start);
	}

}
