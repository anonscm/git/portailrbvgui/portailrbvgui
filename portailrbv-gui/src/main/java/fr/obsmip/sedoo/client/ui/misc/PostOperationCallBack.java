package fr.obsmip.sedoo.client.ui.misc;

public interface PostOperationCallBack {
	
	public void postExecution(boolean result, Object src);

}
