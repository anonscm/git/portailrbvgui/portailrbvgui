package fr.obsmip.sedoo.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.ui.table.log.LogTable;
import fr.sedoo.commons.client.longlist.component.LongListViewImpl;
import fr.sedoo.commons.client.longlist.widget.LongListTable;

public class LogConsultationViewImpl extends LongListViewImpl implements LogConsultationView {

	private static LogConsultationViewImplUiBinder uiBinder = GWT
			.create(LogConsultationViewImplUiBinder.class);

	interface LogConsultationViewImplUiBinder extends UiBinder<Widget, LogConsultationViewImpl> {
	}

	@UiField
	LogTable logTable;
	
	public LogConsultationViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public LongListTable getLongListTable() {
		return logTable;
	}

}
