package fr.obsmip.sedoo.client.ui.misc;

public class KeyComparator 
{
	protected KeyComparator()
	{
		
	}

	public static boolean compare(String str1, String str2) 
	{
		String aux1 =  str1;
		String aux2 =  str2;
		if (aux1 == null)
		{
			aux1="";
		}
		
		if (aux2 == null)
		{
			aux2="";
		}
		
		aux1 = aux1.toLowerCase().replace("_", "");
		aux2 = aux2.toLowerCase().replace("_", "");
		
		
		return (aux1.compareTo(aux2)==0);
	}
	
	
}
