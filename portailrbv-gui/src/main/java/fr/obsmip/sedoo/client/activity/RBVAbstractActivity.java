package fr.obsmip.sedoo.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.sedoo.commons.client.event.ActivityChangeEvent;

public abstract class RBVAbstractActivity extends AbstractActivity {
	
	protected ClientFactory clientFactory;
	
	public RBVAbstractActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	protected void broadcastActivityTitle(String title)
	{
		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
	}
	
	

}
