package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.client.event.ActivityStartEventHandler;
import fr.obsmip.sedoo.client.event.UserLoginEventHandler;
import fr.obsmip.sedoo.client.event.UserLogoutEventHandler;

public interface MenuView  extends IsWidget, UserLoginEventHandler, UserLogoutEventHandler, ActivityStartEventHandler
{
	
	
}
