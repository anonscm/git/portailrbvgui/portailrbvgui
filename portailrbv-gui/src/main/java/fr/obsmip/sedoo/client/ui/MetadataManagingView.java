package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;

public interface MetadataManagingView extends IsWidget
{

	void setPresenter(Presenter presenter);


	/**
	 * Retourne les noms des differents observatoires présents dans les fiches
	 * 
	 * @param result
	 */
	void setObservatoriesFromEntries(ArrayList<String> result);

	void setEntries(int start, List<SummaryDTO> result);

	void reset();

	void setHits(Integer result);

	public interface Presenter
	{

		void editMetadata(String uuid, String observatoryShortLabel);

		void deleteMetadata(String uuid, String title);

		void viewMetadata(String uuid);

		void setCriteria(SearchCriteriaDTO criteria);

		void getEntries(int i);

	}

	void setObservatoriesFromEntries(List<ObservatoryDTO> observatories);

}
