package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.metadata.client.activity.FormatListActivity;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.place.FormatListPlace;

public class RBVFormatManagementActivity extends FormatListActivity
{

	public RBVFormatManagementActivity(FormatListPlace place,
			ClientFactory clientFactory) {
		super(place, clientFactory);
	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		super.start(containerWidget, eventBus);
		ActivityHelper.broadcastActivityTitle(clientFactory.getEventBus(), MetadataMessage.INSTANCE.formatViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getFormatListShortcut());
		((ClientFactory) clientFactory).getBreadCrumb().refresh(shortcuts);
	}
	
	@Override
	protected boolean isValidUser() {
		if (isLoggedUser())
		{
			return ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser().isAdmin();
		}
		else
		{
			return false;
		}
	}
	
}
