package fr.obsmip.sedoo.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;

@RemoteServiceRelativePath("harvest")
public interface HarvestManagementService extends RemoteService {

	boolean execute() throws ServiceException;

}

