package fr.obsmip.sedoo.client.misc;

import com.google.gwt.user.client.Window;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.shared.domain.ParameterConstant;

public class MetadataXmlHandler {
	public static void xml(String uuid)
	{
		String parameter = PortailRBV.getParameter(ParameterConstant.XML_SERVICE_URL_PARAMETER_NAME);
		if (parameter != null)
		{
			Window.open(parameter+"/"+uuid, "_blank", "");
		}
	}
}
