package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class SearchStartEvent extends GwtEvent<SearchStartEventHandler> {

	public static final Type<SearchStartEventHandler> TYPE = new Type<SearchStartEventHandler>();

	public SearchStartEvent() {
	}

	@Override
	protected void dispatch(SearchStartEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SearchStartEventHandler> getAssociatedType() {
		return TYPE;
	}

}
