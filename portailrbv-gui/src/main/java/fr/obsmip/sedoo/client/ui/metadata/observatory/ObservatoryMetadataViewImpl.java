package fr.obsmip.sedoo.client.ui.metadata.observatory;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.ObservatoryEntryView;
import fr.obsmip.sedoo.client.ui.breadcrumb.MetadataBreadcrumb;
import fr.obsmip.sedoo.client.ui.metadata.common.ConstraintTab;
import fr.obsmip.sedoo.client.ui.metadata.common.ContactTab;
import fr.obsmip.sedoo.client.ui.metadata.common.KeywordTab;
import fr.obsmip.sedoo.client.ui.metadata.common.serie.OtherTab;
import fr.obsmip.sedoo.client.ui.metadata.common.serie.TemporalExtendTab;
import fr.obsmip.sedoo.shared.domain.ObservatoryDTO;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.map.impl.AreaSelectorWidget;
import fr.sedoo.commons.shared.domain.AbstractDTO;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.AbstractDTOEditingView;
import fr.sedoo.metadata.client.ui.view.MetadataDisplayingView;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.view.common.MetadataTabPanel;
import fr.sedoo.metadata.client.ui.view.presenter.DisplayPresenter;
import fr.sedoo.metadata.client.ui.view.presenter.EditingPresenter;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.rbv.thesauri.RBVThesauriFactory;

public class ObservatoryMetadataViewImpl extends AbstractDTOEditingView
		implements ObservatoryEntryView, MetadataDisplayingView {

	private static MetadataEditingViewImplUiBinder uiBinder = GWT
			.create(MetadataEditingViewImplUiBinder.class);

	private ObservatoryEntryPresenter presenter;

	ArrayList<AbstractTab> tabs = new ArrayList<AbstractTab>();

	interface MetadataEditingViewImplUiBinder extends
			UiBinder<Widget, ObservatoryMetadataViewImpl> {
	}

	@UiField
	Button backButton;

	@UiField
	Button printButton;

	@UiField
	Button xmlButton;

	@UiField
	Button saveButton;

	@UiField
	Button validateButton;

	@UiField
	MetadataBreadcrumb breadcrumb;

	@UiField(provided = true)
	MetadataTabPanel tabPanel;

	private String uuid;

	private String hierarchyLevel;

	private String hierarchyLevelName;

	private ExperimentalSitesTab experimentalSitesTab;

	private String metadataLastModificationDate;

	private DisplayPresenter displayPresenter;

	private MultipleGeographicalLocationTab geographicalTab;

	private ContactTab contactTab;

	public ObservatoryMetadataViewImpl(ArrayList<String> displayLanguages,
			List<String> dataLanguages) {
		super();
		tabPanel = new MetadataTabPanel();

		tabPanel.addTab(new IdentificationTab(displayLanguages),
				MetadataMessage.INSTANCE
						.metadataEditingIdentificationTabHeader());
		contactTab = new ContactTab();
		tabPanel.addTab(contactTab,
				MetadataMessage.INSTANCE.metadataEditingContactTabHeader());

		ArrayList<String> thesaurusName = new ArrayList<String>();
		thesaurusName.add(RBVThesauriFactory.CLIMATE_THESAURUS);
		thesaurusName.add(RBVThesauriFactory.GEOLOGY_THESAURUS);
		thesaurusName.add(RBVThesauriFactory.LANDUSE_THESAURUS);

		tabPanel.addTab(new KeywordTab(thesaurusName),
				MetadataMessage.INSTANCE.metadataEditingKeywordTabHeader());
		geographicalTab = new MultipleGeographicalLocationTab(
				AreaSelectorWidget.DEFAULT_MAP_LAYER);
		tabPanel.addTab(geographicalTab,
				Message.INSTANCE.metadataEditingGeographicalLocationTabHeader());
		tabPanel.addTab(new TemporalExtendTab(),
				Message.INSTANCE.metadataEditingTemporalExtentHeader());
		tabPanel.addTab(new ConstraintTab(displayLanguages),
				MetadataMessage.INSTANCE.metadataEditingConstraintTabHeader());
		tabPanel.addTab(new OtherTab(displayLanguages, dataLanguages),
				Message.INSTANCE.metadataEditingOtherTabHeader());
		experimentalSitesTab = new ExperimentalSitesTab();
		tabPanel.addTab(experimentalSitesTab,
				MetadataMessage.INSTANCE.experimentalSites());
		tabPanel.activateSelectionHandler();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		reset();

	}

	@Override
	public void reset() {
		breadcrumb.reset();
		breadcrumb.setLevel(MetadataBreadcrumb.OBSERVATORY_LEVEL);
		tabPanel.reset();
		ElementUtil.hide(xmlButton);
		ElementUtil.hide(printButton);
		ElementUtil.hide(saveButton);
		ElementUtil.hide(validateButton);
		uuid = "";
	}

	@Override
	public void edit(AbstractDTO dto) {
		reset();
		ObservatoryDTO metadata = (ObservatoryDTO) dto;
		breadcrumb.setObservatoryName(metadata.getObservatoryName()
				.toUpperCase());
		tabPanel.edit(metadata);
		ElementUtil.hide(xmlButton);
		ElementUtil.hide(printButton);
		ElementUtil.show(saveButton);
		ElementUtil.show(validateButton);
		uuid = StringUtil.trimToEmpty(metadata.getOtherPart().getUuid());
		hierarchyLevel = metadata.getIdentificationPart().getHierarchyLevel();
		hierarchyLevelName = metadata.getIdentificationPart()
				.getHierarchyLevelName();
		metadataLastModificationDate = StringUtil.trimToEmpty(metadata
				.getOtherPart().getMetadataLastModificationDate());
	}

	@Override
	public void display(AbstractDTO dto) {
		reset();
		ObservatoryDTO metadata = (ObservatoryDTO) dto;
		breadcrumb.setObservatoryName(metadata.getObservatoryName()
				.toUpperCase());
		tabPanel.display(metadata);
		uuid = StringUtil.trimToEmpty(metadata.getOtherPart().getUuid());
		ElementUtil.show(xmlButton);
		ElementUtil.show(printButton);
		ElementUtil.hide(saveButton);
		ElementUtil.hide(validateButton);
	}

	@Override
	public MetadataDTO flush() {
		MetadataDTO metadataDTO = new ObservatoryDTO();
		tabPanel.flush(metadataDTO);
		metadataDTO.getIdentificationPart().setHierarchyLevel(hierarchyLevel);
		metadataDTO.getIdentificationPart().setHierarchyLevelName(
				hierarchyLevelName);
		metadataDTO.getOtherPart().setUuid(uuid);
		metadataDTO.getOtherPart().setMetadataLastModificationDate(
				metadataLastModificationDate);
		return metadataDTO;
	}

	@UiHandler("validateButton")
	void onValidateButtonClicked(ClickEvent event) {
		MetadataDTO dto = flush();
		presenter.validate(dto);
	}

	@UiHandler("printButton")
	void onPrintButtonClicked(ClickEvent event) {
		if (StringUtil.isNotEmpty(uuid)) {
			displayPresenter.print(uuid);
		}
	}

	@UiHandler("xmlButton")
	void onXmlButtonClicked(ClickEvent event) {
		if (StringUtil.isNotEmpty(uuid)) {
			displayPresenter.xml(uuid);
		}
	}

	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event) {
		if (presenter != null) {
			presenter.back();
		} else if (presenter != null) {
			presenter.back();
		}
	}

	@UiHandler("saveButton")
	void onSaveButtonClicked(ClickEvent event) {
		MetadataDTO dto = flush();
		presenter.save(dto);
	}

	@Override
	public void setEditingPresenter(EditingPresenter editingPresenter) {
		// Nothing is done this way
	}

	@Override
	public void setDisplayPresenter(DisplayPresenter displayPresenter) {
		this.displayPresenter = displayPresenter;
	}

	@Override
	public void setPresenter(ObservatoryEntryPresenter presenter) {
		this.presenter = presenter;
		experimentalSitesTab.setPresenter(presenter);
		geographicalTab.setPresenter(presenter);
		contactTab.setPresenter(presenter);

	}

	@Override
	public void broadcastExperimentalSiteDeletion(String uuid) {
		experimentalSitesTab.broadcastExperimentalSiteDeletion(uuid);

	}

	@Override
	public void setGeographicalBoxes(ArrayList<GeographicBoundingBoxDTO> boxes) {
		geographicalTab.setBoxes(boxes);
	}

}
