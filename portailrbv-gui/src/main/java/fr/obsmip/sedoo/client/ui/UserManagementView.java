package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.List;

import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.crud.component.CrudView;

public interface UserManagementView extends CrudView {

	public void setUsers(List<RBVUserDTO> users);
	public void setObservatories(ArrayList<? extends IsObservatory> observatories);
}
