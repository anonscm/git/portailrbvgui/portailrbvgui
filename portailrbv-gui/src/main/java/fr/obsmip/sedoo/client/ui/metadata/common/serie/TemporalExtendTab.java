package fr.obsmip.sedoo.client.ui.metadata.common.serie;

import fr.obsmip.sedoo.client.ui.metadata.RBVLabelFactory;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.constant.FieldConstant;
import fr.sedoo.metadata.client.ui.widget.field.impl.EndDate;
import fr.sedoo.metadata.client.ui.widget.field.impl.StartDate;

public class TemporalExtendTab extends AbstractTab {


	public TemporalExtendTab() {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingCoveredPeriods());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.STARTING_DATE));
		addRightComponent(new StartDate());
		addLeftComponent(RBVLabelFactory.getLabelByKey(FieldConstant.ENDING_DATE));
		addRightComponent(new EndDate());
		reset();
	}

}
