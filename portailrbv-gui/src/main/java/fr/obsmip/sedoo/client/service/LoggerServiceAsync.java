package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.LogDTO;

public interface LoggerServiceAsync {

	void add(LogDTO message, AsyncCallback<Void> callback);

	void findPage(int pageNumber, int pageSize,
			AsyncCallback<ArrayList<LogDTO>> callback);

	void getHits(AsyncCallback<Integer> callback);

}
