package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.habilitation.HabilitationUtil;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.misc.ClientObservatoryList;
import fr.obsmip.sedoo.client.place.DirectoryManagementPlace;
import fr.obsmip.sedoo.client.service.DirectoryService;
import fr.obsmip.sedoo.client.service.DirectoryServiceAsync;
import fr.obsmip.sedoo.client.ui.DirectoryManagementView;
import fr.obsmip.sedoo.client.ui.DirectoryManagementView.Presenter;
import fr.obsmip.sedoo.shared.api.IsObservatory;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;
import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public class DirectoryManagementActivity extends RBVAuthenticatedActivity implements Presenter, LoadCallBack<ArrayList<IsObservatory>> {


	public DirectoryManagementActivity(DirectoryManagementPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}

	private final static  DirectoryServiceAsync DIRECTORY_SERVICE = GWT.create(DirectoryService.class);
	DirectoryManagementView directoryManagementView;
	private AcceptsOneWidget containerWidget;


	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		this.containerWidget = containerWidget;
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		sendActivityStartEvent();
		directoryManagementView = clientFactory.getDirectoryManagementView();
		directoryManagementView.setPresenter(this);
		containerWidget.setWidget(directoryManagementView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.directoryManagementViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getDirectoryManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		directoryManagementView.reset();
		ClientObservatoryList.getObservatories(this);
	}


	@Override
	public void deletePerson(final String id) 
	{
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstant.PERSON_DELETING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		DIRECTORY_SERVICE.deletePerson(id, new DefaultAbstractCallBack<Boolean>(e, clientFactory.getEventBus()) 
		{
			@Override
			public void onSuccess(Boolean result) {
				super.onSuccess(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				directoryManagementView.broadcastPersonDeletion(id);
			}
		});
	}

//	@Override
//	public void goToEditPlace(ExperimentalSiteSummaryDTO experimentalSiteSummaryDTO, ObservatorySummaryDTO observatorySummaryDTO) 
//	{
//		ExperimentalSiteEditingPlace place = new ExperimentalSiteEditingPlace();
//		place.setExperimentalSiteName(experimentalSiteSummaryDTO.getName());
//		place.setExperimentalSiteUuid(experimentalSiteSummaryDTO.getUuid());
//		place.setParentSummary(observatorySummaryDTO);
//		clientFactory.getPlaceController().goTo(place);
//	}


	@Override
	public void postLoadProcess(ArrayList<IsObservatory> observatories) 
	{
		ArrayList<GeonetworkObservatoryDTO> aux = new ArrayList<GeonetworkObservatoryDTO>();
		Iterator<IsObservatory> iterator = observatories.iterator();
		while (iterator.hasNext()) {
			aux.add((GeonetworkObservatoryDTO) iterator.next());
		}
		directoryManagementView.setObservatories(HabilitationUtil.filterByUser(aux, (RBVUserDTO) clientFactory.getUserManager().getUser()));
		containerWidget.setWidget(directoryManagementView);
	}


	@Override
	public void loadPersons(String observatoryId) {
		
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.PERSON_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		DIRECTORY_SERVICE.loadPersons(observatoryId, new DefaultAbstractCallBack<ArrayList<PersonDTO>>(e, clientFactory.getEventBus()) 
				{
					@Override
					public void onSuccess(ArrayList<PersonDTO> result) {
						super.onSuccess(result);
						directoryManagementView.setPersons(result);

					}
				});
	}


	@Override
	public void save(final ObservatoryPersonDTO person) {
		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstant.PERSON_SAVING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		DIRECTORY_SERVICE.savePerson(person, new DefaultAbstractCallBack<Void>(e, clientFactory.getEventBus()) 
				{
					@Override
					public void onSuccess(Void result) {
						super.onSuccess(result);
						clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
						loadPersons(""+person.getObservatoryId());
					}
				});
	}

}
