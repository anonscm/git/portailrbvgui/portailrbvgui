package fr.obsmip.sedoo.client.ui.metadata.dataset;

import java.util.Iterator;
import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.ui.AbstractSection;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class DatasetEntryCreationViewImpl extends AbstractSection implements
		DatasetEntryCreationView {

	@UiField
	Alert noObservatoryToCreate;

	@UiField
	Alert noExperimentalSiteAivalable;

	@UiField
	ListBox observatories;

	@UiField
	ListBox experimentalSites;

	@UiField
	VerticalPanel observatoryPanel;

	@UiField
	HorizontalPanel creationPanel;

	@UiField
	Button createButton;

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	private Presenter presenter;

	interface WelcomeViewImplUiBinder extends
			UiBinder<Widget, DatasetEntryCreationViewImpl> {
	}

	public DatasetEntryCreationViewImpl() {
		super();
		GWT.<GlobalBundle> create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		noObservatoryToCreate.setVisible(true);
		observatoryPanel.setVisible(false);
		createButton.setEnabled(false);
	}

	@Override
	public void setObservatories(List<ObservatorySummaryDTO> observatoryList) {
		init(observatoryList);
		if (observatoryList.isEmpty() == false) {
			loadExperimentalSiteFromObservatoryUuid(observatoryList.get(
					ListUtil.FIRST_INDEX).getUuid());
		}
	}

	private void init(List<ObservatorySummaryDTO> observatoryList) {
		if (observatoryList.isEmpty() == false) {
			observatories.clear();
			Iterator<ObservatorySummaryDTO> iterator = observatoryList
					.iterator();
			while (iterator.hasNext()) {
				MetadataSummaryDTO current = iterator.next();
				observatories.addItem(current.getName(), current.getUuid());
			}
			observatoryPanel.setVisible(true);
			noObservatoryToCreate.setVisible(false);
		} else {
			observatories.clear();
			observatoryPanel.setVisible(false);
			noObservatoryToCreate.setVisible(true);
		}
	}

	@UiHandler("createButton")
	void onCreateButtonClicked(ClickEvent event) {
		createButton.setEnabled(false);
		ObservatorySummaryDTO observatory = new ObservatorySummaryDTO();
		observatory.setName(observatories.getItemText(observatories
				.getSelectedIndex()));
		observatory.setUuid(observatories.getValue(observatories
				.getSelectedIndex()));

		ExperimentalSiteSummaryDTO experimentalSite = new ExperimentalSiteSummaryDTO();
		experimentalSite.setName(experimentalSites
				.getItemText(experimentalSites.getSelectedIndex()));
		experimentalSite.setUuid(experimentalSites.getValue(experimentalSites
				.getSelectedIndex()));

		presenter.create(observatory, experimentalSite);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void reset() {
		observatories.clear();
		experimentalSites.clear();
		createButton.setEnabled(true);
	}

	@UiHandler("observatoryEntryCreationLink")
	void onSaveButtonClicked(ClickEvent event) {
		presenter.goToObservatoryEntryCreationPlace();
	}

	@Override
	public void setExperimentalSites(
			List<ExperimentalSiteSummaryDTO> experimentalSitesList) {
		if (experimentalSitesList.isEmpty() == false) {
			experimentalSites.clear();
			createButton.setEnabled(true);
			Iterator<ExperimentalSiteSummaryDTO> iterator = experimentalSitesList
					.iterator();
			while (iterator.hasNext()) {
				MetadataSummaryDTO current = iterator.next();
				experimentalSites.addItem(current.getName(), current.getUuid());
			}
			creationPanel.setVisible(true);
			noExperimentalSiteAivalable.setVisible(false);
		} else {
			experimentalSites.clear();
			creationPanel.setVisible(false);
			noExperimentalSiteAivalable.setVisible(true);
		}
	}

	@Override
	public void selectObservatoryAndExperimentaSite(String observatoryName,
			String experimentalSiteName) {
		// TODO Auto-generated method stub
	}

	@Override
	public void selectObservatory(String observatoryName) {
		int itemCount = observatories.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			String aux = observatories.getValue(i);
			if (aux.compareToIgnoreCase(StringUtil.trimToEmpty(observatoryName)) == 0) {
				observatories.setSelectedIndex(i);
				break;
			}
		}
	}

	@UiHandler("observatories")
	void onObservatoriesChanged(ChangeEvent event) {
		loadExperimentalSiteFromObservatoryUuid(observatories
				.getValue(observatories.getSelectedIndex()));
	}

	@Override
	public void setObservatories(
			List<ObservatorySummaryDTO> observatoriesValues,
			String observatoryName) {
		init(observatoriesValues);
		selectObservatory(observatoryName);
		if (observatoriesValues.isEmpty() == false) {
			loadExperimentalSiteFromObservatoryUuid(observatories
					.getValue(observatories.getSelectedIndex()));
		}
	}

	private void loadExperimentalSiteFromObservatoryUuid(String uuid) {
		experimentalSites.clear();
		createButton.setEnabled(false);
		noExperimentalSiteAivalable.setVisible(false);
		presenter.loadExperimentalSiteFromObservatoryUuid(uuid);
	}

	@Override
	public void selectExperimentalSiteFromUuid(String uuid) {
		int itemCount = experimentalSites.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			String aux = experimentalSites.getValue(i);
			if (aux.compareToIgnoreCase(StringUtil.trimToEmpty(uuid)) == 0) {
				experimentalSites.setSelectedIndex(i);
				break;
			}
		}

	}

	@UiHandler("createExperimentalSiteEntryLink")
	void onExperimentalSiteEntryCreationLinkClicked(ClickEvent event) {
		presenter.goToExperimentalSiteEntryCreationPlace(observatories
				.getItemText(observatories.getSelectedIndex()));
	}

}
