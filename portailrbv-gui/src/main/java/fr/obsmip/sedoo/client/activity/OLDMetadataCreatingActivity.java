package fr.obsmip.sedoo.client.activity;


public class OLDMetadataCreatingActivity 
{
//	extends RBVAuthenticatedActivity implements MetadataCreatingView.Presenter
//
//{
//	private MetadataCreatingView view;
//
//	private final ObservatoryServiceAsync observatoryService = GWT.create(ObservatoryService.class);
//
//	public MetadataCreatingActivity(MetadataCreatingPlace place, ClientFactory clientFactory)
//	{
//		super(clientFactory, place);
//	}
//
//	@Override
//	public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
//	{
//		if (isValidUser() == false)
//		{
//			goToLoginPlace();
//			return;
//		}
//		sendActivityStartEvent();
//		view = clientFactory.getMetadataCreatingView();
//		view.setPresenter(this);
//		containerWidget.setWidget(view.asWidget());
//		broadcastActivityTitle(Message.INSTANCE.metadataProviderMenuManageMetadata());
//		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//		shortcuts.add(ShortcutFactory.getMetadataManagementShortcut());
//		clientFactory.getBreadCrumb().refresh(shortcuts);
//		final ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.loading(), ActionEventConstant.OBSERVATORIES_LOADING_EVENT, true);
//		clientFactory.getEventBus().fireEvent(e);
//
//		observatoryService.getObservatories(new AsyncCallback<List<ObservatoryDTO>>()
//		{
//
//			@Override
//			public void onSuccess(List<ObservatoryDTO> observatories)
//			{
//
//				view.setObservatories(ObservatoryDTO.filterByUser(observatories, (RBVUserDTO) PortailRBV.getClientFactory().getUserManager().getUser()));
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//				clientFactory.getEventBus().fireEvent(e.getEndingEvent());
//			}
//		});
//	}
//
//	
//	@Override
//	public void createMetadata(Long drainageBasinId)
//	{
//
//		MetadataEditingPlace place = new MetadataEditingPlace();
//		place.setMode(Constants.CREATE);
//		place.setDrainageBasinId(drainageBasinId);
//		clientFactory.getPlaceController().goTo(place);
//	}
//
//
//	@Override
//	public void getDrainageBasins(Long observatoryId)
//	{
//		ActionStartEvent e = new ActionStartEvent(Message.INSTANCE.loading(), ActionEventConstant.DRAINAGE_BASIN_LOADING_EVENT, true);
//		clientFactory.getEventBus().fireEvent(e);
//		observatoryService.getObservatoryById(observatoryId, new AsyncCallback<ObservatoryDTO>()
//		{
//
//			@Override
//			public void onSuccess(ObservatoryDTO observatory)
//			{
//				view.setDrainageBasins(observatory.getDrainageBasinDTOs());
//				ActionEndEvent e = new ActionEndEvent(ActionEventConstant.DRAINAGE_BASIN_LOADING_EVENT);
//				clientFactory.getEventBus().fireEvent(e);
//			}
//
//			@Override
//			public void onFailure(Throwable caught)
//			{
//				DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened() + " " + caught.getMessage());
//				ActionEndEvent e = new ActionEndEvent(ActionEventConstant.DRAINAGE_BASIN_LOADING_EVENT);
//				clientFactory.getEventBus().fireEvent(e);
//			}
//		});
//	}

}
