package fr.obsmip.sedoo.client.ui.breadcrumb;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ExperimentalSiteNavigationEvent;
import fr.obsmip.sedoo.client.event.ObservatoryNavigationEvent;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MaximizeEventHandler;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.event.MinimizeEventHandler;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.editing.api.Resetable;

public class MetadataBreadcrumb extends ResizeComposite implements Resetable,
		MaximizeEventHandler, MinimizeEventHandler {

	private static final String EMPTY_STRING = "-";
	public static final int OBSERVATORY_LEVEL = 1;
	public static final int EXPERIMENTAL_SITE_LEVEL = 2;
	public static final int DATASET_LEVEL = 3;

	public static final String DISPLAY_MODE = "DISPLAY";
	public static final String EDIT_MODE = "EDIT";

	private final static int DEFAULT_FONT_SIZE = 18;

	private int currentFontSize = DEFAULT_FONT_SIZE;

	private String observatoryName = "";
	private String observatoryUuid;

	private String experimentalSiteName = "";
	private String experimentalSiteUuid;

	private String metadataTitle;

	private String mode = DISPLAY_MODE;

	@UiField
	HTMLPanel mainContainer;

	@UiField
	Anchor level1;

	@UiField
	Anchor level2;

	@UiField
	Anchor level3;

	@UiField
	LIElement level1container;

	@UiField
	LIElement level2container;

	@UiField
	LIElement level3container;

	@UiField
	UListElement ulContainer;

	private static MetadataBreadcrumbUiBinder uiBinder = GWT
			.create(MetadataBreadcrumbUiBinder.class);
	private int level;

	interface MetadataBreadcrumbUiBinder extends
			UiBinder<Widget, MetadataBreadcrumb> {
	}

	public MetadataBreadcrumb() {
		initWidget(uiBinder.createAndBindUi(this));
		EventBus eventBus = PortailRBV.getClientFactory().getEventBus();
		eventBus.addHandler(MaximizeEvent.TYPE, this);
		eventBus.addHandler(MinimizeEvent.TYPE, this);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				checkFontSize();
			}
		});
	}

	public void setObservatoryName(String observatoryName) {
		if (StringUtil.isNotEmpty(observatoryName)) {
			this.observatoryName = observatoryName.toUpperCase();
		} else {
			this.observatoryName = EMPTY_STRING;
		}
		level1.setText(this.observatoryName);
		ensureVisibility();
		checkFontSize();
	}

	private void ensureVisibility() {

		if ((observatoryName.compareTo(EMPTY_STRING) != 0)
				&& (experimentalSiteName.compareTo(EMPTY_STRING) != 0)) {
			level3.getElement().getStyle().setPaddingLeft(55, Unit.PX);
		} else {
			level3.getElement().getStyle().setPaddingLeft(5, Unit.PX);
		}
		if ((observatoryName.compareTo(EMPTY_STRING) == 0)) {
			ElementUtil.hide(level1container);
		} else {
			ElementUtil.show(level1container);
		}
		if ((experimentalSiteName.compareTo(EMPTY_STRING) == 0)) {
			ElementUtil.hide(level2container);
		} else {
			ElementUtil.show(level2container);
		}
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setExperimentalSiteName(String experimentSiteName) {
		if (StringUtil.isNotEmpty(experimentSiteName)) {
			this.experimentalSiteName = experimentSiteName.toUpperCase();
		} else {
			this.experimentalSiteName = EMPTY_STRING;
		}
		level2.setText(this.experimentalSiteName);
		ensureVisibility();
		checkFontSize();
	}

	public String getExperimentalSiteName() {
		return experimentalSiteName;
	}

	public String getMetadataTitle() {
		return metadataTitle;
	}

	public void setMetadataTitle(String metadataTitle) {
		this.metadataTitle = metadataTitle;
		level3.setText(metadataTitle);
		level3.setTitle(metadataTitle);
		ensureVisibility();
		checkFontSize();
	}

	/**
	 * Fonction vérifiant que la taille de la police permet l'affichage complet
	 * des informations
	 */
	private void checkFontSize() {
		int offsetWidth = mainContainer.getOffsetWidth();
		if (offsetWidth > 0) {
			// Le composant est affiché
			if (ulContainer.getClientHeight() > level1container
					.getClientHeight()) {
				// L'ul est plus haut que le 1er li, cela signifie qu'on est sur
				// deux lignes.
				String aux = level3.getText();
				int length = aux.length();
				if (length > 11) {
					aux = aux.substring(0, length - 10);
				}
				level3.setText(aux + "...");

				// On verifie la taille dans quelques instants
				Timer timer = new Timer() {
					public void run() {
						checkFontSize();
					}
				};
				timer.schedule(30);

			}
		}
	}

	@Override
	public void reset() {
		setExperimentalSiteName("");
		setObservatoryName("");
		setMetadataTitle("");
		setObservatoryUuid("");
		ensureVisibility();
		currentFontSize = DEFAULT_FONT_SIZE;
		// updateFontSize();
	}

	// private void updateFontSize() {
	// // ulContainer.getStyle().setFontSize(currentFontSize, Unit.PX);
	// level3container.getStyle().setFontSize(currentFontSize, Unit.PX);
	//
	// }

	@UiHandler("level1")
	void onObservatoryClicked(ClickEvent event) {
		if (level != OBSERVATORY_LEVEL) {
			if (StringUtil.isNotEmpty(getObservatoryUuid())) {
				PortailRBV
						.getClientFactory()
						.getEventBus()
						.fireEvent(
								new ObservatoryNavigationEvent(
										getObservatoryName(), mode,
										getObservatoryUuid()));
			}
		}
	}

	@UiHandler("level2")
	void onExperimentalSiteClicked(ClickEvent event) {
		if (level != EXPERIMENTAL_SITE_LEVEL) {
			if (StringUtil.isNotEmpty(getExperimentalSiteUuid())) {
				PortailRBV
						.getClientFactory()
						.getEventBus()
						.fireEvent(
								new ExperimentalSiteNavigationEvent(
										getExperimentalSiteName(), mode,
										getExperimentalSiteUuid()));
			}
		}
	}

	public void setLevel(int level) {
		this.level = level;
		if (level == OBSERVATORY_LEVEL) {
			ElementUtil.hide(level2container);
			ElementUtil.hide(level3container);
			level1.getElement().getStyle().setCursor(Style.Cursor.DEFAULT);
			level2.getElement().getStyle().setCursor(Style.Cursor.POINTER);
		} else if (level == EXPERIMENTAL_SITE_LEVEL) {
			ElementUtil.show(level2container);
			ElementUtil.hide(level3container);
			level1.getElement().getStyle().setCursor(Style.Cursor.POINTER);
			level2.getElement().getStyle().setCursor(Style.Cursor.DEFAULT);
		} else if (level == DATASET_LEVEL) {
			ElementUtil.show(level2container);
			ElementUtil.show(level3container);
		}
	}

	public void enableDisplayMode() {
		mode = DISPLAY_MODE;
	}

	public void enableEditMode() {
		mode = EDIT_MODE;
	}

	public String getObservatoryUuid() {
		return observatoryUuid;
	}

	public void setObservatoryUuid(String observatoryUuid) {
		this.observatoryUuid = observatoryUuid;
	}

	public String getExperimentalSiteUuid() {
		return experimentalSiteUuid;
	}

	public void setExperimentalSiteUuid(String experimentalSiteUuid) {
		this.experimentalSiteUuid = experimentalSiteUuid;
	}

	@Override
	public void onResize() {
		if (level3 != null) {
			level3.setText(StringUtil.trimToEmpty(metadataTitle));
		}
		checkFontSize();
		super.onResize();
	}

	@Override
	public void onNotification(MinimizeEvent event) {
		onResize();

	}

	@Override
	public void onNotification(MaximizeEvent event) {
		onResize();

	}

}
