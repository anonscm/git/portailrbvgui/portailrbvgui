package fr.obsmip.sedoo.client.ui;

import fr.sedoo.commons.client.news.widget.NewsArchiveViewImpl;

public class RBVNewsArchiveViewImpl extends NewsArchiveViewImpl {

	public RBVNewsArchiveViewImpl() {
		super();
		//getBackButton().removeStyleName("btn-primary");
		getBackButton().setVisible(false);
		getTitleLabel().setVisible(false);
	}
	
}
