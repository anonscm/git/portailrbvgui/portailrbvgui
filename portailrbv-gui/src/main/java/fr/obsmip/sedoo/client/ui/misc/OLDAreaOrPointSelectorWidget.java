package fr.obsmip.sedoo.client.ui.misc;


public class OLDAreaOrPointSelectorWidget {

//extends OLDAreaSelectorWidget implements RectangularAreaListener
//{
//
//	protected Style markerStyle;
//	protected Style selectedMarkerStyle;
//	protected VectorFeature markerFeature;
//	protected Vector markerLayer;
//	protected DrawFeature drawMarkerControl;
//	protected SelectFeature hoverMarkerSelectFeature;
//	protected ToggleButton drawMarkerButton;
//	protected ToggleButton eraseMarkerButton;
//	private List<ToggleButton> buttonList;
//
//	@UiConstructor
//	public OLDAreaOrPointSelectorWidget(String width)
//	{
//		super(width);
//	}
//
//	@Override
//	protected void init()
//	{
//		drawMarkerButton = new ToggleButton(new Image(GlobalBundle.INSTANCE.siteDraw()));
//		drawMarkerButton.setTitle(Message.INSTANCE.drawSiteButtonTooltip());
//		eraseMarkerButton = new ToggleButton(new Image(GlobalBundle.INSTANCE.siteDelete()));
//		eraseMarkerButton.setTitle(Message.INSTANCE.eraseSiteButtonTooltip());
//		markerStyle = new Style();
//		markerStyle.setExternalGraphic("http://maps.google.com/mapfiles/ms/micons/blue.png");
//		markerStyle.setGraphicSize(20, 20);
//		markerStyle.setFillOpacity(1);
//		markerStyle.setGraphicOffset(-9, -20);
//		markerStyle.setCursor("default");
//
//		selectedMarkerStyle = new Style();
//		selectedMarkerStyle.setExternalGraphic("http://maps.google.com/mapfiles/ms/micons/red.png");
//		selectedMarkerStyle.setGraphicSize(20, 20);
//		selectedMarkerStyle.setFillOpacity(1);
//		selectedMarkerStyle.setGraphicOffset(-9, -20);
//		selectedMarkerStyle.setCursor("pointer");
//
//		markerLayer = new Vector("MarkerLayer");
//
//		MarkerLayerListener markerLayerListener = new MarkerLayerListener();
//
//		PointHandler pointHandler = new PointHandler();
//		drawMarkerControl = new DrawFeature(markerLayer, pointHandler);
//
//		SelectFeatureOptions markerHoverSelectFeatureOptions = new SelectFeatureOptions();
//		markerHoverSelectFeatureOptions.setHover();
//		markerHoverSelectFeatureOptions.clickFeature(markerLayerListener);
//		hoverMarkerSelectFeature = new SelectFeature(markerLayer, markerHoverSelectFeatureOptions);
//
//		drawMarkerControl.deactivate();
//		hoverMarkerSelectFeature.deactivate();
//
//		markerLayer.addVectorFeatureAddedListener(markerLayerListener);
//		markerLayer.addVectorFeatureSelectedListener(markerLayerListener);
//		markerLayer.addVectorFeatureUnselectedListener(markerLayerListener);
//
//		super.init();
//
//		map.addLayer(markerLayer);
//		map.addControl(drawMarkerControl);
//		map.addControl(hoverMarkerSelectFeature);
//
//		addListener(this);
//
//	}
//
//	@Override
//	protected List<ToggleButton> getToolBarButtons()
//	{
//		if (buttonList == null)
//		{
//			buttonList = new ArrayList<ToggleButton>();
//			buttonList.add(drawRectangularAreaButton);
//			buttonList.add(eraseRectangularAreaButton);
//			buttonList.add(drawMarkerButton);
//			buttonList.add(eraseMarkerButton);
//			buttonList.add(dragPanButton);
//		}
//		return buttonList;
//	}
//
//	class MarkerLayerListener implements VectorFeatureAddedListener, VectorFeatureSelectedListener, VectorFeatureUnselectedListener, ClickFeatureListener
//	{
//		@Override
//		public void onFeatureAdded(FeatureAddedEvent eventObject)
//		{
//
//			if (reactangularAreaFeature != null)
//			{
//				reactangularAreaFeature.destroy();
//			}
//
//			if (markerFeature != null)
//			{
//				markerFeature.destroy();
//			}
//
//			markerFeature = eventObject.getVectorFeature();
//			markerFeature.setStyle(markerStyle);
//			markerLayer.redraw();
//
//			Bounds bounds = markerFeature.getGeometry().getBounds();
//			LonLat centerLonLat = bounds.getCenterLonLat();
//			centerLonLat.transform(map.getProjection(), DEFAULT_PROJECTION.getProjectionCode());
//			northBoundLatitude.setText(format(centerLonLat.lat()));
//			eastBoundLongitude.setText(format(centerLonLat.lon()));
//			southBoundLatitude.setText(format(centerLonLat.lat()));
//			westBoundLongitude.setText(format(centerLonLat.lon()));
//
//		}
//
//		@Override
//		public void onFeatureSelected(FeatureSelectedEvent eventObject)
//		{
//			eventObject.getVectorFeature().setStyle(selectedMarkerStyle);
//			markerLayer.redraw();
//		}
//
//		@Override
//		public void onFeatureUnselected(FeatureUnselectedEvent eventObject)
//		{
//			eventObject.getVectorFeature().setStyle(markerStyle);
//			markerLayer.redraw();
//
//		}
//
//		@Override
//		public void onFeatureClicked(VectorFeature vectorFeature)
//		{
//			vectorFeature.destroy();
//			deleteMarker();
//		}
//	}
//
//	@Override
//	protected void deactivateAllControls()
//	{
//		super.deactivateAllControls();
//		if (drawMarkerControl != null)
//		{
//			drawMarkerControl.deactivate();
//		}
//		if (hoverMarkerSelectFeature != null)
//		{
//			hoverMarkerSelectFeature.deactivate();
//		}
//	}
//
//	@Override
//	public void reset()
//	{
//		markerLayer.destroyFeatures();
//		super.reset();
//	}
//
//	@Override
//	public void onClick(ClickEvent event)
//	{
//		super.onClick(event);
//
//		if (event.getSource() == drawMarkerButton)
//		{
//			drawMarkerControl.activate();
//		}
//
//		if (event.getSource() == eraseMarkerButton)
//		{
//			hoverMarkerSelectFeature.activate();
//		}
//
//	}
//
//	@Override
//	public void onRectangleAdded()
//	{
//		deleteMarker();
//	}
//
//	@Override
//	public void onRectangleChanged()
//	{
//		deleteMarker();
//	}
//	
//	private void deleteMarker()
//	{
//		if (markerFeature != null)
//		{
//			markerFeature.destroy();
//			resetTextFields();
//		}
//	}
//	
//	
//	public void center(GeographicBoundingBoxDTO box, boolean displayDefault)
//	{
//		if ((box != null) && ((box.isEmpty() == false)) && (box.validate().isEmpty()))
//		{
//			if (box.isPoint())
//			{
//				LonLat center = box.getRightLowerCorner();
//				center.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
//				Point point = new Point(center.lon(), center.lat());
//				VectorFeature pointFeature = new VectorFeature(point, selectedMarkerStyle);
//				markerLayer.addFeature(pointFeature);
//				markerFeature = pointFeature;
//				Bounds displayBounds = new Bounds();
//				displayBounds.extend(center);
//				map.zoomToExtent(displayBounds, true);
//				map.zoomTo(10);
//			}
//			else
//			{
//				super.center(box, displayDefault);
//			}
//		}
//		else
//		{
//			super.center(box, displayDefault);
//		}
////		if (box.isR)
////		LonLat rightLowerDisplay;
////		LonLat leftUpperDisplay;
////
////		boolean correctBox = false;
////
////		if (box.validate().isEmpty())
////		{
////			correctBox = true;
////			LonLat rightLower = box.getRightLowerCorner();
////			LonLat leftUpper = box.getLeftUpperCorner();
////
////			rightLowerDisplay = box.getRightLowerDisplayCorner();
////			leftUpperDisplay = box.getLeftUpperDisplayCorner();
////
////			rightLower.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
////			leftUpper.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
////
////			Bounds drainageBasinBounds = new Bounds();
////			drainageBasinBounds.extend(rightLower);
////			drainageBasinBounds.extend(leftUpper);
////			reactangularAreaFeature = new VectorFeature(drainageBasinBounds.toGeometry());
////			rectangularAreaLayer.addFeature(reactangularAreaFeature);
////		} else
////		{
////			// On affiche une carte complète du monde par défaut
////			rightLowerDisplay = new LonLat(-170, -80);
////			leftUpperDisplay = new LonLat(170, 80);
////		}
////
////		rightLowerDisplay.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
////		leftUpperDisplay.transform(DEFAULT_PROJECTION.getProjectionCode(), map.getProjection());
////
////		Bounds displayBounds = new Bounds();
////		displayBounds.extend(rightLowerDisplay);
////		displayBounds.extend(leftUpperDisplay);
////		if ((correctBox == true) || (displayDefault == true))
////		{
////			map.zoomToExtent(displayBounds, true);
////		}
//	}
//
//	

}
