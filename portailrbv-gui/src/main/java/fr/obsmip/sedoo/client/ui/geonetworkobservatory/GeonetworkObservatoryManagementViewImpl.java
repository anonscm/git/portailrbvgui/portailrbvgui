package fr.obsmip.sedoo.client.ui.geonetworkobservatory;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.crud.component.CrudViewImpl;
import fr.sedoo.commons.client.crud.widget.CrudTable;

public class GeonetworkObservatoryManagementViewImpl extends CrudViewImpl
		implements GeonetworkObservatoryManagementView {

	@UiField
	GeonetworkObservatoryTable table;

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends
			UiBinder<Widget, GeonetworkObservatoryManagementViewImpl> {
	}

	public GeonetworkObservatoryManagementViewImpl() {
		super();
		GWT.<GlobalBundle> create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		table.init(new ArrayList<GeonetworkObservatoryDTO>());
	}

	@Override
	public void setObservatories(ArrayList<GeonetworkObservatoryDTO> result) {
		table.init(result);
	}

	@Override
	public CrudTable getCrudTable() {
		return table;
	}

}
