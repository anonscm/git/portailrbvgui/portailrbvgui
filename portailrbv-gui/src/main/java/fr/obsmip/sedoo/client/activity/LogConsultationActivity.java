package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.LogConsultationPlace;
import fr.obsmip.sedoo.client.service.LoggerService;
import fr.obsmip.sedoo.client.service.LoggerServiceAsync;
import fr.obsmip.sedoo.client.ui.LogConsultationView;
import fr.obsmip.sedoo.shared.domain.LogDTO;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.longlist.LongListPresenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class LogConsultationActivity extends RBVAdministrationActivity implements LongListPresenter {
	
	private LogConsultationView view = null; 
	
	private final static LoggerServiceAsync LOGGER_SERVICE = GWT.create(LoggerService.class);
	
	public final static int DEFAULT_PAGE_SIZE = 10;
	private final static int DEFAULT_START_PAGE = 0;
	
	public LogConsultationActivity(LogConsultationPlace place, ClientFactory clientFactory) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		
		if (isValidUser() == false)
		{
			goToLoginPlace();
			return;
		}
		
		sendActivityStartEvent();
		view = clientFactory.getLogConsultationView();
		view.setLongListPresenter(this);
		containerWidget.setWidget(view.asWidget());
		broadcastActivityTitle(Message.INSTANCE.logConsultationViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		shortcuts.add(ShortcutFactory.getUserManagementShortcut());
		clientFactory.getBreadCrumb().refresh(shortcuts);
		
		LOGGER_SERVICE.getHits(new AsyncCallback<Integer>() {
			
			@Override
			public void onSuccess(Integer hits) {
				view.setHits(hits);
				loadEntries(DEFAULT_START_PAGE, DEFAULT_PAGE_SIZE);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
			}
		});
		
		
	}

	@Override
	public void getEntries(int position, int pageSize) 
	{
		loadEntries(position, pageSize);
	}
	
	
	private void loadEntries(final int position, int pageSize)
	{
		final ActionStartEvent startEvent = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstant.LOG_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);
		LOGGER_SERVICE.findPage(position, DEFAULT_PAGE_SIZE, new AsyncCallback<ArrayList<LogDTO>>() {

			@Override
			public void onSuccess(ArrayList<LogDTO> logs) {
				view.setEntries(position, logs);
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());

			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());
				clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());				
			}
		});
	}
}
