package fr.obsmip.sedoo.client.ui.searchcriteria;

import java.util.ArrayList;
import java.util.Iterator;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;

public class ErrorButtonContainer extends HorizontalPanel {

	private Button button;
	private Icon stepWarning;

	public ErrorButtonContainer(Button button) {
		super();
		setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
		this.button = button;
		add(button);
		stepWarning = new Icon(IconType.WARNING);
		stepWarning.setSize(IconSize.LARGE);
		stepWarning.getElement().getStyle().setMarginLeft(3, Unit.PX);
		add(stepWarning);
		ElementUtil.hide(stepWarning);

	}

	public void setValidationAlert(ArrayList<ValidationAlert> errors) {
		setErrors(ValidationAlert.toStringArrayList(errors));
	}

	public void setErrors(ArrayList<String> errors) {
		if (ListUtil.isNotEmpty(errors)) {
			ElementUtil.show(stepWarning);
			button.setEnabled(false);
			if (errors.size() == ListUtil.SINGLETON_SIZE) {
				stepWarning.setTitle(errors.get(ListUtil.FIRST_INDEX));
			} else {
				StringBuffer formattedMessage = new StringBuffer();
				Iterator<String> iterator = errors.iterator();
				while (iterator.hasNext()) {
					formattedMessage.append(" - " + iterator.next());
					if (iterator.hasNext()) {
						formattedMessage.append("\n");
					}
				}

				stepWarning.setTitle(formattedMessage.toString());
			}
		} else {
			button.setEnabled(true);
			ElementUtil.hide(stepWarning);
		}
	}
}
