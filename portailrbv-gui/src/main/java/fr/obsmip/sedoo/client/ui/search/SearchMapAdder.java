package fr.obsmip.sedoo.client.ui.search;

import org.gwtopenmaps.openlayers.client.control.Control;

import com.google.gwt.user.client.ui.ToggleButton;

import fr.sedoo.commons.client.widget.map.impl.FeatureAdder;

public class SearchMapAdder implements FeatureAdder {

	public final static String SEARCH_MAP_ADDER = "SEARCH_MAP_ADDER";

	@Override
	public String getType() {
		return SEARCH_MAP_ADDER;
	}

	@Override
	public ToggleButton getToggleButton() {
		return null;
	}

	@Override
	public Control getControl() {
		return null;
	}

}
