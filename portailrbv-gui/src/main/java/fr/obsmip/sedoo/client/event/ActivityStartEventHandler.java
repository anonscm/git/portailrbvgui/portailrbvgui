package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ActivityStartEventHandler extends EventHandler {
    void onNotification(ActivityStartEvent event);
}

