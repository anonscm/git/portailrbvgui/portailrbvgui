package fr.obsmip.sedoo.client.ui.metadata.common.contact;

import fr.obsmip.sedoo.client.ui.table.ObservatoryPersonTable;

public interface DirectoryPresenter {

	void loadDirectoryPersons(ObservatoryPersonTable table);
}
