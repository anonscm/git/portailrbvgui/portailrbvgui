package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.shared.domain.New;

@RemoteServiceRelativePath("news")
public interface NewsService extends RemoteService {
	
	ArrayList<New> getLatest(String preferredLanguage, List<String> alternateLanguages, ArrayList<String> displayLanguages) throws Exception;
	
	
}