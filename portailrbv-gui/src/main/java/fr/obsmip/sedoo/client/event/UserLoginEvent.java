package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.GwtEvent;

import fr.obsmip.sedoo.shared.domain.RBVUserDTO;

public class UserLoginEvent extends GwtEvent<UserLoginEventHandler>{

	public static final Type<UserLoginEventHandler> TYPE = new Type<UserLoginEventHandler>();
	private RBVUserDTO user;
	

	public UserLoginEvent(RBVUserDTO user)
	{
		this.user = user;
		
	}

	@Override
	protected void dispatch(UserLoginEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<UserLoginEventHandler> getAssociatedType() {
		return TYPE;
	}

	public RBVUserDTO getUser() {
		return user;
	}
}
