package fr.obsmip.sedoo.client.ui.searchcriteria.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.ui.searchcriteria.DisclosurePanelCustom;
import fr.obsmip.sedoo.client.ui.searchcriteria.MapCriteriaWidget;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.callback.LoadCallBack;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasValidation;
import fr.sedoo.metadata.client.ui.widget.field.impl.keyword.ClientThesaurusList;
import fr.sedoo.metadata.shared.domain.thesaurus.DefaultSingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class ThesaurusWidget implements HasValidation, MapCriteriaWidget,
		ClickHandler, LoadCallBack<Map<String, DefaultSingleLevelThesaurus>> {

	protected DisclosurePanelCustom panel;
	private VerticalPanel contentPanel;
	private String thesaurusName;
	private ArrayList<CheckBox> checkBoxes = new ArrayList<CheckBox>();

	public ThesaurusWidget(String thesaurusName, String thesaurusLabel) {
		this.thesaurusName = thesaurusName;
		panel = new DisclosurePanelCustom(thesaurusLabel);
		panel.setStyleName("customDisclosurePanel");
		panel.setWidth("100%");
		init();
	}

	@Override
	public Widget asWidget() {
		return panel;
	}

	public void init() {
		contentPanel = new VerticalPanel();
		contentPanel.getElement().getStyle().setMarginLeft(3, Unit.PX);
		contentPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		contentPanel.add(new Label(CommonMessages.INSTANCE.loading()));
		panel.add(contentPanel);
		panel.setOpen(true);
		ArrayList<String> thesaurusNames = new ArrayList<String>();
		thesaurusNames.add(thesaurusName);
		ClientThesaurusList.getThesauri(thesaurusNames, this);
	}

	@Override
	public void onClick(ClickEvent event) {

	}

	@Override
	public void reset() {
		for (CheckBox checkBox : checkBoxes) {
			checkBox.setValue(false);
		}
	}

	@Override
	public List<ValidationAlert> validate() {
		ArrayList<ValidationAlert> result = new ArrayList<ValidationAlert>();
		return result;
	}

	@Override
	public SearchCriteriaDTO flush(SearchCriteriaDTO criteria) {

		ArrayList<String> selectedValues = new ArrayList<String>();
		for (CheckBox checkBox : checkBoxes) {
			if (checkBox.getValue()) {
				selectedValues.add(checkBox.getText());
			}
		}
		criteria.addThesaurusValues(thesaurusName, selectedValues);
		return criteria;
	}

	@Override
	public void postLoadProcess(Map<String, DefaultSingleLevelThesaurus> result) {
		contentPanel.clear();

		DefaultSingleLevelThesaurus defaultSingleLevelThesaurus = result
				.get(thesaurusName);

		ArrayList<ThesaurusItem> keywords = defaultSingleLevelThesaurus
				.getKeywords();

		for (ThesaurusItem thesaurusItem : keywords) {
			CheckBox aux = new CheckBox(thesaurusItem.getValue());
			checkBoxes.add(aux);
			contentPanel.add(aux);
		}

	}
}