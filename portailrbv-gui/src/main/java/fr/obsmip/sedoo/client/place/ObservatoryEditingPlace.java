package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class ObservatoryEditingPlace extends Place implements AuthenticatedPlace
{

	private String observatoryUuid="";
	private String observatoryName="";
	
	public static ObservatoryEditingPlace instance;

	public static class Tokenizer implements PlaceTokenizer<ObservatoryEditingPlace>
	{
		@Override
		public String getToken(ObservatoryEditingPlace place)
		{
			if (place.getObservatoryUuid() == null)
			{
				return "";
			}
			else
			{
				return ""+place.getObservatoryUuid();
			}
			
		}

		@Override
		public ObservatoryEditingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ObservatoryEditingPlace();
				if ((token != null) && (token.trim().length()>0))
				{
					instance.setObservatoryUuid(token);
				}
			}
			return instance;
		}
	}

	public String getObservatoryUuid() {
		return observatoryUuid;
	}

	public void setObservatoryUuid(String observatoryUuid) {
		this.observatoryUuid = observatoryUuid;
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}

	
	
}
