package fr.obsmip.sedoo.client.ui.metadata.common;

import java.util.ArrayList;

import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.impl.Keywords;

public class KeywordTab extends AbstractTab {

	public KeywordTab(ArrayList<String> thesaurusNames) {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingKeywordTabHeader());
		addFullLineComponent(new Keywords(thesaurusNames));
		reset();
	}

}
