package fr.obsmip.sedoo.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.RBVUserDTO;
import fr.sedoo.commons.client.util.ServiceException;


@RemoteServiceRelativePath("google")
public interface GoogleService extends RemoteService {
	
	public RBVUserDTO getUserFromToken(String token) throws ServiceException;
}
