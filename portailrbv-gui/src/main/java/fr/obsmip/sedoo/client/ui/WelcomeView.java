package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.client.misc.SummaryPresenter;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public interface WelcomeView extends IsWidget {

	void setPresenter(Presenter presenter);

	boolean isContentLoaded();

	Presenter getPresenter();

	boolean isFirstLoad();
	void setFirstLoad(boolean firstLoad);
		
	public interface Presenter extends SummaryPresenter {
		void search(SearchCriteriaDTO searchCriteria);

		void displayEntryMetadata(String id, String level);

	}

}
