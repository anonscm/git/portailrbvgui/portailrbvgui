package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ExperimentalSiteNavigationEventHandler extends EventHandler {
    void onNotification(ExperimentalSiteNavigationEvent event);
}

