package fr.obsmip.sedoo.client.ui.metadata.common;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.Label;

import fr.obsmip.sedoo.client.ui.metadata.common.contact.DirectoryPresenter;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.RBVMetadataPointOfContacts;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.RBVOtherRoles;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.RBVOwners;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.RBVPrincipalInvestigators;
import fr.obsmip.sedoo.client.ui.metadata.common.contact.RBVResourcePointOfContacts;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.client.message.MetadataMessage;
import fr.sedoo.metadata.client.ui.label.RoleLabelProvider;
import fr.sedoo.metadata.client.ui.view.common.AbstractTab;
import fr.sedoo.metadata.client.ui.widget.field.impl.OtherRoles;

public class ContactTab extends AbstractTab {

	private RBVPrincipalInvestigators principalInvestigators;
	private RBVResourcePointOfContacts resourcePointOfContacts;
	private RBVOwners owners;
	private RBVMetadataPointOfContacts metadataPointOfContacts;
	private RBVOtherRoles otherRoles;

	public ContactTab() {
		super();
		addSection(MetadataMessage.INSTANCE.metadataEditingPrincipalInvestigators());
		addFullLineComponent(getInstructions(MetadataMessage.INSTANCE.principalInvestigatorRoleTooltip()));
		principalInvestigators = new RBVPrincipalInvestigators();
		addFullLineComponent(principalInvestigators);
		addSection(MetadataMessage.INSTANCE.metadataEditingDataPointsOfContact());
		addFullLineComponent(getInstructions(MetadataMessage.INSTANCE.pointOfContactRoleTooltip()));
		resourcePointOfContacts = new RBVResourcePointOfContacts();
		addFullLineComponent(resourcePointOfContacts);
		addSection(MetadataMessage.INSTANCE.metadataEditingOwners());
		addFullLineComponent(getInstructions(MetadataMessage.INSTANCE.ownerRoleTooltip()));
		owners = new RBVOwners();
		addFullLineComponent(owners);
		addSection(MetadataMessage.INSTANCE.metadataEditingMetadataPointsOfContact());
		addFullLineComponent(getInstructions(MetadataMessage.INSTANCE.metadataPointOfContactRoleTooltip()));
		metadataPointOfContacts = new RBVMetadataPointOfContacts();
		addFullLineComponent(metadataPointOfContacts);
		addSection(MetadataMessage.INSTANCE.metadataEditingOtherRoles());
		ArrayList<String> roles = new ArrayList<String>();
		roles.add(RoleLabelProvider.AUTHOR);
		roles.add(RoleLabelProvider.ORIGINATOR);
		roles.add(RoleLabelProvider.RESOURCE_PROVIDER);
		roles.add(RoleLabelProvider.DISTRIBUTOR);
		roles.add(RoleLabelProvider.CUSTODIAN);
		roles.add(RoleLabelProvider.PROCESSOR);
		roles.add(RoleLabelProvider.USER);
		roles.add(RoleLabelProvider.PUBLISHER);
		otherRoles = new RBVOtherRoles(roles);
		addFullLineComponent(otherRoles);
		reset();
	}

	protected Label getInstructions(String text) {
		Label label = new Label(StringUtil.trimToEmpty(text));
		label.setStyleName("instructions");
		return label;
	}

	public void setPresenter(DirectoryPresenter presenter) {
		principalInvestigators.setPresenter(presenter);
		resourcePointOfContacts.setPresenter(presenter);
		owners.setPresenter(presenter);
		metadataPointOfContacts.setPresenter(presenter);
		otherRoles.setPresenter(presenter);
	}

}
