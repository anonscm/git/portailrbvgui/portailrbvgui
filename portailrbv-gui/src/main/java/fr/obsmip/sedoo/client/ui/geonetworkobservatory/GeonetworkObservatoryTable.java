package fr.obsmip.sedoo.client.ui.geonetworkobservatory;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;

import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.sedoo.commons.client.crud.widget.CreateConfirmCallBack;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.client.crud.widget.EditConfirmCallBack;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.dialog.EditDialogContent;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.util.StringUtil;

public class GeonetworkObservatoryTable extends CrudTable {

	public GeonetworkObservatoryTable() {
		super();
		setAddButtonEnabled(true);
	}

	@Override
	public String getAddItemText() {
		return Message.INSTANCE.observatoryTableAddItem();
	}

	protected Column<HasIdentifier, String> colorCodeColumn;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void initColumns() {

		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				GeonetworkObservatoryDTO observatory = (GeonetworkObservatoryDTO) aux;
				return observatory.getName();
			}
		};

		Cell<String> colorCodeCell = new ColorCell<String>();
		colorCodeColumn = new Column(colorCodeCell) {

			@Override
			public String getValue(Object object) {
				return StringUtil
						.trimToEmpty(((GeonetworkObservatoryDTO) object)
								.getColor());
			}
		};

		table.addColumn(nameColumn, CommonMessages.INSTANCE.name());
		table.setColumnWidth(nameColumn, 100.0, Unit.PX);
		table.addColumn(colorCodeColumn, Message.INSTANCE.observatoryColor());
		table.setColumnWidth(colorCodeColumn, 50.0, Unit.PX);
		table.addColumn(editColumn);
		table.addColumn(deleteColumn);
		table.setColumnWidth(editColumn, 30.0, Unit.PX);
		table.setColumnWidth(deleteColumn, 30.0, Unit.PX);
	}

	@Override
	public String getEditDialogTitle() {
		return Message.INSTANCE.observatory();
	}

	@Override
	public String getCreateDialogTitle() {
		return Message.INSTANCE.observatory();
	}

	@Override
	public EditDialogContent getEditDialogContent(EditConfirmCallBack callBack,
			HasIdentifier hasIdentifier) {
		return new GeonetworkObservatoryEditDialogContent(callBack,
				(GeonetworkObservatoryDTO) hasIdentifier);
	}

	@Override
	public EditDialogContent getCreateDialogContent(
			CreateConfirmCallBack callBack, HasIdentifier hasIdentifier) {
		return new GeonetworkObservatoryEditDialogContent(callBack,
				(GeonetworkObservatoryDTO) hasIdentifier);
	}

	@Override
	public HasIdentifier createNewItem() {

		return new GeonetworkObservatoryDTO();
	}
}
