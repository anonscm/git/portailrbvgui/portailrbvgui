package fr.obsmip.sedoo.client.ui.map;

public class MarkerDefinition {
	
	private String dataSetMarkerUrl;
	private String experimentalSiteMarkerUrl;
	private String color;
	
	public String getColor() {
		return color;
	}
	
	public MarkerDefinition(String dataSetMarkerUrl, String experimentalSiteMarkerUrl, String color) {
		this.setDataSetMarkerUrl(dataSetMarkerUrl);
		this.experimentalSiteMarkerUrl = experimentalSiteMarkerUrl;
		this.setColor(color);
	}

	public String getExperimentalSiteMarkerUrl() {
		return experimentalSiteMarkerUrl;
	}

	public void setExperimentalSiteMarkerUrl(String experimentalSiteMarkerUrl) {
		this.experimentalSiteMarkerUrl = experimentalSiteMarkerUrl;
	}

	public String getDataSetMarkerUrl() {
		return dataSetMarkerUrl;
	}

	public void setDataSetMarkerUrl(String dataSetMarkerUrl) {
		this.dataSetMarkerUrl = dataSetMarkerUrl;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
