package fr.obsmip.sedoo.client.ui.searchcriteria;

import com.google.gwt.event.shared.EventHandler;

public interface ObservatoryCatalogsLoadedEventHandler extends EventHandler {
	void onNotification(ObservatoryCatalogsLoadedEvent event);
}
