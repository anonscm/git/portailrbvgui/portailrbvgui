package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class ExperimentalSiteEditingPlace extends Place implements AuthenticatedPlace
{

	private String experimentalSiteName="";
	private String experimentalSiteUuid="";
	private MetadataSummaryDTO parentSummary;
	
	public static ExperimentalSiteEditingPlace instance;

	public static class Tokenizer implements PlaceTokenizer<ExperimentalSiteEditingPlace>
	{
		@Override
		public String getToken(ExperimentalSiteEditingPlace place)
		{
			return StringUtil.trimToEmpty(place.getExperimentalSiteUuid());
		}

		@Override
		public ExperimentalSiteEditingPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new ExperimentalSiteEditingPlace();
			}
			if (StringUtil.isNotEmpty(token))
			{
				instance.setExperimentalSiteUuid(token);
			}
			return instance;
		}
	}

	

	public String getExperimentalSiteName() {
		return experimentalSiteName;
	}

	public void setExperimentalSiteName(String experimentalSiteName) {
		this.experimentalSiteName = experimentalSiteName;
	}

	public String getExperimentalSiteUuid() {
		return experimentalSiteUuid;
	}

	public void setExperimentalSiteUuid(String experimentalSiteUuid) {
		this.experimentalSiteUuid = experimentalSiteUuid;
	}

	public MetadataSummaryDTO getParentSummary() {
		return parentSummary;
	}

	public void setParentSummary(MetadataSummaryDTO parentSummary) {
		this.parentSummary = parentSummary;
	}

	

	
	
}
