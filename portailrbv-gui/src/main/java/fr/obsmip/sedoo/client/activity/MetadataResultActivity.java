package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.DatasetEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.ExperimentalSiteEntryDisplayingPlace;
import fr.obsmip.sedoo.client.place.MetadataResultPlace;
import fr.obsmip.sedoo.client.place.ObservatoryEntryDisplayingPlace;
import fr.obsmip.sedoo.client.service.SearchService;
import fr.obsmip.sedoo.client.service.SearchServiceAsync;
import fr.obsmip.sedoo.client.ui.MetadataResultView;
import fr.obsmip.sedoo.client.ui.MetadataResultView.Presenter;
import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.obsmip.sedoo.shared.domain.SummaryDTO;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class MetadataResultActivity extends RBVAbstractActivity implements
		Presenter {

	private final SearchServiceAsync searchService = GWT
			.create(SearchService.class);
	public final static int DEFAULT_PAGE_SIZE = 10;
	public final static int DEFAULT_POSITION = 1;

	int hits;
	SearchCriteriaDTO criteria;

	MetadataResultView resultView;

	public MetadataResultActivity(MetadataResultPlace place,
			ClientFactory clientFactory) {
		super(clientFactory);
		hits = place.getHits();
		criteria = place.getCriteria();
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		resultView = clientFactory.getResultView();
		resultView.setPresenter(this);
		resultView.reset();
		containerWidget.setWidget(resultView.asWidget());
		broadcastActivityTitle(Message.INSTANCE.resultViewHeader());
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		shortcuts.add(ShortcutFactory.getWelcomeShortcut());
		if (criteria == null) {
			shortcuts.add(ShortcutFactory.getMetadataSearchShortcut());
		} else {
			shortcuts.add(ShortcutFactory.getMetadataSearchShortcut(criteria));
		}
		shortcuts.add(ShortcutFactory.getMetadataResultShortcut(criteria));
		clientFactory.getBreadCrumb().refresh(shortcuts);

		if (hits >= 0) {
			resultView.setHits(hits);
		} else {
			resultView.hideResultPanel();
			if (criteria != null) {
				calculateHits();
			}
		}

		if (criteria != null) {
			fetchSummaries(DEFAULT_POSITION);
		}

	}

	@Override
	public void back() {
		clientFactory.getBreadCrumb().back();
	}

	@Override
	public void fetchSummaries(final int start) {
		final ActionStartEvent e = new ActionStartEvent(
				Message.INSTANCE.metadataSearchingViewSearchInProgress(),
				ActionEventConstant.METADATA_LOADING_EVENT, true);
		ArrayList<String> displayLanguages = clientFactory
				.getDisplayLanguages();
		searchService
				.getSummaries(
						criteria,
						start,
						DEFAULT_PAGE_SIZE,
						displayLanguages,
						new AsyncCallback<HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>>>() {

							@Override
							public void onSuccess(
									HashMap<GeonetworkObservatoryDTO, ArrayList<SummaryDTO>> results) {
								clientFactory.getEventBus().fireEvent(
										e.getEndingEvent());
								resultView.setResults(start, results);
							}

							@Override
							public void onFailure(Throwable caught) {
								clientFactory.getEventBus().fireEvent(
										e.getEndingEvent());
								DialogBoxTools.modalAlert(
										CommonMessages.INSTANCE.error(),
										CommonMessages.INSTANCE
												.anErrorHasOccurred()
												+ " "
												+ caught.getMessage());
							}
						});
	}

	public void calculateHits() {
		searchService.getHits(criteria, new AsyncCallback<Integer>() {

			@Override
			public void onSuccess(Integer hits) {
				resultView.setHits(hits);
			}

			@Override
			public void onFailure(Throwable caught) {
				DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(),
						CommonMessages.INSTANCE.anErrorHasOccurred() + " "
								+ caught.getMessage());
			}
		});

	}

	@Override
	public void displayObservatoryEntryMetadata(String uuid) {
		ObservatoryEntryDisplayingPlace place = new ObservatoryEntryDisplayingPlace();
		place.setUuid(uuid);
		clientFactory.getPlaceController().goTo(place);

	}

	@Override
	public void displayExperimentalSiteEntryMetadata(String uuid) {
		ExperimentalSiteEntryDisplayingPlace place = new ExperimentalSiteEntryDisplayingPlace();
		place.setUuid(uuid);
		clientFactory.getPlaceController().goTo(place);

	}

	@Override
	public void displayDatasetEntryMetadata(String uuid) {
		DatasetEntryDisplayingPlace place = new DatasetEntryDisplayingPlace();
		place.setUuid(uuid);
		clientFactory.getPlaceController().goTo(place);
	}
}