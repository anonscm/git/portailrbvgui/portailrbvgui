package fr.obsmip.sedoo.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.ShortcutFactory;
import fr.obsmip.sedoo.client.event.ActivityStartEvent;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.NewsPlace;
import fr.obsmip.sedoo.client.ui.menu.ScreenNames;
import fr.sedoo.commons.client.cms.place.CMSConsultPlace;
import fr.sedoo.commons.client.event.ActivityChangeEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.activity.NewsArchiveActivity;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.user.AuthenticatedUser;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class RBVNewsArchiveActivity extends NewsArchiveActivity {

	public RBVNewsArchiveActivity(NewsArchivePlace place, NewsClientFactory clientFactory) {
		super(place, clientFactory);
	}
	
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		
		List<Shortcut> shortcuts = new ArrayList<Shortcut>();
		
		//PortailRBV.getClientFactory().getEventBus().fireEvent(new ActivityChangeEvent(""));
		broadcastActivityTitle(Message.INSTANCE.news());
		shortcuts.add(ShortcutFactory
				.getCMSConsultShortcut(new CMSConsultPlace(ScreenNames.HOME)));
		shortcuts.add(ShortcutFactory.getNewsShortcut());
		//shortcuts.add(ShortcutFactory.getNewsArchiveShortcut());
		PortailRBV.getClientFactory().getBreadCrumb().refresh(shortcuts);
		PortailRBV.getClientFactory().getEventBus().fireEvent(
				new ActivityStartEvent(ActivityStartEvent.CMS_ACTIVITY));
		
		
		super.start(containerWidget, eventBus);
	}
	
	@Override
	protected boolean isEditor(){
		AuthenticatedUser user = ((AuthenticatedClientFactory) PortailRBV.getClientFactory()).getUserManager().getUser();
		if (user == null) {
			return false;
		} else {
			return user.isAdmin();
		}
	}
	
	protected void broadcastActivityTitle(String title) {
		PortailRBV.getClientFactory().getEventBus().fireEvent(new ActivityChangeEvent(title));
	}
	
	@Override
	public void back() {
		PortailRBV.getClientFactory().getPlaceController().goTo(new NewsPlace());
	}

}
