package fr.obsmip.sedoo.client.ui.misc;

import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.MapWidget;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.control.DragPan;
import org.gwtopenmaps.openlayers.client.control.Navigation;
import org.gwtopenmaps.openlayers.client.control.OverviewMap;
import org.gwtopenmaps.openlayers.client.control.ScaleLine;
import org.gwtopenmaps.openlayers.client.layer.OSM;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SimpleMapWidget extends DockLayoutPanel 
{
	public SimpleMapWidget(String width, String height) {
		super(Unit.PCT);
//		setWidth(width);
//		setHeight(height);
		Projection projection = new Projection("EPSG:4326");
		MapOptions defaultMapOptions = new MapOptions();
		defaultMapOptions.setDisplayProjection(projection);
		defaultMapOptions.setNumZoomLevels(16);

		MapWidget mapWidget = new MapWidget(width, height, defaultMapOptions);

		map = mapWidget.getMap();
	

		// addGoogleLayers(map);
		addOSMLayers(map);

		// Lets add some default controls to the map
		// map.addControl(new LayerSwitcher()); // + sign in the upperright
		// corner
		// to display the layer switcher
		map.addControl(new OverviewMap()); // + sign in the lowerright to
		// display the overviewmap
		map.addControl(new ScaleLine()); // Display the scaleline
		map.addControl(new Navigation());
		map.addControl(new DragPan());
		
		LonLat rightLowerDisplay = new LonLat(-180, -80);
		LonLat leftUpperDisplay = new LonLat(180, 80);
		LonLat center = new LonLat(0, 0);
		
		rightLowerDisplay.transform(projection.getProjectionCode(), map.getProjection());
		rightLowerDisplay.transform(projection.getProjectionCode(), map.getProjection());
		center.transform(projection.getProjectionCode(), map.getProjection());
		Bounds bounds = new Bounds();
		bounds.extend(rightLowerDisplay);
		bounds.extend(leftUpperDisplay);
		
		//Bounds bounds = new Bounds(-180, -90, 180, 90);
		//map.setMaxExtent(bounds);
		map.setCenter(center);
		//map.setMaxExtent(bounds);
//		map.zoomToExtent(bounds);

		add(mapWidget);
	


	}

	protected Map map;


	private void addOSMLayers(Map map)
	{
		OSM osm_1 = OSM.Mapnik("Mapnik");
		OSM osm_2 = OSM.CycleMap("CycleMap");
		osm_1.setIsBaseLayer(true);
		osm_2.setIsBaseLayer(true);

		map.addLayer(osm_1);
		map.addLayer(osm_2);

	}
	

}
