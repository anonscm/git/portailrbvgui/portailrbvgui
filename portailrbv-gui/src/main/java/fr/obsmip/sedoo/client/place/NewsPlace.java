package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class NewsPlace extends Place {
	public static NewsPlace instance;
	
	public NewsPlace(){}

	public static class Tokenizer implements PlaceTokenizer<NewsPlace>	{
		
		public NewsPlace getPlace(String token) {
			if (instance == null){
				instance = new NewsPlace();
			}
			return instance;
		}

		public String getToken(NewsPlace place) {
			return "";
		}

		
	}
}
