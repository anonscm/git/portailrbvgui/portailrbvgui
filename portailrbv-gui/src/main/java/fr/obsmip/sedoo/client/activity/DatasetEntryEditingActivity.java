package fr.obsmip.sedoo.client.activity;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.obsmip.sedoo.client.ClientFactory;
import fr.obsmip.sedoo.client.Constants;
import fr.obsmip.sedoo.client.event.ActionEventConstant;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.place.DatasetEditingPlace;
import fr.obsmip.sedoo.client.place.DatasetEntryCreationPlace;
import fr.obsmip.sedoo.client.service.MetadataService;
import fr.obsmip.sedoo.client.service.MetadataServiceAsync;
import fr.obsmip.sedoo.client.ui.DatasetEntryView;
import fr.obsmip.sedoo.client.ui.DatasetEntryView.DatasetEntryPresenter;
import fr.obsmip.sedoo.client.ui.misc.BreadCrumb;
import fr.obsmip.sedoo.client.ui.table.ObservatoryPersonTable;
import fr.obsmip.sedoo.shared.domain.DatasetDTO;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.metadata.shared.domain.MetadataDTO;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;
import fr.sedoo.metadata.shared.domain.dto.I18nString;

public class DatasetEntryEditingActivity extends
		AbstractMetadataEditingActivity implements DatasetEntryPresenter {

	private String datasetUuid;
	private MetadataSummaryDTO observatorySummary;
	private MetadataSummaryDTO experimentalSiteSummary;
	private String mode;

	public DatasetEntryEditingActivity(DatasetEditingPlace place,
			ClientFactory clientFactory) {
		super(clientFactory, place);
		if (StringUtil.isNotEmpty(place.getDatasetUuid())) {
			datasetUuid = place.getDatasetUuid();
			mode = Constants.MODIFY;
		} else {
			experimentalSiteSummary = place.getExperimentalSiteSummary();
			observatorySummary = place.getObservatorySummary();
			mode = Constants.CREATE;
		}
	}

	private static final MetadataServiceAsync METADATA_SERVICE = GWT
			.create(MetadataService.class);

	@Override
	public void start(final AcceptsOneWidget containerWidget, EventBus eventBus) {

		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		if (mode.compareTo(Constants.CREATE) == 0) {
			if ((experimentalSiteSummary == null)
					|| (observatorySummary == null)) {
				clientFactory.getPlaceController().goTo(
						new DatasetEntryCreationPlace());
				return;
			}
		}
		sendActivityStartEvent();
		containerWidget.setWidget(clientFactory.getProgressView());
		if (mode.compareTo(Constants.CREATE) == 0) {
			ActionStartEvent startEvent = new ActionStartEvent(
					CommonMessages.INSTANCE.refreshing(),
					ActionEventConstant.REFRESHING, true);
			clientFactory.getEventBus().fireEvent(startEvent);
			broadcastActivityTitle(Message.INSTANCE
					.datasetEntryCreationHeader());
			previousHash = "";
			DatasetDTO newDataset = clientFactory.createDatasetDTO(
					observatorySummary, experimentalSiteSummary);
			view = clientFactory.getDatasetEntryEditingView();
			((DatasetEntryView) view).setPresenter(this);
			containerWidget.setWidget(view.asWidget());
			view.edit(newDataset);
			clientFactory.getEventBus().fireEvent(startEvent.getEndingEvent());
		} else {
			broadcastActivityTitle(Message.INSTANCE
					.datasetEditingViewModificationHeader());
			final ActionStartEvent startEvent = new ActionStartEvent(
					CommonMessages.INSTANCE.loading(),
					ActionEventConstant.DATASET_LOADING_EVENT, true);
			clientFactory.getEventBus().fireEvent(startEvent);
			METADATA_SERVICE.getDatasetByUuid(datasetUuid, clientFactory
					.getMetadataLanguages(), LocaleUtil
					.getCurrentLanguage(clientFactory),
					new DefaultAbstractCallBack<DatasetDTO>(startEvent,
							clientFactory.getEventBus()) {

						@Override
						public void onSuccess(DatasetDTO result) {
							super.onSuccess(result);
							view = clientFactory.getDatasetEntryEditingView();
							observatorySummary = result.getObservatorySummary();
							experimentalSiteSummary = result
									.getExperimentalSiteSummary();
							((DatasetEntryView) view)
									.setPresenter(DatasetEntryEditingActivity.this);
							containerWidget.setWidget(view.asWidget());
							previousHash = result.getHash();
							view.edit(result);
							clientFactory.getEventBus().fireEvent(
									startEvent.getEndingEvent());
							addShortcut(clientFactory.getBreadCrumb());
						}

					});
		}

	}

	private void traceHash(DatasetDTO dto) {

	}

	@Override
	public void save(final MetadataDTO datasetDTO) {
		List<ValidationAlert> validationResult = datasetDTO.validate();
		if (validationResult.isEmpty() == false) {
			DialogBoxTools.popUpScrollable(CommonMessages.INSTANCE.error(),
					ValidationAlert.toHTML(validationResult),
					DialogBoxTools.HTML_MODE);
			return;
		} else {

			ActionStartEvent startEvent = new ActionStartEvent(
					Message.INSTANCE.saving(),
					ActionEventConstant.EXPERIMENTAL_SITE_SAVING_EVENT, true);
			clientFactory.getEventBus().fireEvent(startEvent);
			DatasetDTO aux = (DatasetDTO) datasetDTO;
			METADATA_SERVICE.saveDataset(datasetDTO, aux
					.getObservatorySummary().getName(), clientFactory
					.getMetadataLanguages(), LocaleUtil
					.getCurrentLanguage(clientFactory),
					new DefaultAbstractCallBack<MetadataDTO>(startEvent,
							clientFactory.getEventBus()) {

						@Override
						public void onSuccess(MetadataDTO result) {
							super.onSuccess(result);
							clientFactory.getEventBus().fireEvent(
									new NotificationEvent(Message.INSTANCE
											.savedModifications()));
							ActionStartEvent refreshEvent = new ActionStartEvent(
									CommonMessages.INSTANCE.refreshing(),
									ActionEventConstant.REFRESHING_EVENT, true);
							clientFactory.getEventBus().fireEvent(refreshEvent);
							view.edit(result);
							clientFactory.getEventBus().fireEvent(
									refreshEvent.getEndingEvent());
							previousHash = result.getHash();
							if (mode.compareTo(Constants.CREATE) == 0) {
								datasetUuid = result.getOtherPart().getUuid();
								setMode(Constants.MODIFY);
								broadcastActivityTitle(Message.INSTANCE
										.datasetEditingViewModificationHeader());
								// clientFactory
								// .getBreadCrumb()
								// .addShortcut(
								// ShortcutFactory
								// .getDatasetModificationShortcut(datasetUuid));
							}
						}
					});
		}

	}

	@Override
	public void back() {
		traceHash((DatasetDTO) view.flush());
		BreadCrumb breadCrumb = clientFactory.getBreadCrumb();
		List<Shortcut> shortcuts = breadCrumb.getShortcuts();
		if ((shortcuts != null) && (shortcuts.size() > 1)) {
			Shortcut shortcut = shortcuts.get(shortcuts.size() - 2);
			clientFactory.getPlaceController().goTo(shortcut.getPlace());
		}

	}

	@Override
	public void generateXML(MetadataDTO metadataDTO) {
	}

	@Override
	public void validate(MetadataDTO dto) {
		List<ValidationAlert> validationResult = dto.validate();
		if (validationResult.isEmpty() == false) {
			DialogBoxTools.popUpScrollable(CommonMessages.INSTANCE.error(),
					ValidationAlert.toHTML(validationResult),
					DialogBoxTools.HTML_MODE);
			return;
		} else {
			DialogBoxTools.modalAlert(CommonMessages.INSTANCE.information(),
					Message.INSTANCE.noValidationProblems());
		}

	}

	@Override
	public void print(String uuid) {
	}

	@Override
	public void xml(String uuid) {
	}

	@Override
	public void getParentUseConditions() {
		final ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		METADATA_SERVICE.computeUseConditionsFromParent(
				experimentalSiteSummary.getUuid(),
				clientFactory.getDisplayLanguages(),
				new DefaultAbstractCallBack<I18nString>(e, clientFactory
						.getEventBus()) {
					@Override
					public void onSuccess(I18nString result) {
						super.onSuccess(result);
						((DatasetEntryView) view).setUseConditions(result);
					}
				});
	}

	@Override
	public void getParentPublicAccessLimitations() {
		final ActionStartEvent e = new ActionStartEvent(
				CommonMessages.INSTANCE.loading(),
				ActionEventConstant.BASIC_LOADING_EVENT, true);
		clientFactory.getEventBus().fireEvent(e);
		METADATA_SERVICE.computePublicAccessLimitationsFromParent(
				experimentalSiteSummary.getUuid(),
				clientFactory.getDisplayLanguages(),
				new DefaultAbstractCallBack<I18nString>(e, clientFactory
						.getEventBus()) {
					@Override
					public void onSuccess(I18nString result) {
						super.onSuccess(result);
						((DatasetEntryView) view)
								.setPublicAccessLimitations(result);
					}
				});
	}

	@Override
	public void loadDirectoryPersons(ObservatoryPersonTable table) {
		loadDirectoryPersonsByObservatoryName(getObservatoryName(), table);
	}

	@Override
	public String getObservatoryName() {
		return observatorySummary.getName();
	}

}
