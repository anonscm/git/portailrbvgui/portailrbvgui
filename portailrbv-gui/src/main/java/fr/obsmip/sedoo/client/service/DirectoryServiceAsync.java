package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public interface DirectoryServiceAsync {

	void deletePerson(String id, AsyncCallback<Boolean> callback);

	void loadPersons(String observatoryId,
			AsyncCallback<ArrayList<PersonDTO>> callback);

	void savePerson(ObservatoryPersonDTO person, AsyncCallback<Void> callback);

}
