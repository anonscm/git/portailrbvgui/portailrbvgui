package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gwtbootstrap3.client.ui.Alert;
import org.gwtbootstrap3.client.ui.Well;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.CellTableResources;
import fr.obsmip.sedoo.client.GlobalBundle;
import fr.obsmip.sedoo.client.ui.table.DatasetEntrySummaryTable;
import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.metadata.shared.domain.MetadataSummaryDTO;

public class DatasetEntryManagementViewImpl extends AbstractSection implements
		DatasetEntryManagementView {

	private static ExprimentalSiteManagementViewImplUiBinder uiBinder = GWT
			.create(ExprimentalSiteManagementViewImplUiBinder.class);

	interface ExprimentalSiteManagementViewImplUiBinder extends
			UiBinder<Widget, DatasetEntryManagementViewImpl> {
	}

	@UiField
	DatasetEntrySummaryTable summaryTable;

	@UiField
	Alert noObservatoryCreated;

	@UiField
	Alert noExperimentalSiteAivalable;

	@UiField
	Alert noExperimentalSiteCreated;

	@UiField
	Alert noDatasetAvailable;

	@UiField
	VerticalPanel tablePanel;

	@UiField
	HorizontalPanel observatoryPanel;

	@UiField
	HorizontalPanel experimentalSitePanel;

	@UiField
	ListBox observatories;

	@UiField
	ListBox experimentalSites;

	@UiField
	Well filterPanel;

	protected Image deleteImage = new Image(GlobalBundle.INSTANCE.delete());
	protected Image editImage = new Image(GlobalBundle.INSTANCE.edit());

	private Presenter presenter;

	public DatasetEntryManagementViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		CellTableResources.INSTANCE.cellTableStyle().ensureInjected();
		summaryTable.init(new ArrayList<HasIdentifier>());
		summaryTable.hideToolBar();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		summaryTable.setPresenter(presenter);
	}

	/*
	 * We switch the visibility of the alert message if the table is empty
	 */
	private void ensureAlertVisibility() {
		ElementUtil.show(tablePanel);
		if (summaryTable.getModel().size() == 0) {
			ElementUtil.hide(summaryTable);
			ElementUtil.show(noExperimentalSiteCreated);
		} else {
			ElementUtil.show(summaryTable);
			ElementUtil.hide(noExperimentalSiteCreated);
		}
	}

	@Override
	public void broadcastDatasetDeletion(String uuid) {
		summaryTable.removeRow(uuid);
		ensureAlertVisibility();
	}

	@Override
	public void reset() {
		summaryTable.init(new ArrayList<HasIdentifier>());
		observatories.clear();
		ElementUtil.hide(filterPanel);
		ElementUtil.hide(observatoryPanel);
		ElementUtil.hide(experimentalSitePanel);
		ElementUtil.hide(tablePanel);
		ElementUtil.hide(noObservatoryCreated);
		ElementUtil.hide(noExperimentalSiteCreated);
		ElementUtil.hide(noDatasetAvailable);
	}

	@Override
	public void init(List<MetadataSummaryDTO> datasetSummaries) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setObservatories(List<ObservatorySummaryDTO> observatoryList) {
		if (observatoryList.isEmpty() == false) {
			observatories.clear();
			Iterator<ObservatorySummaryDTO> iterator = observatoryList
					.iterator();
			while (iterator.hasNext()) {
				MetadataSummaryDTO current = iterator.next();
				observatories.addItem(current.getName(), current.getUuid());
			}
			ElementUtil.hide(noObservatoryCreated);
			ElementUtil.show(filterPanel);
			ElementUtil.show(observatoryPanel);

			// We load the experimental sites
			ObservatorySummaryDTO observatorySummaryDTO = observatoryList
					.get(ListUtil.FIRST_INDEX);
			loadExperimentalSites(observatorySummaryDTO.getUuid());
		} else {
			observatories.clear();
			ElementUtil.show(noObservatoryCreated);
			ElementUtil.hide(observatoryPanel);
			ElementUtil.hide(filterPanel);
		}
	}

	private void loadExperimentalSites(String uuid) {
		ElementUtil.hide(tablePanel);
		experimentalSitePanel.setVisible(false);
		presenter.loadExperimentalSiteFromObservatoryUuid(uuid);
	}

	@UiHandler("observatories")
	void onObservatoriesChanged(ChangeEvent event) {
		experimentalSites.clear();
		loadExperimentalSites(observatories.getValue(observatories
				.getSelectedIndex()));
	}

	@Override
	public void setExperimentalSites(
			ArrayList<ExperimentalSiteSummaryDTO> experimentalSitesList) {
		if (experimentalSitesList.isEmpty() == false) {
			experimentalSites.clear();
			Iterator<ExperimentalSiteSummaryDTO> iterator = experimentalSitesList
					.iterator();
			while (iterator.hasNext()) {
				MetadataSummaryDTO current = iterator.next();
				experimentalSites.addItem(current.getName(), current.getUuid());
			}
			experimentalSitePanel.setVisible(true);
			loadDatasetFromExperimentalSiteUuid(experimentalSites
					.getValue(ListUtil.FIRST_INDEX));
			noExperimentalSiteAivalable.setVisible(false);
		} else {
			experimentalSites.clear();
			experimentalSitePanel.setVisible(false);
			noExperimentalSiteAivalable.setVisible(true);
		}
	}

	@UiHandler("createExperimentalSiteEntryLink")
	void onExperimentalSiteEntryCreationLinkClicked(ClickEvent event) {
		presenter.goToExperimentalSiteEntryCreationPlace(observatories
				.getItemText(observatories.getSelectedIndex()));
	}

	@UiHandler("observatoryEntryCreationLink")
	void onObservatoryEntryCreationLinkClicked(ClickEvent event) {
		presenter.goToObservatoryEntryCreationPlace();
	}

	@UiHandler("experimentalSites")
	void onExperimentalSitesChanged(ChangeEvent event) {
		loadDatasetFromExperimentalSiteUuid(experimentalSites
				.getValue(experimentalSites.getSelectedIndex()));
	}

	@UiHandler("datasetEntryCreationLink")
	void onDatasetEntryCreationLinkClicked(ClickEvent event) {
		presenter.goToDatasetEntryCreationPlace(observatories
				.getValue(observatories.getSelectedIndex()), experimentalSites
				.getValue(experimentalSites.getSelectedIndex()));
	}

	private void loadDatasetFromExperimentalSiteUuid(String uuid) {
		presenter.loadDatasetFromExperimentalSiteUuid(uuid);

	}

	@Override
	public void setDatasets(ArrayList<MetadataSummaryDTO> datasets) {
		if (datasets.isEmpty()) {
			ElementUtil.hide(tablePanel);
			ElementUtil.show(noDatasetAvailable);
		} else {
			summaryTable.init(datasets);
			ElementUtil.show(tablePanel);
			ElementUtil.hide(noDatasetAvailable);
		}

	}

}
