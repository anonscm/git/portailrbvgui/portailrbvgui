package fr.obsmip.sedoo.client.ui.metadata.common.children;

import java.util.ArrayList;

import org.gwtbootstrap3.client.ui.Button;

import com.google.gwt.dom.client.Style.WhiteSpace;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import fr.obsmip.sedoo.client.message.Message;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.metadata.client.ui.widget.field.impl.PublicAccessLimitations;
import fr.sedoo.metadata.shared.domain.MetadataDTO;

public class ChildrenPublicAccessLimitations extends PublicAccessLimitations {

	ComputeFromParentPresenter presenter;
	Button computeFromParent;
	
	public ChildrenPublicAccessLimitations(ArrayList<String> languages) {
		super(languages);
		computeFromParent = new Button(Message.INSTANCE.getParentValue());
		computeFromParent.addStyleName("leftPadding5px");
		computeFromParent.getElement().getStyle().setWhiteSpace(WhiteSpace.NOWRAP);
		computeFromParent.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				ConfirmCallBack callBack = new ConfirmCallBack() {
					
					@Override
					public void confirm(boolean choice) {
						if (choice == true)
						{
							presenter.getParentPublicAccessLimitations();
						}
					}
				};
				
				DialogBoxTools.modalConfirm(CommonMessages.INSTANCE.confirm(), Message.INSTANCE.deleteExistingValuesWarning(), callBack).center();
				
			}
		});
		add(computeFromParent);
		setCellWidth(editPanel,"100%");
	}

	public void setPresenter(ComputeFromParentPresenter presenter) {
		this.presenter = presenter;
	}
	
	@Override
	public void display(MetadataDTO metadata) {
		super.display(metadata);
		ElementUtil.hide(computeFromParent);
	}
	
	@Override
	public void edit(MetadataDTO metadata) {
		super.edit(metadata);
		ElementUtil.show(computeFromParent);
	}
}
