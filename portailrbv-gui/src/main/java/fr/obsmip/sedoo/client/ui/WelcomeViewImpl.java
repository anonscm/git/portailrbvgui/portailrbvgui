package fr.obsmip.sedoo.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.client.event.ResetEvent;
import fr.obsmip.sedoo.client.event.ResetEventHandler;
import fr.obsmip.sedoo.client.event.SearchEvent;
import fr.obsmip.sedoo.client.event.SearchEventHandler;
import fr.obsmip.sedoo.client.event.SearchResultEvent;
import fr.obsmip.sedoo.client.event.SearchResultEventHandler;
import fr.obsmip.sedoo.client.message.Message;
import fr.obsmip.sedoo.client.ui.search.SearchMap;
import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;
import fr.sedoo.commons.client.widget.panel.CenteringPanel;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

public class WelcomeViewImpl extends AbstractSection implements WelcomeView,
		SearchEventHandler, ResetEventHandler, SearchResultEventHandler {

	private static final int RESULT_TAB_INDEX = 1;

	private Presenter presenter;

	private boolean contentLoaded = false;

	
	private boolean firstLoad = true;
	
	@Override
	public boolean isFirstLoad() {
		return firstLoad;
	}
	@Override
	public void setFirstLoad(boolean firstLoad) {
		this.firstLoad = firstLoad;		
	}
	
	private SearchMap map;

	private ResultPanel resultPanel;

	@UiField
	DockLayoutPanel main;

	@UiField
	TabLayoutPanel tabPanel;

	private static WelcomeViewImplUiBinder uiBinder = GWT
			.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, WelcomeViewImpl> {
	}

	public WelcomeViewImpl() {
		super();
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(SearchEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(ResetEvent.TYPE, this);
		PortailRBV.getClientFactory().getEventBus()
				.addHandler(SearchResultEvent.TYPE, this);

		initWidget(uiBinder.createAndBindUi(this));
		map = new SearchMap();
		CenteringPanel centeringPanel = new CenteringPanel(map);

		tabPanel.add(centeringPanel,
				Message.INSTANCE.metadataEditingLocalisation());
		tabPanel.getElement().getStyle().setMargin(5, Unit.PX);

		resultPanel = new ResultPanel();

		DockLayoutPanel aux = new DockLayoutPanel(Unit.PX);
		aux.add(new ScrollPanel(resultPanel));

		tabPanel.add(aux, Message.INSTANCE.results());

		applyCommonStyle();
		reset();
		map.enableEditMode();
		onResize();
	}

	private void reset() {
		if (map != null) {
			map.reset();
			initMap();
		}

		tabPanel.setTabText(RESULT_TAB_INDEX, Message.INSTANCE.results());
	}

	@Override
	public void setPresenter(Presenter presenter) {

		this.presenter = presenter;
		resultPanel.setSummaryPresenter(presenter);
	}

	@Override
	public Presenter getPresenter() {

		return presenter;
	}

	@Override
	public boolean isContentLoaded() {
		return contentLoaded;
	}

	public void initMap() {
		// On initialise avec une boite vide afin d'afficher une carte du monde
		GeographicBoundingBoxDTO geographicBoundingBoxDTO = new GeographicBoundingBoxDTO();
		map.setGeographicBoundingBoxDTO(geographicBoundingBoxDTO);

		contentLoaded = true;
		onResize();
	}

	public void getCriteria(SearchCriteriaDTO criteria) {
		presenter.search(criteria);
	}

	@Override
	public void onNotification(SearchEvent event) {
		tabPanel.setTabText(RESULT_TAB_INDEX, Message.INSTANCE.results());
		SearchCriteriaDTO criteria = event.getCriteria();
		criteria.setBoundingBoxDTO(map.getGeographicBoundingBoxDTO());
		presenter.search(criteria);
	}

	@Override
	public void onNotification(ResetEvent event) {
		reset();

	}

	@Override
	public void onNotification(SearchResultEvent event) {
		tabPanel.setTabText(RESULT_TAB_INDEX, Message.INSTANCE.results() + " ("
				+ event.getAllSummaryDTOs().size() + ")");
		
		/*Place place = PortailRBV.getPresenter().getWhere();
		
		if (place != null
				&& ! (place instanceof WelcomePlace) ){
			PortailRBV.getPresenter().goTo(new WelcomePlace());
		}*/
	}

}
