package fr.obsmip.sedoo.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MenuResetEventHandler extends EventHandler {
	void onNotification(MenuResetEvent event);
}
