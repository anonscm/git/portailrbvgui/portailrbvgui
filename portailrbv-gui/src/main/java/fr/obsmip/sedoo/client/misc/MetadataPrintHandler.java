package fr.obsmip.sedoo.client.misc;

import com.google.gwt.user.client.Window;

import fr.obsmip.sedoo.client.PortailRBV;
import fr.obsmip.sedoo.shared.domain.ParameterConstant;
import fr.sedoo.commons.client.util.LocaleUtil;

public class MetadataPrintHandler {

	public static void print(String uuid)
	{
		String parameter = PortailRBV.getParameter(ParameterConstant.PRINT_SERVICE_URL_PARAMETER_NAME);
		if (parameter != null)
		{
			String locale = LocaleUtil.getCurrentLanguage(PortailRBV.getClientFactory());
			Window.open(parameter+"/"+locale+"/"+uuid, "_blank", "");
		}
	}
	
}
