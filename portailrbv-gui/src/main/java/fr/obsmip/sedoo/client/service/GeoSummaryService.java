package fr.obsmip.sedoo.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.obsmip.sedoo.shared.domain.AbstractGeoSummaryDTO;


@RemoteServiceRelativePath("geosummary")
public interface GeoSummaryService extends RemoteService {
	
	ArrayList<? extends AbstractGeoSummaryDTO> getGeoSummaries(String observatoryName, ArrayList<String> languages) throws Exception;

}

