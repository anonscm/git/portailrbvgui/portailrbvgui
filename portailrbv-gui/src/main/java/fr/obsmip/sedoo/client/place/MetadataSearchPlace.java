package fr.obsmip.sedoo.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.obsmip.sedoo.shared.domain.SearchCriteriaDTO;

public class MetadataSearchPlace extends Place
{
	public static MetadataSearchPlace instance;
	
	private static SearchCriteriaDTO criteria;
	
	public MetadataSearchPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<MetadataSearchPlace>
	{
		
		public MetadataSearchPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new MetadataSearchPlace();
				MetadataSearchPlace.setCriteria(SearchCriteriaDTO.fromToken(token));
			}
			return instance;
		}

		public String getToken(MetadataSearchPlace place) {
			if (MetadataSearchPlace.getCriteria() != null)
			{
				return MetadataSearchPlace.getCriteria().toToken();
			}
			return "";
		}

		
	}

	public static void setCriteria(SearchCriteriaDTO aux) 
	{
		criteria = aux;
	}
	
	public static SearchCriteriaDTO getCriteria() {
		return criteria;
	}
	
	
}
