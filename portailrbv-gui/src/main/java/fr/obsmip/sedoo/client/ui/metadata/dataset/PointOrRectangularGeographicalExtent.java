package fr.obsmip.sedoo.client.ui.metadata.dataset;

import fr.sedoo.commons.client.widget.map.impl.PointOrRectangularLocationWidget;
import fr.sedoo.commons.client.widget.map.impl.SingleAreaSelectorWidget;
import fr.sedoo.metadata.client.ui.widget.field.api.IsDisplay;
import fr.sedoo.metadata.client.ui.widget.field.api.IsEditor;
import fr.sedoo.metadata.client.ui.widget.field.impl.SingleGeographicalExtent;

public class PointOrRectangularGeographicalExtent extends
		SingleGeographicalExtent implements IsDisplay, IsEditor {

	public PointOrRectangularGeographicalExtent(String mapLayer) {
		super(mapLayer);
	}

	@Override
	protected SingleAreaSelectorWidget getSingleAreaSelectorWidget(
			String mapLayer) {
		return new PointOrRectangularLocationWidget(mapLayer);
	}

}
