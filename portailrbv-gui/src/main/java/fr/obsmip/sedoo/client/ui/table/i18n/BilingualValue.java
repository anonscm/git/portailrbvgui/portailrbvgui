package fr.obsmip.sedoo.client.ui.table.i18n;

public class BilingualValue {
	
	private String frenchValue;
	private String englishValue;
	
	public String getFrenchValue() {
		return frenchValue;
	}
	
	public void setFrenchValue(String frenchValue) {
		this.frenchValue = frenchValue;
	}

	public String getEnglishValue() {
		return englishValue;
	}

	public void setEnglishValue(String englishValue) {
		this.englishValue = englishValue;
	}

}
