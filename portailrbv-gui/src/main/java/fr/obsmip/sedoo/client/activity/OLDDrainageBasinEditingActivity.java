package fr.obsmip.sedoo.client.activity;


public class OLDDrainageBasinEditingActivity 
{

//extends AbstractDTOEditingActivity implements Presenter{
//
//	private Long id;
//	private Long observatoryId;
//	private String observatoryShortLabel="";
//
//	public DrainageBasinEditingActivity(DrainageBasinEditingPlace place, ClientFactory clientFactory) {
//		super(clientFactory, place);
//		setMode(place.getMode());
//		if (place.getMode().compareTo(Constants.CREATE)==0)
//		{
//			setObservatoryId(place.getId());
//		}
//		else
//		{
//			id = place.getId();
//		}
//	}
//
//
//	private final ObservatoryServiceAsync observatoryService = GWT.create(ObservatoryService.class);
//
//
//	@Override
//	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
//		if (isValidUser() == false)
//		{
//			goToLoginPlace();
//			return;
//		}
//		sendActivityStartEvent();
//		final DrainageBasinEditingView drainageBasinEditingView = clientFactory.getDrainageBasinEditingView();
//		view = drainageBasinEditingView;
//		drainageBasinEditingView.setPresenter(this);
//		containerWidget.setWidget(drainageBasinEditingView.asWidget());
//		
//		
//		
//		
//		if (getMode().compareTo(Constants.MODIFY) == 0)
//		{
//			observatoryService.getDrainageBasinById(id, new AsyncCallback<DrainageBasinDTO>() {
//
//				@Override
//				public void onSuccess(DrainageBasinDTO result) {
//					broadcastActivityTitle(Message.INSTANCE.drainageBasinEditingViewModificationHeader() +" ("+result.getLabel()+")");
//					setObservatoryId(result.getObservatoryId());
//					previousHash = result.getHash();
//					List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//					shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
//			        observatoryShortLabel = result.getObservatoryShortLabel();
//			        shortcuts.add(ShortcutFactory.getObservatoryModificationShortcut(observatoryShortLabel, observatoryId));
//			        shortcuts.add(ShortcutFactory.getDrainageBasinModificationShortcut(result.getLabel(), id));
//			        clientFactory.getBreadCrumb().refresh(shortcuts);
//					view.edit(result);
//					view.setMode(Constants.MODIFY);
//				}
//
//				@Override
//				public void onFailure(Throwable caught) {
//					DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened()+" : "+caught.getMessage());
//
//				}
//			});
//			
//			
//		}
//		else
//		{
//			previousHash = "";
//			broadcastActivityTitle(Message.INSTANCE.observatoryContactEditingViewCreationHeader());
//			drainageBasinEditingView.edit(new DrainageBasinDTO());
//			observatoryService.getObservatoryById(observatoryId, new AsyncCallback<ObservatoryDTO>() {
//				
//				@Override
//				public void onSuccess(ObservatoryDTO result) {
//					List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//					shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
//			        observatoryShortLabel = result.getShortLabel();
//			        shortcuts.add(ShortcutFactory.getObservatoryModificationShortcut(observatoryShortLabel, observatoryId));
//			        shortcuts.add(ShortcutFactory.getObservatoryDrainageBasinCreationShortcut(id));
//			        clientFactory.getBreadCrumb().refresh(shortcuts);		
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// Nothing done ...
//					
//				}
//			});
//	        
//			view.setMode(Constants.CREATE);
//			
//			
//		}
//	}
//
//
//	@Override
//	public void save(final DrainageBasinDTO drainageBasinDTO) 
//	{
//		List<ValidationAlert> validate = drainageBasinDTO.validate();
//		if (validate.isEmpty() == false)
//		{
//			DialogBoxTools.popUp(Message.INSTANCE.error(), ValidationAlert.toHTML(validate), DialogBoxTools.HTML_MODE);
//			return;
//		}
//		
//		//Modification mode
//		if (getMode().compareTo(Constants.MODIFY) == 0)
//		{
//			drainageBasinDTO.setId(id);
//			ActionStartEvent startEvent = new ActionStartEvent(Message.INSTANCE.saving(), ActionEventConstant.DRAINAGE_BASIN_SAVING_EVENT, true);
//	        clientFactory.getEventBus().fireEvent(startEvent);
//	       
//	        observatoryService.saveDrainageBasin(drainageBasinDTO, new AsyncCallback<Void>() {
//
//	        	@Override
//	        	public void onSuccess(Void result) 
//	        	{
//	        		previousHash = drainageBasinDTO.getHash();
//	        		ActionEndEvent e = new ActionEndEvent(ActionEventConstant.DRAINAGE_BASIN_SAVING_EVENT);
//	        		clientFactory.getEventBus().fireEvent(e);
//	        		broadcastActivityTitle(Message.INSTANCE.observatoryContactEditingViewModificationHeader() +" ("+drainageBasinDTO.getLabel()+")");
//	        		view.setMode(Constants.MODIFY);
//	        		clientFactory.getEventBus().fireEvent(new NotificationEvent(Message.INSTANCE.savedModifications()));
//	        	}
//
//	        	@Override
//	        	public void onFailure(Throwable caught) {
//
//	        		DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened()+" "+caught.getMessage());
//	        		ActionEndEvent e = new ActionEndEvent(ActionEventConstant.DRAINAGE_BASIN_SAVING_EVENT);
//	        		clientFactory.getEventBus().fireEvent(e);
//
//	        	}
//	        });
//		}
//		else
//		{
//			ActionStartEvent startEvent = new ActionStartEvent(Message.INSTANCE.saving(), ActionEventConstant.DRAINAGE_BASIN_SAVING_EVENT, true);
//			clientFactory.getEventBus().fireEvent(startEvent);
//			observatoryService.addDrainageBasin(drainageBasinDTO, observatoryId, new AsyncCallback<Long>() {
//
//				@Override
//				public void onSuccess(Long result) 
//				{
//					previousHash = drainageBasinDTO.getHash();
//					ActionEndEvent e = new ActionEndEvent(ActionEventConstant.DRAINAGE_BASIN_SAVING_EVENT);
//					clientFactory.getEventBus().fireEvent(e);
//					broadcastActivityTitle(Message.INSTANCE.drainageBasinEditingViewModificationHeader() +" ("+drainageBasinDTO.getLabel()+")");
//					view.setMode(Constants.MODIFY);
//					id = result;
//					List<Shortcut> shortcuts = new ArrayList<Shortcut>();
//					shortcuts.add(ShortcutFactory.getWelcomeShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryManagementShortcut());
//			        shortcuts.add(ShortcutFactory.getObservatoryModificationShortcut(observatoryShortLabel, observatoryId));
//			        shortcuts.add(ShortcutFactory.getDrainageBasinModificationShortcut(drainageBasinDTO.getLabel(), id));
//			        clientFactory.getBreadCrumb().refresh(shortcuts);	
//			        clientFactory.getEventBus().fireEvent(new NotificationEvent(Message.INSTANCE.addedElement()));
//				}
//
//				@Override
//				public void onFailure(Throwable caught) {
//
//					DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.anErrorHasHappened()+" "+caught.getMessage());
//					ActionEndEvent e = new ActionEndEvent(ActionEventConstant.DRAINAGE_BASIN_SAVING_EVENT);
//					clientFactory.getEventBus().fireEvent(e);
//
//				}
//			});
//		}
//		
//	}
//
//	@Override
//	public void deleteSite(final Long id) 
//	{
//		observatoryService.deleteSite(id, new AsyncCallback<Void>() {
//			
//			@Override
//			public void onSuccess(Void result) {
//				DrainageBasinEditingView aux = (DrainageBasinEditingView) view;
//				aux.broadcastSiteDeletion(""+id, true);
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				DrainageBasinEditingView aux = (DrainageBasinEditingView) view;
//				aux.broadcastSiteDeletion(""+id, false);				
//			}
//		});
//			
//	}
//
//	@Override
//	public void back() {
//		ObservatoryEditingPlace place = new ObservatoryEditingPlace();
//		place.setObservatoryId(getObservatoryId());
//		
//		clientFactory.getPlaceController().goTo(place);	
//	}
//
//
//
//
//
//
//	public Long getObservatoryId() {
//		return observatoryId;
//	}
//
//
//
//
//
//
//	public void setObservatoryId(Long observatoryId) {
//		this.observatoryId = observatoryId;
//	}
//
//

}
