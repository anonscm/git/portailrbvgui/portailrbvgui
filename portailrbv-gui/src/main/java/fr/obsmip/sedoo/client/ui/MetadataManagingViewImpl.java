package fr.obsmip.sedoo.client.ui;


public class MetadataManagingViewImpl 
{
//	extends AbstractSection implements MetadataManagingView
//}
//{
//
//	private static final String UNSELECTED_ID = "NONE"; 
//
//	private Presenter presenter;
//
//	private Integer hits;
//
//
//	@UiField
//	ListBox observatoryFromEntries;
//	
//	@UiField
//	Image showFilter;
//	
//	@UiField
//	Image hideFilter;
//
//	@UiField
//	MultipleValueTextBox keywords;
//
//	@UiField
//	Button searchButton;
//
//	@UiField
//	Button resetButton;
//
//	@UiField
//	RadioButton anyKeyword;
//
//	@UiField
//	RadioButton allKeyword;
//
//	@UiField
//	SummaryDTOTable table;
//	
//	@UiField
//	HTMLPanel filterPanel;
//
//	private static MetadataManagingViewImplUiBinder uiBinder = GWT.create(MetadataManagingViewImplUiBinder.class);
//
//
//	interface MetadataManagingViewImplUiBinder extends UiBinder<Widget, MetadataManagingViewImpl>
//	{
//	}
//
//	public MetadataManagingViewImpl()
//	{
//		super();
//		initWidget(uiBinder.createAndBindUi(this));
//		applyCommonStyle();
//		keywords.setMultipleSelect(true);
//		keywords.setPlaceholderText("");
//		keywords.setSize("600px", "50px");
//	}
//
//	@Override
//	public void setPresenter(Presenter presenter)
//	{
//		this.presenter = presenter;
//		table.setPresenter(presenter);
//	}
//
//	private void resetFilter()
//	{
//		keywords.reset();
//		allKeyword.setValue(true);
//		observatoryFromEntries.setSelectedIndex(0);
//	}
//
//	@UiHandler("resetButton")
//	void onResetButtonClicked(ClickEvent event)
//	{
//		resetFilter();
//	}
//
//	@UiHandler("searchButton")
//	void onSearchButtonClicked(ClickEvent event)
//	{
//		SearchCriteriaDTO criteria = flush();
//		if (criteria.isEmpty())
//		{
//			DialogBoxTools.modalAlert(Message.INSTANCE.error(), Message.INSTANCE.metadataSearchingEmptyCriteria());
//			return;
//		}
//
//		List<OldValidationAlert> validate = criteria.validate();
//		if (validate.isEmpty() == false)
//		{
//			DialogBoxTools.popUp(Message.INSTANCE.error(), OldValidationAlert.toHTML(validate), DialogBoxTools.HTML_MODE);
//			return;
//		}
//
//		presenter.setCriteria(criteria);
//		presenter.getEntries(MetadataManagingActivity.DEFAULT_POSITION);
//	}
//
//
//	private SearchCriteriaDTO flush()
//	{
//		SearchCriteriaDTO criteria = new SearchCriteriaDTO();
//		criteria.setKeywords(keywords.getValues());
//		if (allKeyword.getValue() == true)
//		{
//			criteria.setKeywordsLogicalLink(SearchCriteriaDTO.AND);
//		} else
//		{
//			criteria.setKeywordsLogicalLink(SearchCriteriaDTO.OR);
//		}
//
//		if (observatoryFromEntries.getSelectedIndex() != 0)
//		{
////			ArrayList<String> aux = new ArrayList<String>();
////			aux.add(observatoryFromEntries.getValue(observatoryFromEntries.getSelectedIndex()));
////			criteria.setObservatories(aux);
//			criteria.setObservatory(observatoryFromEntries.getValue(observatoryFromEntries.getSelectedIndex()));
//		}
//		return criteria;
//	}
//
//	/*
//	 * @UiHandler("createButton") void onCreateButtonClicked(ClickEvent event) {
//	 * presenter.createMetadata(new
//	 * Long(drainageBasins.getValue(drainageBasins.getSelectedIndex()))); }
//	 */
//
//	@Override
//	public void reset()
//	{
//		table.reset();
//		resetFilter();
//	}
//
//
//	@Override
//	public void setObservatoriesFromEntries(ArrayList<String> result)
//	{
//		observatoryFromEntries.clear();
//		observatoryFromEntries.addItem(Message.INSTANCE.selectItem(), UNSELECTED_ID);
//		table.setDrainageBasinId(null);
//		table.setObservatoryId(null);
//		Iterator<String> iterator = result.iterator();
//		while (iterator.hasNext())
//		{
//			String aux = iterator.next();
//			observatoryFromEntries.addItem(aux, aux);
//		}
//		reset();
//	}
//
//	@Override
//	public void setHits(Integer hits)
//	{
//		this.hits = hits;
//	}
//
//	@Override
//	public void setEntries(int position, List<SummaryDTO> result)
//	{
//		table.setDataPage(position - 1, result, hits);
//
//	}
//
//	@Override
//	public void setObservatoriesFromEntries(List<ObservatoryDTO> observatories) {
//		table.setObservatories(observatories);
//	}
//	
//	 @UiHandler("showFilter")
//	  void onShowFilterButtonClicked(ClickEvent event) {
//		 hideFilter.setVisible(true);
//		 showFilter.setVisible(false);
//		 showFilterContent(true);
//		 VerticalSlideAnimation animation = new VerticalSlideAnimation(filterPanel);
//		 animation.run(500);
//	  }
//	 
//	 @UiHandler("hideFilter")
//	  void onHideFilterButtonClicked(ClickEvent event) {
//		 hideFilter.setVisible(false);
//		 showFilter.setVisible(true);
//		 showFilterContent(false);
//		 VerticalSlideAnimation animation = new VerticalSlideAnimation(filterPanel);
//		 animation.run(500);
//	  }
//	 
//	 void showFilterContent(boolean value)
//	 {
//		 int widgetCount = filterPanel.getWidgetCount();
//		 for (int i = 0; i < widgetCount; i++) 
//		 {
//			 filterPanel.getWidget(i).setVisible(value);
//		 }
//	 }

}
