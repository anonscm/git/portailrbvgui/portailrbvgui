package fr.obsmip.sedoo.client.ui.metadata.dataset;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.ExperimentalSiteSummaryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatorySummaryDTO;

public interface DatasetEntryCreationView extends IsWidget {
	void setObservatories(List<ObservatorySummaryDTO> observatories);

	void setExperimentalSites(List<ExperimentalSiteSummaryDTO> experimentalSites);

	void setPresenter(Presenter presenter);

	public interface Presenter {
		void create(ObservatorySummaryDTO observatorySummary,
				ExperimentalSiteSummaryDTO experimentalSiteSummary);

		void goToObservatoryEntryCreationPlace();

		void loadExperimentalSiteFromObservatoryUuid(String uuid);

		void goToExperimentalSiteEntryCreationPlace(String observatoryName);
	}

	void reset();

	void selectObservatory(String observatoryName);

	void selectObservatoryAndExperimentaSite(String observatoryName,
			String experimentalSiteName);

	void setObservatories(List<ObservatorySummaryDTO> aux,
			String observatoryName);

	void selectExperimentalSiteFromUuid(String uuid);
}
