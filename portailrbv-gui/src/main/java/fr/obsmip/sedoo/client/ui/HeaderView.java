package fr.obsmip.sedoo.client.ui;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.RequiresResize;

import fr.obsmip.sedoo.client.event.UserLoginEventHandler;
import fr.obsmip.sedoo.client.event.UserLogoutEventHandler;
import fr.sedoo.commons.client.cms.event.MenuChangeEventHandler;

public interface HeaderView extends IsWidget, UserLoginEventHandler,
		UserLogoutEventHandler, MenuChangeEventHandler, RequiresResize {

	void enableInformationSiteMenu();

	void enableCatalogSiteMenu();

	int getBannerHeight();

	int getSubMenuHeight();

	MenuBar getSubmenu();

	void initSubmenu();

}
