package fr.obsmip.sedoo.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.obsmip.sedoo.shared.domain.GeonetworkObservatoryDTO;
import fr.obsmip.sedoo.shared.domain.ObservatoryPersonDTO;
import fr.sedoo.metadata.shared.domain.dto.PersonDTO;

public interface DirectoryManagementView extends IsWidget{
	
//	void init(List<ExperimentalSiteSummaryDTO> experimentalSiteSummaries);
	public void broadcastPersonDeletion(String id);
	
	void setPresenter(Presenter presenter);
	
	public interface Presenter 
	 {
	        void deletePerson(String id);
			void loadPersons(String observatoryUuid);
//			void goToExperimentalSiteEntryCreationPlace(String observatoryName);
			void save(ObservatoryPersonDTO person);
	 }

	void reset();
	void setObservatories(List<GeonetworkObservatoryDTO> availableObservatories);
	void setPersons(ArrayList<PersonDTO> persons);
	

}
