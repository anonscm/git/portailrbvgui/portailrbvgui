package fr.obsmip.sedoo.client.ui.metadata.observatory;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.message.ValidationMessages;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.ConfirmCallBack;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.UpperCaseTextBox;
import fr.sedoo.commons.client.widget.dialog.OkCancelContent;

public class ExperimentalSiteNameDialogContent extends Composite implements OkCancelContent {

	private static AdministratorEditDialogContentUiBinder uiBinder = GWT
			.create(AdministratorEditDialogContentUiBinder.class);

	interface AdministratorEditDialogContentUiBinder extends
	UiBinder<Widget, ExperimentalSiteNameDialogContent> {
	}

	@UiField
	UpperCaseTextBox name;
	private boolean result = false;
	private DialogBox dialog;

	private ConfirmCallBack callBack;
	private String experimentalSiteName;

	public ExperimentalSiteNameDialogContent(ConfirmCallBack confirmCallback) {
		super();
		this.callBack = confirmCallback;
		initWidget(uiBinder.createAndBindUi(this));
		name.setText("");
		result = false;
	}

	@Override
	public String getPreferredHeight() {
		return "70px";
	}
	
	@Override
	public void cancelClicked() {
		dialog.hide();
	}

	public String getExperimentalSiteName() 
	{
		return experimentalSiteName;
	}

	@Override
	public void setDialogBox(DialogBox dialog) 
	{
		this.dialog = dialog;
	}
	
	@Override
	public void okClicked() {
		if (StringUtil.isEmpty(StringUtil.trimToEmpty(name.getText())))
		{
			DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(),CommonMessages.INSTANCE.name() +" : "+ ValidationMessages.INSTANCE.mandatoryData());
		}
		else
		{
			result = true;
			experimentalSiteName = StringUtil.trimToEmpty(name.getText());
			dialog.hide();
			callBack.confirm(result);
		}
		
	}

	public void setExperimentalSiteName(String experimentalSiteName) {
		this.experimentalSiteName = experimentalSiteName;
	}


}
